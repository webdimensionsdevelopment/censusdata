<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->load->helper(array('url','language'));
        $this->load->library('session');
    }

    function index() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            redirect('/', 'refresh');
        }
    }

    function login() {
        if (!$this->ion_auth->logged_in()) {
            /* Load */
            $this->load->config('admin/dp_config');
            $this->load->config('common/dp_config');

            /* Valid form */
            $this->form_validation->set_rules('identity', 'Identity', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            /* Data */
            $this->data['title']               = $this->config->item('title');
            $this->data['title_lg']            = $this->config->item('title_lg');
            $this->data['auth_social_network'] = $this->config->item('auth_social_network');
            $this->data['forgot_password']     = $this->config->item('forgot_password');
            $this->data['new_membership']      = $this->config->item('new_membership');

            if ($this->form_validation->run() == TRUE) {
                $remember = (bool) $this->input->post('remember');

                if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
                    if (!$this->ion_auth->is_admin() && !$this->ion_auth->is_employee() && !$this->ion_auth->is_member()) {
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        redirect('/', 'refresh');
                    } else {
                        $date = date('Y/m/d H:i:s');
                        $userId = $this->ion_auth->get_user_id();
                        $record = array("user_id" => $userId, "logInTime" => $date);
                        $loginId = $this->ion_auth_model->addLoginAttemptsById($record);
                        $sessionData = array ('logInId'=>$loginId);
                        $this->session->set_userdata('user', $sessionData);
                        /* Data */
                        $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                        if($this->ion_auth->is_employee() || $this->ion_auth->is_member())
                        {
                            
                            redirect('admin/Company/company_list_view', 'refresh');
//                        
                        }
                        /* Load Template */
                        redirect('admin', 'refresh');
//                        $this->template->auth_render('auth/choice', $this->data);
                    }
                } else {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                    redirect('auth/login', 'refresh');
                }
            } else {
                $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

                $this->data['identity'] = array(
                    'name'        => 'identity',
                    'id'          => 'identity',
                    'type'        => 'email',
                    'value'       => $this->form_validation->set_value('identity'),
                    'class'       => 'form-control',
                    'placeholder' => lang('auth_your_email')
                );
                $this->data['password'] = array(
                    'name'        => 'password',
                    'id'          => 'password',
                    'type'        => 'password',
                    'class'       => 'form-control',
                    'placeholder' => lang('auth_your_password')
                );

                /* Load Template */
                $this->template->auth_render('auth/login', $this->data);
            }
        } else {
            redirect('/', 'refresh');
        }
    }

    function logout($src = NULL) {
        $logout = $this->ion_auth->logout();
        $id =$this->session->userdata['user']['logInId'];
        $date = date('Y/m/d H:i:s');
        $record = array("logOutTime" => $date);
        $this->ion_auth_model->updateLogOutAttemptById($record,$id);
        $this->session->unset_userdata('user');
        $this->session->set_flashdata('message', $this->ion_auth->messages());
        
        if ($src == 'admin') {
            redirect('auth/login', 'refresh');
        } else {
            redirect('/', 'refresh');
        }
    }

    public function ga_user_activate($id, $code = FALSE) {
        $id                      = (int) $id;
        $this->data['pagetitle'] = 'Complete your profile';
        $this->data['title']     = 'Complete your profile';
        $this->data['title_lg']  = $this->config->item('title_lg');
        /* Data */
        $user                    = $this->ion_auth->user($id)->row();
        $groups                  = $this->ion_auth->groups()->result_array();
        $currentGroups           = $this->ion_auth->get_users_groups($id)->result();
        /* Validate form input */
        $this->form_validation->set_rules('first_name', 'lang:edit_user_validation_fname_label', 'required');
        $this->form_validation->set_rules('last_name', 'lang:edit_user_validation_lname_label', 'required');
        //$this->form_validation->set_rules('phone', 'lang:edit_user_validation_phone_label', 'required');

        if (isset($_POST) && !empty($_POST)) {
            if ($id != $this->input->post('id')) {
                show_error($this->lang->line('error_csrf'));
            }

            if ($this->input->post('password')) {
                $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
            }

            if ($this->form_validation->run() == TRUE) {
                $data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name'  => $this->input->post('last_name'),
                    'username'   => $this->input->post('first_name') . " " . $this->input->post('last_name'),
                    'company'    => $this->input->post('company'),
                    'phone'      => $this->input->post('phone')
                );

                if ($this->input->post('password')) {
                    $data['password'] = $this->input->post('password');
                }

                if ($this->ion_auth->is_admin()) {
                    $groupData = $this->input->post('groups');
                    if (isset($groupData) && !empty($groupData)) {
                        $this->ion_auth->remove_from_group('', $id);

                        foreach ($groupData as $grp) {
                            $this->ion_auth->add_to_group($grp, $id);
                        }
                    }
                }
                if ($this->ion_auth->update($user->id, $data)) {
                    $activation = $this->ion_auth->activate($id, $code);
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
                    $this->logout('admin');
                } else {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                    if ($this->ion_auth->is_admin()) {
                        redirect('auth', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }
                }
            }
        }

        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
        // pass the user to the view
        $this->data['user']             = $user;
        $this->data['groups']           = $groups;
        $this->data['currentGroups']    = $currentGroups;
        $this->data['first_name']       = array(
            'name'        => 'first_name',
            'id'          => 'first_name',
            'type'        => 'text',
            'class'       => 'form-control',
            'placeholder' => 'First Name',
            'value'       => (!empty($this->form_validation->set_value('first_name', $user->first_name)) ? $this->form_validation->set_value('first_name', $user->first_name) : '')
        );
        $this->data['last_name']        = array(
            'name'        => 'last_name',
            'id'          => 'last_name',
            'type'        => 'text',
            'class'       => 'form-control',
            'placeholder' => 'Last Name',
            'value'       => (!empty($this->form_validation->set_value('last_name', $user->last_name)) ? $this->form_validation->set_value('last_name', $user->last_name) : '')
        );
        $this->data['phone']            = array(
            'name'        => 'phone',
            'id'          => 'phone',
            'type'        => 'tel',
            'pattern'     => '^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$',
            'class'       => 'form-control',
            'placeholder' => 'Contact Number',
            'value'       => ''
        );
        $this->data['password']         = array(
            'name'        => 'password',
            'id'          => 'password',
            'class'       => 'form-control',
            'placeholder' => 'Password',
            'type'        => 'password'
        );
        $this->data['password_confirm'] = array(
            'name'        => 'password_confirm',
            'id'          => 'password_confirm',
            'class'       => 'form-control',
            'placeholder' => 'Confirm Password',
            'type'        => 'password'
        );
        /* Load Template */
        $this->template->auth_render('auth/edit', $this->data);
    }
    // forgot password
	function forgot_password()
	{
            $this->data['pagetitle'] = '';
        $this->data['title']     = '';
        $this->data['title_lg']  = $this->config->item('title_lg');
		// setting validation rules by checking wheather identity is username or email
		if($this->config->item('identity', 'ion_auth') != 'email' )
		{
		   $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
		}
		else
		{
		   $this->form_validation->set_rules('email', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
		}


		if ($this->form_validation->run() == false)
		{
			// setup the input
			$this->data['email'] = array('name' => 'email',
				'id' => 'email',
			);

			if ( $this->config->item('identity', 'ion_auth') != 'email' ){
				$this->data['identity_label'] = $this->lang->line('forgot_password_identity_label');
			}
			else
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}

			// set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->template->auth_render('auth/forgot_password', $this->data);
		}
		else
		{
			$identity_column = $this->config->item('identity','ion_auth');
			$identity = $this->ion_auth->where($identity_column, $this->input->post('email'))->users()->row();

			if(empty($identity)) {

	            		if($this->config->item('identity', 'ion_auth') != 'email')
		            	{
		            		$this->ion_auth->set_error('forgot_password_identity_not_found');
		            	}
		            	else
		            	{
		            	   $this->ion_auth->set_error('forgot_password_email_not_found');
		            	}

		                $this->session->set_flashdata('message', $this->ion_auth->errors());
                		redirect("auth/forgot_password", 'refresh');
            		}

			// run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});
                        
			if ($forgotten)
			{
				// if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("auth/forgot_password", 'refresh');
			}
		}
	}

	// reset password - final step for forgotten password
	public function reset_password($code = NULL)
	{
               $this->data['pagetitle'] = '';
        $this->data['title']     = '';
        $this->data['title_lg']  = $this->config->item('title_lg');
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			// if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() == false)
			{
				// display the form

				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
					'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name'    => 'new_confirm',
					'id'      => 'new_confirm',
					'type'    => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;

				// render
				$this->template->auth_render('auth/reset_password', $this->data);
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					// something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						// if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						redirect("auth/login", 'refresh');
					}
					else
					{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('auth/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			// if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}
        function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}
        function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}


}
