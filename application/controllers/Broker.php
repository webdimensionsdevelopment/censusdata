<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Broker extends Frontend_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model("register_model");
        $this->load->model("brokerage_model");
        $this->load->model("user_model");
        $this->load->model("broker_model");
        $this->load->library('email');
        $this->load->library('session');
    }
//    public function index() {
//        
//        if(isset($this->session->userdata['user']['id'])){
//            $id = $this->session->userdata['user']['id'];
//        }
//        else{$id = 0;}
//        $data = array("userId" => $id);
//        $this->load->view("broker/index",$data);
//    }
    
    public function index() {
        $title = "Dashboard";
        $navbardiv = array(
            'subclass' => 'inverse',
            'top' => 'static',
            'role' => 'navigation',
            'fluid' => FALSE,
        );
        if (isset($this->session->userdata['user']['id'])) {
            $id = $this->session->userdata['user']['id'];
        } else {
            $id = 0;
        }
        $data['userId'] = $id;
        $data['viewport'] = TRUE;
        $data['body_role'] = 'document';
        $data['custom_css'] = 'carousel';
        $data['title'] = $title;
        $data['navbar'] = $this->navbar;
        $data['navbar_dropdown'] = $this->navbar_dropdown;
        $data['navbar_right'] = ($id!=0 ? $this->navbar_right_loggedin_user : $this->navbar_right);
        $data['navbardiv'] = $navbardiv;
        $data['navfoot'] = $this->navfoot;
        $data['carousel'] = $this->carousel_page;
        $this->load->view('frontend/head', $data);
        $this->load->view('frontend/navbar/open_wrapper');
        $this->load->view('frontend/navbar/open_nav');
        $this->load->view('frontend/navbar/open_container');
        $this->load->view('frontend/navbar/header');
        $this->load->view('frontend/navbar/open_collapse');
        $this->load->view('frontend/navbar/open_bar');
        $this->load->view('frontend/navbar/li_core', $data);
        $this->load->view('frontend/navbar/right', $data);
        $this->load->view('frontend/navbar/close_bar');
        $this->load->view('frontend/navbar/close');
        $this->load->view('frontend/navbar/close_wrapper');
        $this->load->view('broker/index', $data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data);
//        $brokerageId = $this->session->userdata['user']['userId'];
//        if (isset($_POST['sendInvitation'])) {
//            $error = false;
//            $email = $errorEmail = $firstName = $lastName = $errorFirstName = $errorLastName = '';
//            $firstName = $_POST['txtFirstName'];
//            $lastName = $_POST['txtLastName'];
//            $email = $_POST['txtEmail'];
//            if (empty($email)) {
//                $error = true;
//                $errorEmail = "Please Enter Email";
//                $this->session->set_flashdata('errorEmail', $errorEmail);
//            }
//            $checkEmail = $this->brokerage_model->checkUserByEmail($email);
//            if (empty($checkEmail)) {
//                if ($error != true) {
//                    $userRegistrationLink = site_url() . "brokerage/registerForm";
//                    $activateCode = md5(rand(1, 10000000000) . "fhsf" . rand(1, 100000));
//                    $record = array("email" => $email, "activation_code" => $activateCode, "active" => 0);
//                    $userId = $this->register_model->addUser($record);
//                    $recordBroker = array("userId" => $userId, "brokrage_id" => $brokerageId);
//                    $userBrokerId = $this->register_model->addUserId_Broker($recordBroker);
//                    $recordGroup = array("userId" => $userId, "group_id" => 4);
//                    $userBrokerId = $this->register_model->addUserType($recordGroup);
//                    $data = array("email" => $email);
//                    $to = $email;
//                    $subject = "Activation Account Info";
//                    $header = array('Content-Type:text/html; charset=UTF-8');
//                    $body = "<html><body>Date:" . date("Y-m-d") . "<br>Applicant EMail:" . $firstName . " " . $lastName . "<br><h3 align='Center'> User Registration Form To Fill</h3><br>Dear" . $firstName . ",<br><pre>Please click below link to Register Your Account</pre><br><b>Activation-Link : </b><a href=" . $userRegistrationLink . "/" . $userId . "/" . $activateCode . ">Click Here</a><br>Sincerely,<br>" . "Census.com</body></html>";
//                    $this->email->from(CB_EMAIL_FROM, 'Census');
//                    $this->email->to($email);
//                    $this->email->subject($subject);
//                    $this->email->message($body);
//                    $this->email->send();
//                    redirect(base_url() . "brokerage/index");
//                }
//            }
//        }
    }
}