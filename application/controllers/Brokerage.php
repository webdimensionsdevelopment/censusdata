<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Brokerage extends Frontend_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model("register_model");
        $this->load->model("brokerage_model");
        $this->load->model("user_model");
        $this->load->library('email');
        $this->load->library('session');
    }

    public function index() {
        $title = "Dashboard";
        $navbardiv = array(
            'subclass' => 'inverse',
            'top' => 'static',
            'role' => 'navigation',
            'fluid' => FALSE,
        );
        if (isset($this->session->userdata['user']['id'])) {
            $id = $this->session->userdata['user']['id'];
        } else {
            $id = 0;
        }
        $brokerList = $this->user_model->getBrokerById($id);
        $data['userId'] = $id;
        $data['brokerList'] = $brokerList;
        $data['viewport'] = TRUE;
        $data['body_role'] = 'document';
        $data['custom_css'] = 'carousel';
        $data['title'] = $title;
        $data['navbar'] = $this->navbar;
        $data['navbar_dropdown'] = $this->navbar_dropdown;
        $data['navbar_right'] = ($id!=0 ? $this->navbar_right_loggedin_user : $this->navbar_right);
        $data['navbardiv'] = $navbardiv;
        $data['navfoot'] = $this->navfoot;
        $data['carousel'] = $this->carousel_page;
        $this->load->view('frontend/head', $data);
        $this->load->view('frontend/navbar/open_wrapper');
        $this->load->view('frontend/navbar/open_nav');
        $this->load->view('frontend/navbar/open_container');
        $this->load->view('frontend/navbar/header');
        $this->load->view('frontend/navbar/open_collapse');
        $this->load->view('frontend/navbar/open_bar');
        $this->load->view('frontend/navbar/li_core', $data);
        $this->load->view('frontend/navbar/right', $data);
        $this->load->view('frontend/navbar/close_bar');
        $this->load->view('frontend/navbar/close');
        $this->load->view('frontend/navbar/close_wrapper');
        $this->load->view('brokerage/index', $data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data);
        if (isset($_POST['sendInvitation'])) {
            $error = false;
            $email = $errorEmail = $firstName = $lastName = $errorFirstName = $errorLastName = '';
            $firstName = $_POST['txtFirstName'];
            $lastName = $_POST['txtLastName'];
            $email = $_POST['txtEmail'];
            if (empty($email)) {
                $error = true;
                $errorEmail = "Please Enter Email";
                $this->session->set_flashdata('errorEmail', $errorEmail);
            }
            $checkEmail = $this->brokerage_model->checkUserByEmail($email);
            if (empty($checkEmail)) {
                if ($error != true) {
                    $userRegistrationLink = site_url() . "brokerage/registerForm";
                    $activateCode = md5(rand(1, 10000000000) . "fhsf" . rand(1, 100000));
                    $value = 0;
                    $record = array("email" => $email, "activation_code" => $activateCode, "active" => $value);
                    $userId = $this->register_model->addUser($record);
                    $date = date('Y/m/d H:i:s');
                    $brokerId = $this->user_model->getBrokerIdByUserId($id);
                    $recordBroker = array("userId" => $userId, "brokrage_id" => $brokerId['id']);
                    $userBrokerId = $this->register_model->addUserId_Broker($recordBroker);
                    $userTypId = $this->user_model->getUserTypeIdByName($name = 'Broker');
                    $recordGroup = array("userId" => $userId, "group_id" => $userTypId['id'],"createdAt"=>$date);
                    $userBrokerId = $this->register_model->addUserType($recordGroup);
                    $data = array("email" => $email);
                    $to = $email;
                    $subject = "Activation Account Info";
                    $header = array('Content-Type:text/html; charset=UTF-8');
                    $body = "<html><body>Date:" . date("Y-m-d") . "<br>Applicant EMail:" . $firstName . " " . $lastName . "<br><h3 align='Center'> User Registration Form To Fill</h3><br>Dear" . $firstName . ",<br><pre>Please click below link to Register Your Account</pre><br><b>Activation-Link : </b><a href=" . $userRegistrationLink . "/" . $userId . "/" . $activateCode . ">Click Here</a><br>Sincerely,<br>" . "Census.com</body></html>";
                    $this->email->from(CB_EMAIL_FROM, 'Census');
                    $this->email->to($email);
                    $this->email->subject($subject);
                    $this->email->message($body);
                    $this->email->send();
                    redirect(base_url() . "brokerage/index");
                }
            }
        }
    }

    public function registerForm($userId, $code) {
        $title = "Dashboard";
        $navbardiv = array(
            'subclass' => 'inverse',
            'top' => 'static',
            'role' => 'navigation',
            'fluid' => FALSE,
        );
        $email = $this->user_model->getEmailById($userId);
        $data['userId'] = $userId;
        $data['code'] = $code;
        $data['email'] = $email;
        $data['viewport'] = TRUE;
        $data['body_role'] = 'document';
        $data['custom_css'] = 'carousel';
        $data['title'] = $title;
        $data['navbar'] = $this->navbar;
        $data['navbar_dropdown'] = $this->navbar_dropdown;
        $data['navbar_right'] = $this->navbar_right;
        $data['navbardiv'] = $navbardiv;
        $data['navfoot'] = $this->navfoot;
        $data['carousel'] = $this->carousel_page;
        $this->load->view('frontend/head', $data);
        $this->load->view('frontend/navbar/open_wrapper');
        $this->load->view('frontend/navbar/open_nav');
        $this->load->view('frontend/navbar/open_container');
        $this->load->view('frontend/navbar/header');
        $this->load->view('frontend/navbar/open_collapse');
        $this->load->view('frontend/navbar/open_bar');
        $this->load->view('frontend/navbar/li_core', $data);
        $this->load->view('frontend/navbar/right', $data);
        $this->load->view('frontend/navbar/close_bar');
        $this->load->view('frontend/navbar/close');
        $this->load->view('frontend/navbar/close_wrapper');
        $this->load->view('brokerage/registerForm', $data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data);
        if (isset($_POST['brokerSubmit'])) {
            $FImage = $_FILES['image'];
            $records = $this->formAdd($_POST, $userId, $FImage, $userId);
            $this->user_model->upDateUserDetails($records, $userId);
            $recordBroker = array("userId" => $userId, "brokrage_id" => $id);
            $addToBrokerId = $this->user_model->addToBrokerUserId($recordBroker);
            $recordGroup = array("userId" => $userId, "group_id" => 4, "createdAt" => date("Y-m-d"));
            $addToGroup = $this->user_model->addToUserGroupId($recordGroup);
            redirect(site_url() . "login/index");
        }
    }

    public function checkEmail() {
        $checkUserEmail = $this->brokerage_model->checkUserByEmail($_GET['email']);
        if (!empty($checkUserEmail)) {
            echo 1;
            exit;
        } else {
            echo 0;
            exit();
        }
    }

    public function formAdd($userDetails, $userId, $FImage = array()) {
        $error = false;
        $firstName = $lastName = $email = $password = $confirmPassword = $image = $phoneNumber = $address = $country = $state = $city = $gender = "";
        $errorFirstName = $errorLastName = $errorEmail = $errorPassword = $errorConfirmPassword = $errorGender = '';
        $firstName = $userDetails['txtFirstName'];
        $lastName = $userDetails['txtLastName'];
        $email = $userDetails['txtEmail'];
        $password = $userDetails['txtPassword'];
        $confirmPassword = $userDetails['txtConfirmPassword'];
        $phoneNumber = $userDetails['txtPhoneNumber'];
        $address = $userDetails['txtAddress'];
        $gender = $userDetails['rdGender'];
        $zipCode = $userDetails['txtZipCode'];
        $country = $userDetails['txtCountry'];
        $state = $userDetails['txtState'];
        $city = $userDetails['txtCity'];
        if (empty($firstName)) {
            $error = true;
            $errorFirstName = "Enter First Name";
            $this->session->set_flashdata('errorFirstName', $errorFirstName);
        }
        if (empty($lastName)) {
            $error = true;
            $errorLastName = "Enter Last Name";
            $this->session->set_flashdata('errorLastName', $errorLastName);
        }
        if (empty($email)) {
            $error = true;
            $errorEmail = "Enter Email";
            $this->session->set_flashdata('errorEmail', $errorEmail);
        }
        if (empty($password)) {
            $error = true;
            $errorPassword = "Enter Password";
            $this->session->set_flashdata('errorPassword', $errorPassword);
        }
        if (empty($confirmPassword)) {
            $error = true;
            $errorConfirmPassword = "Enter Confirm Password";
            $this->session->set_flashdata('errorConfirmPassword', $errorConfirmPassword);
        }
        if (empty($gender)) {
            $error = true;
            $errorGender = "Please select a gender";
            $this->session->set_flashdata('errorGender', $errorGender);
        }
        if (strcmp($password, $confirmPassword) != 0) {
            $error = true;
            $errorUnmatch = "Password did not match, Please enter again";
            $this->session->set_flashdata('errorUnmatch', $errorUnmatch);
        }
        if ($error != true) {

            $insertedId = $this->user_model->getLastInsertedId();
            $newUserId = $insertedId['id'] + 1;
            if ($FImage) {
                $path = $_SERVER['DOCUMENT_ROOT'] . "censusnew/upload/Broker";
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $userFolderName = $firstName . $newUserId;
                $userPath = $_SERVER['DOCUMENT_ROOT'] . "censusnew/upload/Broker/" . $userFolderName . "/";
                if (!file_exists($userPath)) {
                    mkdir($userPath, 0777, true);
                }
                $image = $this->user_model->uploadFile($FImage, $userPath, $userFolderName);
            }
            $record = array(
                "id" => $userId,
                "ip_address" => $_SERVER['REMOTE_ADDR'],
                "userName" => $email,
                "first_name" => $firstName,
                "last_name" => $lastName,
                "gender" => $gender,
                "password" => md5($password),
                "email" => $email,
                "image" => ($image) ? $image : '',
                "phoneNumber" => ($phoneNumber) ? $phoneNumber : '',
                "address" => ($address) ? $address : '',
                "country" => ($country) ? $country : '',
                "state" => ($state) ? $state : '',
                "city" => ($city) ? $city : '',
                "zipcode" => ($zipCode) ? $zipCode : '',
                "activation_code" => '',
                "active" => 1
            );
        }
        return $record;
    }

}
