<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Frontend extends Frontend_Controller
{
    public function __construct() {
        parent::__construct();
    }

    public function index()
    {
        $this->load->helper('url');

         /*Redirect the user to some site*/
         redirect('/Frontend_company');
        /*
        $title = "IXSolution";

        $marketing = array(
            array(
                'img_src' => 'data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==',
                'img_alt' => 'Generic placeholder image 1',
                'h2' => 'Heading 1',
                'p' => "Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.",
                'href' => '#',
                'button' => 'View details 1',
            ),
            array(
                'img_src' => 'data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==',
                'img_alt' => 'Generic placeholder image 2',
                'h2' => 'Heading 2',
                'p' => "Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.",
                'href' => '#',
                'button' => 'View details 2',
            ),
            array(
                'img_src' => 'data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==',
                'img_alt' => 'Generic placeholder image 3',
                'h2' => 'Heading 3',
                'p' => "Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.",
                'href' => '#',
                'button' => 'View details 3',
            ),
        );
        $featurette = array(
            array(
                'h2' => "First featurette heading",
                'muted' => "It'll blow your mind.",
                'lead_p' => "Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.",
                'img_data-src' => 'holder.js/500x500/auto',
                'img_alt' => 'Generic placeholder image',
            ),
            array(
                'h2' => "Oh yeah, it's that good.",
                'muted' => "See for yourself.",
                'lead_p' => "Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.",
                'img_data-src' => 'holder.js/500x500/auto',
                'img_alt' => 'Generic placeholder image',
            ),
            array(
                'h2' => "And lastly, this one.",
                'muted' => "Checkmate.",
                'lead_p' => "Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.",
                'img_data-src' => 'holder.js/500x500/auto',
                'img_alt' => 'Generic placeholder image',
            ),
        );

        $navbardiv = array(
            'subclass' => 'inverse',
            'top' => 'static',
            'role' => 'navigation',
            'fluid' => FALSE,
        );
        $data['viewport'] = TRUE;
        $data['body_role'] = 'document';
        $data['custom_css'] = 'carousel';
        $data['title'] = $title;
        $data['navbar'] = $this->navbar;
        $data['navbar_dropdown'] = $this->navbar_dropdown;
        $data['navbar_right'] = ($this->user->loggedin ? $this->navbar_right_loggedin_user : $this->navbar_right);
        $data['navbardiv'] = $navbardiv;
        $data['navfoot'] = $this->navfoot;
        $data['carousel'] = $this->carousel_page;
        $data['marketing'] = $marketing;
        $data['featurette'] = $featurette;

        $this->load->view('frontend/head', $data);
        $this->load->view('frontend/navbar/open_wrapper');
        $this->load->view('frontend/navbar/open_nav');
        $this->load->view('frontend/navbar/open_container');
        $this->load->view('frontend/navbar/header');
        $this->load->view('frontend/navbar/open_collapse');
        $this->load->view('frontend/navbar/open_bar');
        $this->load->view('frontend/navbar/li_core', $data);
        $this->load->view('frontend/navbar/right', $data);
        $this->load->view('frontend/navbar/close_bar');
        $this->load->view('frontend/navbar/close');
        $this->load->view('frontend/navbar/close_wrapper');
        $this->load->view('frontend/carousel', $data);
        $this->load->view('frontend/carousel/marketing', $data);
        $this->load->view('frontend/carousel/featurette', $data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data);
        */
    }
}