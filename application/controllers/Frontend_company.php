<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Frontend_company extends Frontend_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('admin/Company_model');
        $this->load->library('Ion_auth');
        $this->load->library('email');
        $this->load->helper('url'); 

    }

    public function LoadThemePage($title = "GA Flex Census Data")
    {
        $data['title'] = $title;
        $navbardiv = array(
            'subclass' => 'inverse',
            'top' => 'static',
            'role' => 'navigation',
            'fluid' => FALSE,
        );
        $data['viewport'] = TRUE;
        $data['body_role'] = 'document';
        $data['custom_css'] = 'carousel';

        $data['navbar'] = $this->navbar;
        $data['navbar_dropdown'] = $this->navbar_dropdown;
        $data['navbar_right'] = ($this->user->loggedin ? $this->navbar_right_loggedin_user : $this->navbar_right);
        $data['navbardiv'] = $navbardiv;
        $data['navfoot'] = $this->navfoot;
        $data['carousel'] = $this->carousel_page;
        $this->load->view('frontend/head', $data);
        $this->load->view('frontend/navbar/open_wrapper');
        $this->load->view('frontend/navbar/open_nav');
        $this->load->view('frontend/navbar/open_container');
        $this->load->view('frontend/navbar/header');
        $this->load->view('frontend/navbar/open_collapse');
        $this->load->view('frontend/navbar/open_bar');
        $this->load->view('frontend/navbar/li_core', $data);
        $this->load->view('frontend/navbar/right', $data);
        $this->load->view('frontend/navbar/close_bar');
        $this->load->view('frontend/navbar/close');
        $this->load->view('frontend/navbar/close_wrapper');
    }
    public function index() {
        error_reporting(0);
        $this->LoadThemePage($title);
        $data['product'] = $this->Company_model->get_product();
        $data['sic_code'] = $this->Company_model->get_sic_code();
        $this->load->view('frontend/frontend_Company_view', $data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data);
    }
    public function getCountryStateCity() {
        $count_city = '';
        $coun_State_city = array();
        $state = '';
        $shortstatename = '';
        $city = '';
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . $_GET['zipcode'] . '&sensor=false';
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
            exit;
        } else {
            $country= '';
            $obj = json_decode($response);
            foreach($obj->results[0]->address_components as $address)
            {
                if ($address->types[0] == 'locality')
                {
                    $city = (isset($address->long_name) ? $address->long_name : '');
                }
                if ($address->types[0] == 'administrative_area_level_2') // county
                {
                    $country = (isset($address->long_name) ? $address->long_name : '');
                }
                if ($address->types[0] == 'administrative_area_level_1')
                {
                    $state = (isset($address->long_name) ? $address->long_name : '');
                    $shortstatename = (isset($address->long_name) ? $address->short_name : '');
                }
            }
        $coun_State_city = array("country" => $country, "state" => $state, "city" => $city, 'shortstatename' => $shortstatename);
            echo json_encode($coun_State_city);
            exit;
        }
    }
    public function insert_company() {
        error_reporting(0);
        if ($this->input->post('add') == 'add') {
            $tables = $this->config->item('tables', 'auth');
            $product = $this->input->post('product');
            $group = $this->Company_model->get_group();
            $email = $this->input->post('email');
            $cname = $this->input->post('cname');
            $user = array('username' => $cname,
                'address' => $this->input->post('address'),
                'zipcode' => $this->input->post('zipcode'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'company' => $cname,
                'email' => $email
            );
            $id = $this->Company_model->insert_user($user);
            $date = date('Y-m-d H:i:s');
            $user_type = array('user_id' => $id, 'group_id' => $group[0]->id, 'createdAt' => $date);
            $this->Company_model->insert_user_type($user_type);
            $company = array('userId' => $id,
                'sicId' => $this->input->post('sic'),
                'no_of_emp' => $this->input->post('no_of_emp'),
                'brokerage_name' => $this->input->post('brokerage_name'),
                'broker_fname' => $this->input->post('bfname'),
                'broker_lname' => $this->input->post('blname'),
                'npn' => $this->input->post('npn'),
                'phone' => $this->input->post('phone'),
                'email' => $this->input->post('bemail'),
                'assign_status' => 'unassigned',
                'quote_status' => 'Quote Requested'
            );
            $cid = $this->Company_model->insert_company($company);
            $this->session->set_userdata('companyid',$cid);
            foreach ($product as $product) {
                $comp_product = array('comp_id' => $cid,
                    'prod_id' => $product,
                    'renewal_date' => date('Y-m-d', strtotime($this->input->post('renewal_date' . $product))),
                    'effective_date' => date('Y-m-d', strtotime($this->input->post('effect_date' . $product)))
                );
                $this->Company_model->insert_company_product($comp_product);
            }
            redirect('Frontend_company/employee_view');
        } else {
            $email = $this->input->post('email');
            $cname = $this->input->post('cname');
            $user = array('username' => $cname,
                'address' => $this->input->post('address'),
                'zipcode' => $this->input->post('zipcode'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'company' => $cname,
                'email' => $email
            );
            $this->Company_model->update_user($user, $this->input->post('userid'));
            $company = array('sicId' => $this->input->post('sic'),
                         'no_of_emp'=>$this->input->post('no_of_emp'),
                         'brokerage_name'=>$this->input->post('brokerage_name'),
                         'broker_fname'=>$this->input->post('bfname'),
                         'broker_lname'=>$this->input->post('blname'),
                         'npn'=>$this->input->post('npn'),
                         'phone'=>$this->input->post('phone'),
                         'email'=>$this->input->post('bemail'),
                         'close_status'=>'1'
            );
            $this->Company_model->update_company($company, $this->input->post('companyid'));
             redirect('Frontend_company/edit_employee_view');
        }
    }
    public function employee_view()
    {
         error_reporting(0);
        $cid=$this->session->userdata('companyid');
	    $title = "Census Information";
        $this->LoadThemePage($title);
        $data['company']=$cid;
        $data['company_emp']=$this->Company_model->get_comp_emp($cid);
        $data['occupation']=$this->Company_model->get_occupation($cid);
        $data['salary']=$this->Company_model->get_salary($cid);
        $this->load->view('frontend/frontend_employee_view',$data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data);
    }
    public function employee_upload_view()
    {
        //error_reporting(0);
        $data['company']=$this->session->userdata('companyid');
        $data['company_emp']=$this->input->post('comp_emp');
        $title = "Upload Employee Census";
        $this->LoadThemePage($title);
        $this->load->view('frontend/frontend_employee_upload_view',$data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data);    
    }
    public function insert_employee() {
        error_reporting(0);
        $product_count=0;
        $product_count=$this->Company_model->company_product_count($this->input->post('cid'));
        $file_name = $_FILES['file_name'];
        if($file_name)
        {
            $comp_name=$this->Company_model->get_company($this->input->post('cid'));
            $comp_name_id=$comp_name."_".$this->input->post('cid');
            if (!file_exists(CB_DOWNLOADPATH . "user/employee_csv"))
            {
                mkdir(CB_DOWNLOADPATH . "user/employee_csv",0777);
            }
            $userPath = CB_DOWNLOADPATH . "user/employee_csv/";
            $file = $this->Company_model->uploadFile($_FILES['file_name'], $userPath,$comp_name_id);
            ini_set('auto_detect_line_endings',TRUE);
             $fp = fopen(CB_DOWNLOADPATH . "user/employee_csv/".$file, "r");
             $flag=true;
             $id=0;
             $last_name='';
             $status=0;
             $comp_emp=0;
             $row=1;
             while (($linedata = fgets($fp, 4096)) !== false)
             {
                $data = $this->csv_split($linedata,',');
                 foreach($data as $num => $key)
                 {
                     $data[$num] = trim($data[$num]);
                 }
                 if($flag)
                 {
                     // Handle if they add in the  1st row removing the other data
                     if($row==1)
                     {
                          if(strtolower($data[1])=='first name' && strtolower($data[2])=='last name' && strtolower($data[3])=='date of birth' && strtolower($data[4])=='zipcode' && strtolower($data[5])=='state' && strtolower($data[6])=='salary' && strtolower($data[7])=='occupation' &&  strtolower($data[8])=='relation' && strtolower($data[9])=='gender' && strtolower($data[10])=='status')
                         {
                            $flag=false; continue;
                         }
                     }
                     if($row==3)
                     {
                          if(strtolower($data[1])=='first name' && strtolower($data[2])=='last name' && strtolower($data[3])=='date of birth' && strtolower($data[4])=='zipcode' && strtolower($data[5])=='state' && strtolower($data[6])=='salary' && strtolower($data[7])=='occupation' &&  strtolower($data[8])=='relation' && strtolower($data[9])=='gender' && strtolower($data[10])=='status')
                         {
                            $flag=false; continue;
                         }
                         else
                         {
                            $this->session->set_flashdata('msg','Uploaded format not proper, Please download format from below button');
                            redirect('Frontend_company/employee_upload_view');
                         
                         }
                     }
                     else
                     {
                         $row++;
                         continue;
                         
                     }
                 }
                 if(substr($data[8],0,1)=='e' || substr($data[8],0,1)=='E')
                 {
                     if($data[10]=='Active' || $data[10]=='active'){ $status=1;}
                    $employee = array('companyId' =>$this->input->post('cid') ,
                                       'firstName' => $data[1],
                                       'lastName' => $data[2],
                                       'dateofBirth' => date('Y-m-d',strtotime($data[3])),
                                       'zipcode' => $data[4],
                                       'state' => $data[5],
                                       'salary' => $data[6],
                                       'occupation' => $data[7],
                                       'gender' => $data[9],
                                       'status' => $status,
                                       'createdAt' => date('Y-m-d')
                    );
                    $id = $this->Company_model->insert_employee($employee);
                   $last_name=$data[2];
                   $comp_emp=$comp_emp+1;
                 }
                 else if((substr($data[8],0,1)=='s' || substr($data[8],0,1)=='S') && $last_name==$data[2])
                 {
                     $spouse = array('employeeId' => $id,
                                    'esFirstName' => $data[1],
                                    'esLastName' => $data[2],
                                    'dateOfBirth' => date('Y-m-d',strtotime($data[3])),
                                    'emp_relation' => $data[8],
                                    'gender'=>$data[9]
                        );
                    $this->Company_model->insert_employee_relation_data($spouse);
                 }
                 else if((substr($data[8],0,1)=='c' || substr($data[8],0,1)=='C') && $last_name==$data[2])
                 {  
                    $child = array('employeeId' => $id,
                                   'esFirstName' => $data[1],
                                   'esLastName' => $data[2],
                                   'dateOfBirth' => date('Y-m-d',strtotime($data[3])),
                                   'emp_relation' => $data[8],
                                   'gender'=>$data[9]
                            );
                     $this->Company_model->insert_employee_relation_data($child);
                 }        
             } 
            $cid=$this->session->userdata("companyid");
             fclose($fp);
            // delete the file we don't need it anymore
            if (!empty($file))
                unlink(CB_DOWNLOADPATH . "user/employee_csv/" . $file);
              /*$company_employee=array('no_of_emp'=>$comp_emp);
        $this->Company_model->update_company_employee($company_employee,$this->input->post('cid'));*/
         //redirect('Frontend_company/company_list_view/'.$this->input->post('cid'));
        redirect('Frontend_company/edit_employee_view'); 
       }
        else if ($this->input->post('add') == 'update') {
            $count = $this->input->post('count');
            $comp_emp = 0;
            for ($i = 1; $i <= $count; $i++) {
                if ($this->input->post('fname' . $i) != '') {
                    $empid = $this->input->post('empid' . $i);
                    $employee = array('companyId' => $this->input->post('cid'),
                        'firstName' => $this->input->post('fname' . $i),
                        'lastName' => $this->input->post('lname' . $i),
                        'dateofBirth' => date('Y-m-d', strtotime($this->input->post('dob' . $i))),
                        'zipcode' => $this->input->post('zipcode' . $i),
                        'state' => $this->input->post('state' . $i),
                        'salary' => $this->input->post('salary' . $i),
                        'occupation' => $this->input->post('occupation' . $i),
                        'createdAt' => date('Y-m-d'),
                        'gender' => $this->input->post('gender' . $i),
                        'status' => '1'
                    );
                    if ($empid != '')
                        $this->Company_model->update_employee($employee, $empid);
                    else
                        $empid = $this->Company_model->insert_employee($employee);

                    if ($this->input->post('sfname' . $i) != '' && $this->input->post('slname' . $i) != '' && $this->input->post('sdob' . $i) != '') {
                        $sempid = $this->input->post('sempid' . $i);
                        if ($sempid != '') {
                            $spouse = array(
                                'esFirstName' => $this->input->post('sfname' . $i),
                                'esLastName' => $this->input->post('slname' . $i),
                                'gender' => $this->input->post('sgender' . $i),
                                'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('sdob' . $i))),
                                'emp_relation' => 'Spouse'
                            );
                            $this->Company_model->update_employee_relation_data($spouse, $sempid);
                        } else {
                            $spouse = array('employeeId' => $empid,
                                'esFirstName' => $this->input->post('sfname' . $i),
                                'esLastName' => $this->input->post('slname' . $i),
                                'gender' => $this->input->post('sgender' . $i),
                                'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('sdob' . $i))),
                                'emp_relation' => 'Spouse'
                            );
                            $this->Company_model->insert_employee_relation_data($spouse);
                        }
                    }
                    $child_count = $this->input->post('child' . $i);
                    if ($child_count > 0) {
                        for ($j = 1; $j <= $child_count; $j++) {
                            $cempid = $this->input->post('cempid' . $i . "_" . $j);
                            if ($cempid != '') {
                                $child = array(
                                    'esFirstName' => $this->input->post('cfname' . $i . "_" . $j),
                                    'esLastName' => $this->input->post('clname' . $i . "_" . $j),
                                    'gender' => $this->input->post('cgender' . $i . "_" . $j),
                                    'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('cdob' . $i . "_" . $j))),
                                    'emp_relation' => 'Child'
                                );
                                $this->Company_model->update_employee_relation_data($child, $cempid);
                            } else {
                                $child = array('employeeId' => $empid,
                                    'esFirstName' => $this->input->post('cfname' . $i . "_" . $j),
                                    'esLastName' => $this->input->post('clname' . $i . "_" . $j),
                                    'gender' => $this->input->post('cgender' . $i . "_" . $j),
                                    'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('cdob' . $i . "_" . $j))),
                                    'emp_relation' => 'Child'
                                );
                                $this->Company_model->insert_employee_relation_data($child);
                            }
                        }
                    }
                    $comp_emp++;
                }
            }
        } 
        else
        {
            $count = $this->input->post('count');
            $comp_emp = $this->input->post('comp_emp');
            for ($i = 1; $i <= $count; $i++) {
                if($this->input->post('fname' . $i)!='')
                {
                    $employee = array('companyId' =>$this->input->post('cid') ,
                        'firstName' => $this->input->post('fname' . $i),
                        'lastName' => $this->input->post('lname' . $i),
                        'dateofBirth' => date('Y-m-d', strtotime($this->input->post('dob' . $i))),
                        'zipcode'=>$this->input->post('zipcode'. $i),
                        'state'=>$this->input->post('state'. $i),
                        'salary' => $this->input->post('salary' . $i),
                        'occupation' => $this->input->post('occupation' . $i),
                        'createdAt' => date('Y-m-d'),
                        'gender' => $this->input->post('gender'.$i),
                        'status' => '1'
                    );
                    $id = $this->Company_model->insert_employee($employee);
                    if ($this->input->post('sfname' . $i) != '' && $this->input->post('slname' . $i) != '' && $this->input->post('sdob' . $i) != '') {
                        $spouse = array('employeeId' => $id,
                            'esFirstName' => $this->input->post('sfname' . $i),
                            'esLastName' => $this->input->post('slname' . $i),
                            'gender' => $this->input->post('sgender'.$i),
                            'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('sdob' . $i))),
                            'emp_relation' => 'Spouse'
                        );
                        $this->Company_model->insert_employee_relation_data($spouse);
                    }
                    $child_count = $this->input->post('child' . $i);
                    if ($child_count > 0) {
                        for ($j = 1; $j <= $child_count; $j++) {
                            $child = array('employeeId' => $id,
                                'esFirstName' => $this->input->post('cfname' . $i . "_" . $j),
                                'esLastName' => $this->input->post('clname' . $i . "_" . $j),
                                'gender' => $this->input->post('cgender'. $i . "_" . $j),
                                'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('cdob' . $i . "_" . $j))),
                                'emp_relation' => 'Child'
                            );
                            $this->Company_model->insert_employee_relation_data($child);
                        }
                    }
                }
            }
        }
        $company_employee=array('no_of_emp'=>$comp_emp);
        $this->Company_model->update_company_employee($company_employee,$this->input->post('cid'));
       redirect('Frontend_company/question_answer_view');
    }
    public function question_answer_view()
    {
         error_reporting(0);
       $title = "Questionnaire";
         $cid = $this->session->userdata('companyid');
        $product=$this->Company_model->get_comp_product($cid);
       foreach($product as $pro)
       {
          $data['question'][$pro->name] = $this->Company_model->get_question($pro->productId);
       }
        $data['answer'] = $this->Company_model->get_answer();
        $data['document']=$this->Company_model->get_document();
        $data['cid'] = $cid;
        $data['no_of_emp']=$this->Company_model->get_comp_emp($cid);
        $this->LoadThemePage($title);

        $this->load->view('frontend/frontend_question_answer_view', $data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data);
    }
    public  function question_answer_upload_view()
    {
         error_reporting(0);
        $title = "GAFlex";
        $cid=$this->session->userdata('companyid');
        $data['document']=$this->Company_model->get_document();
        $data['cid'] = $cid;
        $this->LoadThemePage($title);

        $this->load->view('frontend/frontend_question_answer_upload_view', $data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data);
    }
    public function insert_question_answer() {
        error_reporting(0);
          $cid = $this->input->post('cid');
            $count=$this->input->post('count');

            $comp_name=$this->Company_model->get_company($cid);
            if (!file_exists(CB_DOWNLOADPATH . "user/document")) {
                    mkdir(CB_DOWNLOADPATH . "user/document",0777);
                }
            $userPath = CB_DOWNLOADPATH . "user/document/";
            for($cnt=1;$cnt<=$count;$cnt++)
            {
                for($cnt1=1;$cnt1<=$this->input->post('file_count'.$cnt);$cnt1++)
                {
                    if (!empty($_FILES['file_name'.$cnt."_".$cnt1]['name']))
                    {
                        $file_name = $_FILES['file_name'.$cnt."_".$cnt1];
                        $name=explode('.', $file_name['name']);
                        $comp_name_id=urlencode($name[0])."_".$comp_name."_".$cid;
                        $file = $this->Company_model->uploadFile($_FILES['file_name'.$cnt."_".$cnt1], $userPath,$comp_name_id);
                        $question_document=array('comp_id'=>$cid,
                                                 'doc_id'=>$this->input->post('file_type'.$cnt),
                                                 'document_name'=>$file
                                                );
                        $this->Company_model->insert_question_document($question_document);
                    }
                }
            }
            $cnt = $this->input->post('cnt');
            for ($i = 1; $i < $cnt; $i++) {
                $ans_count=count($this->input->post('aid' . $i.'[]'));
               for($j=0;$j<$ans_count;$j++)
               {
                  $question_answer = array("questionId" => $this->input->post('qid' . $i),
                    "ques_answer" => $this->input->post('ques_answer' . $i),
                    "answerId" => $this->input->post('aid' . $i.'['.$j.']'),
                    "companyId" => $cid
                );
                $this->Company_model->insert_question_answer($question_answer);
               }  
            }
       /*$this->session->unset_userdata("companyid");*/
        redirect('Frontend_company/company_list_view'); 
    }

    function company_list_view()
    {
         error_reporting(0);
       global $config;
       if($this->uri->segment(3))
       {
       $cid=$this->uri->segment(3);
       }
       else
       {
           $cid=$this->session->userdata('companyid');
       }
       $data['company_detail']=$this->Company_model->get_company_detail($cid);
       $data['base_url'] = $config['base_url'];
       $email=$this->Company_model->get_active_email();
       foreach($email as $newemail)
       {
            $this->email->from(CB_EMAIL_FROM, ' Flexible Benefit');
            $this->email->to($newemail->admin_email);
            $this->email->subject('New Quote Request');
            $this->email->message($this->load->view($this->config->item('email_templates', 'ion_auth') . $this->config->item('email_new_company', 'ion_auth'), $data, true));
            $this->email->send();
        }
        $title = "Thank you for submitting a quote request";
       $this->LoadThemePage($title);

        $this->load->view('frontend/frontend_company_list_view',$data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data);
    }
    public function export_company_detail()
    {
        error_reporting(0);
        $cid=$this->session->userdata('companyid');
        $comp_name=$this->Company_model->get_company($cid);
        $comp_name_id=$comp_name."_".$cid;
        $this->load->helper('download');
        $detail=$this->Company_model->get_company_employee($cid);
        
        if (!file_exists(CB_DOWNLOADPATH . "user")) {
            mkdir(CB_DOWNLOADPATH . "user",0777,true);
        }
       $fp = fopen(CB_DOWNLOADPATH . "user/".$comp_name_id.".csv", "w");
        $head=array("Employee Id","First Name", "Last Name", "Zipcode", "State", "Relation", "Birth Date", "Gender","Status");
         fputcsv($fp, $head);
        $write_info = array();
        $no=1;
        foreach ($detail as  $detail) {
            $write_info['employeeid'] = $no;
            $write_info['first_name'] = $detail->firstName;
            $write_info['last_name'] = $detail->lastName;
            $write_info['zipcode'] = $detail->zipcode;
            $write_info['state'] = $detail->state;        
            $write_info['relation'] ="Employee";
            $write_info['birth_date'] = date('m/d/Y',strtotime($detail->dateOfBirth));
            $write_info['gender'] = $detail->gender;
            if($detail->status==1)
            {
                $write_info['status'] = "Active";
            }
            else
            {
                $write_info['status'] = "Inactive";
            }
            fputcsv($fp, $write_info);
            $relation_detail=$this->Company_model->get_company_employee_relation($detail->employeeId);
            if(!empty($relation_detail))
            {
                foreach ($relation_detail as  $rel_detail) {
                    $write_info['employeeid'] = $no;
                    $write_info['first_name'] = $rel_detail->esFirstName;
                    $write_info['last_name'] = $rel_detail->esLastName;
                    $write_info['zipcode'] = '';
                    $write_info['state'] = '';  
                    $write_info['relation'] = $rel_detail->emp_relation;
                    $write_info['birth_date'] = date('d/m/Y',strtotime($rel_detail->dateOfBirth));
                    $write_info['gender'] = $rel_detail->gender;
                    $write_info['status'] = "";
                    fputcsv($fp, $write_info);
                }
            }
            $no++;
        }
        fclose($fp);
        $this->session->unset_userdata('companyid');
        $file_name = CB_DOWNLOADPATH . "user/".$comp_name_id.".csv";
        force_download($file_name, NULL); 
         
    }
    public function edit_employee_view()
    {
        error_reporting(0);
        $cid=$this->session->userdata("companyid");
        $data['company']=$cid;
        $data['employee_detail']=$this->Company_model->get_company_employee($cid);
        $data['company_emp']=$this->Company_model->get_comp_emp($cid);
        $data['employee_relation']=$this->Company_model->get_company_all_employee($cid);
        $data['occupation']=$this->Company_model->get_occupation($cid);
        $data['salary']=$this->Company_model->get_salary($cid);
        $title = "GAFlex";
        $this->LoadThemePage($title);

        $this->load->view('frontend/frontend_employee_view',$data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data);
    }
    public function delete_employee($empid)
    {
       $this->Company_model->delete_employee($empid);
       return;
       
    }
    public function delete_employee_relation_data($id)
    {
       $this->Company_model->delete_employee_relation_data($id);
       return;
    }
    public function delete_employee_child($empid,$relation)
    {
       $this->Company_model->delete_employee_child($empid,$relation);
       return;
    }
    public function employee_upload_format()
    {
        $this->load->helper('download');
        $folderpath = $this->config->item('folderPathGA');
        $userPath = $folderpath."employee_csv/";
        $file_name= $userPath."employee_upload_example.csv";
        force_download($file_name, NULL);
    }
     public function update_company_view()
    {
         error_reporting(0);
        $cid=$this->session->userdata("companyid");
        $data['company_detail']=$this->Company_model->get_company_all_detail($cid);
        $data['product']='';
        $data['sic_code'] = $this->Company_model->get_sic_code();
        $data['company']=$cid;
         $title = "GAFlex";
        $this->LoadThemePage($title);

        $this->load->view('frontend/frontend_Company_view',$data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data);
    }

    function csv_split($line, $delim = ',', $removeQuotes = true)
    {
    #$line: the csv line to be split
    #$delim: the delimiter to split by
    #$removeQuotes: if this is false, the quotation marks won't be removed from the fields
        $fields = array();
        $fldCount = 0;
        $inQuotes = false;
        for ($i = 0; $i < strlen($line); $i++)
        {
            if (!isset($fields[$fldCount])) $fields[$fldCount] = "";
            $tmp = substr($line, $i, strlen($delim));
            if ($tmp === $delim && !$inQuotes)
            {
                $fldCount++;
                $i += strlen($delim) - 1;
}
            else if ($fields[$fldCount] == "" && $line[$i] == '"' && !$inQuotes)
            {
                if (!$removeQuotes) $fields[$fldCount] .= $line[$i];
                $inQuotes = true;
            }
            else if ($line[$i] == '"')
            {
                if ($line[$i + 1] == '"')
                {
                    $i++;
                    $fields[$fldCount] .= $line[$i];
                }
                else
                {
                    if (!$removeQuotes) $fields[$fldCount] .= $line[$i];
                    $inQuotes = false;
                }
            }
            else
            {
                $fields[$fldCount] .= $line[$i];
            }
        }
        return $fields;
    }
}
?>