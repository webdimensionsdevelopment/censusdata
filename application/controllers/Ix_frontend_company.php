<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ix_frontend_company extends Frontend_Controller {

    public function __construct() {
        parent::__construct();
          $this->load->library('form_validation');
        $this->load->library('Ixsolution_ion_auth');
        $this->load->model('Company_model');
        $this->load->model('ixsolutions_ion_auth_model');
        $title = "Ixsolution census";
        $navbardiv = array(
            'subclass' => 'inverse',
            'top' => 'static',
            'role' => 'navigation',
            'fluid' => FALSE,
        );
        $data['viewport'] = TRUE;
        $data['body_role'] = 'document';
        $data['custom_css'] = 'carousel';
        $data['title'] = $title;
        $data['navbar'] = ($this->ixsolution_ion_auth->logged_in() ? array(): $this->navbar1);//$this->navbar1;
        $data['navbar_dropdown'] = $this->navbar_dropdown;
        $data['navbar_right'] = ($this->ixsolution_ion_auth->logged_in() ? $this->navbar_right_loggedin_user1 : $this->navbar_right);
        $data['navbardiv'] = $navbardiv;
        $data['navfoot'] = $this->navfoot1;
        $data['carousel'] = $this->carousel_page;
        $this->load->view('frontend/head', $data);
        $this->load->view('frontend/navbar1/open_wrapper');
        $this->load->view('frontend/navbar1/open_nav');
        $this->load->view('frontend/navbar1/open_container');
        $this->load->view('frontend/navbar1/header');
        $this->load->view('frontend/navbar1/open_collapse');
        $this->load->view('frontend/navbar1/open_bar');
        $this->load->view('frontend/navbar1/li_core', $data);
        $this->load->view('frontend/navbar1/right', $data);
        $this->load->view('frontend/navbar1/close_bar');
        $this->load->view('frontend/navbar1/close');
        $this->load->view('frontend/navbar1/close_wrapper');
    }
    /*
     * company view 
     */
    public function index() {
         error_reporting(0);
        $data['product'] = $this->Company_model->get_product();
        $data['sic_code'] = $this->Company_model->get_sic_code();
        $data['company_detail'] = '';
        $this->load->view('frontend/ix_frontend_company_view', $data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data);   
    }
    public function companyDetails($id) {
         error_reporting(0);
       if (!$this->ixsolution_ion_auth->logged_in()) {
            redirect('ixsolutions_auth/login', 'refresh');
        }  else {
            /* Title Page */
            $this->page_title->push('Company Details');
            $this->data['pagetitle'] = $this->page_title->show();
            /* Breadcrumbs */
            $this->breadcrumbs->unshift(2, lang('menu_company'), 'admin/company_list_view');
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $this->data['users'] = $this->Company_model->get_company_and_employee_details_byId($id);
            foreach ($this->data['users'] as $k => $user) {
                $logValue = $this->Company_model->get_product_details_of_employee_byId($user['companyId']);
                if(count($logValue)>0){
                    foreach ($logValue as $j => $productArray) {
                    $this->data['users'][$k]['product'][$j] = $productArray;
                    }
                }
                else {
                    $this->data['users'][$k]['product']='';
                }
                $empValue = $this->Company_model->get_employee_spouse_byId($user['employeeId']);
                $count = count($empValue);
                if($count>0){
                    foreach ($empValue as $j => $employeeArray) {
                        $this->data['users'][$k]['employee'][$j] = $employeeArray;
                    }
                }
                else{
                    $this->data['users'][$k]['employee'] = '';
                }
            }
            $this->ixsolutions_template->admin_render('ixsolutions_admin/company/companyDetails', $this->data);
        }
    }
    /*
     * get country state city from zipcode
     */
    public function getCountryStateCity() {
        $count_city = '';
        $coun_State_city = array();
        $state = '';
        $shortstatename = '';
        $city = '';
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . $_GET['zipcode'] . '&sensor=false';
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
            exit;
        } else {
            $country= '';
            $obj = json_decode($response);
            foreach($obj->results[0]->address_components as $address)
            {
                if ($address->types[0] == 'locality')
                {
                    $city = (isset($address->long_name) ? $address->long_name : '');
                }
                if ($address->types[0] == 'administrative_area_level_2') // county
                {
                    $country = (isset($address->long_name) ? $address->long_name : '');
                }

                if ($address->types[0] == 'administrative_area_level_1')
                {
                    $state = (isset($address->long_name) ? $address->long_name : '');
                    $shortstatename = (isset($address->long_name) ? $address->short_name : '');
                }

            }
        $coun_State_city = array("country" => $country, "state" => $state, "city" => $city, 'shortstatename' => $shortstatename);
            echo json_encode($coun_State_city);
            exit;
        }
    }
    /*
     * insert company
     */
    public function insert_company() {
         error_reporting(0);
         $tables = $this->config->item('tables', 'ixsolutions_ion_auth');
          /* Validate form input */
        $this->form_validation->set_rules('cname', 'Company Name', 'required');
        $this->form_validation->set_rules('address', 'Street Address', 'required');
        $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|is_unique[' . $tables['users'] . '.email]', array('is_unique' => 'User already exists. If you are the user, please sign in
to your existing account or call your broker'));
        if ($this->input->post('add') == 'add') {
            if ($this->form_validation->run() == TRUE) {
                $company = '';
                $tables = $this->config->item('tables', 'IXsolution_auth');
                $products = $this->input->post('product');
                $group = $this->Company_model->get_group();
                $activationCode = md5(rand(1, 10000000000) . "fhsf" . rand(1, 100000));
                $email = $this->input->post('email');
                $cname = $this->input->post('cname');
                $user = array('username' => $cname,
                    'address' => $this->input->post('address'),
                    'zipcode' => $this->input->post('zipcode'),
                    'city' => $this->input->post('city'),
                    'state' => $this->input->post('state'),
                    'country' => $this->input->post('country'),
                    'company' => $cname,
                    'email' => $email,
                    'activation_code' => $activationCode
                );
                $password = '';
                $id = $this->ixsolution_ion_auth->register($cname, $group[0]->name, $password, $activationCode, $email, $user, '');
                $userAccountLink = site_url("Ixsolutions_auth/ga_user_activate/") . $id . "/" . $activationCode;
                $this->session->set_flashdata('message', $this->ixsolution_ion_auth->messages());
                 $company = array('userId' => $id,
                        'sicId' =>$this->input->post('sic'),
                        'no_of_emp' =>$this->input->post('no_of_emp'),
                        'first_name' =>$this->input->post('first_name'),
                        'last_name' =>$this->input->post('last_name'),
                        'phoneNumber'=>$this->input->post('phone_number'),
                        'assign_status' => 'unassign',
                        'quote_status' => 'Quote Requested'
                    );
                $cid = $this->Company_model->insert_company($company);
                $this->session->set_userdata('companyid',$cid);
                foreach ($products as $product) {
                    $comp_product = array('comp_id' => $cid,
                                          'prod_id' => $product,
                                          'renewal_date' => date('Y-m-d', strtotime($this->input->post('renewal_date' . $product))),
                                          'effective_date' => date('Y-m-d', strtotime($this->input->post('effect_date' . $product)))
                     );
                    $this->Company_model->insert_company_product($comp_product);
                }
                redirect('Ix_frontend_company/employee_view');
            }
            else 
            {
               $data['message'] = validation_errors();
               $data['cname'] = array(
                'name' => 'cname',
                'id' => 'cname',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('cname'),
               );
               $data['address'] = array(
                'name' => 'address',
                'id' => 'address',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('address'),
               );
               $data['email'] = array(
                'name' => 'email',
                'id' => 'email',
                'type' => 'email',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('email'),
               );
               $data['product'] = $this->Company_model->get_product();
               $data['sic_code'] = $this->Company_model->get_sic_code();
               $data['company_detail'] = '';
               $this->load->view('frontend/ix_frontend_company_view', $data);
               $this->load->view('frontend/container/close');
               $this->load->view('frontend/footer', $data);
               $this->load->view('frontend/foot_docs', $data);   
            }
         }
         else {
           $email = $this->input->post('email');
           $cname = $this->input->post('cname');
           $user = array('username' => $cname,
                'address' => $this->input->post('address'),
                'zipcode' => $this->input->post('zipcode'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'company' => $cname,
                'email' => $email
            );
            
            $this->Company_model->update_user($user, $this->input->post('userid'));
            
            $company = array('sicId' => $this->input->post('sic'),
                'no_of_emp' => $this->input->post('no_of_emp'),
                'first_name' =>$this->input->post('first_name'),
                'last_name' =>$this->input->post('last_name'),
                'phoneNumber'=>$this->input->post('phone_number')
            );
            $this->Company_model->update_company($company, $this->input->post('companyid'));
           redirect('Ix_frontend_company/edit_employee_view');
        }
    }
    /*
     * employees data view
     */
    public function employee_view() {
        error_reporting(0);
        $cid=$this->session->userdata('companyid');
        $data['company'] = $cid;
        $data['company_emp'] = $this->Company_model->get_comp_emp($cid);
        $data['occupation'] = $this->Company_model->get_occupation($cid);
        $data['salary'] = $this->Company_model->get_salary($cid);
        $data['onboard'] =$this->Company_model->get_onboard_status($cid);
        $data['estimate'] =$this->Company_model->get_estimate($cid);
        $this->load->view('frontend/ix_frontend_employee_view', $data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data);  
    }
    /*
     * employees data upload view
     */
    public function employee_upload_view() {
        error_reporting(0);
        $data['company'] = $this->session->userdata('companyid');
        $data['company_emp'] = $this->input->post('comp_emp');
        $this->load->view('frontend/ix_frontend_employee_upload_view', $data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data);  
    }
    /*
     * insert employees data or upload employees data
     */
    public function insert_employee() {
        error_reporting(0);
        $product_count = 0;
        $product_count = $this->Company_model->company_product_count($this->input->post('cid'));
        $file_name = $_FILES['file_name'];
        if ($file_name) {
            $folderpath = $this->config->item('folderPathIx');
            $comp_name = $this->Company_model->get_company($this->input->post('cid'));
            $comp_name_id = $comp_name . "_" . $this->input->post('cid');
            if (!file_exists($folderpath . "employee_csv")) {
                mkdir($folderpath . "employee_csv");
            }
            $userPath = $folderpath . "employee_csv/";
            $file = $this->Company_model->uploadFile($_FILES['file_name'], $userPath, $comp_name_id,$this->input->post('cid'));
            $fp = fopen($folderpath . "employee_csv/" . $file, "r");
            $flag = true;
            $id = 0;
            $last_name = '';
            $status = 0;
            $comp_emp = 0;
            while (($data = fgetcsv($fp, 1000, ",", "'")) !== false) {
                if ($flag) {
                    if ($data[0] == 'First Name' && $data[1] == 'Last Name' && $data[2] == 'Date of birth' && $data[3] == 'Salary' && $data[4] == 'occupation' && $data[5] == 'Address' && $data[6] == 'zipcode' && $data[7] == 'state' && $data[8] == 'Phone no' && $data[9] == 'Join Date' && $data[10] == 'emailid' && $data[11] == 'maritial status' && $data[12] == 'relation' && $data[13] == 'ssn' && $data[14] == 'sex' && $data[15] == 'status') {
                        $flag = false;
                        continue;
                    } else
                    {
                        
                        $this->session->set_flashdata('msg','Uploaded format not proper, Please download format from below button');
                        redirect('Ix_frontend_company/employee_upload_view');
                       
                    }
                        
                }
                if (substr($data[12], 0, 1) == 'e' || substr($data[12], 0, 1) == 'E') {
                    if ($data[15] == 'Active' || $data[15] == 'active') {
                        $status = 1;
                    }
                    $employee = array('companyId' => $this->input->post('cid'),
                        'firstName' => $data[0],
                        'lastName' => $data[1],
                        'dateofBirth' => date('Y-m-d', strtotime($data[2])),
                        'salary' => $data[3],
                        'occupation' => $data[4],
                        'address' => $data[5],
                        'zipcode' => $data[6],
                        'state' => $data[7],
                        'phoneNumber' => $data[8],
                        'join_date' => date('Y-m-d', strtotime($data[9])),
                        'employeeEmailId' => $data[10],
                        'maritalStatus' => $data[11],
                        'ssn' => $data[13],
                        'gender' => $data[14],
                        'status' => $status,
                        'createdAt' => date('Y-m-d')
                    );
                    $id = $this->Company_model->insert_employee($employee,$this->input->post('cid'));
                    $last_name = $data[1];
                    $comp_emp = $comp_emp + 1;
                } else if ((substr($data[12], 0, 1) == 's' || substr($data[12], 0, 1) == 'S') && $last_name == $data[1]) {
                    $spouse = array('employeeId' => $id,
                        'esFirstName' => $data[0],
                        'esLastName' => $data[1],
                        'dateOfBirth' => date('Y-m-d', strtotime($data[2])),
                        'emp_relation' => $data[12],
                        'gender' => $data[14]
                    );
                    $this->Company_model->insert_employee_relation_data($spouse,$this->input->post('cid'));
                } else if ((substr($data[12], 0, 1) == 'c' || substr($data[12], 0, 1) == 'C') && $last_name == $data[1]) {
                    $child = array('employeeId' => $id,
                        'esFirstName' => $data[0],
                        'esLastName' => $data[1],
                        'dateOfBirth' => date('Y-m-d', strtotime($data[2])),
                        'emp_relation' => $data[12],
                        'gender' => $data[14]
                    );
                    $this->Company_model->insert_employee_relation_data($child,$this->input->post('cid'));
                }
            }
            $company_employee = array('no_of_emp' => $comp_emp);
            $this->Company_model->update_company_employee($company_employee, $this->input->post('cid'));
            redirect('Ix_frontend_company/edit_employee_view');
        } else if ($this->input->post('add') == 'update') {
            $count = $this->input->post('count');
            $comp_emp = 0;
            for ($i = 1; $i <= $count; $i++) {
                if ($this->input->post('fname' . $i) != '') {
                    $empid = $this->input->post('empid' . $i);
                    $employee = array('companyId' => $this->input->post('cid'),
                        'firstName' => $this->input->post('fname' . $i),
                        'lastName' => $this->input->post('lname' . $i),
                        'address' => $this->input->post('address' . $i),
                        'zipcode' => $this->input->post('zipcode' . $i),
                        'state' => $this->input->post('state' . $i),
                        'phoneNumber' => $this->input->post('phone' . $i),
                        'employeeEmailId' => $this->input->post('email' . $i),
                        'dateofBirth' => date('Y-m-d', strtotime($this->input->post('dob' . $i))),
                        'salary' => $this->input->post('salary' . $i),
                        'occupation' => $this->input->post('occupation' . $i),
                        'createdAt' => date('Y-m-d'),
                        'join_date' => date('Y-m-d', strtotime($this->input->post('join_date' . $i))),
                        'ssn' => $this->input->post('ssn' . $i),
                        'gender' => $this->input->post('gender' . $i),
                        'status' => '1'
                    );
                    if ($empid != '')
                        $this->Company_model->update_employee($employee, $empid);
                    else
                        $empid = $this->Company_model->insert_employee($employee,$this->input->post('cid'));
                    if ($this->input->post('sfname' . $i) != '' && $this->input->post('slname' . $i) != '' && $this->input->post('sdob' . $i) != '') {
                        $sempid = $this->input->post('sempid' . $i);
                        if ($sempid != '') {
                            $spouse = array(
                                'esFirstName' => $this->input->post('sfname' . $i),
                                'esLastName' => $this->input->post('slname' . $i),
                                'gender' => $this->input->post('sgender' . $i),
                                'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('sdob' . $i))),
                                'emp_relation' => 'spouse',
                                'ssn' => $this->input->post('sssn' . $i)
                            );
                            $this->Company_model->update_employee_relation_data($spouse, $sempid);
                        } else {
                            $spouse = array('employeeId' => $empid,
                                'esFirstName' => $this->input->post('sfname' . $i),
                                'esLastName' => $this->input->post('slname' . $i),
                                'gender' => $this->input->post('sgender' . $i),
                                'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('sdob' . $i))),
                                'emp_relation' => 'spouse',
                                'ssn' => $this->input->post('sssn' . $i)
                            );
                            $this->Company_model->insert_employee_relation_data($spouse,$this->input->post('cid'));
                        }
                    }
                    $child_count = $this->input->post('child' . $i);
                    if ($child_count > 0) {
                        for ($j = 1; $j <= $child_count; $j++) {
                            $cempid = $this->input->post('cempid' . $i . "_" . $j);
                            if ($cempid != '') {
                                $child = array(
                                    'esFirstName' => $this->input->post('cfname' . $i . "_" . $j),
                                    'esLastName' => $this->input->post('clname' . $i . "_" . $j),
                                    'gender' => $this->input->post('cgender' . $i . "_" . $j),
                                    'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('cdob' . $i . "_" . $j))),
                                    'emp_relation' => 'child',
                                    'ssn' => $this->input->post('cssn' . $i . "_" . $j)
                                );
                                $this->Company_model->update_employee_relation_data($child, $cempid);
                            } else {
                                $child = array('employeeId' => $empid,
                                    'esFirstName' => $this->input->post('cfname' . $i . "_" . $j),
                                    'esLastName' => $this->input->post('clname' . $i . "_" . $j),
                                    'gender' => $this->input->post('cgender' . $i . "_" . $j),
                                    'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('cdob' . $i . "_" . $j))),
                                    'emp_relation' => 'child',
                                    'ssn' => $this->input->post('cssn' . $i . "_" . $j)
                                );
                                $this->Company_model->insert_employee_relation_data($child,$this->input->post('cid'));
                            }
                        }
                    }
                    $comp_emp++;
                }
            }
            //redirect('Ix_frontend_company/company_list_view/'.$this->input->post('cid'));
        } else {
            $count = $this->input->post('count');
            $comp_emp = $this->input->post('comp_emp');
            for ($i = 1; $i <= $count; $i++) {
                if ($this->input->post('fname' . $i) != '') {
                    $employee = array('companyId' => $this->input->post('cid'),
                        'firstName' => $this->input->post('fname' . $i),
                        'lastName' => $this->input->post('lname' . $i),
                        'address' => $this->input->post('address' . $i),
                        'zipcode' => $this->input->post('zipcode' . $i),
                        'state' => $this->input->post('state' . $i),
                        'phoneNumber' => $this->input->post('phone' . $i),
                        'employeeEmailId' => $this->input->post('email' . $i),
                        'dateofBirth' => date('Y-m-d', strtotime($this->input->post('dob' . $i))),
                        'salary' => $this->input->post('salary' . $i),
                        'occupation' => $this->input->post('occupation' . $i),
                        'createdAt' => date('Y-m-d'),
                        'join_date' => date('Y-m-d', strtotime($this->input->post('join_date' . $i))),
                        'ssn' => $this->input->post('ssn' . $i),
                        'gender' => $this->input->post('gender' . $i),
                        'status' => '1'
                    );
                    $id = $this->Company_model->insert_employee($employee,$this->input->post('cid'));
                    if ($this->input->post('sfname' . $i) != '' && $this->input->post('slname' . $i) != '' && $this->input->post('sdob' . $i) != '') {
                        $spouse = array('employeeId' => $id,
                            'esFirstName' => $this->input->post('sfname' . $i),
                            'esLastName' => $this->input->post('slname' . $i),
                            'gender' => $this->input->post('sgender' . $i),
                            'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('sdob' . $i))),
                            'emp_relation' => 'spouse',
                            'ssn' => $this->input->post('sssn' . $i)
                        );
                        $this->Company_model->insert_employee_relation_data($spouse,$this->input->post('cid'));
                    }
                    $child_count = $this->input->post('child' . $i);
                    if ($child_count > 0) {
                        for ($j = 1; $j <= $child_count; $j++) {
                            $child = array('employeeId' => $id,
                                'esFirstName' => $this->input->post('cfname' . $i . "_" . $j),
                                'esLastName' => $this->input->post('clname' . $i . "_" . $j),
                                'gender' => $this->input->post('cgender' . $i . "_" . $j),
                                'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('cdob' . $i . "_" . $j))),
                                'emp_relation' => 'child',
                                'ssn' => $this->input->post('cssn' . $i . "_" . $j)
                            );
                            $this->Company_model->insert_employee_relation_data($child, $this->input->post('cid'));
                        }
                    }
                }
            }
            
            }
            $company_employee = array('no_of_emp' => $comp_emp,
                                      'employee_estimate'=>$this->input->post('emp_estimate'),
                                      'dependant_estimate'=>$this->input->post('depen_estimate'));
            $this->Company_model->update_company_employee($company_employee, $this->input->post('cid'));
            $cid = $this->session->userdata('companyid');
            if($this->session->userdata('userid'))
            {
                redirect('Ix_frontend_company/question_answer_upload_view'); 
            }
            else
            {
                redirect('Ix_frontend_company/question_answer_view'); 
            } 
        }   

    /*
     * questionnaries view
     */

    public function question_answer_view() {
         error_reporting(0);
        $cid=$this->session->userdata('companyid');
        $product = $this->Company_model->get_comp_product($cid);
        $pid[] = '';
        foreach ($product as $pro) {
            $data['question'][$pro->name] = $this->Company_model->get_question($pro->productId);
        }
        $data['document'] = $this->Company_model->get_document();
        $data['answer'] = $this->Company_model->get_answer();
        $data['cid'] = $cid;
        $data['no_of_emp']=$this->Company_model->get_comp_emp($cid);
        $this->load->view('frontend/ix_frontend_question_answer_view', $data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data); 
        
    }

    /*
     * questionnaries upload view
     */

    public function question_answer_upload_view() {
          error_reporting(0);
          $cid = $this->session->userdata('companyid');
          $data['docValue'] = $this->Company_model->get_company_documents_byId($cid);
          $data['document'] = $this->Company_model->get_document();
          $data['cid'] = $cid;
          $this->load->view('frontend/ix_frontend_question_answer_upload_view', $data);
          $this->load->view('frontend/container/close');
          $this->load->view('frontend/footer', $data);
          $this->load->view('frontend/foot_docs', $data);  
    }

    /*
     * insert question or upload questions
     */

    public function insert_question_answer() {
        error_reporting(0);
        $folderpath = $this->config->item('folderPathIx');
        $cid = $this->input->post('cid');
        $count=$this->input->post('count');
         $comp_name = $this->Company_model->get_company($cid);
         $comp_name_id = $comp_name . "_" . $cid;
            if (!file_exists($folderpath . "document")) {
                mkdir($folderpath . "document");
            }
            $userPath = $folderpath . "document/";
            for($cnt=1;$cnt<=$count;$cnt++)
            {
                for($cnt1=1;$cnt1<=$this->input->post('file_count'.$cnt);$cnt1++)
                {
                    if (!empty($_FILES['file_name'.$cnt."_".$cnt1]['name']))
                    {
             $file_name = $_FILES['file_name'.$cnt."_".$cnt1];
                        $name=explode('.', $file_name['name']);
                        $comp_name_id=urlencode($name[0])."_".$comp_name."_".$cid;
                        $file = $this->Company_model->uploadFile($_FILES['file_name'.$cnt."_".$cnt1], $userPath,$comp_name_id,$cid);
                        $question_document = array('comp_id' => $cid,
                'doc_id' => $this->input->post('file_type'.$cnt),
                'document_name' => $file
            );
            
            $this->Company_model->insert_question_document($question_document);
                    }
                }
            }
            $cnt = $this->input->post('cnt');
            for ($i = 1; $i < $cnt; $i++) {
                $ans_count = count($this->input->post('aid' . $i . '[]'));
                for ($j = 0; $j < $ans_count; $j++) {
                    $question_answer = array("questionId" => $this->input->post('qid' . $i),
                        "ques_answer" => $this->input->post('ques_answer' . $i),
                        "answerId" => $this->input->post('aid' . $i . '[' . $j . ']'),
                        "companyId" => $cid
                    );
                    $this->Company_model->insert_question_answer($question_answer);
                }
            }
            redirect('Ix_frontend_company/company_list_view'); 
    }

    /*
     * display all company which inserted by user
     */

    function company_list_view() {
        error_reporting(0);
        $cid=$this->session->userdata('companyid');
        if($this->session->userdata('userid'))
        {
        }
        else
        {
            $email=$this->Company_model->get_brokerage_email();
            foreach($email as $newemail)
            {
                $this->email->from(CB_EMAIL_FROM, ' Flexible Benefit');
                $this->email->to($newemail->email);
                $this->email->subject('New Quote Request');
                $this->email->message($this->load->view($this->config->item('email_templates', 'ixsolutions_ion_auth') . $this->config->item('email_new_company', 'ixsolutions_ion_auth'), $data, true));
                $this->email->send();
            }
            $this->email->from(CB_EMAIL_FROM, ' Flexible Benefit');
            $this->email->to($company_detail[0]->email);
            $this->email->subject('Quote Request');
            $this->email->message('Your Quote Request successfully completed');
            $this->email->send();
        }
        $this->load->view('frontend/ix_frontend_company_list_view',$data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data);  
    }
    /*
     * csv for export all employee's data of perticular company
     */
    public function export_company_detail() {
       error_reporting(0);
        $cid=$this->session->userdata('companyid');
        $comp_name = $this->Company_model->get_company($cid);
        $comp_name_id = $comp_name . "_" . $cid;
        $this->load->helper('download');
        $detail = $this->Company_model->get_company_employee($cid);
        if (!file_exists("download")) {
            mkdir("download");
        }
        $fp = fopen("download/" . $comp_name_id . ".csv", "w");
        $head = array("Employee Id", "First Name", "Last Name", "Relation", "Birth Date", "Sex", "Zipcode", "State", "Phone No", "Status");
        fputcsv($fp, $head);
        $write_info = array();
        $no=1;
        foreach ($detail as $detail) {
            $write_info['employeeid'] = $no;
            $write_info['first_name'] = $detail->firstName;
            $write_info['last_name'] = $detail->lastName;
            $write_info['relation'] = "Employee";
            $write_info['birth_date'] = date('d/m/Y', strtotime($detail->dateOfBirth));
            $write_info['gender'] = $detail->gender;
            $write_info['zipcode'] = $detail->zipcode;
            $write_info['state'] = $detail->state;
            $write_info['phone_no'] = $detail->phoneNumber;
            if ($detail->status == 1) {
                $write_info['status'] = "Active";
            } else {
                $write_info['status'] = "Inactive";
            }
            fputcsv($fp, $write_info);
            $relation_detail = $this->Company_model->get_company_employee_relation($detail->employeeId);
            if (!empty($relation_detail)) {
                foreach ($relation_detail as $rel_detail) {
                    $write_info['employeeid'] = $no;
                    $write_info['first_name'] = $rel_detail->esFirstName;
                    $write_info['last_name'] = $rel_detail->esLastName;
                    $write_info['relation'] = $rel_detail->emp_relation;
                    $write_info['birth_date'] = date('d/m/Y', strtotime($rel_detail->dateOfBirth));
                    $write_info['gender'] = $rel_detail->gender;
                    $write_info['zipcode'] = "";
                    $write_info['state'] = "";
                    $write_info['phone_no'] = "";
                    $write_info['status'] = "";
                    fputcsv($fp, $write_info);
                }
            }
            $no++;
        }
        fclose($fp);
        $this->session->unset_userdata('companyid');
        $file_name = "download/" . $comp_name_id . ".csv";
        $query = 'FILE IS BEING EXPORTED';
        $queryName = "EXPORTED FILE";
        $fileName = $file_name;
        $this->ixsolution_ion_auth->logFile($queryName, $query, $fileName,$cid);
        force_download($file_name, NULL);
    }

    public function edit_employee_view() {
        error_reporting(0);
        $cid = $this->session->userdata('companyid');
        $data['company'] = $cid;
        $data['employee_detail'] = $this->Company_model->get_company_employee($cid);
        $data['company_emp'] = $this->Company_model->get_comp_emp($cid);
        $data['employee_relation'] = $this->Company_model->get_company_all_employee($cid);
        $data['occupation'] = $this->Company_model->get_occupation($cid);
        $data['salary'] = $this->Company_model->get_salary($cid);
        $data['onboard'] =$this->Company_model->get_onboard_status($cid);
        $data['estimate'] =$this->Company_model->get_estimate($cid);
        $this->load->view('frontend/ix_frontend_employee_view',$data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data);
    }

    public function delete_employee($empid) {
        $this->Company_model->delete_employee($empid);
        return;
    }

    public function delete_employee_relation_data($id) {
        $this->Company_model->delete_employee_relation_data($id);
        return;
    }

    public function delete_employee_child($empid, $relation) {
        $this->Company_model->delete_employee_child($empid, $relation);
        return;
    }

    public function employee_upload_format() {
        $this->load->helper('download');
        $folderpath = $this->config->item('folderPathIx');
        $userPath = $folderpath . "employee_csv/";
        $file_name = $userPath . "employee_upload_example.csv";
        force_download($file_name, NULL);
        $query = null;
        $queryName = "UPLOADED FILE";
        $fileName = $file_name;
        $this->ixsolution_ion_auth->logFile($queryName, $query, $fileName);
    }

    public function update_company_view() {
            error_reporting(0);
       
            if($this->session->userdata('userid'))
            {
                $comp_id=$this->ixsolutions_ion_auth_model->get_company_byuserId($this->session->userdata('userid'));
                $this->session->set_userdata('companyid',$comp_id);
            }
            $cid = $this->session->userdata('companyid');
            $data['company_detail'] = $this->Company_model->get_company_all_detail($cid);
            $data['product'] = '';
            $data['sic_code'] = $this->Company_model->get_sic_code();
            $data['company'] = $cid;
            $this->load->view('frontend/ix_frontend_company_view',$data);
            $this->load->view('frontend/container/close');
            $this->load->view('frontend/footer', $data);
            $this->load->view('frontend/foot_docs', $data);
            
    }

    function aetna_export($id) {
        $comp_name = $this->Company_model->get_company($id);
        $comp_name_id = $comp_name . "_" . $id;
        $detail = $this->Company_model->get_company_employee($id);
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(38.25);
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(10.14);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(11);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20.57);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(8.71);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(13.86);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(11.43);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(14.57);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(11.57);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(14.86);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(11.57);
        $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(10.86);
        $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(8.43);
        $this->excel->getActiveSheet()->getColumnDimension('p')->setWidth(3.86);
        $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(21.43);
        $this->excel->getActiveSheet()->setCellValue('A1', 'Employee ID');
        $this->excel->getActiveSheet()->setCellValue('B1', 'Member Class');
        $this->excel->getActiveSheet()->setCellValue('C1', 'Last Name');
        $this->excel->getActiveSheet()->setCellValue('D1', 'First Name');
        $this->excel->getActiveSheet()->setCellValue('E1', 'Home Zipcode');
        $this->excel->getActiveSheet()->setCellValue('F1', 'Age/ Date of Birth');
        $this->excel->getActiveSheet()->setCellValue('G1', 'Gender');
        $this->excel->getActiveSheet()->setCellValue('H1', 'Employment Status');
        $this->excel->getActiveSheet()->setCellValue('I1', 'Tobacco Status');
        $this->excel->getActiveSheet()->setCellValue('J1', 'Medical Tier');
        $this->excel->getActiveSheet()->setCellValue('K1', 'Dental Tier');
        $this->excel->getActiveSheet()->setCellValue('L1', 'Life - AD&D Tier');
        $this->excel->getActiveSheet()->setCellValue('M1', 'STD Tier');
        $this->excel->getActiveSheet()->setCellValue('N1', 'Work Zipcode');
        $this->excel->getActiveSheet()->getStyle('A1:Q100')->getFont()->setName('Arial');
        $this->excel->getActiveSheet()->getStyle('A1:Q100')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('A1:N1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1:N1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1:N1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ffff00');
        $this->excel->getActiveSheet()->getStyle('A1:N1')->getAlignment()->setWrapText(true);
        //$this->excel->getDefaultStyle()->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
        $cnt = 2;
        $no = 1;
        foreach ($detail as $detail) {
            $this->excel->getActiveSheet()->setCellValue('A' . $cnt, $no);
            $this->excel->getActiveSheet()->setCellValue('B' . $cnt, 'Subscriber');
            $this->excel->getActiveSheet()->setCellValue('C' . $cnt, $detail->lastName);
            $this->excel->getActiveSheet()->setCellValue('D' . $cnt, $detail->firstName);
            $this->excel->getActiveSheet()->setCellValue('E' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('F' . $cnt, date('m/d/Y', strtotime($detail->dateOfBirth)));
            $this->excel->getActiveSheet()->setCellValue('G' . $cnt, $detail->gender);
            $this->excel->getActiveSheet()->setCellValue('H' . $cnt, '0');
            $this->excel->getActiveSheet()->setCellValue('I' . $cnt, 'N');
            $this->excel->getActiveSheet()->setCellValue('J' . $cnt, 'Enroll');
            $this->excel->getActiveSheet()->setCellValue('K' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('L' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('M' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('N' . $cnt, '');
            $cnt++;
            $relation_detail = $this->Company_model->get_company_employee_relation($detail->employeeId);
            if (!empty($relation_detail)) {
                foreach ($relation_detail as $rel_detail) {
                    $this->excel->getActiveSheet()->setCellValue('A' . $cnt, $no);
                    $this->excel->getActiveSheet()->setCellValue('B' . $cnt, $rel_detail->emp_relation);
                    $this->excel->getActiveSheet()->setCellValue('C' . $cnt, $rel_detail->esLastName);
                    $this->excel->getActiveSheet()->setCellValue('D' . $cnt, $rel_detail->esFirstName);
                    $this->excel->getActiveSheet()->setCellValue('E' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('F' . $cnt, date('m/d/Y', strtotime($rel_detail->dateOfBirth)));
                    $this->excel->getActiveSheet()->setCellValue('G' . $cnt, $rel_detail->gender);
                    $this->excel->getActiveSheet()->setCellValue('H' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('I' . $cnt, 'N');
                    $this->excel->getActiveSheet()->setCellValue('J' . $cnt, 'Enroll');
                    $this->excel->getActiveSheet()->setCellValue('K' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('L' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('M' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('N' . $cnt, '');
                    $this->excel->getActiveSheet()->getStyle('E' . $cnt)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('aaaaaa');
                    $this->excel->getActiveSheet()->getStyle('H' . $cnt)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('aaaaaa');
                    $this->excel->getActiveSheet()->getStyle('K' . $cnt . ':N' . $cnt)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('aaaaaa');
                    $cnt++;
                }
            }
            $no++;
        }
        $this->excel->getActiveSheet()->getStyle('A1:N' . ($cnt - 1))->applyFromArray($styleArray);

        $BStyle = array('borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));
        $this->excel->getActiveSheet()->getStyle('P1')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('P1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('P1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('P1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ffff00');
        $this->excel->getActiveSheet()->getStyle('P1')->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('P1:Q5')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('P1:Q5')->applyFromArray($BStyle);
        $this->excel->setActiveSheetIndex(0)->mergeCells("P1:Q1");
        $this->excel->getActiveSheet()->setCellValue('P1', 'Member Class');
        $this->excel->getActiveSheet()->setCellValue('P2', 'ID');
        $this->excel->getActiveSheet()->setCellValue('Q2', 'Name');
        $this->excel->getActiveSheet()->setCellValue('P3', '0');
        $this->excel->getActiveSheet()->setCellValue('Q3', 'Subscriber');
        $this->excel->getActiveSheet()->setCellValue('P4', '1');
        $this->excel->getActiveSheet()->setCellValue('Q4', 'Spouse');
        $this->excel->getActiveSheet()->setCellValue('P5', '2');
        $this->excel->getActiveSheet()->setCellValue('Q5', 'Child');

        $this->excel->getActiveSheet()->getStyle('P7')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('P7')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('P7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('P7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ffff00');
        $this->excel->getActiveSheet()->getStyle('P7')->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('P7:Q10')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('P7:Q10')->applyFromArray($BStyle);
        $this->excel->setActiveSheetIndex(0)->mergeCells("P7:Q7");
        $this->excel->getActiveSheet()->setCellValue('P7', 'Medical Tier Key');
        $this->excel->getActiveSheet()->setCellValue('P8', 'ID');
        $this->excel->getActiveSheet()->setCellValue('Q8', 'Name');
        $this->excel->getActiveSheet()->setCellValue('P9', '0');
        $this->excel->getActiveSheet()->setCellValue('Q9', 'Waive');
        $this->excel->getActiveSheet()->setCellValue('P10', '1');
        $this->excel->getActiveSheet()->setCellValue('Q10', 'Enroll');

        $this->excel->getActiveSheet()->getStyle('P12')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('P12')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('P12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('P12')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ffff00');
        $this->excel->getActiveSheet()->getStyle('P12')->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('P12:Q19')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('P12:Q19')->applyFromArray($BStyle);
        $this->excel->setActiveSheetIndex(0)->mergeCells("P12:Q12");
        $this->excel->getActiveSheet()->setCellValue('P12', 'Dental Tier Key');
        $this->excel->getActiveSheet()->setCellValue('P13', 'ID');
        $this->excel->getActiveSheet()->setCellValue('Q13', 'Name');
        $this->excel->getActiveSheet()->setCellValue('P14', '1');
        $this->excel->getActiveSheet()->setCellValue('Q14', 'EE only');
        $this->excel->getActiveSheet()->setCellValue('P15', '2');
        $this->excel->getActiveSheet()->setCellValue('Q15', 'EE & Spouse only');
        $this->excel->getActiveSheet()->setCellValue('P16', '3');
        $this->excel->getActiveSheet()->setCellValue('Q16', 'EE & Child (ren)');
        $this->excel->getActiveSheet()->setCellValue('P17', '4');
        $this->excel->getActiveSheet()->setCellValue('Q17', 'Family');
        $this->excel->getActiveSheet()->setCellValue('P18', '5');
        $this->excel->getActiveSheet()->setCellValue('Q18', 'Spousal Waiver');
        $this->excel->getActiveSheet()->setCellValue('P19', '6');
        $this->excel->getActiveSheet()->setCellValue('Q19', 'Decline Coverage');

        $this->excel->getActiveSheet()->getStyle('P21')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('P21')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('P21')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('P21')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ffff00');
        $this->excel->getActiveSheet()->getStyle('P21')->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('P21:Q25')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('P21:Q25')->applyFromArray($BStyle);
        $this->excel->setActiveSheetIndex(0)->mergeCells("P21:Q21");
        $this->excel->getActiveSheet()->setCellValue('P21', 'Life Tier Key');
        $this->excel->getActiveSheet()->setCellValue('P22', 'ID');
        $this->excel->getActiveSheet()->setCellValue('Q22', 'Name');
        $this->excel->getActiveSheet()->setCellValue('P23', '0');
        $this->excel->getActiveSheet()->setCellValue('Q23', 'Wavie');
        $this->excel->getActiveSheet()->setCellValue('P24', '1');
        $this->excel->getActiveSheet()->setCellValue('Q24', 'EE only');
        $this->excel->getActiveSheet()->setCellValue('P25', '7');
        $this->excel->getActiveSheet()->setCellValue('Q25', 'EE & Dep');

        $this->excel->getActiveSheet()->getStyle('P27')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('P27')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('P27')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('P27')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ffff00');
        $this->excel->getActiveSheet()->getStyle('P27')->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('P27:Q30')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('P27:Q30')->applyFromArray($BStyle);
        $this->excel->setActiveSheetIndex(0)->mergeCells("P27:Q27");
        $this->excel->getActiveSheet()->setCellValue('P27', 'STD Tier Key');
        $this->excel->getActiveSheet()->setCellValue('P28', 'ID');
        $this->excel->getActiveSheet()->setCellValue('Q28', 'Name');
        $this->excel->getActiveSheet()->setCellValue('P29', '0');
        $this->excel->getActiveSheet()->setCellValue('Q29', 'Wavie');
        $this->excel->getActiveSheet()->setCellValue('P30', '1');
        $this->excel->getActiveSheet()->setCellValue('Q30', 'EE only');

        $this->excel->getActiveSheet()->getStyle('P32')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('P32')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('P32')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('P32')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ffff00');
        $this->excel->getActiveSheet()->getStyle('P32')->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('P32:Q37')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('P32:Q37')->applyFromArray($BStyle);
        $this->excel->setActiveSheetIndex(0)->mergeCells("P32:Q32");
        $this->excel->getActiveSheet()->setCellValue('P32', 'Employement Status Tier Key');
        $this->excel->getActiveSheet()->setCellValue('P33', 'ID');
        $this->excel->getActiveSheet()->setCellValue('Q33', 'Name');
        $this->excel->getActiveSheet()->setCellValue('P34', '0');
        $this->excel->getActiveSheet()->setCellValue('Q34', 'Active');
        $this->excel->getActiveSheet()->setCellValue('P35', '1');
        $this->excel->getActiveSheet()->setCellValue('Q35', 'COBRA');
        $this->excel->getActiveSheet()->setCellValue('P36', '2');
        $this->excel->getActiveSheet()->setCellValue('Q36', 'CalCOBRA');
        $this->excel->getActiveSheet()->setCellValue('P37', '4');
        $this->excel->getActiveSheet()->setCellValue('Q37', 'Mini-COBRA');

        $this->excel->getActiveSheet()->getStyle('P39')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('P39')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('P39')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('P39')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ffff00');
        $this->excel->getActiveSheet()->getStyle('P39')->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('P39:Q43')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('P39:Q43')->applyFromArray($BStyle);
        $this->excel->setActiveSheetIndex(0)->mergeCells("P39:Q39");
        $this->excel->getActiveSheet()->setCellValue('P39', 'Tobacco Status Tier Key');
        $this->excel->getActiveSheet()->setCellValue('P40', 'ID');
        $this->excel->getActiveSheet()->setCellValue('Q40', 'Name');
        $this->excel->getActiveSheet()->setCellValue('P41', '1');
        $this->excel->getActiveSheet()->setCellValue('Q41', 'UNK');
        $this->excel->getActiveSheet()->setCellValue('P42', '2');
        $this->excel->getActiveSheet()->setCellValue('Q42', 'Y');
        $this->excel->getActiveSheet()->setCellValue('P43', '3');
        $this->excel->getActiveSheet()->setCellValue('Q43', 'N');
        /* for($col = 'A'; $col !== 'AZ'; $col++) {
          echo (int)$calculatedWidth = $this->excel->getActiveSheet()->getColumnDimension($col)->getWidth();
          exit;
          $this->excel->getActiveSheet()->getColumnDimension($col)->setWidth((int) $calculatedWidth * 1);
          } */
        /* for($col = 'A'; $col !== 'o'; $col++) {
          $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
          } */
        $filename = $comp_name_id . '.xls';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
        $query = null;
        $queryName = "AETNA EXPORT FILE";
        $fileName = $filename;
        $this->ixsolution_ion_auth->logFile($queryName, $query, $fileName);
    }

    function bcbsil_export($id) {
        $comp_name = $this->Company_model->get_company($id);
        $comp_name_id = $comp_name . "_" . $id;
        $detail = $this->Company_model->get_company_employee($id);
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(9.43);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(9.86);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(16.71);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(6.86);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(13.43);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(4.86);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(11.29);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(6.86);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(12.43);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(17.57);

        $this->excel->getActiveSheet()->setCellValue('A1', 'Last Name');
        $this->excel->getActiveSheet()->setCellValue('B1', 'First Name');
        $this->excel->getActiveSheet()->setCellValue('C1', 'Relationship Code');
        $this->excel->getActiveSheet()->setCellValue('D1', 'Gender');
        $this->excel->getActiveSheet()->setCellValue('E1', 'DOB');
        $this->excel->getActiveSheet()->setCellValue('F1', 'Coverage Type');
        $this->excel->getActiveSheet()->setCellValue('G1', 'State');
        $this->excel->getActiveSheet()->setCellValue('H1', 'Current Plan');
        $this->excel->getActiveSheet()->setCellValue('I1', 'Retiree');
        $this->excel->getActiveSheet()->setCellValue('J1', 'Anual Salary');
        $this->excel->getActiveSheet()->setCellValue('K1', 'Life Class');
        $this->excel->getActiveSheet()->getStyle('A1:Q100')->getFont()->setName('Arial');
        $this->excel->getActiveSheet()->getStyle('A1:Q100')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('A1:K1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $cnt = 2;
        foreach ($detail as $detail) {
            $this->excel->getActiveSheet()->setCellValue('A' . $cnt, $detail->lastName);
            $this->excel->getActiveSheet()->setCellValue('B' . $cnt, $detail->firstName);
            $this->excel->getActiveSheet()->setCellValue('C' . $cnt, 'Employee');
            $this->excel->getActiveSheet()->setCellValue('D' . $cnt, $detail->gender);
            $this->excel->getActiveSheet()->setCellValue('E' . $cnt, date('m/d/Y', strtotime($detail->dateOfBirth)));
            $this->excel->getActiveSheet()->setCellValue('F' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('G' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('H' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('I' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('J' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('K' . $cnt, '');

            $cnt++;
            $relation_detail = $this->Company_model->get_company_employee_relation($detail->employeeId);
            if (!empty($relation_detail)) {
                foreach ($relation_detail as $rel_detail) {
                    $this->excel->getActiveSheet()->setCellValue('A' . $cnt, $rel_detail->esLastName);
                    $this->excel->getActiveSheet()->setCellValue('B' . $cnt, $rel_detail->esFirstName);
                    if ($rel_detail->emp_relation == 'spouse' || $rel_detail->emp_relation == 'spouse')
                        $this->excel->getActiveSheet()->setCellValue('C' . $cnt, 'Spouse');
                    else
                        $this->excel->getActiveSheet()->setCellValue('C' . $cnt, 'Dependent');
                    $this->excel->getActiveSheet()->setCellValue('D' . $cnt, $rel_detail->gender);
                    $this->excel->getActiveSheet()->setCellValue('E' . $cnt, date('m/d/Y', strtotime($rel_detail->dateOfBirth)));
                    $this->excel->getActiveSheet()->setCellValue('F' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('G' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('H' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('I' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('J' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('K' . $cnt, '');
                    $cnt++;
                }
            }
        }
        /* for($col = 'A'; $col !== 'o'; $col++) {
          $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
          } */
        $filename = $comp_name_id . '.xls';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
    }

    function easecentral_export($id) {
        $comp_name = $this->Company_model->get_company($id);
        $comp_name_id = $comp_name . "_" . $id;
        $this->load->helper('download');
        $detail = $this->Company_model->get_company_employee($id);
        if (!file_exists("download")) {
            mkdir("download");
        }
        $fp = fopen("download/" . $comp_name_id . ".csv", "w");
        $head = array("Employee Id", "First Name", "Last Name", "Relation", "SSN", "Birth Date", "Sex", "Address", "Email", "Hire Date", "Status");
        fputcsv($fp, $head);
        $write_info = array();
        $no = 1;
        foreach ($detail as $detail) {
            $write_info['employeeid'] = $no;
            $write_info['first_name'] = $detail->firstName;
            $write_info['last_name'] = $detail->lastName;
            $write_info['relation'] = "Employee";
            $write_info['ssn'] = $detail->ssn;
            $write_info['birth_date'] = date('m/d/Y', strtotime($detail->dateOfBirth));
            $write_info['gender'] = $detail->gender;
            $write_info['address'] = $detail->address;
            $write_info['email'] = $detail->employeeEmailId;
            $write_info['join_date'] = date('m/d/Y', strtotime($detail->join_date));
            if ($detail->status == 1) {
                $write_info['status'] = "Active";
            } else {
                $write_info['status'] = "Inactive";
            }
            fputcsv($fp, $write_info);
            $relation_detail = $this->Company_model->get_company_employee_relation($detail->employeeId);
            if (!empty($relation_detail)) {
                foreach ($relation_detail as $rel_detail) {
                    $write_info['employeeid'] = $no;
                    $write_info['first_name'] = $rel_detail->esFirstName;
                    $write_info['last_name'] = $rel_detail->esLastName;
                    $write_info['relation'] = $rel_detail->emp_relation;
                    $write_info['ssn'] = $rel_detail->ssn;
                    $write_info['birth_date'] = date('m/d/Y', strtotime($rel_detail->dateOfBirth));
                    $write_info['gender'] = $rel_detail->gender;
                    $write_info['address'] = "";
                    $write_info['email'] = "";
                    $write_info['join_date'] = "";
                    $write_info['status'] = "";
                    fputcsv($fp, $write_info);
                }
            }
            $no++;
        }
        fclose($fp);
        $file_name = "download/" . $comp_name_id . ".csv";
        $query = null;
        $queryName = "EXPORTED FILE";
        $fileName = $file_name;
        $this->ixsolution_ion_auth->logFile($queryName, $query, $fileName);
        force_download($file_name, NULL);
    }

    function uhc_export($id) {
        error_reporting(0);
        $comp_name = $this->Company_model->get_company($id);
        $comp_name_id = $comp_name . "_" . $id;
        $detail = $this->Company_model->get_company_employee_all_detail($id);
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setCellValue('C2', '* Required Field');
        $this->excel->getActiveSheet()->setCellValue('B3', 'COMPANY            LOCATION ZIP CODE Location information will be ignored for Single-site groups.');
        $this->excel->getActiveSheet()->setCellValue('C3', 'EMPLOYEE DETAILS');
        $this->excel->getActiveSheet()->setCellValue('M3', '"SPOUSE
Enter Age OR DOB only if Spouse is being enrolled, if not, leave it blank."');
        $this->excel->getActiveSheet()->setCellValue('P3', 'CHILD1');
        $this->excel->getActiveSheet()->setCellValue('S3', 'CHILD2');
        $this->excel->getActiveSheet()->setCellValue('V3', 'CHILD3');
        $this->excel->getActiveSheet()->setCellValue('Y3', 'CHILD4');
        $this->excel->getActiveSheet()->setCellValue('AB3', 'CHILD5');
        $this->excel->getActiveSheet()->setCellValue('AE3', 'CHILD6');
        $this->excel->getActiveSheet()->setCellValue('AH3', 'CHILD7');
        $this->excel->getActiveSheet()->setCellValue('AK3', 'CHILD8');
        $this->excel->getActiveSheet()->setCellValue('AN3', 'CHILD9');
        $this->excel->getActiveSheet()->setCellValue('AQ3', 'CHILD10');
        $this->excel->getActiveSheet()->setCellValue('A4', '');
        $this->excel->getActiveSheet()->setCellValue('B4', 'ZIP CODE*                           (5-Digits)');
        $this->excel->getActiveSheet()->setCellValue('C4', 'CLASS TYPE');
        $this->excel->getActiveSheet()->setCellValue('D4', 'FIRST NAME*');
        $this->excel->getActiveSheet()->setCellValue('E4', 'LAST NAME*');
        $this->excel->getActiveSheet()->setCellValue('F4', 'SEX*');
        $this->excel->getActiveSheet()->setCellValue('G4', '"AGE* Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('H4', '"DOB* (MM/DD/YYYY) Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('I4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('J4', 'EMPLOYMENT STATUS*');
        $this->excel->getActiveSheet()->setCellValue('K4', 'ANNUAL SALARY');
        $this->excel->getActiveSheet()->setCellValue('L4', 'OUT OF          AREA               OOA information will be ignored for Multi-site groups.');
        $this->excel->getActiveSheet()->setCellValue('M4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('N4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('O4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('P4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('Q4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('R4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('S4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('T4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('U4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('V4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('W4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('X4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('Y4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('Z4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AA4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('AB4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AC4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AD4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('AE4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AF4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AG4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('AH4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AI4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AJ4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('AK4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AL4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AM4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('AN4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AO4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AP4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('AQ4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AR4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AS4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->getStyle('A1:Q100')->getFont()->setName('Arial');
        $this->excel->getActiveSheet()->getStyle('A1:Q100')->getFont()->setSize(10);
        $cnt = 5;
        $no = 1;
        foreach ($detail as $detail) {
            $dob = explode(',', $detail->birth);
            $this->excel->getActiveSheet()->setCellValue('A' . $cnt, $no);
            $this->excel->getActiveSheet()->setCellValue('B' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('C' . $cnt, '1');
            $this->excel->getActiveSheet()->setCellValue('D' . $cnt, $detail->firstName);
            $this->excel->getActiveSheet()->setCellValue('E' . $cnt, $cnt);
            $this->excel->getActiveSheet()->setCellValue('F' . $cnt, $detail->gender);
            $this->excel->getActiveSheet()->setCellValue('G' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('H' . $cnt, date('m/d/Y', strtotime($detail->dateOfBirth)));
            $this->excel->getActiveSheet()->setCellValue('I' . $cnt, 'N');
            $this->excel->getActiveSheet()->setCellValue('J' . $cnt, 'A');
            $this->excel->getActiveSheet()->setCellValue('K' . $cnt, $detail->salary);
            $this->excel->getActiveSheet()->setCellValue('L' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('M' . $cnt, '');
            if (!empty($dob[0]))
                $this->excel->getActiveSheet()->setCellValue('N' . $cnt, date('m/d/Y', strtotime($dob[0])));
            else
                $this->excel->getActiveSheet()->setCellValue('N' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('O' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('P' . $cnt, '');
            if (!empty($dob[1]))
                $this->excel->getActiveSheet()->setCellValue('Q' . $cnt, date('m/d/Y', strtotime($dob[1])));
            else
                $this->excel->getActiveSheet()->setCellValue('Q' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('R' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('S' . $cnt, '');
            if (!empty($dob[2]))
                $this->excel->getActiveSheet()->setCellValue('T' . $cnt, date('m/d/Y', strtotime($dob[2])));
            else
                $this->excel->getActiveSheet()->setCellValue('T' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('U' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('V' . $cnt, '');
            if (!empty($dob[3]))
                $this->excel->getActiveSheet()->setCellValue('W' . $cnt, date('m/d/Y', strtotime($dob[3])));
            else
                $this->excel->getActiveSheet()->setCellValue('W' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('X' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('Y' . $cnt, '');
            if (!empty($dob[4]))
                $this->excel->getActiveSheet()->setCellValue('Z' . $cnt, date('m/d/Y', strtotime($dob[4])));
            else
                $this->excel->getActiveSheet()->setCellValue('Z' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('AA' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('AB' . $cnt, '');
            if (!empty($dob[5]))
                $this->excel->getActiveSheet()->setCellValue('AC' . $cnt, date('m/d/Y', strtotime($dob[5])));
            else
                $this->excel->getActiveSheet()->setCellValue('AC' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('AD' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('AE' . $cnt, '');
            if (!empty($dob[6]))
                $this->excel->getActiveSheet()->setCellValue('AF' . $cnt, date('m/d/Y', strtotime($dob[6])));
            else
                $this->excel->getActiveSheet()->setCellValue('AF' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('AG' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('AH' . $cnt, '');
            if (!empty($dob[7]))
                $this->excel->getActiveSheet()->setCellValue('AI' . $cnt, date('m/d/Y', strtotime($dob[7])));
            else
                $this->excel->getActiveSheet()->setCellValue('AI' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('AJ' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('AK' . $cnt, '');
            if (!empty($dob[8]))
                $this->excel->getActiveSheet()->setCellValue('AL' . $cnt, date('m/d/Y', strtotime($dob[8])));
            else
                $this->excel->getActiveSheet()->setCellValue('AL' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('AM' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('AN' . $cnt, '');
            if (!empty($dob[9]))
                $this->excel->getActiveSheet()->setCellValue('AO' . $cnt, date('m/d/Y', strtotime($dob[9])));
            else
                $this->excel->getActiveSheet()->setCellValue('AO' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('AP' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('AQ' . $cnt, '');
            if (!empty($dob[10]))
                $this->excel->getActiveSheet()->setCellValue('AR' . $cnt, date('m/d/Y', strtotime($dob[10])));
            else
                $this->excel->getActiveSheet()->setCellValue('AR' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('AS' . $cnt, '');
            $cnt++;
            $no++;
        }

        $filename = $comp_name_id . '.csv';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
    }

    function limelight_export($id) {
        $comp_name = $this->Company_model->get_company($id);
        $comp_name_id = $comp_name . "_" . $id;
        $detail = $this->Company_model->get_company_employee($id);
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(340);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(66.75);
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(16);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(16.75);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(14.75);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(6.88);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(11.75);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(38.25);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(38.25);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(38.25);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(16.25);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(24.38);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(18.88);
        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
        $this->excel->getActiveSheet()->getStyle('A1:Q100')->getFont()->setName('Calibri');
        $this->excel->getActiveSheet()->getStyle('A1:Q100')->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle('A1:L2')->getAlignment()->setWrapText(true);
        $this->excel->setActiveSheetIndex(0)->mergeCells("A1:B1");
        $this->excel->setActiveSheetIndex(0)->mergeCells("C1:D1");
        $this->excel->getActiveSheet()->setCellValue('A1', 'Not Required but Recommended');
        $this->excel->getActiveSheet()->setCellValue('C1', 'Date of Birth - OR - Age is required for quoting all products');

        $this->excel->getActiveSheet()->setCellValue('E1', 'Required for quoting dental, vision, life');
        $this->excel->getActiveSheet()->setCellValue('F1', 'Required for quoting Medical');
        $this->excel->getActiveSheet()->setCellValue('G1', 'Required Status References:
EE = Employee Only
ES = Employee + Spouse
EC1 = Employee + 1 Child
EC2 = Employee + 2 Children (or more)
EF = Employee + Spouse + Child(ren)

CS = Covered Spouse
CD = Covered Dependent (Child)');
        $this->excel->getActiveSheet()->setCellValue('H1', 'Required Status References:
EE = Employee Only
ES = Employee + Spouse
EC1 = Employee + 1 Child
EC2 = Employee + 2 Children (or more)
EF = Employee + Spouse + Child(ren)

CS = Covered Spouse
CD = Covered Dependent (Child)');
        $this->excel->getActiveSheet()->setCellValue('I1', 'Required Status References:
EE = Employee Only
ES = Employee + Spouse
EC1 = Employee + 1 Child
EC2 = Employee + 2 Children (or more)
EF = Employee + Spouse + Child(ren)

CS = Covered Spouse
CD = Covered Dependent (Child)');
        $this->excel->getActiveSheet()->setCellValue('J1', 'Required for Life');
        $this->excel->getActiveSheet()->setCellValue('K1', 'Optional');
        $this->excel->getActiveSheet()->setCellValue('L1', 'Required for STD and LTD');

        $this->excel->getActiveSheet()->setCellValue('A2', 'First Name');
        $this->excel->getActiveSheet()->setCellValue('B2', 'Last Name');
        $this->excel->getActiveSheet()->setCellValue('C2', 'Date of Birth');
        $this->excel->getActiveSheet()->setCellValue('D2', 'Age');
        $this->excel->getActiveSheet()->setCellValue('E2', 'Gender');
        $this->excel->getActiveSheet()->setCellValue('F2', 'Employee Zip');
        $this->excel->getActiveSheet()->setCellValue('G2', 'Medical Coverage Status (EE, ES, EC1, EC2, EF, WE, CS, WS, CD, WD, NE, NS, ND)');
        $this->excel->getActiveSheet()->setCellValue('H2', 'Dental Coverage Status
(EE, ES, EC1, EC2, EF, WE, CS, WS, CD, WD, NE, NS, ND)');
        $this->excel->getActiveSheet()->setCellValue('I2', 'Vision Coverage Status
(EE, ES, EC1, EC2, EF, WE, CS, WS, CD, WD, NE, NS, ND)');
        $this->excel->getActiveSheet()->setCellValue('J2', 'Salary ');
        $this->excel->getActiveSheet()->setCellValue('K2', 'Salary Mode');
        $this->excel->getActiveSheet()->setCellValue('L2', 'Occupation');
        $this->excel->getActiveSheet()->getStyle('A1:L2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1:L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1:L1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('99cc00');
        $this->excel->getActiveSheet()->getStyle('A1:L2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A2:L2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('009922');
        $cnt = 3;
        foreach ($detail as $detail) {
            $this->excel->getActiveSheet()->setCellValue('A' . $cnt, $detail->firstName);
            $this->excel->getActiveSheet()->setCellValue('B' . $cnt, $detail->lastName);
            $this->excel->getActiveSheet()->setCellValue('C' . $cnt, date('m/d/Y', strtotime($detail->dateOfBirth)));
            $this->excel->getActiveSheet()->setCellValue('D' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('E' . $cnt, $detail->gender);
            $this->excel->getActiveSheet()->setCellValue('F' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('G' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('H' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('I' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('J' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('K' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('L' . $cnt, '');
            $cnt++;
            $relation_detail = $this->Company_model->get_company_employee_relation($detail->employeeId);
            if (!empty($relation_detail)) {
                foreach ($relation_detail as $rel_detail) {
                    $this->excel->getActiveSheet()->setCellValue('A' . $cnt, $rel_detail->esFirstName);
                    $this->excel->getActiveSheet()->setCellValue('B' . $cnt, $rel_detail->esLastName);
                    $this->excel->getActiveSheet()->setCellValue('C' . $cnt, date('m/d/Y', strtotime($rel_detail->dateOfBirth)));
                    $this->excel->getActiveSheet()->setCellValue('D' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('E' . $cnt, $rel_detail->gender);
                    $this->excel->getActiveSheet()->setCellValue('F' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('G' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('H' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('I' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('J' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('K' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('L' . $cnt, '');
                    $cnt++;
                }
            }
        }
        $this->excel->getActiveSheet()->getStyle('A1:L' . ($cnt - 1))->applyFromArray($styleArray);
        /* for($col = 'A'; $col !== 'o'; $col++) {
          $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
          } */
        $filename = $comp_name_id . '.xls';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
    }
      function download_doc()
    {
        if (!$this->ixsolution_ion_auth->logged_in()) {
            redirect('ixsolutions_auth/login', 'refresh');
        } else {
             $docID = (int) $this->input->get('docid');
             $docValue = $this->Company_model->get_company_document_byDocumentId($docID);
             if (!empty($docValue[0]->document_name))
             {
                 $this->load->helper('download');
                 $folderpath = $this->config->item('folderPathIx');
                 $userPath = $folderpath . "document/";
                 $file_name = $userPath . $docValue[0]->document_name;
                 force_download($file_name, NULL);
             }
             else
                    die("No file found");
         }

    }
    public function delete_file($id)
    {
        $this->Company_model->delete_file($id);
        exit;
    }
      public function employee_detail()
    {
           error_reporting(0);
        $token=$this->uri->segment(3);
        $data['employee_detail'] = $this->Company_model->get_employee_bytoken($token);
        $cid=$data['employee_detail'][0]->companyId;
        $data['company_emp'] = $this->Company_model->get_comp_emp($cid);
        $data['employee_relation'] = $this->Company_model->get_employee_spouse($data['employee_detail'][0]->employeeId);
        $data['occupation'] = $this->Company_model->get_occupation($cid);
        $data['salary'] = $this->Company_model->get_salary($cid);
        $data['company'] = $cid;
        $data['onboard'] =$this->Company_model->get_onboard_status($cid);
        $this->load->view('frontend/ix_employee_detail',$data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data);
    }
    public function update_employee_detail()
    {
        error_reporting(0);
        $data=array();
      $count = $this->input->post('count');
               for ($i = 1; $i <= $count; $i++) {
                if ($this->input->post('fname' . $i) != '') {
                    $empid = $this->input->post('empid' . $i);
                    $employee = array('companyId' => $this->input->post('cid'),
                        'firstName' => $this->input->post('fname' . $i),
                        'lastName' => $this->input->post('lname' . $i),
                        'address' => $this->input->post('address' . $i),
                        'zipcode' => $this->input->post('zipcode' . $i),
                        'state' => $this->input->post('state' . $i),
                        'phoneNumber' => $this->input->post('phone' . $i),
                        'employeeEmailId' => $this->input->post('email' . $i),
                        'dateofBirth' => date('Y-m-d', strtotime($this->input->post('dob' . $i))),
                        'salary' => $this->input->post('salary' . $i),
                        'occupation' => $this->input->post('occupation' . $i),
                        'createdAt' => date('Y-m-d'),
                        'join_date' => date('Y-m-d', strtotime($this->input->post('join_date' . $i))),
                        'ssn' => $this->input->post('ssn' . $i),
                        'gender' => $this->input->post('gender' . $i),
                        'status' => '1'
                    );
                    if ($empid != '')
                        $this->Company_model->update_employee($employee, $empid);
                    else
                        $empid = $this->Company_model->insert_employee($employee);
                    if ($this->input->post('sfname' . $i) != '' && $this->input->post('slname' . $i) != '' && $this->input->post('sdob' . $i) != '') {
                        $sempid = $this->input->post('sempid' . $i);
                        if ($sempid != '') {
                            $spouse = array(
                                'esFirstName' => $this->input->post('sfname' . $i),
                                'esLastName' => $this->input->post('slname' . $i),
                                'gender' => $this->input->post('sgender' . $i),
                                'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('sdob' . $i))),
                                'emp_relation' => 'spouse',
                                'ssn' => $this->input->post('sssn' . $i)
                            );
                            $this->Company_model->update_employee_relation_data($spouse, $sempid);
                        } else {
                            $spouse = array('employeeId' => $empid,
                                'esFirstName' => $this->input->post('sfname' . $i),
                                'esLastName' => $this->input->post('slname' . $i),
                                'gender' => $this->input->post('sgender' . $i),
                                'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('sdob' . $i))),
                                'emp_relation' => 'spouse',
                                'ssn' => $this->input->post('sssn' . $i)
                            );
                            $this->Company_model->insert_employee_relation_data($spouse);
                        }
                    }
                    $child_count = $this->input->post('child' . $i);
                    if ($child_count > 0) {
                        for ($j = 1; $j <= $child_count; $j++) {
                            $cempid = $this->input->post('cempid' . $i . "_" . $j);
                            if ($cempid != '') {
                                $child = array(
                                    'esFirstName' => $this->input->post('cfname' . $i . "_" . $j),
                                    'esLastName' => $this->input->post('clname' . $i . "_" . $j),
                                    'gender' => $this->input->post('cgender' . $i . "_" . $j),
                                    'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('cdob' . $i . "_" . $j))),
                                    'emp_relation' => 'child',
                                    'ssn' => $this->input->post('cssn' . $i . "_" . $j)
                                );
                                $this->Company_model->update_employee_relation_data($child, $cempid);
                            } else {
                                $child = array('employeeId' => $empid,
                                    'esFirstName' => $this->input->post('cfname' . $i . "_" . $j),
                                    'esLastName' => $this->input->post('clname' . $i . "_" . $j),
                                    'gender' => $this->input->post('cgender' . $i . "_" . $j),
                                    'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('cdob' . $i . "_" . $j))),
                                    'emp_relation' => 'child',
                                    'ssn' => $this->input->post('cssn' . $i . "_" . $j)
                                );
                                $this->Company_model->insert_employee_relation_data($child);
                            }
                        }
                    }
                }
            }
        $this->load->view('frontend/ix_employee_list',$data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data); 
    }
}

?>