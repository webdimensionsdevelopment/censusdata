<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ix_privacy extends Frontend_Controller {

    public function __construct() {
        parent::__construct();
       $this->load->library('Ixsolution_ion_auth');
        $this->load->model('Company_model');
        $this->load->library('email');
    }
    public function index()
	    {
            $title = "Privacy";
	        $navbardiv = array(
	            'subclass' => 'inverse',
	            'top' => 'static',
	            'role' => 'navigation',
	            'fluid' => FALSE,
	        );
	        $data['viewport'] = TRUE;
	        $data['body_role'] = 'document';
	        $data['custom_css'] = 'carousel';
	        $data['title'] = $title;
	        $data['navbar'] = $this->navbar1;
	        $data['navbar_dropdown'] = $this->navbar_dropdown;
	        $data['navbar_right'] = ($this->user->loggedin ? $this->navbar_right_loggedin_user1 : $this->navbar_right);
	        $data['navbardiv'] = $navbardiv;
	        $data['navfoot'] = $this->navfoot1;
	        $data['carousel'] = $this->carousel_page;
	        $this->load->view('frontend/head', $data);
	        $this->load->view('frontend/navbar1/open_wrapper');
	        $this->load->view('frontend/navbar1/open_nav');
	        $this->load->view('frontend/navbar1/open_container');
	        $this->load->view('frontend/navbar1/header');
	        $this->load->view('frontend/navbar1/open_collapse');
	        $this->load->view('frontend/navbar1/open_bar');
	        $this->load->view('frontend/navbar1/li_core', $data);
	        $this->load->view('frontend/navbar1/right', $data);
	        $this->load->view('frontend/navbar1/close_bar');
	        $this->load->view('frontend/navbar1/close');
	        $this->load->view('frontend/navbar1/close_wrapper');
	        $this->load->view('frontend/ix_privacy',$data);
	        $this->load->view('frontend/container/close');
	        $this->load->view('frontend/footer', $data);
	        $this->load->view('frontend/foot_docs', $data);

    }
   }