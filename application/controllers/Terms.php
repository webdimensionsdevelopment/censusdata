<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Terms extends Frontend_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin/Company_model');
        $this->load->library('Ion_auth');
        $this->load->library('email');
    }
    public function index()
	    {
	         $title = "Terms";
	        $navbardiv = array(
	            'subclass' => 'inverse',
	            'top' => 'static',
	            'role' => 'navigation',
	            'fluid' => FALSE,
	        );
	        $data['viewport'] = TRUE;
	        $data['body_role'] = 'document';
	        $data['custom_css'] = 'carousel';
	        $data['title'] = $title;
	        $data['navbar'] = $this->navbar;
	        $data['navbar_dropdown'] = $this->navbar_dropdown;
	        $data['navbar_right'] = ($this->user->loggedin ? $this->navbar_right_loggedin_user : $this->navbar_right);
	        $data['navbardiv'] = $navbardiv;
	        $data['navfoot'] = $this->navfoot;
	        $data['carousel'] = $this->carousel_page;
	        $this->load->view('frontend/head', $data);
	        $this->load->view('frontend/navbar/open_wrapper');
	        $this->load->view('frontend/navbar/open_nav');
	        $this->load->view('frontend/navbar/open_container');
	        $this->load->view('frontend/navbar/header');
	        $this->load->view('frontend/navbar/open_collapse');
	        $this->load->view('frontend/navbar/open_bar');
	        $this->load->view('frontend/navbar/li_core', $data);
	        $this->load->view('frontend/navbar/right', $data);
	        $this->load->view('frontend/navbar/close_bar');
	        $this->load->view('frontend/navbar/close');
	        $this->load->view('frontend/navbar/close_wrapper');
	        $this->load->view('frontend/terms',$data);
	        $this->load->view('frontend/container/close');
	        $this->load->view('frontend/footer', $data);
	        $this->load->view('frontend/foot_docs', $data);

    }
   }