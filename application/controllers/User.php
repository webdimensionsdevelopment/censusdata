<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends Ixsolution_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('upload');
        $this->load->model("user_model");
        $this->load->helper(array('form', 'url'));
        $this->load->model("register_model");
        $this->load->library('email');
        $this->load->library('session');
    }

    public function index() {
        if (isset($this->session->userdata['user']['userId'])) {
            $id = $this->session->userdata['user']['userId'];
        } else {
            $id = 0;
        }
        $userType = $this->register_model->getUserType();
        $data = array("userType" => $userType, "userId" => $id);
        $this->load->view('user/index', $data);


        if (isset($_POST['brokerageSubmit'])) {
            $error = false;
            $firstName = $lastName = $email = $password = $confirmPassword = $FImage = $phoneNumber = $address = $country = $state = $city = $gender = "";
            $errorFirstName = $errorLastName = $errorEmail = $errorPassword = $errorConfirmPassword = $errorGender = '';
            $firstName = $_POST['txtFirstName'];
            $lastName = $_POST['txtLastName'];
            $email = $_POST['txtEmail'];
            $password = $_POST['txtPassword'];
            $confirmPassword = $_POST['txtConfirmPassword'];
            $FImage = $_FILES['image'];
            $phoneNumber = $_POST['txtPhoneNumber'];
            $address = $_POST['txtAddress'];
            $gender = $_POST['rdGender'];
            $zipCode = $_POST['txtZipCode'];
            $country = $_POST['txtCountry'];
            $state = $_POST['txtState'];
            $city = $_POST['txtCity'];
            if (empty($firstName)) {
                $error = true;
                $errorFirstName = "Enter First Name";
                $this->session->set_flashdata('errorFirstName', $errorFirstName);
            }
            if (empty($lastName)) {
                $error = true;
                $errorLastName = "Enter Last Name";
                $this->session->set_flashdata('errorLastName', $errorLastName);
            }
            if (empty($email)) {
                $error = true;
                $errorEmail = "Enter Email";
                $this->session->set_flashdata('errorEmail', $errorEmail);
            }
            if (empty($password)) {
                $error = true;
                $errorPassword = "Enter Password";
                $this->session->set_flashdata('errorPassword', $errorPassword);
            }
            if (empty($confirmPassword)) {
                $error = true;
                $errorConfirmPassword = "Enter Confirm Password";
                $this->session->set_flashdata('errorConfirmPassword', $errorConfirmPassword);
            }
            if (empty($gender)) {
                $error = true;
                $errorGender = "Please select gender";
                $this->session->set_flashdata('errorGender', $errorGender);
            }
            if (strcmp($password, $confirmPassword) != 0) {
                $error = true;
                $errorUnmatch = "Password did not match, Please enter again";
                $this->session->set_flashdata('errorUnmatch', $errorUnmatch);
            }

            if ($error != true) {
                $activationCode = md5(rand(1, 10000000000) . "fhsf" . rand(1, 100000));
                $insertedId = $this->user_model->getLastInsertedId();
                $newUserId = $insertedId['id'] + 1;
                if ($_FILES && $_FILES['image']) {
                    $path = $_SERVER['DOCUMENT_ROOT'] . "CI-AdminLTE-master/upload/brokerage";
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
                    $userFolderName = $firstName . $newUserId;
                    $userPath = $_SERVER['DOCUMENT_ROOT'] . "CI-AdminLTE-master/upload/brokerage/" . $userFolderName . "/";
                    if (!file_exists($userPath)) {
                        mkdir($userPath, 0777, true);
                    }
                    $FImage = $this->user_model->uploadFile($_FILES['image'], $userPath, $userFolderName);
                }
                $record = array(
                    "ip_address" => $_SERVER['REMOTE_ADDR'],
                    "userName" => $email,
                    "first_name" => $firstName,
                    "last_name" => $lastName,
                    "gender" => $gender,
                    "password" => md5($password),
                    "email" => $email,
                    "image" => ($FImage) ? $FImage : '',
                    "phoneNumber" => ($phoneNumber) ? $phoneNumber : '',
                    "address" => ($address) ? $address : '',
                    "country" => ($country) ? $country : '',
                    "state" => ($state) ? $state : '',
                    "city" => ($city) ? $city : '',
                    "zipcode" => ($zipCode) ? $zipCode : '',
                    "activation_code" => $activationCode
                );
                $id = $this->user_model->addBrokerageUser($record);
                $recordBrokerage = array("userId" => $id);
                $addToBrokerId = $this->user_model->addToBrokerageUserId($recordBrokerage);
                $recordGroup = array("userId" => $id, "group_id" => 3);
                $addToGroup = $this->user_model->addToUserGroupId($recordGroup);
                $userAccountLink = site_url("user/activateAccount/") . $id . "/" . $activationCode;
                $to = $email;
                $subject = "Activation Account";
                $header = array('Content-Type:text/html; charset=UTF-8');
                $body = "<html><body>Date:" . date("Y-m-d") . "<br>ApplicantName:" . $firstName . " " . $lastName . "<br><h3 align='Center'> Account Activation Info</h3><br>Dear" . $firstName . ",<br><pre>Please click below link to activate your account and <b>Login</b></pre><br><b>Activation-Link : </b><a href=" . $userAccountLink . ">Click Here</a><br>Sincerely,<br>" . "Census.com</body></html>";
                $this->email->from(CB_EMAIL_FROM, 'Census');
                $this->email->to($email);
                $this->email->subject($subject);
                $this->email->message($body);
                $this->email->send();
                redirect(base_url() . "login/index");
            }
        }
        if (isset($_POST['brokerSubmit'])) {
            $error = false;
            $firstName = $lastName = $email = $password = $confirmPassword = $FImage = $phoneNumber = $address = $country = $state = $city = $gender = "";
            $errorFirstName = $errorLastName = $errorEmail = $errorPassword = $errorConfirmPassword = $errorGender = '';
            $firstName = isset($_POST['txtFirstName']) ? $_POST['txtFirstName'] : '';
            $lastName = $_POST['txtLastName'];
            $email = $_POST['txtEmail'];
            $password = $_POST['txtPassword'];
            $confirmPassword = $_POST['txtConfirmPassword'];
            $FImage = $_FILES['image'];
            $phoneNumber = $_POST['txtPhoneNumber'];
            $address = $_POST['txtAddress'];
            $gender = $_POST['gender'];
            $zipCode = $_POST['txtZipCode'];
            $country = $_POST['txtCountry'];
            $state = $_POST['txtState'];
            $city = $_POST['txtCity'];
            if (empty($firstName)) {
                $error = true;
                $errorFirstName = "Enter First Name";
                $this->session->set_flashdata('errorFirstName', $errorFirstName);
            }
            if (empty($lastName)) {
                $error = true;
                $errorLastName = "Enter Last Name";
                $this->session->set_flashdata('errorLastName', $errorLastName);
            }
            if (empty($email)) {
                $error = true;
                $errorEmail = "Enter Email";
                $this->session->set_flashdata('errorEmail', $errorEmail);
            }
            if (empty($password)) {
                $error = true;
                $errorPassword = "Enter Password";
                $this->session->set_flashdata('errorPassword', $errorPassword);
            }
            if (empty($confirmPassword)) {
                $error = true;
                $errorConfirmPassword = "Enter Confirm Password";
                $this->session->set_flashdata('errorConfirmPassword', $errorConfirmPassword);
            }
            if (empty($gender)) {
                $error = true;
                $errorGender = "Please select gender";
//                $this->session->set_flashdata('errorGender', $errorGender);
            }
            if (strcmp($password, $confirmPassword) != 0) {
                $error = true;
                $errorUnmatch = "Password did not match, Please enter again";
//                $this->session->set_flashdata('errorUnmatch', $errorUnmatch);
            }
            if ($error != true) {
                $activationCode = md5(rand(1, 10000000000) . "fhsf" . rand(1, 100000));
                $insertedId = $this->user_model->getLastInsertedId();
                $newUserId = $insertedId['id'] + 1;
                if ($_FILES && $_FILES['image']) {
                    $path = $_SERVER['DOCUMENT_ROOT'] . "CI-AdminLTE-master/upload/broker";
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
                    $userFolderName = $firstName . $newUserId;
                    $userPath = $_SERVER['DOCUMENT_ROOT'] . "CI-AdminLTE-master/upload/broker/" . $userFolderName . "/";
                    if (!file_exists($userPath)) {
                        mkdir($userPath, 0777, true);
                    }
                    $FImage = $this->user_model->uploadFile($_FILES['image'], $userPath, $userFolderName);
                }
                $record = array(
                    "ip_address" => $_SERVER['REMOTE_ADDR'],
                    "username" => $email,
                    "first_name" => $firstName,
                    "last_name" => $lastName,
                    "gender" => $gender,
                    "password" => md5($password),
                    "email" => $email,
                    "image" => ($FImage) ? $FImage : '',
                    "phoneNumber" => ($phoneNumber) ? $phoneNumber : '',
                    "address" => ($address) ? $address : '',
                    "country" => ($country) ? $country : '',
                    "state" => ($state) ? $state : '',
                    "city" => ($city) ? $city : '',
                    "zipcode" => ($zipCode) ? $zipCode : '',
                    "activation_code" => $activationCode
                );
                $id = $this->user_model->addBrokerUser($record);
                $recordBroker = array("userId" => $id);
                $recordGroup = array("userId" => $id, "group_id" => 4);
                $addToBrokerId = $this->user_model->addToBrokerUserId($recordBroker);
                $addToGroup = $this->user_model->addToUserGroupId($recordGroup);
                $userAccountLink = site_url("user/activateAccount") . "/" . $id . "/" . $activationCode;
                $to = $email;
                $subject = "Activation Account Info";
                $header = array('Content-Type:text/html; charset=UTF-8');
                $body = "<html><body>Date:" . date("Y-m-d") . "<br>ApplicantName:" . $firstName . " " . $lastName . "<br><h3 align='Center'> Account Activation Info</h3><br>Dear" . $firstName . ",<br><pre>Please click below link to activate your account</pre><br><b>Activation-Link : </b><a href=" . $userAccountLink . ">Click Here</a><br>Sincerely,<br>" . "Census.com</body></html>";
                $this->email->from(CB_EMAIL_FROM, 'Census');
                $this->email->to($email);
                $this->email->subject($subject);
                $this->email->message($body);
                $this->email->send();
                redirect(base_url() . "login/index");
            }
        }
    }

    public function activateAccount($userId, $code) {
        $data = array("activation_code" => '', "active" => 1);
        $userId = $this->user_model->upDateUserDetails($data, $userId);
        echo "
            <script type=\"text/javascript\">
                alert(\"User/is/being/Activated\");
            </script>
        ";
        redirect(base_url() . "login/index");
    }

    public function getCountryStateCity() {
        $count_city = '';
        $coun_State_city = array();
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . $_GET['zipcode'] . '&sensor=false';
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
            exit;
        } else {
            $obj = json_decode($response);
            foreach($obj->results[0]->address_components as $address)
            {
                if ($address->types[0] == 'locality')
                {
                    $city = (isset($address->long_name) ? $address->long_name : '');
                }
                if ($address->types[0] == 'administrative_area_level_2') // county
                {
                    $country = (isset($address->long_name) ? $address->long_name : '');
                }

                if ($address->types[0] == 'administrative_area_level_1')
                {
                    $state = (isset($address->long_name) ? $address->long_name : '');
                    $shortstatename = (isset($address->long_name) ? $address->short_name : '');
                }

            }
            $coun_State_city = array ("country"=>$country, "state"=>$state, "city"=>$city);
            echo json_encode($coun_State_city);
            exit;
        }
    }

    public function profileViewEdit() {
        $id = '';
        if (isset($this->session->userdata['user']['id'])) {
            $id = $this->session->userdata['user']['id'];
        } else {
            $id = 0;
        }
        $record = $this->user_model->getUserDetailsById($id);
        $type = $this->user_model->getUserTypeNameById($id);
        $imageName = $record['image'];
        $record['image'] = $filePath = base_url() . "upload/" . $type['name'] . "/" . $record['first_name'] . $id . "/" . $record['image'];
        $this->load->view('layout/layout');
        $record['name'] = $type['name'];
        $this->load->view('user/profileViewEdit', $record);
        if (isset($_POST['submit'])) {
            if($_FILES && $_FILES['image']['size']>0){
                $userFolderName = $record['first_name'] . $record['id'];
                $path = $_SERVER['DOCUMENT_ROOT'] . "CI-AdminLTE-master/upload/" . $type['name'] . "/".$userFolderName."/";
                $FImage = $this->user_model->uploadFile($_FILES['image'], $path, $userFolderName);
            }
            else {
                $FImage = $imageName;
            }
            $record = array (
                    "username" => $_POST['email'],
                    "first_name" => $_POST['txtFirstName'],
                    "last_name" => $_POST['txtLastName'],
                    "gender" => $_POST['rdGender'],
                    "email" => $_POST['txtEmail'],
                    "password" => md5($_POST['txtPassword']),
                    "image" => ($FImage) ? $FImage : '',
                    "phoneNumber" => $_POST['txtPhoneNumber'],
                    "address" => $_POST['txtAddress'],
                    "country" => $_POST['txtCountry'],
                    "state" => $_POST['txtState'],
                    "city" => $_POST['txtCity'],
                    "zipcode" => $_POST['txtZipCode'],
            );
            $userId = $this->user_model->upDateUserDetails($record, $id);
            redirect(base_url() . "user/profileViewEdit");
        }
    }

}
