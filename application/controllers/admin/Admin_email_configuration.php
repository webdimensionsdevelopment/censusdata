<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_email_configuration extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->lang->load('admin/admin_email_config');

        /* Title Page :: Common */
        $this->page_title->push(lang('menu_admin_mail_config'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_admin_mail_config'), 'admin/admin_email_configuration');
        $this->load->model('admin/Admin_configure_model');
    }

    /*
     * Admin Mail Config view
     */

    public function index() {

        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Title Page */
            $this->page_title->push('Admin Mail Configuration');

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* Data */
            $this->data['mail_config'] = $this->Admin_configure_model->get_all_mail();
            /* Load Template */
            $this->template->admin_render('admin/admin_mail_config/index', $this->data);
        }
    }

    public function create() {
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_users_create'), 'admin/users/create');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Variables */
        $tables = $this->config->item('tables', 'ion_auth');

        /* Validate form input */
        $this->form_validation->set_rules('config_email_address', 'lang:users_firstname', 'required');

        if ($this->form_validation->run() == TRUE) {
            $email_config_data = array(
                'admin_email' => strtolower($this->input->post('config_email_address')),
            );
            $emailaddedID    = $this->Admin_configure_model->create_new_email_address($email_config_data);
        }
        if ($this->form_validation->run() == TRUE && $emailaddedID) {
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect('admin/admin_email_configuration/', 'refresh');
        } else {
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['config_email_address'] = array(
                'name'  => 'config_email_address',
                'id'    => 'config_email_address',
                'type'  => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('config_email_address'),
            );

            /* Load Template */
            $this->template->admin_render('admin/admin_mail_config/create', $this->data);
        }
    }

    public function edit($id) {
        $id = (int) $id;

        if (!$this->ion_auth->logged_in() OR ( !$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id))) {
            redirect('auth', 'refresh');
        }

        $emailConfig = $this->Admin_configure_model->get_email_address_by_id($id);
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_users_edit'), 'admin/admin_mail_config/edit');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Data */
        /* Validate form input */
        $this->form_validation->set_rules('config_email_address', 'lang:edit_admin_configuration_email_address', 'required');

        if (isset($_POST) && !empty($_POST)) {
//            if ($this->_valid_csrf_nonce() === FALSE OR $id != $this->input->post('id')) {
//                show_error($this->lang->line('error_csrf'));
//            }

            if ($this->form_validation->run() == TRUE) {
                $data = array(
                    'admin_email' => $this->input->post('config_email_address'),
                );

                if ($this->Admin_configure_model->update_config_email_address($this->input->post('mail_id'), $data)) {
                    $this->session->set_flashdata('message', $this->ion_auth->messages());

                    if ($this->ion_auth->is_admin()) {
                        redirect('admin/admin_email_configuration', 'refresh');
                    } else {
                        redirect('admin', 'refresh');
                    }
                } else {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());

                    if ($this->ion_auth->is_admin()) {
                        redirect('admin/admin_email_configuration', 'refresh');
                    } else {
                        redirect('admin/admin_email_configuration', 'refresh');
                    }
                }
            }
        }

        // display the edit user form
//        $this->data['csrf'] = $this->_get_csrf_nonce();

        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        // pass the user to the view
        $this->data['config_email']          = $emailConfig;

        $this->data['config_email_address']       = array(
            'name'  => 'config_email_address',
            'id'    => 'config_email_address',
            'type'  => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('config_email_address', $emailConfig[0]->admin_email)
        );
        /* Load Template */
        $this->template->admin_render('admin/admin_mail_config/edit', $this->data);
    }

    function activate($id)
    {
        $id = (int)$id;
        $activation = $this->Admin_configure_model->config_email_activate($id);
        if ($activation) ;
        redirect('admin/admin_email_configuration', 'refresh');
    }

    public function deactivate($id = NULL)
    {
        $id = (int)$id;
        $deactivation = $this->Admin_configure_model->config_email_deactivate($id);
        if ($deactivation) ;
        redirect('admin/admin_email_configuration', 'refresh');
    }

}

?>