<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Company extends Admin_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('admin/Company_model');
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }
    public function companyDetails($id) {

        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Title Page */
            $this->page_title->push('Company Details');
            $this->data['pagetitle'] = $this->page_title->show();
            /* Breadcrumbs */
            $this->breadcrumbs->unshift(2, lang('menu_company'), 'admin/company_list_view');
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $this->data['company'] = $this->Company_model->get_company_and_employee_details_byId($id);
            $docValue = $this->Company_model->get_company_documents_byId($this->data['company'][0]['companyId']);
            if ($docValue) {
                foreach ($docValue as $j => $document) {
                    $this->data['documents'][$j] = $document;
                }
            } else {
                $this->data['documents'] = 0;
            }
            $this->data['product'] = $this->Company_model->get_product_details_of_CompanyProduct_byId($this->data['company'][0]['companyId']);
            if ($this->data['product']) {
                foreach ($this->data['product'] as $k => $product) {
                    $count = 0;
                    $this->data['product'][$k]['questions'] = $this->Company_model->get_all_question_byProductId($product['prod_id']);
                    foreach ($this->data['product'][$k]['questions'] as $key => $questionsAnswerArr) {
                        $this->data['product'][$k]['questions'][$key]['answers'] = $this->Company_model->get_all_answers_byquestionId($this->data['company'][0]['companyId'], $questionsAnswerArr['id']);
                    }
                    $this->data['product'][$k]['note'][$count] = $this->Company_model->getNotes($this->data['company'][0]['companyId'],$product['prod_id']);
                } 
            } else {
                $this->data['product'][$k]['questions'] = 0;
            }
            $this->data['employee'] = $this->Company_model->get_employees_ByCompanyId($this->data['company'][0]['companyId']);
            if ($this->data['employee']) {
                foreach ($this->data['employee'] as $key => $employeeArray) {
                    if($employeeArray['employeeId']){
                        $this->data['employee'][$key]['employeeSpouse'] = $this->Company_model->get_employee_spouse_byId($employeeArray['employeeId']);
                    }
                }
            } 
            $this->template->admin_render('admin/company/companyDetails', $this->data);
        }
    }
    function download_doc(){
         if (!$this->ion_auth->logged_in()) {
                    redirect('auth/login', 'refresh');
         } else {
             $docID = (int) $this->input->get('docid');
             $docValue = $this->Company_model->get_company_document_byDocumentId($docID);
             if (!empty($docValue[0]->document_name))
             {
                 $this->load->helper('download');
                 $userPath = CB_DOWNLOADPATH . "user/document/";
                 $file_name = $userPath . $docValue[0]->document_name;

                 force_download($file_name, NULL);
             }
             else
                    die("No file found");
         }
    }
    /*
     * company view 
     */
    public function index() {

        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            $this->page_title->push('Company');
            $this->data['pagetitle'] = $this->page_title->show();
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $this->data['product'] = $this->Company_model->get_product();
            $this->data['sic_code'] = $this->Company_model->get_sic_code();
            $this->data['company_detail'] = '';
            $this->template->admin_render('admin/Company_view', $this->data);
        }
    }
    /*
     * get country state city from zipcode
     */
    public function getCountryStateCity() {
        $count_city = '';
        $coun_State_city = array();
        $state = '';
        $zip = '';
        $city = '';
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . $_GET['zipcode'] . '&sensor=false';
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
            exit;
        } else {
            $country = '';
            $obj = json_decode($response);
            foreach($obj->results[0]->address_components as $address)
            {
                if ($address->types[0] == 'locality')
                {
                    $city = (isset($address->long_name) ? $address->long_name : '');
                }
                if ($address->types[0] == 'administrative_area_level_2') // county
                {
                    $country = (isset($address->long_name) ? $address->long_name : '');
                }

                if ($address->types[0] == 'administrative_area_level_1')
                {
                    $state = (isset($address->long_name) ? $address->long_name : '');
                    $shortstatename = (isset($address->long_name) ? $address->short_name : '');
                }
            }
            $coun_State_city = array("country" => $country, "state" => $state, "city" => $city);
            echo json_encode($coun_State_city);
            exit;
        }
    }
    /*
     * insert company
     */
    public function insert_company() {

        /*$this->form_validation->set_rules('brokerage_name', 'Broker Name', 'required');
        $this->form_validation->set_rules('bfname', 'Broker First Name', 'required');
        $this->form_validation->set_rules('blname', 'Broker Last Name', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            echo $this->form_validation->set_value('brokerage_name','required');
            echo $this->form_validation->set_value('bfname','required');
            echo $this->form_validation->set_value('blname','required');
            redirect('admin/Company');  
        }
        else
        {*/
             
         if ($this->input->post('add') == 'add') {
            /* Variables */
            $tables = $this->config->item('tables', 'auth');
            $product = $this->input->post('product');
            $group = $this->Company_model->get_group();
            $email = $this->input->post('email');
            $cname = $this->input->post('cname');
            $user = array('username' => $cname,
                'address' => $this->input->post('address'),
                'zipcode' => $this->input->post('zipcode'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'company' => $cname,
                'email' => $email
            );
            $id = $this->Company_model->insert_user($user);
            $date = date('Y-m-d H:i:s');
            $user_type = array('user_id' => $id, 'group_id' => $group[0]->id, 'createdAt' => $date);
            $this->Company_model->insert_user_type($user_type);
            $company = array('userId' => $id,
                'sicId' => $this->input->post('sic'),
                'no_of_emp' => $this->input->post('no_of_emp'),
                'brokerage_name' => $this->input->post('brokerage_name'),
                'broker_fname' => $this->input->post('bfname'),
                'broker_lname' => $this->input->post('blname'),
                'npn' => $this->input->post('npn'),
                'phone' => $this->input->post('phone'),
                'email' => $this->input->post('bemail'),
                'assign_status' => 'unassigned',
                'quote_status' => 'Quote Requested'
            );
            $cid = $this->Company_model->insert_company($company);
            foreach ($product as $product) {
                $comp_product = array('comp_id' => $cid,
                    'prod_id' => $product,
                    'renewal_date' => date('Y-m-d', strtotime($this->input->post('renewal_date' . $product))),
                    'effective_date' => date('Y-m-d', strtotime($this->input->post('effect_date' . $product)))
                );
                $this->Company_model->insert_company_product($comp_product);
            }
            $this->session->set_userdata('comp_id',$cid);
            redirect('admin/Company/employee_view');
        } else if ($this->input->post('add') == 'update'){
            $email = $this->input->post('email');
            $cname = $this->input->post('cname');
            $user = array('username' => $cname,
                'address' => $this->input->post('address'),
                'zipcode' => $this->input->post('zipcode'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'company' => $cname,
                'email' => $email
            );
            $this->Company_model->update_user($user, $this->input->post('userid'));
            $company = array('sicId' => $this->input->post('sic'),
                         'no_of_emp'=>$this->input->post('no_of_emp'),
                         'brokerage_name'=>$this->input->post('brokerage_name'),
                         'broker_fname'=>$this->input->post('bfname'),
                         'broker_lname'=>$this->input->post('blname'),
                         'npn'=>$this->input->post('npn'),
                         'phone'=>$this->input->post('phone'),
                         'email'=>$this->input->post('bemail'),
                         'quote_status'=>'Updated',
                         'close_status'=>'1'
            );
            $this->Company_model->update_company($company, $this->input->post('companyid'));
            redirect('admin/Company/edit_employee_view/'.$this->input->post('companyid'));
        }
        else
        {
            $company = array('quote_status'=>'Closed','close_status'=>'0');
            $this->Company_model->update_company($company, $this->uri->segment(4));
            redirect('admin/Company/company_list_view');
        }
        //}
    }
    /*
     * employee view 
     */
    public function employee_view()
    {
        $cid=$this->session->userdata('comp_id');
            $this->page_title->push('Employees');
            $this->data['pagetitle'] = $this->page_title->show();
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $this->data['url_exist'] = is_url_exist('http://www.domprojects.com');
            $this->data['company'] = $this->session->userdata('comp_id');
            $this->data['company_emp'] = $this->Company_model->get_comp_emp($cid);
            $this->data['occupation'] = $this->Company_model->get_occupation($cid);
            $this->data['salary'] = $this->Company_model->get_salary($cid);
            $this->template->admin_render('admin/employee_view', $this->data);
    }
    /*
     * employees data upload view
     */
    public function employee_upload_view() {

        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            $this->page_title->push('Employees');
            $this->data['pagetitle'] = $this->page_title->show();
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $this->data['url_exist'] = is_url_exist('http://www.domprojects.com');
            $this->data['company'] = $this->session->userdata('comp_id');
            $this->data['company_emp'] = $this->input->post('comp_emp');
            $this->template->admin_render('admin/employee_upload_view', $this->data);
        }
    }
    /*
     * insert employees data or upload employees data
     */
    public function insert_employee() {

        $product_count = 0;
        $product_count = $this->Company_model->company_product_count($this->input->post('cid'));
        $file_name = $_FILES['file_name'];
        if ($file_name) {

            $comp_name = $this->Company_model->get_company($this->input->post('cid'));
            $comp_name_id = $comp_name . "_" . $this->input->post('cid');
            if (!file_exists(CB_DOWNLOADPATH . "user/employee_csv")) {
                mkdir(CB_DOWNLOADPATH . "user/employee_csv", 0777);
            }
            $userPath = CB_DOWNLOADPATH . "user/employee_csv/";
            $file = $this->Company_model->uploadFile($_FILES['file_name'], $userPath, $comp_name_id);
            $fp = fopen(CB_DOWNLOADPATH . "user/employee_csv/" . $file, "r");
            $flag = true;
            $id = 0;
            $last_name = '';
            $status = 0;
            $comp_emp = 0;
            $row=1;
             while (($linedata = fgets($fp, 4096)) !== false)
             {
                 $data = $this->csv_split($linedata,',');
                 // Remove any extra spaces
                 foreach($data as $num => $key)
                 {
                     $data[$num] = trim($data[$num]);
                 }
                if($flag)
                 {
                    if($row==1)
                     {
                         if(strtolower($data[1])=='first name' && strtolower($data[2])=='last name' && strtolower($data[3])=='date of birth' && strtolower($data[4])=='zipcode' && strtolower($data[5])=='state' && strtolower($data[6])=='salary' && strtolower($data[7])=='occupation' &&  strtolower($data[8])=='relation' && strtolower($data[9])=='gender' && strtolower($data[10])=='status')
                         {
                            $flag=false; continue;
                         }
                     }
                     if($row==3)
                     {
                        if(strtolower($data[1])=='first name' && strtolower($data[2])=='last name' && strtolower($data[3])=='date of birth' && strtolower($data[4])=='zipcode' && strtolower($data[5])=='state' && strtolower($data[6])=='salary' && strtolower($data[7])=='occupation' &&  strtolower($data[8])=='relation' && strtolower($data[9])=='gender' && strtolower($data[10])=='status')
                         {
                            $flag=false; continue;
                         }
                         else
                         {
                              $this->session->set_flashdata('msg','Uploaded format not proper, Please download format from below button');
                              redirect('admin/Company/employee_upload_view');      
                         }
                     }
                     else
                     {
                         $row++;
                         continue;
                         
                     }
                 }


                if(substr($data[8],0,1)=='e' || substr($data[8],0,1)=='E')
                 {
                     if($data[10]=='Active' || $data[10]=='active'){ $status=1;}
                    $employee = array('companyId' =>$this->input->post('cid') ,
                                       'firstName' => $data[1],
                                       'lastName' => $data[2],
                                       'dateofBirth' => date('Y-m-d',strtotime($data[3])),
                                       'zipcode' => $data[4],
                                       'state' => $data[5],
                                       'salary' => $data[6],
                                       'occupation' => $data[7],
                                       'gender' => $data[9],
                                       'status' => $status,
                                       'createdAt' => date('Y-m-d')
                    );
                    $id = $this->Company_model->insert_employee($employee);
                   $last_name=$data[2];
                   $comp_emp=$comp_emp+1;
                 }
                 else if((substr($data[8],0,1)=='s' || substr($data[8],0,1)=='S') && $last_name==$data[2])
                 {
                     $spouse = array('employeeId' => $id,
                                    'esFirstName' => $data[1],
                                    'esLastName' => $data[2],
                                    'dateOfBirth' => date('Y-m-d',strtotime($data[3])),
                                    'emp_relation' => $data[8],
                                    'gender'=>$data[9]
                        );
                    $this->Company_model->insert_employee_relation_data($spouse);
                 }
                 else if((substr($data[8],0,1)=='c' || substr($data[8],0,1)=='C') && $last_name==$data[2])
                 {  
                    $child = array('employeeId' => $id,
                                   'esFirstName' => $data[1],
                                   'esLastName' => $data[2],
                                   'dateOfBirth' => date('Y-m-d',strtotime($data[3])),
                                   'emp_relation' => $data[8],
                                   'gender'=>$data[9]
                            );
                     $this->Company_model->insert_employee_relation_data($child);
                 } 
            }
            fclose($fp);
            // delete the file we don't need it anymore
            if (!empty($file))
                unlink(CB_DOWNLOADPATH . "user/employee_csv/" . $file);
            $company_employee = array('no_of_emp' => $comp_emp);
            $this->Company_model->update_company_employee($company_employee, $this->input->post('cid'));
           redirect('admin/Company/edit_employee_view');
        } else if ($this->input->post('add') == 'update') {
            $count = $this->input->post('count');
            //$comp_emp = $this->input->post('comp_emp');
            $comp_emp = 0;
            for ($i = 1; $i <= $count; $i++) {
                if ($this->input->post('fname' . $i) != '') {
                    $empid = $this->input->post('empid' . $i);
                    $employee = array('companyId' => $this->input->post('cid'),
                        'firstName' => $this->input->post('fname' . $i),
                        'lastName' => $this->input->post('lname' . $i),
                        'dateofBirth' => date('Y-m-d', strtotime($this->input->post('dob' . $i))),
                        'zipcode' => $this->input->post('zipcode' . $i),
                        'state' => $this->input->post('state' . $i),
                        'salary' => $this->input->post('salary' . $i),
                        'occupation' => $this->input->post('occupation' . $i),
                        'createdAt' => date('Y-m-d'),
                        'gender' => $this->input->post('gender' . $i),
                        'status' => '1'
                    );
                    if ($empid != '')
                        $this->Company_model->update_employee($employee, $empid);
                    else
                        $empid = $this->Company_model->insert_employee($employee);

                    if ($this->input->post('sfname' . $i) != '' && $this->input->post('slname' . $i) != '' && $this->input->post('sdob' . $i) != '') {
                        $sempid = $this->input->post('sempid' . $i);
                        if ($sempid != '') {
                            $spouse = array(
                                'esFirstName' => $this->input->post('sfname' . $i),
                                'esLastName' => $this->input->post('slname' . $i),
                                'gender' => $this->input->post('sgender' . $i),
                                'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('sdob' . $i))),
                                'emp_relation' => 'Spouse'
                            );
                            $this->Company_model->update_employee_relation_data($spouse, $sempid);
                        } else {
                            $spouse = array('employeeId' => $empid,
                                'esFirstName' => $this->input->post('sfname' . $i),
                                'esLastName' => $this->input->post('slname' . $i),
                                'gender' => $this->input->post('sgender' . $i),
                                'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('sdob' . $i))),
                                'emp_relation' => 'Spouse'
                            );
                            $this->Company_model->insert_employee_relation_data($spouse);
                        }
                    }
                    $child_count = $this->input->post('child' . $i);
                    if ($child_count > 0) {
                        for ($j = 1; $j <= $child_count; $j++) {
                            $cempid = $this->input->post('cempid' . $i . "_" . $j);
                            if ($cempid != '') {
                                $child = array(
                                    'esFirstName' => $this->input->post('cfname' . $i . "_" . $j),
                                    'esLastName' => $this->input->post('clname' . $i . "_" . $j),
                                    'gender' => $this->input->post('cgender' . $i . "_" . $j),
                                    'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('cdob' . $i . "_" . $j))),
                                    'emp_relation' => 'Child'
                                );
                                $this->Company_model->update_employee_relation_data($child, $cempid);
                            } else {
                                $child = array('employeeId' => $empid,
                                    'esFirstName' => $this->input->post('cfname' . $i . "_" . $j),
                                    'esLastName' => $this->input->post('clname' . $i . "_" . $j),
                                    'gender' => $this->input->post('cgender' . $i . "_" . $j),
                                    'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('cdob' . $i . "_" . $j))),
                                    'emp_relation' => 'Child'
                                );
                                $this->Company_model->insert_employee_relation_data($child);
                            }
                        }
                    }
                    $comp_emp++;
                }
            }
            $company_employee = array('no_of_emp' => $comp_emp);
            $this->Company_model->update_company_employee($company_employee, $this->input->post('cid'));
            if($this->session->userdata("companyid") || $this->session->userdata('comp_id'))
            {
                    redirect('admin/Company/question_answer_view');
            }
            else
            {
                redirect('admin/Company/question_answer_upload_view/'.$this->input->post('cid'));
            }
         } else {
            $count = $this->input->post('count');
            $comp_emp = $this->input->post('comp_emp');
            for ($i = 1; $i <= $count; $i++) {
                if ($this->input->post('fname' . $i) != '') {
                    $employee = array('companyId' => $this->input->post('cid'),
                        'firstName' => $this->input->post('fname' . $i),
                        'lastName' => $this->input->post('lname' . $i),
                        'dateofBirth' => date('Y-m-d', strtotime($this->input->post('dob' . $i))),
                        'zipcode' => $this->input->post('zipcode' . $i),
                        'state' => $this->input->post('state' . $i),
                        'salary' => $this->input->post('salary' . $i),
                        'occupation' => $this->input->post('occupation' . $i),
                        'createdAt' => date('Y-m-d'),
                        'gender' => $this->input->post('gender' . $i),
                        'status' => '1'
                    );
                    $id = $this->Company_model->insert_employee($employee);
                    if ($this->input->post('sfname' . $i) != '' && $this->input->post('slname' . $i) != '' && $this->input->post('sdob' . $i) != '') {
                        $spouse = array('employeeId' => $id,
                            'esFirstName' => $this->input->post('sfname' . $i),
                            'esLastName' => $this->input->post('slname' . $i),
                            'gender' => $this->input->post('sgender' . $i),
                            'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('sdob' . $i))),
                            'emp_relation' => 'Spouse'
                        );
                        $this->Company_model->insert_employee_relation_data($spouse);
                    }
                    $child_count = $this->input->post('child' . $i);
                    if ($child_count > 0) {
                        for ($j = 1; $j <= $child_count; $j++) {
                            $child = array('employeeId' => $id,
                                'esFirstName' => $this->input->post('cfname' . $i . "_" . $j),
                                'esLastName' => $this->input->post('clname' . $i . "_" . $j),
                                'gender' => $this->input->post('cgender' . $i . "_" . $j),
                                'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('cdob' . $i . "_" . $j))),
                                'emp_relation' => 'Child'
                            );
                            $this->Company_model->insert_employee_relation_data($child);
                        }
                    }
                }
            }
            $company_employee = array('no_of_emp' => $comp_emp);
            $this->Company_model->update_company_employee($company_employee, $this->input->post('cid'));
           redirect('admin/Company/question_answer_view');   
        }
    }
    public function question_answer_view()
    {

        $cid = $this->session->userdata('comp_id'); 
            $this->page_title->push('Questionnaries');
            $this->data['pagetitle'] = $this->page_title->show();
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $this->data['url_exist'] = is_url_exist('http://www.domprojects.com');
            $product = $this->Company_model->get_comp_product($cid);
            $pid[] = '';
            foreach($product as $pro)
            {
            $this->data['question'][$pro->name] = $this->Company_model->get_question($pro->productId);
            }
            $this->data['answer'] = $this->Company_model->get_answer();
            $this->data['document'] = $this->Company_model->get_document();
            $this->data['cid'] = $cid;
            $this->data['no_of_emp']=$this->Company_model->get_comp_emp($cid);
            $this->template->admin_render('admin/question_answer_view', $this->data);
    }
    /*
     * questionnaires upload view
     */
    public function question_answer_upload_view() {

        $cid=$this->uri->segment(4);
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Title Page */
            $this->page_title->push('Questionnaires');
            $this->data['pagetitle'] = $this->page_title->show();
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* TEST */
            $this->data['url_exist'] = is_url_exist('http://www.domprojects.com');
            //$cid = $this->input->post('cid');
            $this->data['document'] = $this->Company_model->get_document();
            $this->data['docValue'] = $this->Company_model->get_company_documents_byId($cid);
            $this->data['cid'] = $cid;
            $this->template->admin_render('admin/question_answer_upload_view', $this->data);
        }
    }
    /*
     * insert question or upload questions
     */
    public function insert_question_answer() {

        $cid = $this->input->post('cid');
        $count=$this->input->post('count');
        $comp_name=$this->Company_model->get_company($cid);
            if (!file_exists(CB_DOWNLOADPATH . "user/document")) {
                    mkdir(CB_DOWNLOADPATH . "user/document",0777);
        }
            $userPath = CB_DOWNLOADPATH . "user/document/";
        for($cnt=1;$cnt<=$count;$cnt++)
        {
            for($cnt1=1;$cnt1<=$this->input->post('file_count'.$cnt);$cnt1++)
            {
                $file_name = $_FILES['file_name'.$cnt."_".$cnt1];
                if (!empty($_FILES['file_name'.$cnt."_".$cnt1]['name'])) {
                        $name = explode('.', $file_name['name']);
                        $comp_name_id = urlencode($name[0]) . "_" . $comp_name . "_" . $cid;
                        $file = $this->Company_model->uploadFile($_FILES['file_name' . $cnt . "_" . $cnt1], $userPath, $comp_name_id);
                        $question_document = array('comp_id' => $cid,
                            'doc_id' => $this->input->post('file_type' . $cnt),
                            'document_name' => $file
                        );
                   $this->Company_model->insert_question_document($question_document);
                }
             }
         }
         $cnt = $this->input->post('cnt');
         for ($i = 1; $i < $cnt; $i++) {
            $ans_count=count($this->input->post('aid' . $i.'[]'));
            for($j=0;$j<$ans_count;$j++)
            {
                $question_answer = array("questionId" => $this->input->post('qid' . $i),
                    "ques_answer" => $this->input->post('ques_answer' . $i),
                    "answerId" => $this->input->post('aid' . $i.'['.$j.']'),
                    "companyId" => $cid
                );
                $this->Company_model->insert_question_answer($question_answer);
            }  
         }
         redirect('admin/Company/company_list_view');
         //echo "HELLO";exit;
    }
    /*
     * display all company which inserted by user
     */
    function company_list_view() {

        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Title Page */
            $this->session->unset_userdata("companyid");
            $this->session->unset_userdata('comp_id');
            $this->page_title->push('Company List');
            $this->data['pagetitle'] = $this->page_title->show();
            /* Breadcrumbs */
            $this->breadcrumbs->unshift(2, lang('menu_company'), 'admin/company_list_view');
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* TEST */
            $this->data['company_detail'] = $this->Company_model->get_company_detail('');
            $this->data['member'] = $this->ion_auth->is_member($this->data['user_login']['id']);
             $this->data['admin'] = $this->ion_auth->is_admin($this->data['user_login']['id']);
            $this->data['userid']=$this->data['user_login']['id'];
            $this->data['employee']=$this->Company_model->get_employees();
            $this->data['assign_company']=$this->Company_model->get_assign_company($this->data['user_login']['id']);
            $this->template->admin_render('admin/company_list_view', $this->data);
        }
    }
    /*
     * csv for export all employee's data of particular company
     */
    public function export_company_detail() {
        
        $cid = $this->uri->segment(4);
        $comp_name = $this->Company_model->get_company($cid);
        $comp_name_id = $comp_name . "_" . $cid;
        $this->load->helper('download');
        $detail = $this->Company_model->get_company_employee($cid);
        if (!file_exists(CB_DOWNLOADPATH . "admin")) {
            mkdir(CB_DOWNLOADPATH . "admin", 0777, true);
        }
        $fp = fopen(CB_DOWNLOADPATH . "admin/" . $comp_name_id . ".csv", "w");
        $head = array("Employee Id", "First Name", "Last Name", "Zipcode", "State", "Relation", "Birth Date", "Gender", "Status");
        fputcsv($fp, $head);
        $write_info = array();
        $no = 1;
        foreach ($detail as $detail) {

            $write_info['employeeid'] = $no;
            $write_info['first_name'] = $detail->firstName;
            $write_info['last_name'] = $detail->lastName;
            $write_info['zipcode'] = $detail->zipcode;
            $write_info['state'] = $detail->state;
            $write_info['relation'] = "Employee";

            $write_info['birth_date'] = date('m/d/Y', strtotime($detail->dateOfBirth));
            $write_info['gender'] = $detail->gender;

            if ($detail->status == 1) {
                $write_info['status'] = "Active";
            } else {
                $write_info['status'] = "Inactive";
            }
            fputcsv($fp, $write_info);
            $relation_detail = $this->Company_model->get_company_employee_relation($detail->employeeId);
            if (!empty($relation_detail)) {
                foreach ($relation_detail as $rel_detail) {

                    $write_info['employeeid'] = $no;
                    $write_info['first_name'] = $rel_detail->esFirstName;
                    $write_info['last_name'] = $rel_detail->esLastName;
                    $write_info['zipcode'] = '';
                    $write_info['state'] = '';
                    $write_info['relation'] = $rel_detail->emp_relation;

                    $write_info['birth_date'] = date('m/d/Y', strtotime($rel_detail->dateOfBirth));
                    $write_info['gender'] = $rel_detail->gender;

                    $write_info['status'] = "";
                    fputcsv($fp, $write_info);
                }
            }
            $no++;
        }
        fclose($fp);
        $data=array('quote_status'=>'Downloaded');
        $this->Company_model->update_company($data, $cid);
        $file_name = CB_DOWNLOADPATH . "admin/" . $comp_name_id . ".csv";
        $query = 'FILE IS BEING DOWNLOADED';
        $queryName = "EXPORTED FILE";
        $fileName = $comp_name_id. ".csv";
        $this->ion_auth->logFile($queryName, $query, $fileName,$cid);
        force_download($file_name, NULL);
    }
    public function edit_employee_view() {

         if($this->uri->segment(4))
        {
            $cid = $this->uri->segment(4);
        }
        else
        {
           $cid=$this->session->userdata('comp_id');
        }
        $this->data['company'] = $cid;
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
             if($this->uri->segment(5))
            {
                $this->session->set_userdata("companyid",$cid);
            }
            /* Title Page */
            $this->page_title->push('Edit Employee');
            $this->data['pagetitle'] = $this->page_title->show();
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* TEST */
            $this->data['url_exist'] = is_url_exist('http://www.domprojects.com');
            $this->data['employee_detail'] = $this->Company_model->get_company_employee($cid);
            $this->data['company_emp'] = $this->Company_model->get_comp_emp($cid);
            $this->data['employee_relation'] = $this->Company_model->get_company_all_employee($cid);

            $this->data['occupation'] = $this->Company_model->get_occupation($cid);
            $this->data['salary'] = $this->Company_model->get_salary($cid);
            $this->template->admin_render('admin/employee_view', $this->data);
        }
    }

    public function delete_employee($empid) {
        $this->Company_model->delete_employee($empid);
        return;
    }

    public function delete_employee_relation_data($id) {
        $this->Company_model->delete_employee_relation_data($id);
        return;
    }

    public function delete_employee_child($empid, $relation) {
        $this->Company_model->delete_employee_child($empid, $relation);
        return;
    }

    public function employee_upload_format() {
        $this->load->helper('download');
        $folderpath = $this->config->item('folderPathGA');
        $userPath = $folderpath . "employee_csv/";
        $file_name = $userPath . "employee_upload_example.csv";
        $query = 'FILE IS BEING UPLOADED';
        $queryName = "UPLOADED FILE";
        $fileName = $file_name;
        $this->ion_auth->logFile($queryName, $query, $fileName);
        force_download($file_name, NULL);
    }

    public function update_company_view() {

        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            $cid = $this->uri->segment(4);
            if($this->uri->segment(5))
            {
                $this->session->set_userdata("companyid",$cid);
            }
            /* Title Page */
            $this->page_title->push('Edit Company');
            $this->data['pagetitle'] = $this->page_title->show();
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* TEST */
            $this->data['url_exist'] = is_url_exist('http://www.domprojects.com');
            $this->data['company_detail'] = $this->Company_model->get_company_all_detail($cid);
            $this->data['product'] = '';
            $this->data['sic_code'] = $this->Company_model->get_sic_code();
            $this->data['company'] = $cid;
            $this->template->admin_render('admin/Company_view', $this->data);
        }
    }

    function aetna_export($id) {
        $comp_name = $this->Company_model->get_company($id);
        $comp_name_id = $comp_name . "_" . $id;
        $detail = $this->Company_model->get_company_employee($id);
        $company_detail = $this->Company_model->get_company_detail($id);
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(38.25);
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(10.14);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(11);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20.57);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(8.71);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(13.86);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(11.43);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(14.57);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(11.57);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(14.86);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(11.57);
        $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(10.86);
        $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(8.43);
        $this->excel->getActiveSheet()->getColumnDimension('p')->setWidth(3.86);
        $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(21.43);
        $this->excel->getActiveSheet()->setCellValue('A1', 'Employee ID');
        $this->excel->getActiveSheet()->setCellValue('B1', 'Member Class');
        $this->excel->getActiveSheet()->setCellValue('C1', 'Last Name');
        $this->excel->getActiveSheet()->setCellValue('D1', 'First Name');
        $this->excel->getActiveSheet()->setCellValue('E1', 'Home Zipcode');
        $this->excel->getActiveSheet()->setCellValue('F1', 'Age/ Date of Birth');
        $this->excel->getActiveSheet()->setCellValue('G1', 'Gender');
        $this->excel->getActiveSheet()->setCellValue('H1', 'Employment Status');
        $this->excel->getActiveSheet()->setCellValue('I1', 'Tobacco Status');
        $this->excel->getActiveSheet()->setCellValue('J1', 'Medical Tier');
        $this->excel->getActiveSheet()->setCellValue('K1', 'Dental Tier');
        $this->excel->getActiveSheet()->setCellValue('L1', 'Life - AD&D Tier');
        $this->excel->getActiveSheet()->setCellValue('M1', 'STD Tier');
        $this->excel->getActiveSheet()->setCellValue('N1', 'Work Zipcode');
        $this->excel->getActiveSheet()->getStyle('A1:Q100')->getFont()->setName('Arial');
        $this->excel->getActiveSheet()->getStyle('A1:Q100')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('A1:N1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1:N1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1:N1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ffff00');
        $this->excel->getActiveSheet()->getStyle('A1:N1')->getAlignment()->setWrapText(true);
        //$this->excel->getDefaultStyle()->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

        $cnt = 2;
        $no = 1;
        foreach ($detail as $detail) {
            $spouse_count = $this->Company_model->get_emp_relation_count($detail->employeeId, 'Spouse');
            $child_count = $this->Company_model->get_emp_relation_count($detail->employeeId, 'Child');
            $this->excel->getActiveSheet()->setCellValue('A' . $cnt, $no);
            $this->excel->getActiveSheet()->setCellValue('B' . $cnt, 'Subscriber');
            $this->excel->getActiveSheet()->setCellValue('C' . $cnt, $detail->lastName);
            $this->excel->getActiveSheet()->setCellValue('D' . $cnt, $detail->firstName);
            $this->excel->getActiveSheet()->setCellValue('E' . $cnt, $detail->zipcode);
            $this->excel->getActiveSheet()->setCellValue('F' . $cnt, date('m/d/Y', strtotime($detail->dateOfBirth)));
            $this->excel->getActiveSheet()->setCellValue('G' . $cnt, $detail->gender);
            $this->excel->getActiveSheet()->setCellValue('H' . $cnt, '0');
            $this->excel->getActiveSheet()->setCellValue('I' . $cnt, 'N');
            $this->excel->getActiveSheet()->setCellValue('J' . $cnt, 'Enroll');
            if ($spouse_count[0]->count == '0' && $child_count[0]->count == '0')
                $this->excel->getActiveSheet()->setCellValue('K' . $cnt, '1');
            else if ($spouse_count[0]->count > '0' && $child_count[0]->count == '0')
                $this->excel->getActiveSheet()->setCellValue('K' . $cnt, '2');
            else if ($spouse_count[0]->count == '0' && $child_count[0]->count > '0')
                $this->excel->getActiveSheet()->setCellValue('K' . $cnt, '3');
            else if ($spouse_count[0]->count > '0' && $child_count[0]->count > '0')
                $this->excel->getActiveSheet()->setCellValue('K' . $cnt, '4');
            else
                $this->excel->getActiveSheet()->setCellValue('K' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('L' . $cnt, '0');
            $this->excel->getActiveSheet()->setCellValue('M' . $cnt, '0');
            $this->excel->getActiveSheet()->setCellValue('N' . $cnt, $company_detail[0]->zipcode);
            $cnt++;

            $relation_detail = $this->Company_model->get_company_employee_relation($detail->employeeId);
            if (!empty($relation_detail)) {
                foreach ($relation_detail as $rel_detail) {
                    $this->excel->getActiveSheet()->setCellValue('A' . $cnt, $no);
                    $this->excel->getActiveSheet()->setCellValue('B' . $cnt, ucfirst($rel_detail->emp_relation));
                    $this->excel->getActiveSheet()->setCellValue('C' . $cnt, $rel_detail->esLastName);
                    $this->excel->getActiveSheet()->setCellValue('D' . $cnt, $rel_detail->esFirstName);
                    $this->excel->getActiveSheet()->setCellValue('E' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('F' . $cnt, date('m/d/Y', strtotime($rel_detail->dateOfBirth)));
                    $this->excel->getActiveSheet()->setCellValue('G' . $cnt, $rel_detail->gender);
                    $this->excel->getActiveSheet()->setCellValue('H' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('I' . $cnt, 'N');
                    $this->excel->getActiveSheet()->setCellValue('J' . $cnt, 'Enroll');
                    $this->excel->getActiveSheet()->setCellValue('K' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('L' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('M' . $cnt, '');
                    $this->excel->getActiveSheet()->setCellValue('N' . $cnt, '');
                    $this->excel->getActiveSheet()->getStyle('E' . $cnt)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('aaaaaa');
                    $this->excel->getActiveSheet()->getStyle('H' . $cnt)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('aaaaaa');
                    $this->excel->getActiveSheet()->getStyle('K' . $cnt . ':N' . $cnt)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('aaaaaa');
                    $cnt++;
                }
            }
            $no++;
        }
        $this->excel->getActiveSheet()->getStyle('A1:N' . ($cnt - 1))->applyFromArray($styleArray);

        $BStyle = array('borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THICK)));
        $this->excel->getActiveSheet()->getStyle('P1')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('P1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('P1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('P1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ffff00');
        $this->excel->getActiveSheet()->getStyle('P1')->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('P1:Q5')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('P1:Q5')->applyFromArray($BStyle);
        $this->excel->setActiveSheetIndex(0)->mergeCells("P1:Q1");
        $this->excel->getActiveSheet()->setCellValue('P1', 'Member Class');
        $this->excel->getActiveSheet()->setCellValue('P2', 'ID');
        $this->excel->getActiveSheet()->setCellValue('Q2', 'Name');
        $this->excel->getActiveSheet()->setCellValue('P3', '0');
        $this->excel->getActiveSheet()->setCellValue('Q3', 'Subscriber');
        $this->excel->getActiveSheet()->setCellValue('P4', '1');
        $this->excel->getActiveSheet()->setCellValue('Q4', 'Spouse');
        $this->excel->getActiveSheet()->setCellValue('P5', '2');

        $this->excel->getActiveSheet()->setCellValue('Q5', 'Child');

        $this->excel->getActiveSheet()->getStyle('P7')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('P7')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('P7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('P7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ffff00');
        $this->excel->getActiveSheet()->getStyle('P7')->getAlignment()->setWrapText(true);

        $this->excel->getActiveSheet()->getStyle('P7:Q10')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('P7:Q10')->applyFromArray($BStyle);
        $this->excel->setActiveSheetIndex(0)->mergeCells("P7:Q7");
        $this->excel->getActiveSheet()->setCellValue('P7', 'Medical Tier Key');
        $this->excel->getActiveSheet()->setCellValue('P8', 'ID');

        $this->excel->getActiveSheet()->setCellValue('Q8', 'Name');
        $this->excel->getActiveSheet()->setCellValue('P9', '0');

        $this->excel->getActiveSheet()->setCellValue('Q9', 'Waive');
        $this->excel->getActiveSheet()->setCellValue('P10', '1');

        $this->excel->getActiveSheet()->setCellValue('Q10', 'Enroll');

        $this->excel->getActiveSheet()->getStyle('P12')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('P12')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('P12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('P12')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ffff00');
        $this->excel->getActiveSheet()->getStyle('P12')->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('P12:Q19')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('P12:Q19')->applyFromArray($BStyle);
        $this->excel->setActiveSheetIndex(0)->mergeCells("P12:Q12");
        $this->excel->getActiveSheet()->setCellValue('P12', 'Dental Tier Key');
        $this->excel->getActiveSheet()->setCellValue('P13', 'ID');
        $this->excel->getActiveSheet()->setCellValue('Q13', 'Name');
        $this->excel->getActiveSheet()->setCellValue('P14', '1');

        $this->excel->getActiveSheet()->setCellValue('Q14', 'EE only');
        $this->excel->getActiveSheet()->setCellValue('P15', '2');

        $this->excel->getActiveSheet()->setCellValue('Q15', 'EE & Spouse only');
        $this->excel->getActiveSheet()->setCellValue('P16', '3');

        $this->excel->getActiveSheet()->setCellValue('Q16', 'EE & Child (ren)');
        $this->excel->getActiveSheet()->setCellValue('P17', '4');

        $this->excel->getActiveSheet()->setCellValue('Q17', 'Family');
        $this->excel->getActiveSheet()->setCellValue('P18', '5');

        $this->excel->getActiveSheet()->setCellValue('Q18', 'Spousal Waiver');
        $this->excel->getActiveSheet()->setCellValue('P19', '6');
        $this->excel->getActiveSheet()->setCellValue('Q19', 'Decline Coverage');

        $this->excel->getActiveSheet()->getStyle('P21')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('P21')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('P21')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('P21')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ffff00');
        $this->excel->getActiveSheet()->getStyle('P21')->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('P21:Q25')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('P21:Q25')->applyFromArray($BStyle);
        $this->excel->setActiveSheetIndex(0)->mergeCells("P21:Q21");
        $this->excel->getActiveSheet()->setCellValue('P21', 'Life Tier Key');
        $this->excel->getActiveSheet()->setCellValue('P22', 'ID');

        $this->excel->getActiveSheet()->setCellValue('Q22', 'Name');
        $this->excel->getActiveSheet()->setCellValue('P23', '0');

        $this->excel->getActiveSheet()->setCellValue('Q23', 'Waive');
        $this->excel->getActiveSheet()->setCellValue('P24', '1');

        $this->excel->getActiveSheet()->setCellValue('Q24', 'EE only');
        $this->excel->getActiveSheet()->setCellValue('P25', '7');

        $this->excel->getActiveSheet()->setCellValue('Q25', 'EE & Dep');

        $this->excel->getActiveSheet()->getStyle('P27')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('P27')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('P27')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('P27')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ffff00');
        $this->excel->getActiveSheet()->getStyle('P27')->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('P27:Q30')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('P27:Q30')->applyFromArray($BStyle);
        $this->excel->setActiveSheetIndex(0)->mergeCells("P27:Q27");
        $this->excel->getActiveSheet()->setCellValue('P27', 'STD Tier Key');
        $this->excel->getActiveSheet()->setCellValue('P28', 'ID');

        $this->excel->getActiveSheet()->setCellValue('Q28', 'Name');
        $this->excel->getActiveSheet()->setCellValue('P29', '0');

        $this->excel->getActiveSheet()->setCellValue('Q29', 'Waive');
        $this->excel->getActiveSheet()->setCellValue('P30', '1');

        $this->excel->getActiveSheet()->setCellValue('Q30', 'EE only');

        $this->excel->getActiveSheet()->getStyle('P32')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('P32')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('P32')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('P32')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ffff00');
        $this->excel->getActiveSheet()->getStyle('P32')->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('P32:Q37')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('P32:Q37')->applyFromArray($BStyle);
        $this->excel->setActiveSheetIndex(0)->mergeCells("P32:Q32");
        $this->excel->getActiveSheet()->setCellValue('P32', 'Employment Status Tier Key');
        $this->excel->getActiveSheet()->setCellValue('P33', 'ID');

        $this->excel->getActiveSheet()->setCellValue('Q33', 'Name');
        $this->excel->getActiveSheet()->setCellValue('P34', '0');

        $this->excel->getActiveSheet()->setCellValue('Q34', 'Active');
        $this->excel->getActiveSheet()->setCellValue('P35', '1');

        $this->excel->getActiveSheet()->setCellValue('Q35', 'COBRA');
        $this->excel->getActiveSheet()->setCellValue('P36', '2');

        $this->excel->getActiveSheet()->setCellValue('Q36', 'CalCOBRA');
        $this->excel->getActiveSheet()->setCellValue('P37', '4');

        $this->excel->getActiveSheet()->setCellValue('Q37', 'Mini-COBRA');

        $this->excel->getActiveSheet()->getStyle('P39')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('P39')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('P39')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('P39')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ffff00');
        $this->excel->getActiveSheet()->getStyle('P39')->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getStyle('P39:Q43')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getStyle('P39:Q43')->applyFromArray($BStyle);
        $this->excel->setActiveSheetIndex(0)->mergeCells("P39:Q39");
        $this->excel->getActiveSheet()->setCellValue('P39', 'Tobacco Status Tier Key');
        $this->excel->getActiveSheet()->setCellValue('P40', 'ID');

        $this->excel->getActiveSheet()->setCellValue('Q40', 'Name');
        $this->excel->getActiveSheet()->setCellValue('P41', '1');

        $this->excel->getActiveSheet()->setCellValue('Q41', 'UNK');
        $this->excel->getActiveSheet()->setCellValue('P42', '2');

        $this->excel->getActiveSheet()->setCellValue('Q42', 'Y');
        $this->excel->getActiveSheet()->setCellValue('P43', '3');

        $this->excel->getActiveSheet()->setCellValue('Q43','N');
        $data=array('quote_status'=>'Downloaded');
        $this->Company_model->update_company($data, $id);       
        $filename=$comp_name_id.'.xls'; 
        header('Content-Type: application/vnd.ms-excel'); 
        header('Content-Disposition: attachment;filename="'.$filename.'"'); 
        header('Cache-Control: max-age=0'); 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        $objWriter->save('php://output');
        $query = 'File is being Downloaded';
        $queryName = "EXPORTED THROUGH AETNA TEMPLATE";
        $fileName = $filename;
        $this->ion_auth->logFile($queryName, $query, $fileName, $id);
        
    }

    function bcbsil_export($id) {
        $comp_name = $this->Company_model->get_company($id);
        $comp_name_id = $comp_name . "_" . $id;
        $detail = $this->Company_model->get_company_employee($id);
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(9.43);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(9.86);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(16.71);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(6.86);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(13.43);

        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(11);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(11.29);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(6.86);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(12.43);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(17.57);

        $this->excel->getActiveSheet()->setCellValue('A1', 'Last Name');
        $this->excel->getActiveSheet()->setCellValue('B1', 'First Name');
        $this->excel->getActiveSheet()->setCellValue('C1', 'Relationship Code');
        $this->excel->getActiveSheet()->setCellValue('D1', 'Gender');
        $this->excel->getActiveSheet()->setCellValue('E1', 'DOB');
        $this->excel->getActiveSheet()->setCellValue('F1', 'Coverage Type');
        $this->excel->getActiveSheet()->setCellValue('G1', 'State');
        $this->excel->getActiveSheet()->setCellValue('H1', 'Current Plan');
        $this->excel->getActiveSheet()->setCellValue('I1', 'Retiree');
        $this->excel->getActiveSheet()->setCellValue('J1', 'Annual Salary');
        $this->excel->getActiveSheet()->setCellValue('K1', 'Life Class');
        $this->excel->getActiveSheet()->getStyle('A1:Q100')->getFont()->setName('Arial');
        $this->excel->getActiveSheet()->getStyle('A1:Q100')->getFont()->setSize(10);
        $this->excel->getActiveSheet()->getStyle('A1:K1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


        $cnt = 2;
        foreach ($detail as $detail) {
            $spouse_count = $this->Company_model->get_emp_relation_count($detail->employeeId, 'Spouse');
            $child_count = $this->Company_model->get_emp_relation_count($detail->employeeId, 'Child');
            $this->excel->getActiveSheet()->setCellValue('A' . $cnt, $detail->lastName);
            $this->excel->getActiveSheet()->setCellValue('B' . $cnt, $detail->firstName);
            $this->excel->getActiveSheet()->setCellValue('C' . $cnt, 'Employee');
            if ($detail->gender == 'male' || $detail->gender == 'Male')
                $this->excel->getActiveSheet()->setCellValue('D' . $cnt, 'M');
            else if ($detail->gender == 'female' || $detail->gender == 'Female')
                $this->excel->getActiveSheet()->setCellValue('D' . $cnt, 'F');
            else
            $this->excel->getActiveSheet()->setCellValue('D'.$cnt, '');    
            $this->excel->getActiveSheet()->setCellValue('E'.$cnt, date('m/d/Y',strtotime($detail->dateOfBirth)));
            if($spouse_count[0]->count=='0' && $child_count[0]->count=='0') 
            $this->excel->getActiveSheet()->setCellValue('F'.$cnt, 'EO');
            else if($spouse_count[0]->count > '0' && $child_count[0]->count=='0')
            $this->excel->getActiveSheet()->setCellValue('F'.$cnt, 'ES');
            else if($spouse_count[0]->count=='0' && $child_count[0]->count > '0')
            $this->excel->getActiveSheet()->setCellValue('F'.$cnt, 'EC');    
            else if($spouse_count[0]->count >'0' && $child_count[0]->count > '0')
            $this->excel->getActiveSheet()->setCellValue('F'.$cnt, 'EF');
            else
            $this->excel->getActiveSheet()->setCellValue('F'.$cnt, '');
            $this->excel->getActiveSheet()->setCellValue('G'.$cnt, strtoupper(substr($detail->state,0,2)));
            $this->excel->getActiveSheet()->setCellValue('H'.$cnt, '');
            $this->excel->getActiveSheet()->setCellValue('I'.$cnt, 'N');
            if($detail->salary=='0' || $detail->salary=='')
            {
                $this->excel->getActiveSheet()->setCellValue('J'.$cnt, '');
            }
            else
            {
                $this->excel->getActiveSheet()->getStyle("J".$cnt)->getNumberFormat()->setFormatCode('* ###0_)');
                $this->excel->getActiveSheet()->setCellValue('J'.$cnt, $detail->salary);
            }
            
            $this->excel->getActiveSheet()->setCellValue('K'.$cnt, '');
            
           $cnt++;
            $relation_detail=$this->Company_model->get_company_employee_relation($detail->employeeId); 
           if(!empty($relation_detail))
           {
               foreach ($relation_detail as $rel_detail)
               {
                $this->excel->getActiveSheet()->setCellValue('A'.$cnt, $rel_detail->esLastName);
                $this->excel->getActiveSheet()->setCellValue('B'.$cnt, $rel_detail->esFirstName);
                if($rel_detail->emp_relation=='spouse' || $rel_detail->emp_relation=='Spouse')
                $this->excel->getActiveSheet()->setCellValue('C'.$cnt, 'Spouse');
                else
                $this->excel->getActiveSheet()->setCellValue('C'.$cnt, 'Dependent');
                if($rel_detail->gender=='male' || $rel_detail->gender=='Male')
            $this->excel->getActiveSheet()->setCellValue('D'.$cnt, 'M');
            else if($rel_detail->gender=='female' || $rel_detail->gender=='Female')
            $this->excel->getActiveSheet()->setCellValue('D'.$cnt, 'F');
            else
            $this->excel->getActiveSheet()->setCellValue('D'.$cnt, '');   
                $this->excel->getActiveSheet()->setCellValue('E'.$cnt, date('m/d/Y',strtotime($rel_detail->dateOfBirth)));
                $this->excel->getActiveSheet()->setCellValue('F'.$cnt, '');
                $this->excel->getActiveSheet()->setCellValue('G'.$cnt, '');
                $this->excel->getActiveSheet()->setCellValue('H'.$cnt, '');
                $this->excel->getActiveSheet()->setCellValue('I'.$cnt, '');
                $this->excel->getActiveSheet()->setCellValue('J'.$cnt, '');
                $this->excel->getActiveSheet()->setCellValue('K'.$cnt, '');
                $cnt++;
               }
           }
        }
        $data=array('quote_status'=>'Downloaded');
        $this->Company_model->update_company($data, $id);
        $filename = $comp_name_id . '.xls';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
        $query = 'File is being Downloaded';
        $queryName = "BCBSIL EXPORTED FILE";
        $fileName = $filename;
        $this->ion_auth->logFile($queryName, $query, $fileName, $id);
        
        
    }

    function easecentral_export($id) {
        $comp_name = $this->Company_model->get_company($id);
        $comp_name_id = $comp_name . "_" . $id;
        $this->load->helper('download');

        $detail = $this->Company_model->get_company_employee($id);
        if (!file_exists(CB_DOWNLOADPATH. "admin")) {
            mkdir(CB_DOWNLOADPATH . "admin", 0777, true);
        }
        $fp = fopen(CB_DOWNLOADPATH . "admin/" . $comp_name_id . ".csv", "w");
        $head = array("Employee Id", "First Name", "Last Name", "Zipcode", "State", "Relation", "Birth Date", "Gender", "Status");
        fputcsv($fp, $head);
        $write_info = array();

        $no = 1;
        foreach ($detail as $detail) {
            $write_info['employeeid'] = $no;
            $write_info['first_name'] = $detail->firstName;
            $write_info['last_name'] = $detail->lastName;

            $write_info['zipcode'] = $detail->zipcode;
            $write_info['state'] = $detail->state;
            $write_info['relation'] = "Employee";
            $write_info['birth_date'] = date('m/d/Y', strtotime($detail->dateOfBirth));
            $write_info['gender'] = $detail->gender;

            if ($detail->status == 1) {
                $write_info['status'] = "Active";
            } else {
                $write_info['status'] = "Inactive";
            }
            fputcsv($fp, $write_info);

            $relation_detail = $this->Company_model->get_company_employee_relation($detail->employeeId);
            if (!empty($relation_detail)) {
                foreach ($relation_detail as $rel_detail) {
                    $write_info['employeeid'] = $no;
                    $write_info['first_name'] = $rel_detail->esFirstName;
                    $write_info['last_name'] = $rel_detail->esLastName;
                    $write_info['zipcode'] = '';
                    $write_info['state'] = '';
                    $write_info['relation'] = $rel_detail->emp_relation;

                    $write_info['birth_date'] = date('m/d/Y', strtotime($rel_detail->dateOfBirth));
                    $write_info['gender'] = $rel_detail->gender;

                    $write_info['status'] = "";
                    fputcsv($fp, $write_info);
                }
            }
            $no++;
        }
        fclose($fp);
        $data=array('quote_status'=>'Downloaded');
        $this->Company_model->update_company($data, $id);
        $file_name = CB_DOWNLOADPATH. "admin/" . $comp_name_id . ".csv";
        $query = 'File is being Downloaded';
        $queryName = "EXPORTED THROUGH EASECENTRAL TEMPLATE";
        $fileName = $comp_name_id. ".csv";
        $this->ion_auth->logFile($queryName, $query, $fileName, $id);
        force_download($file_name, NULL);
    }

    function uhc_export($id) {
        

        $comp_name=$this->Company_model->get_company($id);
        $comp_name_id=$comp_name."_".$id;
        $detail=$this->Company_model->get_company_employee($id);
        
        //$detail=$this->Company_model->get_company_employee_all_detail($id);
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setCellValue('C2', '* Required Field');
        $this->excel->getActiveSheet()->setCellValue('B3', 'COMPANY            LOCATION ZIP CODE Location information will be ignored for Single-site groups.');
        $this->excel->getActiveSheet()->setCellValue('C3', 'EMPLOYEE DETAILS');
        $this->excel->getActiveSheet()->setCellValue('M3', '"SPOUSE
Enter Age OR DOB only if Spouse is being enrolled, if not, leave it blank."');
        $this->excel->getActiveSheet()->setCellValue('P3', 'CHILD1');
        $this->excel->getActiveSheet()->setCellValue('S3', 'CHILD2');
        $this->excel->getActiveSheet()->setCellValue('V3', 'CHILD3');
        $this->excel->getActiveSheet()->setCellValue('Y3', 'CHILD4');
        $this->excel->getActiveSheet()->setCellValue('AB3', 'CHILD5');
        $this->excel->getActiveSheet()->setCellValue('AE3', 'CHILD6');
        $this->excel->getActiveSheet()->setCellValue('AH3', 'CHILD7');
        $this->excel->getActiveSheet()->setCellValue('AK3', 'CHILD8');
        $this->excel->getActiveSheet()->setCellValue('AN3', 'CHILD9');
        $this->excel->getActiveSheet()->setCellValue('AQ3', 'CHILD10');
        $this->excel->getActiveSheet()->setCellValue('A4', '');
        $this->excel->getActiveSheet()->setCellValue('B4', 'ZIP CODE*                           (5-Digits)');
        $this->excel->getActiveSheet()->setCellValue('C4', 'CLASS TYPE');
        $this->excel->getActiveSheet()->setCellValue('D4', 'FIRST NAME*');
        $this->excel->getActiveSheet()->setCellValue('E4', 'LAST NAME*');
        $this->excel->getActiveSheet()->setCellValue('F4', 'SEX*');
        $this->excel->getActiveSheet()->setCellValue('G4', '"AGE* Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('H4', '"DOB* (MM/DD/YYYY) Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('I4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('J4', 'EMPLOYMENT STATUS*');
        $this->excel->getActiveSheet()->setCellValue('K4', 'ANNUAL SALARY');
        $this->excel->getActiveSheet()->setCellValue('L4', 'OUT OF          AREA               OOA information will be ignored for Multi-site groups.');
        $this->excel->getActiveSheet()->setCellValue('M4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('N4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('O4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('P4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('Q4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('R4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('S4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('T4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('U4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('V4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('W4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('X4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('Y4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('Z4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AA4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('AB4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AC4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AD4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('AE4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AF4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AG4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('AH4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AI4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AJ4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('AK4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AL4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AM4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('AN4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AO4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AP4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->setCellValue('AQ4', '"AGE Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AR4', '"DOB (MM/DD/YYYY)Enter Age OR DOB"');
        $this->excel->getActiveSheet()->setCellValue('AS4', '"MEDICARE STATUS"');
        $this->excel->getActiveSheet()->getStyle('A1:Q100')->getFont()->setName('Arial');
        $this->excel->getActiveSheet()->getStyle('A1:Q100')->getFont()->setSize(10);

        $cnt=5;
        $no=1;
        foreach($detail as $detail)
        {
             $dob=array();
            $relation_detail = $this->Company_model->get_company_employee_relation($detail->employeeId);            $this->excel->getActiveSheet()->setCellValue('A'.$cnt, $no);
            $this->excel->getActiveSheet()->setCellValue('B'.$cnt, $detail->zipcode);
            $this->excel->getActiveSheet()->setCellValue('C'.$cnt, '1');
            $this->excel->getActiveSheet()->setCellValue('D'.$cnt, $detail->firstName);
            $this->excel->getActiveSheet()->setCellValue('E'.$cnt, $detail->lastName);
            if($detail->gender=='male' || $detail->gender=='Male')
            $this->excel->getActiveSheet()->setCellValue('F'.$cnt, 'M');
            else if($detail->gender=='female' || $detail->gender=='Female')
            $this->excel->getActiveSheet()->setCellValue('F'.$cnt, 'F');
            else
                $this->excel->getActiveSheet()->setCellValue('F' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('G' . $cnt, '');
            $this->excel->getActiveSheet()->setCellValue('H' . $cnt, date('m/d/Y', strtotime($detail->dateOfBirth)));
            $this->excel->getActiveSheet()->setCellValue('I' . $cnt, 'N');
            $this->excel->getActiveSheet()->setCellValue('J' . $cnt, 'A');
            if ($detail->salary == 0 || $detail->salary == '')
                $this->excel->getActiveSheet()->setCellValue('K' . $cnt, '');
            else
            $this->excel->getActiveSheet()->setCellValue('K'.$cnt, $detail->salary);
            $this->excel->getActiveSheet()->setCellValue('L'.$cnt, '');
            $this->excel->getActiveSheet()->setCellValue('M'.$cnt, '');
             
            if(!empty($relation_detail))
            {
              foreach($relation_detail as $rel_detail)
              {
                if($rel_detail->emp_relation=='Spouse' || $rel_detail->emp_relation=='spouse')
                {
                    
            $this->excel->getActiveSheet()->setCellValue('N'.$cnt, date('m/d/Y',strtotime($relation_detail->birth)));
            $this->excel->getActiveSheet()->setCellValue('O'.$cnt, 'N');
            $this->excel->getActiveSheet()->setCellValue('P'.$cnt, '');
                }
              }
             
              foreach($relation_detail as $rel_detail)
            {
                 if($rel_detail->emp_relation=='Child' || $rel_detail->emp_relation=='child')
                {
                     $dob[]=date('m/d/Y',strtotime($rel_detail->dateOfBirth));
                } 
              }
              if(!empty($dob[0]))
            {
            $this->excel->getActiveSheet()->setCellValue('Q'.$cnt, date('m/d/Y',strtotime($dob[0])));
            $this->excel->getActiveSheet()->setCellValue('R'.$cnt, 'N');
            }
            $this->excel->getActiveSheet()->setCellValue('V' . $cnt, '');
            if (!empty($dob[3])) {
                $this->excel->getActiveSheet()->setCellValue('W' . $cnt, date('m/d/Y', strtotime($dob[3])));
                $this->excel->getActiveSheet()->setCellValue('X' . $cnt, 'N');
            } else {
                $this->excel->getActiveSheet()->setCellValue('W' . $cnt, '');
                $this->excel->getActiveSheet()->setCellValue('X' . $cnt, '');
            }
            $this->excel->getActiveSheet()->setCellValue('S'.$cnt, '');
            if(!empty($dob[1]))
            {
            $this->excel->getActiveSheet()->setCellValue('T'.$cnt, date('m/d/Y',strtotime($dob[1])));
            $this->excel->getActiveSheet()->setCellValue('U'.$cnt, 'N');
            }
            $this->excel->getActiveSheet()->setCellValue('AB' . $cnt, '');
            if (!empty($dob[5])) {
                $this->excel->getActiveSheet()->setCellValue('AC' . $cnt, date('m/d/Y', strtotime($dob[5])));
                $this->excel->getActiveSheet()->setCellValue('AD' . $cnt, 'N');
            } else {
                $this->excel->getActiveSheet()->setCellValue('AC' . $cnt, '');
                $this->excel->getActiveSheet()->setCellValue('AD' . $cnt, '');
            }
            $this->excel->getActiveSheet()->setCellValue('V'.$cnt, '');
            if(!empty($dob[2]))
            {
            $this->excel->getActiveSheet()->setCellValue('W'.$cnt, date('m/d/Y',strtotime($dob[2])));
            $this->excel->getActiveSheet()->setCellValue('X'.$cnt, 'N');
            }
            $this->excel->getActiveSheet()->setCellValue('AH' . $cnt, '');
            if (!empty($dob[7])) {
                $this->excel->getActiveSheet()->setCellValue('AI' . $cnt, date('m/d/Y', strtotime($dob[7])));
                $this->excel->getActiveSheet()->setCellValue('AJ' . $cnt, 'N');
            } else {
                $this->excel->getActiveSheet()->setCellValue('AI' . $cnt, '');
                $this->excel->getActiveSheet()->setCellValue('AJ' . $cnt, '');
            }
            $this->excel->getActiveSheet()->setCellValue('Y'.$cnt, '');
            if(!empty($dob[3]))
            {
            $this->excel->getActiveSheet()->setCellValue('Z'.$cnt, date('m/d/Y',strtotime($dob[3])));
            $this->excel->getActiveSheet()->setCellValue('AA'.$cnt, 'N');
            }
            $this->excel->getActiveSheet()->setCellValue('AN' . $cnt, '');
            if (!empty($dob[9])) {
                $this->excel->getActiveSheet()->setCellValue('AO' . $cnt, date('m/d/Y', strtotime($dob[9])));
                $this->excel->getActiveSheet()->setCellValue('AP' . $cnt, 'N');
            } else {
                $this->excel->getActiveSheet()->setCellValue('AO' . $cnt, '');
                $this->excel->getActiveSheet()->setCellValue('AP' . $cnt, '');
            }
            $this->excel->getActiveSheet()->setCellValue('AB'.$cnt, '');
            if(!empty($dob[4]))
            {
            $this->excel->getActiveSheet()->setCellValue('AC'.$cnt, date('m/d/Y',strtotime($dob[4])));
            $this->excel->getActiveSheet()->setCellValue('AD'.$cnt, 'N');
            }
            else

            {
            $this->excel->getActiveSheet()->setCellValue('AC'.$cnt, '');    
            $this->excel->getActiveSheet()->setCellValue('AD'.$cnt, '');
            }
            $this->excel->getActiveSheet()->setCellValue('AE'.$cnt, '');
            if(!empty($dob[5]))
            {
            $this->excel->getActiveSheet()->setCellValue('AF'.$cnt, date('m/d/Y',strtotime($dob[5])));
            $this->excel->getActiveSheet()->setCellValue('AG'.$cnt, 'N');
            }
            else

            {
            $this->excel->getActiveSheet()->setCellValue('AF'.$cnt, '');    
            $this->excel->getActiveSheet()->setCellValue('AG'.$cnt, '');
            }
            $this->excel->getActiveSheet()->setCellValue('AH'.$cnt, '');
            if(!empty($dob[6]))
            {
            $this->excel->getActiveSheet()->setCellValue('AI'.$cnt, date('m/d/Y',strtotime($dob[6])));
            $this->excel->getActiveSheet()->setCellValue('AJ'.$cnt, 'N');
            }
            else

            {
            $this->excel->getActiveSheet()->setCellValue('AI'.$cnt, '');    
            $this->excel->getActiveSheet()->setCellValue('AJ'.$cnt, '');
            }
            $this->excel->getActiveSheet()->setCellValue('AK'.$cnt, '');
            if(!empty($dob[7]))
            {
            $this->excel->getActiveSheet()->setCellValue('AL'.$cnt, date('m/d/Y',strtotime($dob[7])));
            $this->excel->getActiveSheet()->setCellValue('AM'.$cnt, 'N');
            }
            else

            {
            $this->excel->getActiveSheet()->setCellValue('AL'.$cnt, '');    
            $this->excel->getActiveSheet()->setCellValue('AM'.$cnt, '');
            }
            $this->excel->getActiveSheet()->setCellValue('AN'.$cnt, '');
            if(!empty($dob[8]))
            {
            $this->excel->getActiveSheet()->setCellValue('AO'.$cnt, date('m/d/Y',strtotime($dob[8])));
            $this->excel->getActiveSheet()->setCellValue('AP'.$cnt, 'N');
            }
            else

            {
            $this->excel->getActiveSheet()->setCellValue('AO'.$cnt, '');    
            $this->excel->getActiveSheet()->setCellValue('AP'.$cnt, '');
            }
            $this->excel->getActiveSheet()->setCellValue('AQ'.$cnt, '');
            if(!empty($dob[9]))
            {
            $this->excel->getActiveSheet()->setCellValue('AR'.$cnt, date('m/d/Y',strtotime($dob[9])));
            $this->excel->getActiveSheet()->setCellValue('AS'.$cnt, 'N');
            }
            else

            {
            $this->excel->getActiveSheet()->setCellValue('AR'.$cnt, '');    
            $this->excel->getActiveSheet()->setCellValue('AS'.$cnt, '');
            }
              
            }
            $cnt++;
            $no++;
        }
      
        $data=array('quote_status'=>'Downloaded');
        $this->Company_model->update_company($data, $id);

        $filename = $comp_name_id . '.csv';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
        $query = 'File is being Downloaded';
        $queryName = "EXPORTED THROUGH UHC TEMPLATE";
        $fileName = $filename;
        $this->ion_auth->logFile($queryName, $query, $fileName, $id);
        
    }

    function limelight_export($id) {
        $comp_name = $this->Company_model->get_company($id);
        $comp_name_id = $comp_name . "_" . $id;
        $detail = $this->Company_model->get_company_employee($id);
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(340);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(66.75);
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(16);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(16.75);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(14.75);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(6.88);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(11.75);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(38.25);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(38.25);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(38.25);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(16.25);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(24.38);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(18.88);
        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
        $this->excel->getActiveSheet()->getStyle('A1:Q100')->getFont()->setName('Calibri');
        $this->excel->getActiveSheet()->getStyle('A1:Q100')->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle('A1:L2')->getAlignment()->setWrapText(true);
        $this->excel->setActiveSheetIndex(0)->mergeCells("A1:B1");
        $this->excel->setActiveSheetIndex(0)->mergeCells("C1:D1");
        $this->excel->getActiveSheet()->setCellValue('A1', 'Not Required but Recommended');
        $this->excel->getActiveSheet()->setCellValue('C1', 'Date of Birth - OR - Age is required for quoting all products');

        $this->excel->getActiveSheet()->setCellValue('E1', 'Required for quoting dental, vision, life');
        $this->excel->getActiveSheet()->setCellValue('F1', 'Required for quoting Medical');
        $this->excel->getActiveSheet()->setCellValue('G1', 'Required Status References:
EE = Employee Only
ES = Employee + Spouse
EC1 = Employee + 1 Child
EC2 = Employee + 2 Children (or more)
EF = Employee + Spouse + Child(ren)

CS = Covered Spouse
CD = Covered Dependent (Child)');
        $this->excel->getActiveSheet()->setCellValue('H1', 'Required Status References:
EE = Employee Only
ES = Employee + Spouse
EC1 = Employee + 1 Child
EC2 = Employee + 2 Children (or more)
EF = Employee + Spouse + Child(ren)

CS = Covered Spouse
CD = Covered Dependent (Child)');
        $this->excel->getActiveSheet()->setCellValue('I1', 'Required Status References:
EE = Employee Only
ES = Employee + Spouse
EC1 = Employee + 1 Child
EC2 = Employee + 2 Children (or more)
EF = Employee + Spouse + Child(ren)

CS = Covered Spouse
CD = Covered Dependent (Child)');
        $this->excel->getActiveSheet()->setCellValue('J1', 'Required for Life');
        $this->excel->getActiveSheet()->setCellValue('K1', 'Optional');

        $this->excel->getActiveSheet()->setCellValue('L1', 'Required for STD and LTD');

        $this->excel->getActiveSheet()->setCellValue('A2', 'First Name');
        $this->excel->getActiveSheet()->setCellValue('B2', 'Last Name');
        $this->excel->getActiveSheet()->setCellValue('C2', 'Date of Birth');
        $this->excel->getActiveSheet()->setCellValue('D2', 'Age');
        $this->excel->getActiveSheet()->setCellValue('E2', 'Gender');
        $this->excel->getActiveSheet()->setCellValue('F2', 'Employee Zip');
        $this->excel->getActiveSheet()->setCellValue('G2', 'Medical Coverage Status (EE, ES, EC1, EC2, EF, WE, CS, WS, CD, WD, NE, NS, ND)');
        $this->excel->getActiveSheet()->setCellValue('H2', 'Dental Coverage Status
(EE, ES, EC1, EC2, EF, WE, CS, WS, CD, WD, NE, NS, ND)');
        $this->excel->getActiveSheet()->setCellValue('I2', 'Vision Coverage Status
(EE, ES, EC1, EC2, EF, WE, CS, WS, CD, WD, NE, NS, ND)');
        $this->excel->getActiveSheet()->setCellValue('J2', 'Salary ');
        $this->excel->getActiveSheet()->setCellValue('K2', 'Salary Mode');
        $this->excel->getActiveSheet()->setCellValue('L2', 'Occupation');
        $this->excel->getActiveSheet()->getStyle('A1:L2')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1:L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1:L1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('99cc00');
        $this->excel->getActiveSheet()->getStyle('A1:L2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A2:L2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('009922');

        $cnt = 3;

        foreach ($detail as $detail) {
            $spouse_count=$this->Company_model->get_emp_relation_count($detail->employeeId,'Spouse');
            $child_count=$this->Company_model->get_emp_relation_count($detail->employeeId,'Child');
            $this->excel->getActiveSheet()->setCellValue('A'.$cnt, $detail->firstName);
            $this->excel->getActiveSheet()->setCellValue('B'.$cnt, $detail->lastName);
            $this->excel->getActiveSheet()->setCellValue('C'.$cnt, date('m/d/Y',strtotime($detail->dateOfBirth)));
            $this->excel->getActiveSheet()->setCellValue('D'.$cnt, '');
            $this->excel->getActiveSheet()->setCellValue('E'.$cnt, $detail->gender);
            $this->excel->getActiveSheet()->setCellValue('F'.$cnt, $detail->zipcode);
            if($spouse_count[0]->count=='0' && $child_count[0]->count=='0') 
            {
            $this->excel->getActiveSheet()->setCellValue('G'.$cnt, 'EE');
            $this->excel->getActiveSheet()->setCellValue('H'.$cnt, 'EE');
            $this->excel->getActiveSheet()->setCellValue('I'.$cnt, 'EE');
            }
            else if($spouse_count[0]->count > '0' && $child_count[0]->count=='0')
            {
            $this->excel->getActiveSheet()->setCellValue('G'.$cnt, 'ES');
            $this->excel->getActiveSheet()->setCellValue('H'.$cnt, 'ES');
            $this->excel->getActiveSheet()->setCellValue('I'.$cnt, 'ES');
            }
            else if($spouse_count[0]->count=='0' && $child_count[0]->count > '0')
            {   
                if($child_count[0]->count=='1')
                {
                $this->excel->getActiveSheet()->setCellValue('G'.$cnt, 'EC1');
                $this->excel->getActiveSheet()->setCellValue('H'.$cnt, 'EC1');
                $this->excel->getActiveSheet()->setCellValue('I'.$cnt, 'EC1');
                }
                else
                {
                $this->excel->getActiveSheet()->setCellValue('G'.$cnt, 'EC2');
                $this->excel->getActiveSheet()->setCellValue('H'.$cnt, 'EC2');
                $this->excel->getActiveSheet()->setCellValue('I'.$cnt, 'EC2');
                }
            }
            else if($spouse_count[0]->count >'0' && $child_count[0]->count > '0')
            {
            $this->excel->getActiveSheet()->setCellValue('G'.$cnt, 'EF');
            $this->excel->getActiveSheet()->setCellValue('H'.$cnt, 'EF');
            $this->excel->getActiveSheet()->setCellValue('I'.$cnt, 'EF');
            }
            else
            {
            $this->excel->getActiveSheet()->setCellValue('G'.$cnt, '');
            $this->excel->getActiveSheet()->setCellValue('H'.$cnt, '');
            $this->excel->getActiveSheet()->setCellValue('I'.$cnt, '');
            }
            if($detail->salary=='0' || $detail->salary=='')
            {
                $this->excel->getActiveSheet()->setCellValue('J'.$cnt, '');
            }
            else {
            $this->excel->getActiveSheet()->getStyle("J".$cnt)->getNumberFormat()->setFormatCode('$* #,##0_)');
            $this->excel->getActiveSheet()->setCellValue('J'.$cnt, $detail->salary);
            }    
            $this->excel->getActiveSheet()->getStyle('K'.$cnt)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->setCellValue('K'.$cnt, 'Annual');
            $this->excel->getActiveSheet()->setCellValue('L'.$cnt, $detail->occupation);
           $cnt++;
            $relation_detail=$this->Company_model->get_company_employee_relation($detail->employeeId); 
          
            if (!empty($relation_detail)) {
                foreach ($relation_detail as $rel_detail) {
                $this->excel->getActiveSheet()->setCellValue('A'.$cnt, $rel_detail->esFirstName);
                $this->excel->getActiveSheet()->setCellValue('B'.$cnt, $rel_detail->esLastName);
                $this->excel->getActiveSheet()->setCellValue('C'.$cnt, date('m/d/Y',strtotime($rel_detail->dateOfBirth)));
                $this->excel->getActiveSheet()->setCellValue('D'.$cnt, '');
                $this->excel->getActiveSheet()->setCellValue('E'.$cnt, $rel_detail->gender);
                $this->excel->getActiveSheet()->setCellValue('F'.$cnt, $detail->zipcode);
                if($rel_detail->emp_relation=='spouse' || $rel_detail->emp_relation=='Spouse')
                {
                $this->excel->getActiveSheet()->setCellValue('G'.$cnt, 'CS');
                $this->excel->getActiveSheet()->setCellValue('H'.$cnt, 'CS');
                $this->excel->getActiveSheet()->setCellValue('I'.$cnt, 'CS');
                }
                else
                {
                $this->excel->getActiveSheet()->setCellValue('G'.$cnt, 'CD');
                $this->excel->getActiveSheet()->setCellValue('H'.$cnt, 'CD');
                $this->excel->getActiveSheet()->setCellValue('I'.$cnt, 'CD');
                }
                $this->excel->getActiveSheet()->setCellValue('J'.$cnt, '');
                $this->excel->getActiveSheet()->setCellValue('K'.$cnt, '');
                $this->excel->getActiveSheet()->setCellValue('L'.$cnt, '');
                $cnt++;
               }
           }
        }

        $this->excel->getActiveSheet()->getStyle('A1:L'.($cnt-1))->applyFromArray($styleArray);
        
        $data=array('quote_status'=>'Downloaded');
        $this->Company_model->update_company($data, $id);
        $filename = $comp_name_id . '.xls';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
        $query = 'File is being Downloaded';
        $queryName = "EXPORTED THROUGH LIMELIGHT TEMPLATE";
        $fileName = $filename;
        $this->ion_auth->logFile($queryName, $query, $fileName, $id);
    }
    public function add_company_note($note,$cid,$uid)
    {
       $data=array('userId'=>$uid,
                    'companyId'=>$cid,
                    'note'=>urldecode($note),
                    'created_at'=>date('Y-m-d')
                    );
       $this->Company_model->add_company_note($data);
       echo json_encode($data);
       exit;
    }
    public function get_company_note($cid)
    {
        $company_note=$this->Company_model->get_company_note($cid,$this->data['user_login']['id']);
        echo json_encode($company_note);
        exit;
        
    }
    public function delete_note($id)
    {
        $this->Company_model->delete_note($id);
         redirect('admin/Company/company_list_view');
    }
    public function company_assign_employee($emp,$cid)
    {
       $data=array('userId'=>$emp,
                    'companyId'=>$cid);
       $this->Company_model->company_assign_employee($data,$cid);
       $data1=array('assign_status'=>'assign');
       $this->Company_model->update_company($data1,$cid);
       exit;
//redirect('admin/Company/company_list_view');
    }
    public function get_company_assign_employee($cid)
    {
        $company_employee=$this->Company_model->get_company_assign_employee($cid,$this->data['user_login']['id']);
        echo json_encode($company_employee);
        exit;
        
    }
    public function delete_file($id)
    {
        $this->Company_model->delete_file($id);
        exit;
    }

   function csv_split($line, $delim = ',', $removeQuotes = true)
    {
    #$line: the csv line to be split
    #$delim: the delimiter to split by
    #$removeQuotes: if this is false, the quotation marks won't be removed from the fields
        $fields = array();
        $fldCount = 0;
        $inQuotes = false;
        for ($i = 0; $i < strlen($line); $i++)
        {
            if (!isset($fields[$fldCount])) $fields[$fldCount] = "";
            $tmp = substr($line, $i, strlen($delim));
            if ($tmp === $delim && !$inQuotes)
            {
                $fldCount++;
                $i += strlen($delim) - 1;
}
            else if ($fields[$fldCount] == "" && $line[$i] == '"' && !$inQuotes)
            {
                if (!$removeQuotes) $fields[$fldCount] .= $line[$i];
                $inQuotes = true;
            }
            else if ($line[$i] == '"')
            {
                if ($line[$i + 1] == '"')
                {
                    $i++;
                    $fields[$fldCount] .= $line[$i];
                }
                else
                {
                    if (!$removeQuotes) $fields[$fldCount] .= $line[$i];
                    $inQuotes = false;
                }
            }
            else
            {
                $fields[$fldCount] .= $line[$i];
            }
        }
        return $fields;
    }



}

?>