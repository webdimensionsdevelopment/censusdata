<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Document extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->lang->load('admin/document');

        $this->load->helper(array('form', 'url'));
        /* Title Page :: Common */
        $this->page_title->push(lang('menu_document'));
        $this->data['pagetitle'] = $this->page_title->show();
        $this->load->model('admin/questionnaire_model');
        $this->load->model('admin/document_model');
        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_document'), 'admin/document');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Get all products */
            $this->data['documents'] = $this->document_model->get_all_documents();
            /* Load Template */
            $this->template->admin_render('admin/document/index', $this->data);
        }
    }

    public function create()
    {
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_document_create'), 'admin/document/create');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Variables */
        $tables = $this->config->item('tables', 'ion_auth');

        /* Validate form input */
        $this->form_validation->set_rules('document_name', 'lang:document_name', 'required');

        if ($this->form_validation->run() == TRUE) {
            $additional_data = array(
                'documentFile' => $this->input->post('document_name')
            );
            $insertedID = $this->document_model->document_insert($additional_data);
        }
        if ($this->form_validation->run() == TRUE && $insertedID) {
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect('admin/document', 'refresh');
        } else {
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['document_name'] = array(
                'name' => 'document_name',
                'id' => 'document_name',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('document_name'),
            );

            /* Load Template */
            $this->template->admin_render('admin/document/create', $this->data);
        }
    }

    public function edit($id)
    {
        $id = (int)$id;
        if (!$this->ion_auth->logged_in() OR (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id))) {
            redirect('auth', 'refresh');
        }

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_document_edit'), 'admin/document/edit');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Data */
        $documents['document'] = $this->document_model->get_document_by_id($id);
        /* Validate form input */
        $this->form_validation->set_rules('document_name', 'lang:document_name', 'required');
        $this->data['document'] = $documents;
        if (isset($_POST) && !empty($_POST)) {
            if ($this->form_validation->run() == TRUE) {
                $data = array(
                    'documentFile' => $this->input->post('document_name'),
                );

                if ($this->document_model->document_update($this->input->post('id'), $data)) {
                    $this->session->set_flashdata('message', $this->ion_auth->messages());

                    if ($this->ion_auth->is_admin()) {
                        redirect('admin/document', 'refresh');
                    } else {
                        redirect('admin', 'refresh');
                    }
                } else {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());

                    if ($this->ion_auth->is_admin()) {
                        redirect('auth', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }
                }
            }
        }

        // display the edit user form
        $this->data['csrf'] = $this->_get_csrf_nonce();

        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        // pass the user to the view

        $this->data['document_name'] = array(
            'name' => 'document_name',
            'id' => 'document_name',
            'type' => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('products_name', $documents['document']['documentFile']),
        );
        /* Load Template */
        $this->template->admin_render('admin/document/edit', $this->data);
    }

    public function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    function activate($id)
    {
        $id = (int)$id;
        $activation = $this->document_model->document_activate($id);
        if ($activation) ;
        redirect('admin/document', 'refresh');
    }

    public function deactivate($id = NULL)
    {
        $id = (int)$id;
        $deactivation = $this->document_model->document_deactivate($id);
        if ($deactivation) ;
        redirect('admin/document', 'refresh');
    }

    public function _valid_csrf_nonce()
    {
        if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE && $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
