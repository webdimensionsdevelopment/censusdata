<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class LogDetails extends Admin_Controller {

    public function __construct() {
        parent::__construct();

        /* Load :: Common */
        $this->lang->load('admin/users');

        /* Title Page :: Common */
        $this->page_title->push(lang('menu_logs'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_logs'), 'admin/logdetails');
    }

    public function index() {

        //        if (!$this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin()) {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Get all users */
            $this->data['users'] = $this->ion_auth->users()->result();

            foreach ($this->data['users'] as $k => $user) {
                $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
            }
            /* Load Template */
            $this->logsDetails();
        }
       
    }

    public function logsDetails() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Breadcrumbs */
            $this->breadcrumbs->unshift(2, lang('menu_logs'), 'admin/users/create');
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* Get all users */
            $this->data['users'] = $this->ion_auth->get_users_groupsLogs();
            foreach ($this->data['users'] as $k => $user) {
                $logValue = $this->ion_auth->get_last_user_log($user['id']);
                array_push($this->data['users'][$k], $logValue);
            }
            /* Load Template */
            $this->template->admin_render('admin/users/logsDetails', $this->data);
        }
    }
    public function logQueries($id) {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Breadcrumbs */
            $this->breadcrumbs->unshift(2, lang('menu_logs_Queries'), 'admin/users/create');
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* Get all users */
            $this->data['users'] = $this->ion_auth->get_user_Queries($id);
            $count = 0;
            foreach ($this->data['users'] as $k => $user) {
                $logValue = $this->ion_auth->get_user_activites($user['id']);
                $count = count($logValue);
                if($count>0){
                    foreach ($logValue as $j => $logRecords) {
                        $this->data['users'][$k]['logRecords'][$j] = $logRecords;
                    }
                }
                else{
                    $this->data['users'][$k]['logRecords'] = 0;
                }
            }
            /* Load Template */
            $this->template->admin_render('admin/users/logsQueries', $this->data);
        }
    }
}
