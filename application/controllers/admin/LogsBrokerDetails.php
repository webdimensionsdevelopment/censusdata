<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class LogsBrokerDetails extends Admin_Controller {

    public function __construct() {
        parent::__construct();

        /* Load :: Common */
        $this->lang->load('admin/users');
        /* Title Page :: Common */
        $this->page_title->push(lang('menu_broker_logs'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_broker_logs'), 'admin/logbrokerdetails');
    }

    public function index() {
       $this->logsBrokerDetails();
    }
    public function logsBrokerDetails() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Breadcrumbs */
            $this->breadcrumbs->unshift(2, lang('menu_broker_logs'), 'admin/users/create');
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $this->data['users'] = $this->ion_auth->get_all_broker_details();
            foreach ($this->data['users'] as $k => $user) {
                $logValue = $this->ion_auth->get_all_sorted_broker_details($user['npn']);
                $this->data['users'][$k]['company'] = $logValue;
            }
//            echo "<pre />"; print_r($this->data['users']);exit;
            /* Get all users */
            $this->template->admin_render('admin/users/logsBrokerDetails', $this->data);
        }
    }
    public function logBrokerQueries($id) {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Breadcrumbs */
            $this->breadcrumbs->unshift(2, lang('menu_broker_Queries'), 'admin/users/create');
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* Get all users */
            $this->data['users'] = $this->ion_auth->get_broker_queries($id);
            foreach ($this->data['users'] as $k => $user) {
                $logValue = $this->ion_auth->getDetailsQueries($user['companyId']);
                 array_push($this->data['users'][$k], $logValue);
            }
            /* Load Template */
//            echo "<pre/>";print_r($this->data['users'] );exit();
            $this->template->admin_render('admin/users/logBrokerQueries', $this->data);
        }
    }

}
