<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Questionnaire extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->lang->load('admin/questionnaire');

        $this->load->helper(array('form', 'url'));
        /* Title Page :: Common */
        $this->page_title->push(lang('menu_questionnaire'));
        $this->data['pagetitle'] = $this->page_title->show();
        $this->load->model('admin/questionnaire_model');
        $this->load->model('admin/product_model');
        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_questionnaire'), 'admin/questionnaire');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Get all products */
            $this->data['questions'] = $this->questionnaire_model->get_all_questions();
            foreach ($this->data['questions'] as $k => $question) {
                $this->data['questions'][$k]['product'] = $this->questionnaire_model->get_question_product($question['prod_id']);
            }
//            echo "<pre />"; print_r($this->data);exit;
            /* Load Template */
            $this->template->admin_render('admin/questionnaire/index', $this->data);
        }
    }

    public function create()
    {
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_questionnaire_create'), 'admin/questionnaire/create');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Variables */
        $tables = $this->config->item('tables', 'ion_auth');

        /* Validate form input */
        $this->form_validation->set_rules('products_name', 'lang:product_id', 'required');
        $this->form_validation->set_rules('questionnaire_question', 'lang:questionnaire_question', 'required');
        $this->form_validation->set_rules('questionnaire_selection', 'lang:questionnaire_selection', 'required');

        if ($this->form_validation->run() == TRUE) {
            $additional_data = array(
                'prod_id' => $this->input->post('products_name'),
                'question' => $this->input->post('questionnaire_question'),
                'question_desc' => $this->input->post('questionnaire_description'),
                'question_type' => $this->input->post('questionnaire_selection'),
            );
            $insertedID = $this->questionnaire_model->question_insert($additional_data);
        }
        if ($this->form_validation->run() == TRUE && $insertedID) {
            foreach ($_POST['questionnaire_answer'] as $answer) {
                $answer = trim($answer);
                $answerArray = array(
                    'question_id' => $insertedID,
                    'answer' => $answer
                );


                $addAnswer = true;
                if ($this->input->post('questionnaire_selection') == "single" || $this->input->post('questionnaire_selection') == "multiple")
                {
                    if (empty($answer))
                        $addAnswer = false;
                }

                if ($addAnswer == true)
                    $this->questionnaire_model->answer_insert($answerArray);
            }
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect('admin/questionnaire', 'refresh');
        } else {
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
            $result = $this->product_model->get_all_products();
            foreach ($result as $Index => $resultArray) {
                $optionArray[$resultArray['productId']] = $resultArray['name'];
            }
            $this->data['options'] = $optionArray;
            $this->data['products_name'] = array(
                'name' => 'products_name',
                'id' => 'products_name',
                'type' => 'products_name',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('products_name'),
            );
            $this->data['questionnaire_question'] = array(
                'name' => 'questionnaire_question',
                'id' => 'questionnaire_question',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('questionnaire_question'),
            );
            $this->data['questionnaire_description'] = array(
                'name' => 'questionnaire_description',
                'id' => 'questionnaire_description',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('questionnaire_description'),
            );
            $this->data['questionnaire_selection'] = array(
                'name' => 'questionnaire_selection',
                'id' => 'questionnaire_selection',
                'type' => 'questionnaire_selection',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('questionnaire_question'),
            );
            $optionArraySelection = array(
                'single' => 'Single Select',
                'multiple' => 'Multiple Select',
                'textbox' => 'Textbox'
            );
            $this->data['options_selection'] = $optionArraySelection;
            /* Load Template */
            $this->template->admin_render('admin/questionnaire/create', $this->data);
        }
    }

    public function delete($id)
    {
        //$this->questionnaire_model->questionnaire_answer_delete($id);
        redirect('admin/questionnaire', 'refresh');
        /* Load Template */
    }

    public function edit($id)
    {
        $id = (int)$id;
        if (!$this->ion_auth->logged_in() OR (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id))) {
            redirect('auth', 'refresh');
        }

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_products_edit'), 'admin/product/edit');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Data */
        $questions['question'] = $this->questionnaire_model->get_questions_by_id($id);
        $questions['answer'] = $this->questionnaire_model->get_questions_answer_by_question_id($id);
        /* Validate form input */
        $this->form_validation->set_rules('products_name', 'lang:edit_product_validation_pname_label', 'required');

        if (isset($_POST) && !empty($_POST)) {

//            if ($this->_valid_csrf_nonce() === FALSE OR $id != $this->input->post('id')) {
//                show_error($this->lang->line('error_csrf'));
//            }

            if ($this->form_validation->run() == TRUE) {
                $data = array(
                    'prod_id' => $this->input->post('products_name'),
                    'question' => $this->input->post('questionnaire_question'),
                    'question_desc' => $this->input->post('questionnaire_description'),
                    'question_type' => $this->input->post('questionnaire_selection'),
                );

                if ($this->questionnaire_model->questionnaire_update($this->input->post('id'), $data)) {


                    foreach ($_POST['questionnaire_answer'] as $index => $answer) {
                        $answer = trim($answer);
                        $answerArray = array(
                            'question_id' => $this->input->post('id'),
                            'answer' => $answer
                        );
                        $answerResult = $this->questionnaire_model->answer_update($index, $this->input->post('id'), $answerArray);

                        $addAnswer = true;
                        if ($this->input->post('questionnaire_selection') == "single" || $this->input->post('questionnaire_selection') == "multiple")
                        {
                            if (empty($answer))
                                $addAnswer = false;
                        }

                        if ($answerResult == '0' && $addAnswer == true)
                            $this->questionnaire_model->answer_insert($answerArray);

                        // Check if single or multiple select
                        if ($this->input->post('questionnaire_selection') == "single" || $this->input->post('questionnaire_selection') == "multiple")
                        {
                            // If answer is empty then delete the answer
                           //echo "Answer update: $answer I: $index #" . $this->input->post('id');
                            if (empty($answer) && $answerResult != 0)
                                $this->questionnaire_model->questionnaire_answer_delete($index, $this->input->post('id'));
                        }



                    }

                    // Delete any answers that are empty for single and multiselect
                    if ($this->input->post('questionnaire_selection') == "single" || $this->input->post('questionnaire_selection') == "multiple")
                    {
                        $this->questionnaire_model->questionnaire_delete_empty_answers($this->input->post('id'));
                    }


                    $this->session->set_flashdata('message', $this->ion_auth->messages());

                    if ($this->ion_auth->is_admin()) {
                        redirect('admin/questionnaire', 'refresh');
                    } else {
                        redirect('admin', 'refresh');
                    }
                } else {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());

                    if ($this->ion_auth->is_admin()) {
                        redirect('auth', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }
                }
            }
        }

        // display the edit user form
        $this->data['csrf'] = $this->_get_csrf_nonce();

        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        // pass the user to the view
        $this->data['question'] = $questions;
        $result = $this->product_model->get_all_products();
        foreach ($result as $Index => $resultArray) {
            $optionArray[$resultArray['productId']] = $resultArray['name'];
        }
        $this->data['options'] = $optionArray;
        $this->data['products_name'] = array(
            'name' => 'products_name',
            'id' => 'products_name',
            'type' => 'products_name',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('products_name', $questions['question']['prod_id']),
        );
        $this->data['questionnaire_question'] = array(
            'name' => 'questionnaire_question',
            'id' => 'questionnaire_question',
            'type' => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('questionnaire_question', $questions['question']['question']),
        );
        $this->data['questionnaire_description'] = array(
            'name' => 'questionnaire_description',
            'id' => 'questionnaire_description',
            'type' => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('questionnaire_description',$questions['question']['question_desc']),
        );
        $this->data['questionnaire_selection'] = array(
            'name' => 'questionnaire_selection',
            'id' => 'questionnaire_selection',
            'type' => 'questionnaire_selection',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('questionnaire_question'),
        );
        $optionArraySelection = array(
            'single' => 'Single Select',
            'multiple' => 'Multiple Select',
            'textbox' => 'Textbox'
        );
        $this->data['options_selection'] = $optionArraySelection;

//        echo "<pre />"; print_r($this->data); exit;
        /* Load Template */
        $this->template->admin_render('admin/questionnaire/edit', $this->data);
    }

    public function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    function activate($id)
    {
        $id = (int)$id;
        $activation = $this->questionnaire_model->questionnaire_activate($id);
        if ($activation) ;
        redirect('admin/questionnaire', 'refresh');
    }

    public function deactivate($id = NULL)
    {
        $id = (int)$id;
        $deactivation = $this->questionnaire_model->questionnaire_deactivate($id);
        if ($deactivation) ;
        redirect('admin/questionnaire', 'refresh');
    }

    public function profile($id)
    {
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_users_profile'), 'admin/groups/profile');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Data */
        $id = (int)$id;

        $this->data['user_info'] = $this->ion_auth->user($id)->result();
        foreach ($this->data['user_info'] as $k => $user) {
            $this->data['user_info'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
        }

        /* Load Template */
        $this->template->admin_render('admin/users/profile', $this->data);
    }

    public function _valid_csrf_nonce()
    {
        if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE && $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
