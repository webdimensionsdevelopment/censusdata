<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Quoterequestlog extends Admin_Controller {

    public function __construct() {
        parent::__construct();

        /* Load :: Common */
        $this->lang->load('admin/users');

        /* Title Page :: Common */
        $this->page_title->push(lang('menu_logs'));
        $this->data['pagetitle'] = $this->page_title->show();
        $this->load->model('admin/Company_model');
        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_logs'), 'admin/quoterequestlog');
    }

    public function index() {
            $this->quoterequestdetails();
    }
    public function quoterequestdetails() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Breadcrumbs */
            $this->breadcrumbs->unshift(2, lang('menu_quoteRequest_Log'), 'admin/Quoterequestlog');
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* Get all users */
            $this->data['company'] = $this->Company_model->get_all_company();
            foreach ($this->data['company'] as $k => $company) {
                $logValue = $this->Company_model->get_last_company_log($company['companyId']);
                array_push($this->data['company'][$k], $logValue);
            }
//            echo "<pre />"; print_r($this->data['company']);exit;
            /* Load Template */
            $this->template->admin_render('admin/companylog/logsDetails', $this->data);
        }
    }
    public function logQueries($id) {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Breadcrumbs */
            $this->breadcrumbs->unshift(2, lang('menu_quoteRequest_Queries'), 'admin/Quoterequestlog');
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* Get all users */
//            $this->data['users'] = $this->ion_auth->get_userLog_byCompanyId($id);
            $this->data['users'] = $this->ion_auth->get_companyDetailsById($id);
            $this->data['userActivity'] = $this->ion_auth->get_userLog_byCompanyId($this->data['users']['companyId']);
            /* Load Template */
            $this->template->admin_render('admin/companylog/logsQueries', $this->data);
//            echo "<pre/>";print_r($this->data);exit;
            
            
        }
    }
}
