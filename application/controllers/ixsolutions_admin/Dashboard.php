<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Ixsolutions_admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->helper('number');
        $this->load->model('ixsolution_admin/dashboard_model');
    }


	public function index()
	{
        //        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        if ( ! $this->ixsolution_ion_auth->logged_in())
        {
            redirect('ixsolutions_auth/login', 'refresh');
        }
        else
        {
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Data */
            $this->data['count_users']          = $this->dashboard_model->get_count_record('users');
            $this->data['count_groups']         = $this->dashboard_model->get_count_record('groups');
            $this->data['disk_totalspace']      = $this->dashboard_model->disk_totalspace(DIRECTORY_SEPARATOR);
            $this->data['disk_freespace']       = $this->dashboard_model->disk_freespace(DIRECTORY_SEPARATOR);
            $this->data['disk_usespace']        = $this->data['disk_totalspace'] - $this->data['disk_freespace'];
            $this->data['disk_usepercent']      = $this->dashboard_model->disk_usepercent(DIRECTORY_SEPARATOR, FALSE);
            $this->data['memory_usage']         = $this->dashboard_model->memory_usage();
            $this->data['memory_peak_usage']    = $this->dashboard_model->memory_peak_usage(TRUE);
            $this->data['memory_usepercent']    = $this->dashboard_model->memory_usepercent(TRUE, FALSE);
            $this->data['noOfCompany']          = $this->dashboard_model->get_no_of_companies();
            $this->data['assignedQuote']        = $this->dashboard_model->get_no_of_quote_assigned();
            $this->data['unassignedQuote']      = $this->dashboard_model->get_no_of_quote_unassigned();
            $this->data['quoteStatus']          = $this->dashboard_model->get_no_of_QuoteStatus();

            /* TEST */
            $this->data['url_exist']    = is_url_exist('http://www.domprojects.com');
            /* Load Template */
            $this->ixsolutions_template->admin_render('ixsolutions_admin/dashboard/index', $this->data);
        }
	}
}
