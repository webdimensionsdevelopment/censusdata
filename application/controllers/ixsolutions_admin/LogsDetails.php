<?php defined('BASEPATH') OR exit('No direct script access allowed');
class LogsDetails extends Ixsolutions_admin_Controller {
    public function __construct() {
        parent::__construct();
        /* Load :: Common */
        $this->lang->load('ixsolutions_admin/users');
        $this->load->model('ixsolution_admin/user_model');
        /* Title Page :: Common */
        $this->page_title->push(lang('menu_users'));
        $this->data['pagetitle'] = $this->page_title->show();
        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_users'), 'ixsolutions_admin/users');
    }
    public function index() {
//        if (!$this->ixsolution_ion_auth->logged_in() OR ! $this->ixsolution_ion_auth->logged_in()) {
        if (!$this->ixsolution_ion_auth->logged_in()) {
            redirect('ixsolutions_auth/login', 'refresh');
        } else {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Get all users */
            $this->data['users'] = $this->ixsolution_ion_auth->users()->result();

            foreach ($this->data['users'] as $k => $user) {
                $this->data['users'][$k]->groups = $this->ixsolution_ion_auth->get_users_groups($user->id)->result();
            }
            /* Load Template */
            $this->logsDetails();
        }
    }
    public function logsDetails() {
        if (!$this->ixsolution_ion_auth->logged_in()) {
            redirect('ixsolutions_auth/login', 'refresh');
        } else {
            /* Breadcrumbs */
            $this->breadcrumbs->unshift(2, lang('menu_logs'), 'admin/users/create');
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* Get all users */
            $userId = $this->ixsolution_ion_auth->get_user_id();
            if($this->ixsolution_ion_auth->is_brokerage($userId)){
                $this->data['brokers'] = $this->ixsolution_ion_auth->get_AllUserIds_of_Brokers();
                $this->data['companies'] = $this->ixsolution_ion_auth->get_AllUserIds_of_Companies();
                if($this->data['companies']){
                    foreach($this->data['companies'] as $key=>$logDetails){
                        if($this->ixsolution_ion_auth->getUserLogsByUserId($logDetails['userId'])){
                            $this->data['companies'][$key]['logDetails'] = $this->ixsolution_ion_auth->getUserLogsByUserId($logDetails['userId']);
                        }
                        else {
                            $this->data['companies'][$key]['logDetails'] =0;
                        }
                    }
                }
            }
            $this->data['brokers'] = array();
            $this->data['companies'] = array();
            /* Load Template */ //echo "<pre/>";print_r($this->data['users']);exit;
            $this->ixsolutions_template->admin_render('ixsolutions_admin/users/logsDetails', $this->data);
        }
    }
    public function companiesLog($id = null){
        $brokerId = '';
        if($id){
            $brokerId = $id;
        }
        else{
            $userId = $this->ixsolution_ion_auth->get_user_id();
            $brokerIdArray = $this->ixsolution_ion_auth->get_brokerId_by_UserId($userId);
            $brokerId = $brokerIdArray['brokerId'];
        }
        if (!$this->ixsolution_ion_auth->logged_in()) {
            redirect('ixsolutions_auth/login', 'refresh');
        }else {
            /* Breadcrumbs */
            $this->breadcrumbs->unshift(2, lang('company_logs'), 'ixsolutions_admin/users/logsDetails');
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            $this->data['companies'] = $this->ixsolution_ion_auth->getAllCompaniesLogById($brokerId);
            $this->ixsolutions_template->admin_render('ixsolutions_admin/users/companiesLog', $this->data);
        }
    }
    public function logQueries($id) {
        if (!$this->ixsolution_ion_auth->logged_in()) {
            redirect('ixsolutions_auth/login', 'refresh');
        } else {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* Get all users */
            $this->data['users'] = $this->ixsolution_ion_auth->get_user_Queries($id);
            $this->data['logRecords'] = $this->ixsolution_ion_auth->get_user_activites($this->data['users']['companyId']);
            
            /* Load Template //echo "<pre/>";print_r($this->data['logRecords']);exit; */
            $this->ixsolutions_template->admin_render('ixsolutions_admin/users/logsQueries', $this->data);
        }
    }
}
?>