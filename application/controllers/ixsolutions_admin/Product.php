<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends Ixsolutions_admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->lang->load('ixsolutions_admin/product');

        $this->load->helper(array('form', 'url'));
        /* Title Page :: Common */
        $this->page_title->push(lang('menu_products'));
        $this->data['pagetitle'] = $this->page_title->show();
        $this->load->model('ixsolution_admin/product_model');
        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_products'), 'ixsolution_admin/product');
    }

    public function index()
    {
        if (!$this->ixsolution_ion_auth->logged_in()) {
            redirect('ixsolutions_auth/login', 'refresh');
        } else {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Get all products */
            $this->data['products'] = $this->product_model->get_all_products();
            /* Load Template */
            $this->ixsolutions_template->admin_render('ixsolutions_admin/product/index', $this->data);
        }
    }

    public function create()
    {
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_products_create'), 'ixsolution_admin/product/create');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Variables */
        $tables = $this->config->item('tables', 'ion_auth');

        /* Validate form input */
        $this->form_validation->set_rules('products_name', 'lang:products_name', 'required');
        $this->form_validation->set_rules('product_description', 'lang:product_description', 'required');

        if ($this->form_validation->run() == TRUE) {
            $image = $_FILES['product_image'];
            if ($image) {
                $path = $this->config->config['folderPath']."ixsolution_admin/products";
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $productPath = $this->config->config['folderPath']."ixsolution_admin/products/". $this->input->post('products_name') . "/";
                if (!file_exists($productPath)) {
                    mkdir($productPath, 0777, true);
                }
                $image = $this->ixsolution_ion_auth->uploadFile($image, $productPath, $this->input->post('products_name'));
            }
            $additional_data = array(
                'name' => $this->input->post('products_name'),
                'description' => $this->input->post('product_description'),
                'occupation' => $this->input->post('occupation'),
                'salary' => $this->input->post('salary'),
                'productImage' => $image
            );
            $insertedID = $this->product_model->product_insert($additional_data);
        }
        if ($this->form_validation->run() == TRUE && $insertedID) {

            $this->session->set_flashdata('message', $this->ixsolution_ion_auth->messages());
            redirect('ixsolutions_admin/product', 'refresh');
        } else {
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ixsolution_ion_auth->errors() ? $this->ixsolution_ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['products_name'] = array(
                'name' => 'products_name',
                'id' => 'products_name',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('products_name'),
            );
            $this->data['product_description'] = array(
                'name' => 'product_description',
                'id' => 'product_description',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('product_description'),
            );
            $this->data['product_image'] = array(
                'name' => 'product_image',
                'id' => 'product_image',
                'type' => 'file',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('product_image'),
            );
            $this->data['occupation'] = array(
                'name' => 'occupation',
                'id' => 'occupation',
                'type' => 'occupation',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('occupation'),
            );
            $this->data['salary'] = array(
                'name' => 'salary',
                'id' => 'salary',
                'type' => 'salary',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('salary'),
            );
            $optionArraySelection = array(
                '0' => 'No',
                '1' => 'Yes'
            );
            $this->data['options_selection'] = $optionArraySelection;
            /* Load Template */
            $this->ixsolutions_template->admin_render('ixsolutions_admin/product/create', $this->data);
        }
    }

    public function delete($id)
    {
        $this->product_model->product_delete($id);
        redirect('ixsolution_admin/product', 'refresh');
        /* Load Template */
        $this->ixsolutions_template->admin_render('ixsolutions_admin/users/delete', $this->data);
    }

    public function edit($id)
    {
        $id = (int)$id;
        if (!$this->ixsolution_ion_auth->logged_in()) {
            redirect('auth', 'refresh');
        }

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_products_edit'), 'ixsolution_admin/product/edit');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Data */
        $product = $this->product_model->get_product_by_id($id);

        /* Validate form input */
        $this->form_validation->set_rules('product_name', 'lang:edit_product_validation_pname_label', 'required');

        if (isset($_POST) && !empty($_POST)) {
            if ($this->_valid_csrf_nonce() === FALSE OR $id != $this->input->post('id')) {
                show_error($this->lang->line('error_csrf'));
            }

            if ($this->form_validation->run() == TRUE) {
                $image = $_FILES['product_image'];
                if ($image) {
                    $path = $this->config->config['folderPath']."ixsolution_admin/products";
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
                    $productPath = $this->config->config['folderPath']."ixsolution_admin/products/". $_POST['product_name'] . "/";
                    if (!file_exists($productPath)) {
                        mkdir($productPath, 0777, true);
                    }
                    $image = $this->ixsolution_ion_auth->uploadFile($image, $productPath, $_POST['product_name']);
                }
                $data = array(
                    'name' => $this->input->post('product_name'),
                    'description' => $this->input->post('product_description'),
                    'occupation' => $this->input->post('occupation'),
                    'salary' => $this->input->post('salary'),
                    'productImage' => $image
                );

                if ($this->product_model->product_update($this->input->post('id'), $data)) {
                    
                    $this->session->set_flashdata('message', $this->ixsolution_ion_auth->messages());

                    if ($this->ixsolution_ion_auth->is_admin()) {
                        redirect('ixsolutions_admin/product', 'refresh');
                    } else {
                        redirect('ixsolutions_admin/product', 'refresh');
                    }
                } else {
                    $this->session->set_flashdata('message', $this->ixsolution_ion_auth->errors());

                    if ($this->ixsolution_ion_auth->is_admin()) {
                        redirect('auth', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }
                }
            }
        }

        // display the edit user form
        $this->data['csrf'] = $this->_get_csrf_nonce();

        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ixsolution_ion_auth->errors() ? $this->ixsolution_ion_auth->errors() : $this->session->flashdata('message')));

        // pass the user to the view
        $this->data['product'] = $product;

        $this->data['product_name'] = array(
            'name' => 'product_name',
            'id' => 'product_name',
            'type' => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('product_name', $product['name'])
        );
        $this->data['product_description'] = array(
            'name' => 'product_description',
            'id' => 'product_description',
            'type' => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('product_description', $product['description'])
        );
        $this->data['product_image'] = array(
            'name' => 'product_image',
            'id' => 'product_image',
            'type' => 'file',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('product_image'),
        );
        $this->data['occupation'] = array(
            'name' => 'occupation',
            'id' => 'occupation',
            'type' => 'occupation',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('occupation', $product['occupation']),
        );
        $this->data['salary'] = array(
            'name' => 'salary',
            'id' => 'salary',
            'type' => 'salary',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('salary', $product['salary']),
        );
        $optionArraySelection = array(
            '0' => 'No',
            '1' => 'Yes'
        );
        $this->data['options_selection'] = $optionArraySelection;
        /* Load Template */
        $this->ixsolutions_template->admin_render('ixsolutions_admin/product/edit', $this->data);
    }

    function activate($id)
    {
        $id = (int)$id;
        $activation = $this->product_model->product_activate($id);
        if ($activation) ;
        redirect('ixsolutions_admin/product', 'refresh');
    }

    public function deactivate($id = NULL)
    {
        $id = (int)$id;
        $deactivation = $this->product_model->product_deactivate($id);
        if ($deactivation) ;
        redirect('ixsolutions_admin/product', 'refresh');
    }

    public function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    public function _valid_csrf_nonce()
    {
        if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE && $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
