<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Ixsolutions_admin_Controller {

    public function __construct() {
        parent::__construct();

        /* Load :: Common */
        $this->lang->load('ixsolutions_admin/users');
        $this->load->model('ixsolution_admin/user_model');

        /* Title Page :: Common */
        $this->page_title->push(lang('menu_users'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_users'), 'ixsolutions_admin/users');
    }

    public function index() {
//        if (!$this->ixsolution_ion_auth->logged_in() OR ! $this->ixsolution_ion_auth->logged_in()) {
        if (!$this->ixsolution_ion_auth->logged_in()) {
            redirect('ixsolutions_auth/login', 'refresh');
        } else {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Get all users */
            $this->data['users'] = $this->ixsolution_ion_auth->users()->result();

            foreach ($this->data['users'] as $k => $user) {
                $this->data['users'][$k]->groups = $this->ixsolution_ion_auth->get_users_groups($user->id)->result();
            }
            /* Load Template */
            $this->ixsolutions_template->admin_render('ixsolutions_admin/users/index', $this->data);
        }
    }
    public function create() {
        /* Breadcrumbs */
        $registeredID = $memberType = '';
        $this->breadcrumbs->unshift(2, lang('menu_users_create'), 'ixsolutions_admin/users/create');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();
        /* Variables */
        $tables = $this->config->item('tables', 'ixsolutions_ion_auth');
        /* Validate form input */
        $this->form_validation->set_rules('first_name', 'lang:users_firstname', 'required');
        $this->form_validation->set_rules('last_name', 'lang:users_lastname', 'required');
        $this->form_validation->set_rules('email', 'lang:users_email', 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        if ($this->form_validation->run() == TRUE) {
            $username = strtolower($this->input->post('first_name')) . ' ' . strtolower($this->input->post('last_name'));
            $email = strtolower($this->input->post('email'));
            $memberType = $this->input->post('membertype');
//            $password = $this->input->post('password');
            $password = '';
            $activationCode = md5(rand(1, 10000000000) . "fhsf" . rand(1, 100000));
            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'company' => '',
                'phone' => '',
            );
            $record = array();
            $registeredID = $this->ixsolution_ion_auth->register($username, $memberType, $password, $activationCode, $email, $additional_data, $record);
        }
        if ($registeredID) {
            if ($memberType == 'brokerage') {
                $records = array('userId' => $registeredID);
                $this->user_model->addToBrokerageUserId($records);
            } else if ($memberType == 'broker') {
                $records = array('userId' => $registeredID);
                $this->user_model->addToBrokerUserId($records);
            }
            $this->session->set_flashdata('message', $this->ixsolution_ion_auth->messages());
            redirect('ixsolutions_admin/users', 'refresh');
        } else {
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ixsolution_ion_auth->errors() ? $this->ixsolution_ion_auth->errors() : $this->session->flashdata('message')));
            $this->data['first_name'] = array(
                'name' => 'first_name',
                'id' => 'first_name',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $this->data['last_name'] = array(
                'name' => 'last_name',
                'id' => 'last_name',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $this->data['email'] = array(
                'name' => 'email',
                'id' => 'email',
                'type' => 'email',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('email'),
            );
            $this->data['membertype'] = array(
                'name' => 'membertype',
                'id' => 'membertype',
                'type' => 'membertype',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('membertype'),
            );
            $this->load->model('ixsolution_admin/preferences_model');
            $result = $this->preferences_model->get_interface('groups');
            foreach ($result as $Index => $resultArray) {
                $optionArray[$resultArray['name']] = $resultArray['name'];
            }
            $this->data['options'] = $optionArray;
            /* Load Template */
            $this->ixsolutions_template->admin_render('ixsolutions_admin/users/create', $this->data);
        }
    }

    public function register_user() {
        $error = false;
        $registeredID = null;
        $memberType = '';
        if (isset($_POST['submit'])) {
            if (empty($_POST['txtEmail'])) {
                $error = true;
                $this->data['firstName'] = "Enter Email";
            }
            $email = $this->user_model->checkEmail($_POST['txtEmail']);
            if (empty($email)) {
                $username = $_POST['txtFirstName'] . ' ' . $_POST['txtLastName'];
                $email = $_POST['txtEmail'];
                if (empty($_POST['txtFirstName'])) {
                    $error = true;
                    $this->data['firstName'] = "Enter First Name";
                }
                if (empty($_POST['txtLastName'])) {
                    $error = true;
                    $this->data['lastName'] = "Enter Last Name";
                }
                if (empty($_POST['txtPassword'])) {
                    $error = true;
                    $this->data['password'] = "Enter password";
                }
                if (empty($_POST['txtConfirmPassword'])) {
                    $error = true;
                    $this->data['confirmPassword'] = "Enter Confirm Password";
                }
                if (strcasecmp($_POST['txtPassword'], $_POST['txtConfirmPassword']) == 1) {
                    $error = true;
                    $this->data['matchPassword'] = "Enter password did not match";
                }
                $gender = $_POST['rdGender'];
                $activationCode = md5(rand(1, 10000000000) . "fhsf" . rand(1, 100000));
                $memberType = $_POST['options'];
                $firstName = $_POST['txtFirstName'];
                $FImage = $_FILES['image'];
                $insertedId = $this->ixsolution_ion_auth->getLastInsertedId();
                $newUserId = $insertedId['id'] + 1;
                if ($_FILES && $_FILES['image']) {
                    $path = $this->config->config['folderPathIx'] . $memberType;
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
                    $userFolderName = $firstName . $newUserId;
                    $userPath = $this->config->config['folderPathIx'] . $memberType . "/" . $userFolderName . "/";
                    if (!file_exists($userPath)) {
                        mkdir($userPath, 0777, true);
                    }
                    $FImage = $this->ixsolution_ion_auth->uploadFile($_FILES['image'], $userPath, $userFolderName);
                }
                $additional_data = array(
                    'first_name' => $_POST['txtFirstName'],
                    'last_name' => $_POST['txtLastName'],
                    'username' => $_POST['txtEmail'],
                    'image' => $FImage,
                    'gender' => $_POST['rdGender'],
                    'phone_number' => $_POST['txtPhoneNumber'],
                    'address' => $_POST['txtAddress'],
                    'zipcode' => $_POST['txtZipCode'],
                    'country' => $_POST['txtCountry'],
                    'state' => $_POST['txtState'],
                    'city' => $_POST['txtCity']
                );
                $record = array();
                $password = $_POST['txtPassword'];
                $memberType = $_POST['options'];
                $registeredID = $this->ixsolution_ion_auth->register($username, $memberType, $password, $activationCode, $email, $additional_data, $record = '');
            }
        }
        if ($error == false && $registeredID) {
            if ($memberType == 'brokerage') {
                $records = array('userId' => $registeredID);
                $brokerageId = $this->user_model->addToBrokerageUserId($records);
            } else if ($memberType == 'broker') {
                $records = array('userId', $registeredID);
                $brokerId = $this->user_model->addToBrokerUserId($records);
            }
            $this->session->set_flashdata('message', $this->ixsolution_ion_auth->messages());
            redirect('ixsolutions_admin/users', 'refresh');
        } else {
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ixsolution_ion_auth->errors() ? $this->ixsolution_ion_auth->errors() : $this->session->flashdata('message')));
            $this->load->model('ixsolution_admin/preferences_model');
            $result = $this->preferences_model->get_interface('groups');
            foreach ($result as $Index => $resultArray) {
                $optionArray[$resultArray['name']] = $resultArray['name'];
            }
            $this->data['options'] = $optionArray;
            /* Load Template */
            redirect(base_url() . 'register', $this->data);
        }
    }

    public function delete() {
        /* Load Template */
        $this->ixsolutions_template->admin_render('ixsolutions_admin/users/delete', $this->data);
    }

    public function edit($id) {
        $id = (int) $id;

        if (!$this->ixsolution_ion_auth->logged_in()) {
            redirect('auth', 'refresh');
        }

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_users_edit'), 'ixsolutions_admin/users/edit');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Data */
        $user = $this->ixsolution_ion_auth->user($id)->row();
        $groups = $this->ixsolution_ion_auth->groups()->result_array();
        $currentGroups = $this->ixsolution_ion_auth->get_users_groups($id)->result();

        /* Validate form input */
        $this->form_validation->set_rules('first_name', 'lang:edit_user_validation_fname_label', 'required');
        $this->form_validation->set_rules('last_name', 'lang:edit_user_validation_lname_label', 'required');
        $this->form_validation->set_rules('phone', 'lang:edit_user_validation_phone_label', 'required');
        $this->form_validation->set_rules('company', 'lang:edit_user_validation_company_label', 'required');

        if (isset($_POST) && !empty($_POST)) {
            if ($this->_valid_csrf_nonce() === FALSE OR $id != $this->input->post('id')) {
                show_error($this->lang->line('error_csrf'));
            }

            if ($this->input->post('password')) {
                $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ixsolution_ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ixsolution_ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
            }

            if ($this->form_validation->run() == TRUE) {
                $data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'company' => $this->input->post('company'),
                    'phone' => $this->input->post('phone')
                );

                if ($this->input->post('password')) {
                    $data['password'] = $this->input->post('password');
                }

                if ($this->ixsolution_ion_auth->logged_in()) {
                    $groupData = $this->input->post('groups');

                    if (isset($groupData) && !empty($groupData)) {
                        $this->ixsolution_ion_auth->remove_from_group('', $id);

                        foreach ($groupData as $grp) {
                            $this->ixsolution_ion_auth->add_to_group($grp, $id);
                        }
                    }
                }

                if ($this->ixsolution_ion_auth->update($user->id, $data)) {
                    $this->session->set_flashdata('message', $this->ixsolution_ion_auth->messages());

                    if ($this->ixsolution_ion_auth->logged_in()) {
                        redirect('ixsolutions_admin/users', 'refresh');
                    } else {
                        redirect('admin', 'refresh');
                    }
                } else {
                    $this->session->set_flashdata('message', $this->ixsolution_ion_auth->errors());

                    if ($this->ixsolution_ion_auth->logged_in()) {
                        redirect('auth', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }
                }
            }
        }

        // display the edit user form
        $this->data['csrf'] = $this->_get_csrf_nonce();

        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ixsolution_ion_auth->errors() ? $this->ixsolution_ion_auth->errors() : $this->session->flashdata('message')));

        // pass the user to the view
        $this->data['user'] = $user;
        $this->data['groups'] = $groups;
        $this->data['currentGroups'] = $currentGroups;

        $this->data['first_name'] = array(
            'name' => 'first_name',
            'id' => 'first_name',
            'type' => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('first_name', $user->first_name)
        );
        $this->data['last_name'] = array(
            'name' => 'last_name',
            'id' => 'last_name',
            'type' => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('last_name', $user->last_name)
        );
        $this->data['company'] = array(
            'name' => 'company',
            'id' => 'company',
            'type' => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('company', $user->company)
        );
        $this->data['phone'] = array(
            'name' => 'phone',
            'id' => 'phone',
            'type' => 'tel',
            'pattern' => '^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('phone', $user->phone)
        );
        $this->data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'class' => 'form-control',
            'type' => 'password'
        );
        $this->data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id' => 'password_confirm',
            'class' => 'form-control',
            'type' => 'password'
        );


        /* Load Template */
        $this->ixsolutions_template->admin_render('ixsolutions_admin/users/edit', $this->data);
    }

    function activate($id, $code = FALSE) {
        $id = (int) $id;

        if ($code !== FALSE) {
            $activation = $this->ixsolution_ion_auth->activate($id, $code);
        } else if ($this->ixsolution_ion_auth->logged_in()) {
            $activation = $this->ixsolution_ion_auth->activate($id);
        }

        if ($activation) {
            $this->session->set_flashdata('message', $this->ixsolution_ion_auth->messages());
            redirect('ixsolutions_admin/users', 'refresh');
        } else {
            $this->session->set_flashdata('message', $this->ixsolution_ion_auth->errors());
            redirect('auth/forgot_password', 'refresh');
        }
    }

    public function deactivate($id = NULL) {
        if (!$this->ixsolution_ion_auth->logged_in() OR ! $this->ixsolution_ion_auth->logged_in()) {
            return show_error('You must be an administrator to view this page.');
        }

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_users_deactivate'), 'ixsolutions_admin/users/deactivate');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Validate form input */
        $this->form_validation->set_rules('confirm', 'lang:deactivate_validation_confirm_label', 'required');
        $this->form_validation->set_rules('id', 'lang:deactivate_validation_user_id_label', 'required|alpha_numeric');

        $id = (int) $id;

        if ($this->form_validation->run() === FALSE) {
            $user = $this->ixsolution_ion_auth->user($id)->row();

            $this->data['csrf'] = $this->_get_csrf_nonce();
            $this->data['id'] = (int) $user->id;
            $this->data['firstname'] = !empty($user->first_name) ? htmlspecialchars($user->first_name, ENT_QUOTES, 'UTF-8') : NULL;
            $this->data['lastname'] = !empty($user->last_name) ? ' ' . htmlspecialchars($user->last_name, ENT_QUOTES, 'UTF-8') : NULL;

            /* Load Template */
            $this->ixsolutions_template->admin_render('ixsolutions_admin/users/deactivate', $this->data);
        } else {
            if ($this->input->post('confirm') == 'yes') {
                if ($this->_valid_csrf_nonce() === FALSE OR $id != $this->input->post('id')) {
                    show_error($this->lang->line('error_csrf'));
                }

                if ($this->ixsolution_ion_auth->logged_in() && $this->ixsolution_ion_auth->logged_in()) {
                    $this->ixsolution_ion_auth->deactivate($id);
                }
            }

            redirect('ixsolutions_admin/users', 'refresh');
        }
    }

    public function profile($id) {
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_users_profile'), 'ixsolutions_admin/groups/profile');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Data */
        $id = (int) $id;

        $this->data['user_info'] = $this->ixsolution_ion_auth->user($id)->result();
        foreach ($this->data['user_info'] as $k => $user) {
            $this->data['user_info'][$k]->groups = $this->ixsolution_ion_auth->get_users_groups($user->id)->result();
        }

        /* Load Template */
        $this->ixsolutions_template->admin_render('ixsolutions_admin/users/profile', $this->data);
    }

    public function _get_csrf_nonce() {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    public function _valid_csrf_nonce() {
        if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE && $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
