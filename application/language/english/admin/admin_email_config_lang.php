<?php

/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['admin_email_config_action']               = 'Action';
$lang['admin_email_config_active']               = 'Active';
$lang['admin_email_config_company']              = 'Company';
$lang['create_new_email_configuration']          = 'Create Admin Email';
$lang['edit_new_email_configuration']          = 'Edit Admin Email Address';
$lang['admin_email_config_created_on']           = 'Created on';
$lang['admin_email_config_deactivate_question']  = 'Are you sure you want to deactivate the user %s';
$lang['admin_email_config_edit_user']            = 'Edit';
$lang['admin_email_config_email']                = 'Email Address';
$lang['admin_email_config_userName']             = 'User Name';
$lang['admin_email_config_logInTime']            = 'Logged In Time';
$lang['admin_email_config_logOutTime']           = 'Logged Out Time';
$lang['admin_email_config_downloadTime']         = 'Executed Time';
$lang['admin_email_config_downloadFile']         = 'Downloaded File Name';
$lang['admin_email_config_activity']             = 'Activity';
$lang['admin_email_config_queryString']          = 'Query';
$lang['admin_email_config_firstname']            = 'First name';
$lang['admin_email_config_groups']               = 'Groups';
$lang['admin_email_config_inactive']             = 'Inactive';
$lang['admin_email_config_ip_address']           = 'IP address';
$lang['admin_email_config_last_login']           = 'Last login';
$lang['admin_email_config_lastname']             = 'Last name';
$lang['admin_email_config_member_of_groups']     = 'Member of groups';
$lang['admin_email_config_password']             = 'Password';
$lang['admin_email_config_password_confirm']     = 'Password confirm';
$lang['admin_email_config_phone']                = 'Phone';
$lang['admin_email_config_status']               = 'Status';
$lang['admin_email_config_username']             = 'User name / Pseudo';
$lang['user_type']                  = 'Member Type';
$lang['user_name']                  = 'Broker Name';
$lang['user_Npn']                   = 'NPN#';
$lang['user_email']                 = 'Email Address';
$lang['user_no_ofCompany']          = '# Of Quotes';
$lang['user_createdAt']             = 'Created At';
$lang['user_companyName']           = 'Company Name';
$lang['user_no_ofEmployee']         = '# Of Employees';
$lang['user_product']               = 'Product';
$lang['user_sicCode']               = 'SIC Code';
$lang['user_county']               = 'County';

