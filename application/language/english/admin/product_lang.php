<?php

/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['products_action']              = 'Action';
$lang['products_active']              = 'Active';
$lang['products_company']             = 'Company';
$lang['products_create_product']         = 'Create Product';
$lang['products_created_on']          = 'Created on';
$lang['products_deactivate_question'] = 'Are you sure you want to deactivate the product %s';
$lang['products_edit_product']           = 'Edit Product';
$lang['products_email']               = 'Email';
$lang['products_name']                = 'Product name';
$lang['product_description']         = 'Product description';
$lang['products_groups']              = 'Groups';
$lang['products_inactive']            = 'Inactive';
$lang['occupation']                   = 'Occupation Display';
$lang['salary']                   = 'Salary Display';
$lang['products_last_login']          = 'Last login';
$lang['products_lastname']            = 'Last name';
$lang['products_member_of_groups']    = 'Member of groups';
$lang['products_password']            = 'Password';
$lang['products_password_confirm']    = 'Password confirm';
$lang['products_phone']               = 'Phone';
$lang['products_status']              = 'Status';
$lang['products_productname']            = 'User name / Pseudo';
$lang['product_image']                 = 'Product Image';
