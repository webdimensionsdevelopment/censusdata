<?php

/**
 * @package    CodeIgniter
 * @author    domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since    Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['questionnaire_action'] = 'Action';
$lang['questionnaire_active'] = 'Active';
$lang['questionnaire_company'] = 'Company';
$lang['questionnaire_create_questionnaire'] = 'Create Questionnaire';
$lang['questionnaire_created_on'] = 'Created on';
$lang['questionnaire_deactivate_question'] = 'Are you sure you want to deactivate the questionnaire %s';
$lang['questionnaire_edit_questionnaire'] = 'Edit questionnaire';
$lang['questionnaire_question'] = 'Questionnaire Question';
$lang['product_name'] = 'Product Name';
$lang['product'] = 'Product';
$lang['product_id'] = 'Product';
$lang['questionnaire_inactive'] = 'Inactive';
$lang['questionnaire_selection'] = 'Type of question';
$lang['questionnaire_status'] = 'Status';
$lang['questionnaire_lastname'] = 'Last name';
$lang['questionnaire_member_of_groups'] = 'Member of groups';
$lang['questionnaire_description'] = 'Questionnaire Description';
$lang['questionnaire_password_confirm'] = 'Password confirm';
$lang['questionnaire_phone'] = 'Phone';
$lang['questionnaire_answer'] = 'Answer';
$lang['questionnaire_questionnairename'] = 'User name / Pseudo';
