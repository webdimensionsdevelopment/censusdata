<?php

/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['company_action']              = 'Action';
$lang['company_active']              = 'Active';
$lang['company_company']             = 'Broker Company';
$lang['company_create_company']      = 'Create Company';
$lang['company_created_on']          = 'Created on';
$lang['company_deactivate_question'] = 'Are you sure you want to deactivate the product %s';
$lang['company_deactivate_company'] = 'Are you sure you want to deactivate the Company %s';
$lang['company_edit_broker']         = 'Edit broker';
$lang['company_email']               = 'Email Address';
$lang['company_name']                = 'Product name';
$lang['product_description']           = 'Product description';
$lang['company_groups']              = 'Groups';
$lang['company_inactive']            = 'Inactive';
$lang['company_ip_address']          = 'IP address';
$lang['company_last_login']          = 'Last login';
$lang['company_lastname']            = 'Last name';
$lang['company_member_of_groups']    = 'Member of groups';
$lang['company_password']            = 'Password';
$lang['company_password_confirm']    = 'Password confirm';
$lang['company_phone']               = 'Phone';
$lang['company_status']              = 'Status';
$lang['company_companyName']       = 'User name / Pseudo';
$lang['company_image']               = 'Brokerage Image';
$lang['company_UsersName']           = 'User Name';
$lang['company_companyName']           = 'Company Name';
$lang['company_FirstName']           = 'First Name';
$lang['company_LastName']           = 'Last Name';
$lang['company_email']           = 'Email';
$lang['actions_submit']           = 'Send';
$lang['company_status']           = 'Status';
$lang['company_submit']           = 'Submit';


