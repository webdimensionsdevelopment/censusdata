<?php

/**
 * @package    CodeIgniter
 * @author    domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since    Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['document_action'] = 'Action';
$lang['document_active'] = 'Active';
$lang['document_create_document'] = 'Create document';
$lang['document_edit_document'] = 'Edit document';
$lang['document_question'] = 'Questionnaire question';
$lang['document_name'] = 'Document Name';
$lang['document_inactive'] = 'Inactive';
$lang['document_status'] = 'Status';
