<?php

/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['users_action']              = 'Action';
$lang['users_active']              = 'Active';
$lang['users_company']             = 'Company';
$lang['users_create_user']         = 'Create user';
$lang['users_created_on']          = 'Created on';
$lang['users_updated_By']          = 'Updated By';
$lang['users_deactivate_question'] = 'Are you sure you want to deactivate the user %s';
$lang['users_edit_user']           = 'Edit user';
$lang['users_userName']           = 'User Name';
$lang['users_logInTime']           = 'Logged In Time';
$lang['users_logOutTime']           = 'Logged Out Time';
$lang['users_downloadTime']           = 'Executed Time';
$lang['users_downloadFile']           = 'Downloaded File Name';
$lang['users_activity']           = 'Activity';
$lang['users_queryString']           = 'Query';
$lang['users_email']               = 'Email Address';
$lang['users_firstname']           = 'First name';
$lang['users_groups']              = 'Groups';
$lang['users_inactive']            = 'Inactive';
$lang['users_ip_address']          = 'IP address';
$lang['users_last_login']          = 'Last login';
$lang['users_lastname']            = 'Last name';
$lang['users_member_of_groups']    = 'Member of groups';
$lang['users_password']            = 'Password';
$lang['users_password_confirm']    = 'Password confirm';
$lang['users_phone']               = 'Phone';
$lang['users_status']              = 'Status';
$lang['users_username']            = 'User name / Pseudo';
$lang['user_type']                 = 'Member Type';
