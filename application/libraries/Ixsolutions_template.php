<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ixsolutions_template {

    protected $CI;

    public function __construct()
    {	
		$this->CI =& get_instance();
    }


    public function admin_render($content, $data = NULL)
    {
        if ( ! $content)
        {
            return NULL;
        }
        else
        {
            $this->template['header']          = $this->CI->load->view('ixsolutions_admin/_templates/header', $data, TRUE);
            $this->template['main_header']     = $this->CI->load->view('ixsolutions_admin/_templates/main_header', $data, TRUE);
            $this->template['main_sidebar']    = $this->CI->load->view('ixsolutions_admin/_templates/main_sidebar', $data, TRUE);
            $this->template['content']         = $this->CI->load->view($content, $data, TRUE);
            $this->template['control_sidebar'] = $this->CI->load->view('ixsolutions_admin/_templates/control_sidebar', $data, TRUE);
            $this->template['footer']          = $this->CI->load->view('ixsolutions_admin/_templates/footer', $data, TRUE);

            return $this->CI->load->view('ixsolutions_admin/_templates/template', $this->template);
        }
	}


    public function auth_render($content, $data = NULL)
    {
        if ( ! $content)
        {
            return NULL;
        }
        else
        {
            $this->template['header']  = $this->CI->load->view('ixsolutions_auth/_templates/header', $data, TRUE);
            $this->template['content'] = $this->CI->load->view($content, $data, TRUE);
            $this->template['footer']  = $this->CI->load->view('ixsolutions_auth/_templates/footer', $data, TRUE);

            return $this->CI->load->view('ixsolutions_auth/_templates/template', $this->template);
        }
	}

}