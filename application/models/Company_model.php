<?php

class Company_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    public function get_company_and_employee_details_byId($Id){
        $this->db->select('
            sicbox.code,
            users_groups.createdAt,
            users.username,users.email,users.company,users.zipcode,users.city,users.state,
            company.companyId,company.no_of_emp, company.employee_estimate, company.dependant_estimate');
        $this->db->from('company');
        $this->db->join('sicbox','sicbox.sicId=company.sicId','left');
        $this->db->join('users','users.id=company.userId','left');
        $this->db->join('users_groups','users_groups.user_id=company.userId','left');
        $this->db->where('company.companyId',$Id);
        return $result = $this->db->get()->result_array();
//        echo "<pre/>";print_r($this->db->last_query());exit;
    }
    public function get_product_details_of_CompanyProduct_byId($Id){
        $this->db->select('
            product.name,
            company_product.prod_id,company_product.renewal_date,company_product.effective_date');
        $this->db->from('company_product');
        $this->db->join('product','product.productId=company_product.prod_id')->group_by('product.productId');
        $this->db->where('company_product.comp_id',$Id);
        return $result = $this->db->get()->result_array();
    }
    public function get_all_question_byProductId($productId){
        $this->db->select('questionnaires_questions.id,questionnaires_questions.prod_id,questionnaires_questions.question');
        $this->db->where('prod_id',$productId);
        $this->db->from('questionnaires_questions');
        return $result= $this->db->get()->result_array();
    }
    public function get_all_answers_byquestionId($companyId, $questionId){
        $this->db->select('company_ques_answer.questionId,company_ques_answer.answerId,questions_answers.answer');
        $this->db->where('questionId',$questionId);
        $this->db->where('companyId',$companyId);
        $this->db->from('company_ques_answer');
        $this->db->join('questions_answers', 'questions_answers.id=company_ques_answer.answerId');
        return $result= $this->db->get()->result_array();
    }
    public function getNotes($companyId,$productId){
        $this->db->select('company_ques_answer.ques_answer,company_ques_answer.companyQuestionId');
        $this->db->from('company_ques_answer');
        $this->db->join('questionnaires_questions','questionnaires_questions.id=company_ques_answer.questionId','left');
        $this->db->join('product','product.productId=questionnaires_questions.prod_id','left');
        $this->db->where('product.productId',$productId);
        $this->db->where('company_ques_answer.ques_answer!=','');
        $this->db->where('company_ques_answer.companyId',$companyId);
        return $result = $this->db->get()->row_array();
    }
    public function get_employees_ByCompanyId($companyId){
        $this->db->select('employee.employeeId,employee.firstName,employee.lastName,employee.gender,employee.dateOfBirth,employee.salary,employee.zipcode,employee.state');
        $this->db->from('employee');
        $this->db->where('companyId',$companyId)->group_by('employeeId');
        return $result =  $this->db->get()->result_array();
    }
    public function get_employee_spouse_byId($Id){
        $this->db->select('employeespouse.esFirstName,employeespouse.esLastName,employeespouse.gender,employeespouse.dateOfBirth,employeespouse.emp_relation');
        $this->db->from('employeespouse');
        $this->db->where('employeeId',$Id);
        return $result =  $this->db->get()->result_array();
    }
    public function get_product_details_of_employee_byId($Id){
        $this->db->select('
            product.name,
            company_product.prod_id,company_product.renewal_date,company_product.effective_date');
        $this->db->from('company_product');
        $this->db->join('product','product.productId=company_product.prod_id');
        $this->db->where('company_product.comp_id',$Id);
        return $this->db->get()->result_array();
    }
    /*
     * get all active product
     */

    public function get_product() {
        return $this->db->get_where('product', 'status=1')->result();
    }

    /*
     * get all sic codes
     */

    public function get_sic_code() {
        return $this->db->get('sicbox')->result();
    }

    /*
     * get  group
     */

    public function get_group() {
        $this->db->where('name', 'company');
        return $this->db->get('groups')->result();
    }

    /*
     * insert new user
     */

    public function insert_user($data) {
        $this->db->insert('users', $data);
        $insert_id = $this->db->insert_id();
        $query = $this->db->last_query();
        $queryName = "INSERTED USER";
        $this->ixsolution_ion_auth->logFile($queryName, $query,'',$insert_id);
        return $insert_id;
    }

    /*
     * insert new users group
     */

    public function insert_user_type($data) {
        $this->db->insert('users_groups', $data);
        $query = $this->db->last_query();
        $queryName = "INSERTED USERTYPE";
        $this->ixsolution_ion_auth->logFile($queryName, $query);
    }

    /*
     * insert new company
     */

    public function insert_company($data) {

        $this->db->insert('company', $data);
        $comp_id = $this->db->insert_id();
        $query = $this->db->last_query();
        $queryName = "INSERTED COMPANY";
        $this->ixsolution_ion_auth->logFile($queryName, $query,'',$comp_id);
        return $comp_id;
    }

    /*
     * insert company product
     */

    public function insert_company_product($data) {
        $this->db->insert('company_product', $data);
        $query = $this->db->last_query();
        $queryName = "INSERTED PRODUCT";
        $this->ixsolution_ion_auth->logFile($queryName, $query,'',$data['comp_id']);
        $prod_id = $this->db->insert_id();
    }

    /*
     * get product wise question
     */

    public function get_question($pid) {

        $this->db->where_in('prod_id', $pid);
        return $this->db->get('questionnaires_questions')->result();
    }

    /*
     * get all answers
     */

    public function get_answer() {
        return $this->db->get('questions_answers')->result();
    }

    /*
     * insert company wise question answer
     */

    public function insert_question_answer($data) {
        $this->db->insert('company_ques_answer', $data);
        $query = $this->db->last_query();
        $queryName = "INSERTED QUESTION_ANSWER";
        $this->ixsolution_ion_auth->logFile($queryName, $query,'',$data['companyId']);
    }

    /*
     * insert company's employee
     */

    public function insert_employee($data,$companyId) {
        $this->db->insert('employee', $data);
        $insert_id = $this->db->insert_id();
        $query = $this->db->last_query();
        $queryName = "INSERTED EMPLOYEE";
        $this->ixsolution_ion_auth->logFile($queryName, $query,'',$companyId);
        return $insert_id;
    }

    /*
     * insert employee's relation data 
     */

    public function insert_employee_relation_data($data,$companyId) {
        $this->db->insert('employeespouse', $data);
        $query = $this->db->last_query();
        $queryName = "INSERTED EMPLOYEE SPOUSE";
        $this->ixsolution_ion_auth->logFile($queryName, $query,'',$companyId);
    }

    /*
     * get company's produt
     */

    public function get_comp_product($cid) {
       /* $this->db->where('comp_id', $cid);
        return $this->db->get('company_product')->result();*/
        $this->db->select('p.name, p.productId, cp.renewal_date, cp.effective_date');
        $this->db->where('comp_id', $cid);
        $this->db->join('product as p','p.productId=cp.prod_id');
        return $this->db->get('company_product as cp')->result();
    }

    /*
     * for upload files
     */

    public function uploadFile($Files, $DestPath, $comp_name_id, $cid, $extra = '') {
       
        $FileName = '';
        $Lastpos = strrpos($Files['name'], '.');
        $FileExtension = strtolower(substr($Files['name'], $Lastpos, strlen($Files['name'])));
        $ValidExtensionArr = array(".jpg", ".jpeg", ".png", ".gif", ".pdf", ".doc", ".csv");
        if (in_array(strtolower($FileExtension), $ValidExtensionArr)) {
            if (!empty($extra)) {
                $FileName = time() . "_" . $extra . $FileExtension;
            } else {
                $FileName = $comp_name_id . $FileExtension;
            }
            if (move_uploaded_file($Files['tmp_name'], $DestPath . $FileName)) {
                $query = 'FILE IS BEING UPLOADED';
                $queryName = "UPLOADED FILE";
                $fileName = $FileName;
                $this->ixsolution_ion_auth->logFile($queryName, $query, $fileName,$cid);
                return $FileName;
            } else
                return false;
        }else {
            return false;
        }
    }

    /*
     * get perticular company
     */

    public function get_company($cid) {
        $this->db->select('u.company');
        $this->db->where('c.companyId', $cid);
        $this->db->join('company c', 'c.userId=u.id');
        $query = $this->db->get('users as u')->result();
        return $query[0]->company;
    }

    /*
     * get document type
     */

    public function get_document() {
        return $this->db->get('document')->result();
    }

    /*
     * insert company document
     */

    public function insert_question_document($data) {
        $this->db->insert('comp_emp_doc', $data);
        $query = $this->db->last_query();
        $queryName = "INSERTED QUESTION_DOCUMENTS";
        $this->ixsolution_ion_auth->logFile($queryName, $query,'',$data['comp_id']);
    }

    /*
     * get company's no of employee 
     */

    public function get_comp_emp($cid) {
        $this->db->select('no_of_emp');
        $this->db->where('companyId', $cid);
        $query = $this->db->get('company')->result();
        return $query[0]->no_of_emp;
    }

    /*
     * update no of employee of perticular company
     */

    public function update_company_employee($data, $cid) {
        $this->db->where('companyId', $cid);
        $this->db->update('company', $data);
        $query = $this->db->last_query();
        $queryName = "UPDATED COMPANY_EMPLOYEE";
        $this->ixsolution_ion_auth->logFile($queryName, $query,'',$cid);
    }

    /*
     * get company detail of perticular user 
     */

    public function get_company_detail($id, $user) {
        $this->db->select('*');
        if ($user == 'broker')
            $this->db->where('c.brokerId', $id);
        else if ($user == 'brokerage')
        {}//$this->db->where('c.brokrage_id', $id);
        else
            //$this->db->where('u.id', $id);
        $this->db->join('company c', 'c.userId=u.id');
        $this->db->join('users_groups', 'users_groups.user_id=u.id');
        return $query = $this->db->get('users as u')->result();

    }

    /*
     * get employee of perticular company
     */

    public function get_company_employee($cid) {
        $this->db->where('companyId', $cid);
        return $this->db->get('employee')->result();
    }

    /*
     * get employee relation data of perticular employee
     */

    public function get_company_employee_relation($employeeId) {
        $this->db->where('employeeId', $employeeId);
        return $this->db->get('employeespouse')->result();
    }

    public function get_company_all_employee($cid) {
        $this->db->select('spouse.employeeId, spouse.employeeSpouseId, spouse.esFirstName, spouse.esLastName, spouse.gender as gen, spouse.dateOfBirth as birthdate, spouse.emp_relation, spouse.ssn');
        $this->db->where('emp.companyId', $cid);
        $this->db->join('employeespouse as spouse', 'emp.employeeId=spouse.employeeId');
        return $this->db->get('employee as emp')->result();
    }

    public function update_employee($employee, $empid) {
        $this->db->where('employeeId', $empid);
        $this->db->update('employee', $employee);
        $query = $this->db->last_query();
        $this->db->select('companyId');
        $this->db->from('employee')->where('employeeId',$empid);
        $companyId = $this->db->get()->row_array();
        $queryName = "UPDATE EMPLOYEE";
        $this->ixsolution_ion_auth->logFile($queryName, $query,'',$companyId['companyId']);
    }

    public function update_employee_relation_data($data, $sempid) {
        $this->db->where('employeeSpouseId', $sempid);
        $this->db->update('employeespouse', $data);
        $query = $this->db->last_query();
        $this->db->select('companyId');
        $this->db->from('employee');
        $this->db->join('employeespouse','employeespouse.employeeId=employee.employeeId','left')->where('employeespouse.employeeSpouseId',$sempid);
        $companyId = $this->db->get()->row_array();
        $queryName = "UPDATE EMPLOYEE RELATION";
        $this->ixsolution_ion_auth->logFile($queryName, $query,'',$companyId['companyId']);
    }

    public function delete_employee_relation_data($sempid) {
        $this->db->where('employeeSpouseId', $sempid);
        $this->db->delete('employeespouse');
        $query = $this->db->last_query();
        $this->db->select('companyId');
        $this->db->from('employee');
        $this->db->join('employeespouse','employeespouse.employeeId=employee.employeeId','left')->where('employeespouse.employeeSpouseId',$sempid);
        $companyId = $this->db->get()->row_array();
        $query = $this->db->last_query();
        $queryName = "DELETE EMPLOYEE_RELATION";
        $this->ixsolution_ion_auth->logFile($queryName, $query,'',$companyId['companyId']);
    }

    public function delete_employee($empid) {
        $this->db->where('employeeId', $empid);
        $this->db->delete('employeespouse');
        $query = $this->db->last_query();
        $this->db->select('companyId');
        $this->db->from('employee')->where('employeeId',$empid);
        $companyId = $this->db->get()->row_array();
        $queryName = "DELTED EMPLOYEE SPOUSE";
        $this->ixsolution_ion_auth->logFile($queryName, $query,'',$companyId['companyId']);
        $this->db->where('employeeId', $empid);
        $this->db->delete('employee');
        $query = $this->db->last_query();
        $this->db->select('companyId');
        $this->db->from('employee')->where('employeeId',$empid);
        $companyId = $this->db->get()->row_array();
        $queryName = "DELETED EMPLOYEE";
        $this->ixsolution_ion_auth->logFile($queryName, $query,'',$companyId['companyId']);
    }

    public function delete_employee_child($empid, $relation) {
        $this->db->where('employeeId', $empid);
        $this->db->where('emp_relation', $relation);
        $this->db->delete('employeespouse');
        $query = $this->db->last_query();
        $this->db->select('companyId');
        $this->db->from('employee')->where('employeeId',$empid);
        $companyId = $this->db->get()->row_array();
        $queryName = "DELETED EMPLOYEE CHILD";
        $this->ixsolution_ion_auth->logFile($queryName, $query,'',$companyId);
    }

    public function company_product_count($cid) {
        $this->db->select('count(prod_id) as count');
        $this->db->where('comp_id', $cid);
        $query = $this->db->get('company_product')->result();
        return $query[0]->count;
    }

    public function get_occupation($cid) {
        $this->db->select('count(p.productId) as count');
        $this->db->where('c.comp_id', $cid);
        $this->db->where('p.occupation', '1');
        $this->db->join('company_product as c', 'c.prod_id=p.productId');
        $query = $this->db->get('product as p')->result();
        return $query[0]->count;
    }
     public function get_salary($cid) {
        $this->db->select('count(p.productId) as count');
        $this->db->where('c.comp_id', $cid);
        $this->db->where('p.salary', '1');
        $this->db->join('company_product as c', 'c.prod_id=p.productId');
        $query = $this->db->get('product as p')->result();
        return $query[0]->count;
    }
    public function get_company_all_detail($cid) {
        $this->db->select('u.id, u.company, u.address, u.zipcode, u.country, u.state, u.city, u.email, c.sicId, c.no_of_emp, c.companyId, c.first_name, c.last_name, c.phoneNumber');
        $this->db->where('c.companyId', $cid);
        $this->db->join('company as c', 'c.userId=u.id');
        return $this->db->get('users as u')->result();
    }

    public function update_user($data, $uid) {
        $this->db->where('id', $uid);
        $this->db->update('users', $data);
        $query = $this->db->last_query();
        $this->db->select('companyId');
        $this->db->from('company');
        $this->db->where('userId',$uid);
        $result = $this->db->get()->row_array();
        $queryName = "UPDATE USER";
        $this->ixsolution_ion_auth->logFile($queryName, $query,'',$result['companyId']);
    }

    public function update_company($data, $cid) {
         
        $this->db->where('companyId', $cid);
        $this->db->update('company', $data);
        $query = $this->db->last_query();
        $queryName = "UPDATED COMPANY";
        $this->ixsolution_ion_auth->logFile($queryName, $query, '', $cid);
    }

    public function getBrokerIdByUserId($userId) {
        return $this->db->select('brokerId')->where('userId', $userId)->get('broker')->row_array();
    }

    public function get_brokerage_id($userId) {
        return $this->db->select('id')->where('userId', $userId)->get('brokerage')->row_array();
    }
    public function get_company_employee_all_detail($cid)
    {
        //$this->db->distinct('es.employeeId');
        $this->db->select('e.employeeId, e.firstName, e.lastName, e.gender, e.dateOfBirth, e.salary, GROUP_CONCAT(es.dateofBirth) as birth');
        $this->db->where('e.companyId',$cid);
        $this->db->join('employeespouse as es','es.employeeId=e.employeeId','left');
        $this->db->group_by('e.employeeId');
        return $this->db->get('employee as e')->result();
    }
    public function get_brokerage_email()
    {
        $this->db->select('email');
        $this->db->join('brokerage as b','b.userId=u.id');
        return $this->db->get('users as u')->result();
    }
    public function get_company_documents_byId($companyId){
        $this->db->select('comp_emp_doc.document_name,document.documentFile, comp_emp_doc.id');
        $this->db->from('comp_emp_doc');
        $this->db->join('document','document.documentId=comp_emp_doc.doc_id','left');
        $this->db->where('comp_emp_doc.comp_id',$companyId);
        return $result= $this->db->get()->result_array();
    }

    public function get_company_document_byDocumentId($docID){
        $this->db->select('comp_emp_doc.document_name,document.documentFile, comp_emp_doc.id');
        $this->db->from('comp_emp_doc');
        $this->db->join('document','document.documentId=comp_emp_doc.doc_id','left');
        $this->db->where('comp_emp_doc.id',$docID);
        return $result= $this->db->get()->result();
    }
    public function delete_file($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('comp_emp_doc');
    }
    public function employee_update($data,$eid)
    {
           $this->db->where('employeeId',$eid);
           $this->db->update('employee',$data);
    }
    public function get_employee_bytoken($token)
    {
        $this->db->where('activation_code',$token);
        return $this->db->get('employee')->result();
    }
    public function get_employee_spouse($Id){
         $this->db->select('spouse.employeeId, spouse.employeeSpouseId, spouse.esFirstName, spouse.esLastName, spouse.gender as gen, spouse.dateOfBirth as birthdate, spouse.emp_relation, spouse.ssn');
         $this->db->where('emp.employeeId',$Id);
         $this->db->join('employeespouse as spouse', 'emp.employeeId=spouse.employeeId');
        return $this->db->get('employee as emp')->result();
    }
    public function get_all_broker()
    {
        $this->db->select('u.first_name, u.last_name, b.brokerId');
        $this->db->where('group_id','3');
        $this->db->join('users_groups as ug','ug.user_id=u.id');
        $this->db->join('broker as b','b.userId=u.id');
        return $this->db->get('users as u')->result();
   }
   public function get_broker_email($broker_Id) {
        $this->db->select('u.email');
        $this->db->where('b.brokerId', $broker_Id);
        $this->db->join('broker as b','b.userId=u.id');
        $query=$this->db->get('users as u')->result();
        return $query[0]->email;
    }
    public function get_onboard_status($cid)
    {
        $this->db->select('on_board');
        $this->db->where('companyId',$cid);
        $query=$this->db->get('company')->result();
        return $query[0]->on_board;
    }
    public function get_estimate($cid)
    {
        $this->db->select('employee_estimate, dependant_estimate');
        $this->db->where('companyId',$cid);
        return $this->db->get('company')->result();
    }
    public function get_company_assign_product($cid)
    {
        $ids=array('');
        $this->db->select('p.*, cp.renewal_date, cp.effective_date');
        $this->db->where('cp.comp_id',$cid);
        $this->db->join('product as p','p.productId=cp.prod_id');
        $result['assigned']= $this->db->get('company_product as cp')->result();  
         foreach($result['assigned'] as $assign)
         {
             $ids[]=$assign->productId;    
         }
        $this->db->select('*');
        $this->db->where_not_in('productId',$ids);
        $this->db->where('status','1');
        $result['unassigned'] = $this->db->get('product')->result();
        return $result;
    }
    public function remove_company_product($remove_product,$cid)
    {
       $this->db->select('id');
       $this->db->where_in('prod_id', $remove_product);
       $query=$this->db->get('questionnaires_questions')->result();
       foreach($query as $query)
       {
           $question_id[]=$query->id;
       }
       $this->db->where_in('questionId', $question_id);
       $this->db->where('companyId',$cid);
       $this->db->delete('company_ques_answer');
       
       $this->db->where_in('prod_id', $remove_product);
       $this->db->where('comp_id',$cid);
       $this->db->delete('company_product');
    }
    public function update_company_product($data,$cid,$product_id) {
        
        $this->db->where('prod_id',$product_id);
        $this->db->where('comp_id',$cid);
        $this->db->update('company_product', $data);
        $query = $this->db->last_query();
        $queryName = "UPDATED PRODUCT";
        $this->ixsolution_ion_auth->logFile($queryName, $query,'',$cid);
    }
    public function get_assign_answer($cid)
    {
        $this->db->where('companyId',$cid);
        return $this->db->get('company_ques_answer')->result();
    }
    public function get_all_brokerage()
    {
        $this->db->select('u.first_name, u.last_name, b.id');
        $this->db->where('group_id','2');
        $this->db->join('users_groups as ug','ug.user_id=u.id');
        $this->db->join('brokerage as b','b.userId=u.id');
        return $this->db->get('users as u')->result();
   }
}
?>
