<?php

class Product_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }
    public function addProduct($record){
        $this->db->insert("product",  $record);
        return $this->db->insert_id();
    }
    public function getProductDetails(){
        return $this->db->Select('*')->get('product')->result_array();
    }
    public function getProductDetailsById($productId){
        return $this->db->select('*')->where('productId',$productId)->get('product')->row_array();
    }
    public function updateProduct($record, $productId){
        $this->db->where('productId', $productId);
        $this->db->update('product', $record);
    }
    public function deleteProductById($productId){
        $this->db->delete('product', array('productId' => $productId));
    }
}