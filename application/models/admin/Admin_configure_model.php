<?php

class Admin_configure_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    /*
     * get all active product
     */

    public function get_all_mail()
    {
        return $this->db2->get('admin_mail_configuration')->result();
    }

    public function get_email_address_by_id($id)
    {
        return $this->db2->where('id', $id)->get('admin_mail_configuration')->result();
    }

    public function config_email_activate($id)
    {
        $status = array('status' => '1');
        return $this->db2->where('id', $id)->update("admin_mail_configuration", $status);
    }

    public function config_email_deactivate($id)
    {
        $status = array('status' => '0');
        return $this->db2->where('id', $id)->update("admin_mail_configuration", $status);
    }

    public function update_config_email_address($id, $data){
        $this->db2->where('id', $id)->update("admin_mail_configuration", $data);
    }

    public function create_new_email_address($data){
        $this->db2->insert("admin_mail_configuration", $data);
        return $this->db2->insert_id();
    }
}

?>
