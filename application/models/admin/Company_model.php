<?php

class Company_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    public function get_company_and_employee_details_byId($Id){
        $this->db2->select('
            sicbox.code,
            users_groups.createdAt,
            users.username,users.email,users.company,users.zipcode,users.city,users.state,
            company.companyId,company.brokerage_name,company.npn,company.no_of_emp');
        $this->db2->from('company');
        $this->db2->join('sicbox','sicbox.sicId=company.sicId','left');
        $this->db2->join('users','users.id=company.userId','left');
        $this->db2->join('users_groups','users_groups.user_id=company.userId','left');
        $this->db2->where('company.companyId',$Id);
        return $result = $this->db2->get()->result_array();
    }
    public function get_employee_spouse_byId($Id){
        $this->db2->select('employeespouse.esFirstName,employeespouse.esLastName,employeespouse.gender,employeespouse.dateOfBirth,employeespouse.emp_relation');
        $this->db2->from('employeespouse');
        $this->db2->where('employeeId',$Id);
        return $result =  $this->db2->get()->result_array();
    }
    public function get_employees_ByCompanyId($companyId){
        $this->db2->select('employee.employeeId,employee.firstName,employee.lastName,employee.gender,employee.dateOfBirth,employee.salary,employee.zipcode,employee.state');
        $this->db2->from('employee');
        $this->db2->where('companyId',$companyId)->group_by('employeeId');
        return $result =  $this->db2->get()->result_array();
    }
    public function getNotes($companyId,$productId){
        $this->db2->select('company_ques_answer.ques_answer,company_ques_answer.companyQuestionId');
        $this->db2->from('company_ques_answer');
        $this->db2->join('questionnaires_questions','questionnaires_questions.id=company_ques_answer.questionId','left');
        $this->db2->join('product','product.productId=questionnaires_questions.prod_id','left');
        $this->db2->where('product.productId',$productId);
        $this->db2->where('company_ques_answer.ques_answer!=','');
        $this->db2->where('company_ques_answer.companyId',$companyId);
        return $result = $this->db2->get()->row_array();
    }
    public function get_product_details_of_CompanyProduct_byId($Id){
        $this->db2->select('
            product.name,
            company_product.prod_id,company_product.renewal_date,company_product.effective_date');
        $this->db2->from('company_product');
        $this->db2->join('product','product.productId=company_product.prod_id')->group_by('product.productId');
        $this->db2->where('company_product.comp_id',$Id);
        return $result = $this->db2->get()->result_array();
    }
    public function get_all_question_byProductId($productId){
        $this->db2->select('questionnaires_questions.id,questionnaires_questions.prod_id,questionnaires_questions.question');
        $this->db2->where('prod_id',$productId);
        $this->db2->from('questionnaires_questions');
        return $result= $this->db2->get()->result_array();
    }
    public function get_all_answers_byquestionId($companyId, $questionId){
        $this->db2->select('company_ques_answer.questionId,company_ques_answer.answerId,questions_answers.answer');
        $this->db2->where('questionId',$questionId);
        $this->db2->where('companyId',$companyId);
        $this->db2->from('company_ques_answer');
        $this->db2->join('questions_answers', 'questions_answers.id=company_ques_answer.answerId');
        return $result= $this->db2->get()->result_array();
    }
    
    public function get_company_documents_byId($companyId){
        $this->db2->select('comp_emp_doc.document_name,document.documentFile, comp_emp_doc.id');
        $this->db2->from('comp_emp_doc');
        $this->db2->join('document','document.documentId=comp_emp_doc.doc_id','left');
        $this->db2->where('comp_emp_doc.comp_id',$companyId);
        return $result= $this->db2->get()->result_array();
    }

    public function get_company_document_byDocumentId($docID){
        $this->db2->select('comp_emp_doc.document_name,document.documentFile, comp_emp_doc.id');
        $this->db2->from('comp_emp_doc');
        $this->db2->join('document','document.documentId=comp_emp_doc.doc_id','left');
        $this->db2->where('comp_emp_doc.id',$docID);
        return $result= $this->db2->get()->result();
    }
    /*
     * get all active product
     */
      
    public function get_product() {
        return $this->db2->get_where('product', 'status=1')->result();
    }
    
    /*
     * get all sic codes
     */
    

    public function get_sic_code() {
        return $this->db2->get('sicbox')->result();
    }

    /*
     * get  group
     */
    

    public function get_group() {
        $this->db2->where('name', 'company');
        return $this->db2->get('groups')->result();
    }

    /*
     * insert new user
     */
   

    public function insert_user($data) {
        $this->db2->insert('users', $data);
        $insert_id = $this->db2->insert_id();
        $query = $this->db2->last_query();
        $queryName = "INSERTED USER";
        $this->ion_auth->logFile($queryName, $query);
        return $insert_id;
    }

    /*
     * insert new users group
     */
     

    public function insert_user_type($data) {
        $this->db2->insert('users_groups', $data);
        $query = $this->db2->last_query();
        $queryName = "INSERTED USERTYPE";
        $this->ion_auth->logFile($queryName, $query);
    }

    /*
     * insert new company
     */
    

    public function insert_company($data) {
        $this->db2->insert('company', $data);
        $comp_id = $this->db2->insert_id();
        $companyID = $comp_id;
        $query = $this->db2->last_query();
        $queryName = "INSERTED COMPANY";
        $this->ion_auth->logFile($queryName, $query,$fileName= null, $companyID);
        return $comp_id;
    }

    /*
     * insert company product
     */
    

    public function insert_company_product($data) {
        $this->db2->insert('company_product', $data);
        $query = $this->db2->last_query();
        $queryName = "INSERTED PRODUCT";
        $this->ion_auth->logFile($queryName, $query,'',$data['comp_id']);
    }

    /*
     * get product wise question
     */
    

    public function get_question($pid) {

        $this->db2->where('prod_id', $pid);
        return $this->db2->get('questionnaires_questions')->result();
    }

    /*
     * get all answers
     */
    

    public function get_answer() {
        return $this->db2->get('questions_answers')->result();
    }

    /*
     * insert company wise question answer
     */
    

    public function insert_question_answer($data) {
        $this->db2->insert('company_ques_answer', $data);
        $query = $this->db2->last_query();
        $queryName = "INSERTED QUESTION_ANSWER";
        $this->ion_auth->logFile($queryName, $query,'',$data['companyId']);
    }

    /*
     * insert company's employee
     */
    

    public function insert_employee($data) {
        $this->db2->insert('employee', $data);
        $insert_id = $this->db2->insert_id();
        $query = $this->db2->last_query();
        $queryName = "INSERTED EMPLOYEE";
        $this->ion_auth->logFile($queryName, $query,'',$data['companyId']);
        return $insert_id;
    }

    /*
     * insert employee's relation data 
     */
    

    public function insert_employee_relation_data($data) {
        $id = $this->db2->insert('employeespouse', $data);
        $query = $this->db2->last_query();
        $this->db2->select('companyId');
        $this->db2->from('employee');
        $this->db2->join('employeespouse','employeespouse.employeeId=employee.employeeId','left');
        $this->db2->where('employeespouse.employeeSpouseId',$id);
        $companyId = $this->db2->get()->row_array();
        
        $queryName = "INSERTED RELATION OF EMPLOYEE";
        $this->ion_auth->logFile($queryName, $query,'',$companyId['companyId']);
    }

    /*
     * get company's produt
     */
    

    public function get_comp_product($cid) {
        $this->db2->select('p.name, p.productId');
        $this->db2->where('comp_id', $cid);
        $this->db2->join('product as p','p.productId=cp.prod_id');
        return $this->db2->get('company_product as cp')->result();
    }

    /*
     * for upload files
     */

    public function uploadFile($Files, $DestPath, $comp_name_id, $extra = '') {
        $FileName = '';
        $Lastpos = strrpos($Files['name'], '.');
        $FileExtension = strtolower(substr($Files['name'], $Lastpos, strlen($Files['name'])));
        $ValidExtensionArr = array(".jpg", ".jpeg", ".png", ".gif", ".pdf", ".doc", ".csv");
        if (in_array(strtolower($FileExtension), $ValidExtensionArr)) {
            if (!empty($extra)) {
                $FileName = time() . "_" . $extra . $FileExtension;
            } else {
                $FileName = $comp_name_id . $FileExtension;
           }
           if (move_uploaded_file($Files['tmp_name'], $DestPath . $FileName)) {
                $query = 'File IS BEING UPLOADED';
                $queryName = "UPLOADED FILE";
                $fileName = $FileName;
                $this->ion_auth->logFile($queryName, $query, $fileName,$extra);
                return $FileName;
            
            } else
                return false;
        }else {
            return false;
        }
    }

    /*
     * get perticular company
     */
    

    public function get_company($cid) {
        $this->db2->select('u.company');
        $this->db2->where('c.companyId', $cid);
        $this->db2->join('company c', 'c.userId=u.id');
        $query = $this->db2->get('users as u')->result();
        return $query[0]->company;
    }

    /*
     * get document type
     */
    

    public function get_document() {
        return $this->db2->get('document')->result();
    }

    /*
     * insert company document
     */
    

    public function insert_question_document($data) {
        $this->db2->insert('comp_emp_doc', $data);
        $query = $this->db2->last_query();
        $queryName = "INSERT QUESTION_DOCUMENTS";
        $this->ion_auth->logFile($queryName, $query,'',$data['comp_id']);
    }

    /*
     * get company's no of employee 
     */
    

    public function get_comp_emp($cid) {
        $this->db2->select('no_of_emp');
        $this->db2->where('companyId', $cid);
        $query = $this->db2->get('company')->result();
        return $query[0]->no_of_emp;
    }

    /*
     * update no of employee of particular company
     */
    public function update_company_employee($data, $cid) {
        $this->db2->where('companyId', $cid);
        $this->db2->update('company', $data);
        $query = $this->db2->last_query();
        $queryName = "UPDATE COMPANYEMPLOYEE";
        $this->ion_auth->logFile($queryName, $query, '', $cid);
    }

    /*
     * get company detail of particular user
     */
    public function get_company_detail($cid){
      $this->db2->select('u.*,c.*,users_groups.*,accountrep.first_name repfirst, accountrep.last_name as replast');

      if(!empty($cid))
         $this->db2->where('c.companyId',$cid);
         //$this->db2->order_by('c.companyId','desc');
        $this->db2->join('company c', 'c.userId=u.id')->order_by('c.userId','desc');
        $this->db2->join('users_groups','users_groups.user_id=c.userId','left');
        $this->db2->join('company_assign','company_assign.companyId=c.companyId','left');
        $this->db2->join('users as accountrep','accountrep.id = company_assign.userId','left');

        return $query = $this->db2->get('users as u')->result();
    }

    /*
     * get employee of perticular company
     */
    public function get_company_employee($cid) {
        $this->db2->where('companyId', $cid);
        return $this->db2->get('employee')->result();
    }

    /*
     * get employee relation data of perticular employee
     */
    

    public function get_company_employee_relation($employeeId) {
        $this->db2->where('employeeId', $employeeId);
        return $this->db2->get('employeespouse')->result();
    }
    

    public function get_company_all_employee($cid) {
        $this->db2->select('spouse.employeeId, spouse.employeeSpouseId, spouse.esFirstName, spouse.esLastName, spouse.gender as gen, spouse.dateOfBirth as birthdate, spouse.emp_relation');
        $this->db2->where('emp.companyId', $cid);
        $this->db2->join('employeespouse as spouse', 'emp.employeeId=spouse.employeeId');
        return $this->db2->get('employee as emp')->result();
    }
    

    public function update_employee($employee, $empid) {
        $this->db2->where('employeeId', $empid);
        $this->db2->update('employee', $employee);
        $query = $this->db2->last_query();
        $this->db2->select('companyId');
        $this->db2->from('employee')->where('employeeId',$empid);
        $companyId = $this->db2->get()->row_array();
        $queryName = "UPDATE EMPLOYEE";
        $this->ion_auth->logFile($queryName, $query,'',$companyId['companyId']);
    }
    public function update_employee_relation_data($data, $sempid) {
        $this->db2->where('employeeSpouseId', $sempid);
        $this->db2->update('employeespouse', $data);
        $query = $this->db2->last_query();
        $this->db2->select('companyId');
        $this->db2->from('employee');
        $this->db2->join('employeespouse','employeespouse.employeeId=employee.employeeId','left')->where('employeespouse.employeeSpouseId',$sempid);
        $companyId = $this->db2->get()->row_array();
        $queryName = "UPDATE EMPLOYEE RELATION";
        $this->ion_auth->logFile($queryName, $query,'',$companyId['companyId']);
    }
   

    public function delete_employee_relation_data($sempid) {
        $this->db2->where('employeeSpouseId', $sempid);
        $this->db2->delete('employeespouse');
         $query = $this->db2->last_query();
        $this->db2->select('companyId');
        $this->db2->from('employee');
        $this->db2->join('employeespouse','employeespouse.employeeId=employee.employeeId','left')->where('employeespouse.employeeSpouseId',$sempid);
        $companyId = $this->db2->get()->row_array();
        $query = $this->db2->last_query();
        $queryName = "DELETED EMPLOYEE RELATION";
        $this->ion_auth->logFile($queryName, $query,'',$companyId['companyId']);
    }
    

    public function delete_employee($empid) {
        $this->db2->where('employeeId', $empid);
        $this->db2->delete('employeespouse');
         $query = $this->db2->last_query();
        $this->db2->select('companyId');
        $this->db2->from('employee')->where('employeeId',$empid);
        $companyId = $this->db2->get()->row_array();
        $queryName = "DELTED EMPLOYEE SPOUSE";
        $this->ion_auth->logFile($queryName, $query,'',$companyId['companyId']);
        $this->db2->where('employeeId', $empid);
        $this->db2->delete('employee');
        $query = $this->db2->last_query();
        $this->db2->select('companyId');
        $this->db2->from('employee')->where('employeeId',$empid);
        $companyId = $this->db2->get()->row_array();
        $queryName = "DELETED EMPLOYEE";
        $this->ion_auth->logFile($queryName, $query,'',$companyId['companyId']);
        
    }

    public function delete_employee_child($empid, $relation) {
        $this->db2->where('employeeId', $empid);
        $this->db2->where('emp_relation', $relation);
        $this->db2->delete('employeespouse');
        $query = $this->db2->last_query();
        $this->db2->select('companyId');
        $this->db2->from('employee')->where('employeeId',$empid);
        $companyId = $this->db2->get()->row_array();
        $queryName = "DELETED EMPLOYEE CHILD";
        $this->ion_auth->logFile($queryName, $query,'',$companyId);
    }
    

    public function company_product_count($cid) {
        $this->db2->select('count(prod_id) as count');
        $this->db2->where('comp_id', $cid);
        $query = $this->db2->get('company_product')->result();
        return $query[0]->count;
    }
    

    public function get_occupation($cid) {
        $this->db2->select('count(p.productId) as count');
        $this->db2->where('c.comp_id', $cid);
        $this->db2->where('p.occupation', '1');
        $this->db2->join('company_product as c', 'c.prod_id=p.productId');
        $query = $this->db2->get('product as p')->result();
        return $query[0]->count;
    }
     public function get_salary($cid) {
        $this->db2->select('count(p.productId) as count');
        $this->db2->where('c.comp_id', $cid);
        $this->db2->where('p.salary', '1');
        $this->db2->join('company_product as c', 'c.prod_id=p.productId');
        $query = $this->db2->get('product as p')->result();
        return $query[0]->count;
    }
    

    public function get_company_all_detail($cid) {
        $this->db2->select('u.id, u.company, u.address, u.zipcode, u.country, u.state, u.city, u.email, c.sicId, c.no_of_emp, c.companyId, c.brokerage_name, c.broker_fname, c.broker_lname, c.npn, c.phone, c.email as bemail');
        $this->db2->where('c.companyId', $cid);
        $this->db2->join('company as c', 'c.userId=u.id');
        return $this->db2->get('users as u')->result();
    }
    

    public function update_user($data, $uid) {
        $this->db2->where('id', $uid);
        $this->db2->update('users', $data);
        $query = $this->db2->last_query();
        $this->db2->select('companyId');
        $this->db2->from('company');
        $this->db2->where('userId',$uid);
        $result = $this->db2->get()->row_array();
        $queryName = "UPDATE USER";
        $this->ion_auth->logFile($queryName, $query,'',$result['companyId']);
    }
    

    public function update_company($data, $cid) {
        $this->db2->where('companyId', $cid);
        $this->db2->update('company', $data);
        $query = $this->db2->last_query();
        $queryName = "UPDATE COMPANY";
        $this->ion_auth->logFile($queryName, $query, '', $cid);
    }
     public function get_company_employee_all_detail($cid)
    {
        //$this->db->distinct('es.employeeId');
        $this->db2->select('e.employeeId, e.firstName, e.lastName, e.gender, e.dateOfBirth, e.salary, es.emp_relation, es.dateofBirth as birth, e.zipcode');
        $this->db2->where('e.companyId',$cid);
        $this->db2->join('employeespouse as es','es.employeeId=e.employeeId','left');
        $this->db2->group_by('e.employeeId');
        return $this->db2->get('employee as e')->result();
        
    }

    /* public function getBrokerIdByUserId($userId){
      return $this->db2->select('brokerId')->where('userId',$userId)->get('broker')->row_array();
      }
      public function get_brokerage_id($userId){
      return $this->db2->select('id')->where('userId',$userId)->get('brokerage')->row_array();
      } */
    public function get_emp_relation_count($eid,$relation)
    {
        $this->db2->select('count(employeeSpouseId) as count');
        $this->db2->where('employeeId',$eid);
        $this->db2->where('emp_relation',$relation);
        //$this->db2->group_by('emp_relation');
        return $this->db2->get('employeespouse')->result();
        
    }
    public function get_active_email()
    {
        $this->db2->where('status','1');
        return $this->db2->get('admin_mail_configuration')->result();
    }
    public function add_company_note($data)
    {
        $this->db2->insert('company_note',$data);
    }
    public function get_company_note($cid,$uid)
    {
        $this->db2->where('companyId',$cid);
        $this->db2->where('userId',$uid);
     return $this->db2->get('company_note')->result();   
    }
    public function get_employees()
    {
        $this->db2->select('u.first_name, u.last_name, u.id');
        $this->db2->where_in('group_id',array('2','3'));
        $this->db2->join('users_groups as ug','ug.user_id=u.id');
        return $this->db2->get('users as u')->result();
        //$this->db2->last_query();
        //exit;
    }
    public function company_assign_employee($data,$cid)
    {
        $this->db2->where('companyId',$cid);
        $query=$this->db2->get('company_assign')->result();
        if(empty($query))
         $this->db2->insert('company_assign',$data);
        else
        {
         $this->db2->where('companyId',$cid);
         $this->db2->update('company_assign',$data);
        }
    }
    public function get_company_assign_employee($cid,$uid)
    {
        /*$this->db2->select('u.first_name, u.last_name');
        $this->db2->where('ca.companyId',$cid);
        $this->db2->join('users as u','u.id=ca.userId');
     return $this->db2->get('company_assign as ca')->result();*/
       
        $ids=array('');
        $this->db2->select('u.first_name, u.last_name, u.id');
        $this->db2->where('ca.companyId',$cid);
        $this->db2->join('users as u','u.id=ca.userId');
        $result['assigned']= $this->db2->get('company_assign as ca')->result();  
         foreach($result['assigned'] as $assign)
         {
             $ids[]=$assign->id;    
         }
        $this->db2->select('u.first_name, u.last_name, u.id');
        $this->db2->where_not_in('u.id',$uid);
        $this->db2->where_not_in('u.id',$ids);
        $this->db2->where_in('group_id',array('2','3'));
        $this->db2->join('users_groups as ug','ug.user_id=u.id');
        $result['unassigned'] = $this->db2->get('users as u')->result();
        return $result;
    }
    public function delete_note($id)
    {
        $this->db2->where('id',$id);
        $this->db2->delete('company_note');
    }
    public function get_assign_company($id)
    {
        $this->db2->where('userId',$id);
        return $this->db2->get('company_assign')->result();
    }
    public function delete_file($id)
    {
        $this->db2->where('id',$id);
        $this->db2->delete('comp_emp_doc');
    }

    public function get_all_company(){
        $this->db2->join('users', 'users.id = company.userId');
        return $this->db2->get('company')->result_array();
    }

    public function get_last_company_log($companyID){
        $this->db2->select('*');
        $this->db2->from('user_log')->order_by("logInId", "desc");
        $this->db2->where('user_log.company_id',$companyID);
        $this->db2->join('users', 'users.id = user_log.user_id');
//        $this->db2->where('logInTime!=',null);
        $this->db2->limit(1);
        return $result = $this->db2->get()->row_array();
    }
}
?>
