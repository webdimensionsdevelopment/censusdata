<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }
    public function get_no_of_companies(){
        return $this->db2->get('company')->num_rows();
    }
    public function get_no_of_quote_assigned(){
        return $this->db2->where('assign_status','assigned')->get('company')->num_rows();
    }
    public function get_no_of_quote_unassigned(){
        return $this->db2->where('assign_status','unassigned')->get('company')->num_rows();
    }
    public function get_no_of_QuoteStatus(){
        return $this->db2->where('quote_status','Census Downloaded')->get('company')->num_rows();
    }
    public function get_no_of_activeEmployees(){
        $this->db2->select('*');
        $this->db2->from('users');
        $this->db2->join('users_groups','users_groups.user_id=users.id');
        $this->db2->where('users_groups.group_id',3);
        $this->db2->where('users.active',1);
        return $this->db2->get()->result_array();
    }
    public function get_no_of_inactiveEmployees(){
        $this->db2->select('*');
        $this->db2->from('users');
        $this->db2->join('users_groups','users_groups.user_id=users.id');
        $this->db2->where('users.active!=','1');
        $this->db2->where('users_groups.group_id',3);
        return $this->db2->get()->result_array();
    }
    public function get_no_of_activeMembers(){
        $this->db2->select('*');
        $this->db2->from('users');
        $this->db2->join('users_groups','users_groups.user_id=users.id');
        $this->db2->where('users_groups.group_id',4);
        $this->db2->where('users.active',1);
        return $this->db2->get()->result_array();
    }
    public function get_no_of_inactiveMembers(){
        $this->db2->select('*');
        $this->db2->from('users');
        $this->db2->join('users_groups','users_groups.user_id=users.id');
        $this->db2->where('users.active!=','1');
        $this->db2->where('users_groups.group_id',4);
        return $this->db2->get()->result_array();
    }
    public function get_no_quote_basedON_NPN($npn){
        $this->db2->select('*');
        $this->db2->from('company');
        $this->db2->where('npn',$npn);
        return $this->db2->get()->num_rows();
    }
    public function get_count_record($table)
    {
        $query = $this->db2->count_all($table);

        return $query;
    }


    public function disk_totalspace($dir = DIRECTORY_SEPARATOR)
    {
        return disk_total_space($dir);
    }


    public function disk_freespace($dir = DIRECTORY_SEPARATOR)
    {
        return disk_free_space($dir);
    }


    public function disk_usespace($dir = DIRECTORY_SEPARATOR)
    {
        return $this->disk_totalspace($dir) - $this->disk_freespace($dir);
    }


    public function disk_freepercent($dir = DIRECTORY_SEPARATOR, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->disk_freespace($dir) * 100) / $this->disk_totalspace($dir), 0).$unit;
    }


    public function disk_usepercent($dir = DIRECTORY_SEPARATOR, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->disk_usespace($dir) * 100) / $this->disk_totalspace($dir), 0).$unit;
    }


    public function memory_usage()
    {
        return memory_get_usage();
    }


    public function memory_peak_usage($real = TRUE)
    {
        if ($real)
        {
            return memory_get_peak_usage(TRUE);
        }
        else
        {
            return memory_get_peak_usage(FALSE);
        }
    }


    public function memory_usepercent($real = TRUE, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->memory_usage() * 100) / $this->memory_peak_usage($real), 0).$unit;
    }
}
