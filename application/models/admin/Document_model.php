<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Document_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_documents(){
        return $this->db2->get("document")->result_array();
    }

    public function get_document_by_id($id){
        $this->db2->where('documentId', $id);
        return $this->db2->get("document")->row_array();
    }

    public function document_update($id, $data){
        return $this->db2->where('documentId', $id)->update("document", $data);
    }

    public function document_insert($data){
        $this->db2->insert("document", $data);
        return $this->db2->insert_id();
    }

    public function document_activate($id){
        $status = array('status'=> '1');
        return $this->db2->where('documentId', $id)->update("document", $status);
    }

    public function document_deactivate($id){
        $status = array('status'=> '0');
        return $this->db2->where('documentId', $id)->update("document", $status);
    }
}
