<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questionnaire_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_questions(){
        return $this->db2->get("questionnaires_questions")->result_array();
    }

    public function get_question_product($id){
        $this->db2->select('name');
        $this->db2->where('productId', $id);
        return $this->db2->get("product")->row_array();
    }

    public function get_questions_by_id($id){
        $this->db2->where('id', $id);
        return $this->db2->get("questionnaires_questions")->row_array();
    }

    public function get_questions_answer_by_question_id($id){
        $this->db2->where('question_id', $id);
        return $this->db2->get("questions_answers")->result_array();
    }

    public function questionnaire_update($id, $data){
        return $this->db2->where('id', $id)->update("questionnaires_questions", $data);
    }

    public function answer_update($id, $questionId, $data){
        $count = $this->db2->where('id', $id)->where('question_id', $questionId)->get("questions_answers")->num_rows();
        if($count == 1){
            $this->db2->where('id', $id)->where('question_id', $questionId)->update("questions_answers", $data);
            return $count;
        } else {
            return $count;
        }
    }

    public function question_insert($data){
        $this->db2->insert("questionnaires_questions", $data);
        return $this->db2->insert_id();
    }

    public function answer_insert($data){
        return $this->db2->insert("questions_answers", $data);
    }

    public function questionnaire_answer_delete($id, $questionId) {
        // Delete a single answer
        return $this->db2->where('id', $id)->where('question_id', $questionId)->delete("questions_answers");
      //  return $this->db2->where('id', $id)->delete("questionnaires_questions");
    }

    public function questionnaire_delete_empty_answers($questionId) {
        // Delete all empty answers
        $deleteArray = array('answer' => "", 'question_id' =>  $questionId);
        return $this->db2->where($deleteArray)->delete("questions_answers");
      //  return $this->db2->where('id', $id)->delete("questionnaires_questions");
    }

    public function questionnaire_activate($id){
        $status = array('status'=> '1');
        return $this->db2->where('id', $id)->update("questionnaires_questions", $status);
    }

    public function questionnaire_deactivate($id){
        $status = array('status'=> '0');
        return $this->db2->where('id', $id)->update("questionnaires_questions", $status);
    }
}
