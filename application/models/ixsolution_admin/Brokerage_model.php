<?php

class Brokerage_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    public function getBrokersById($userId){
        $this->db->select('*');
        $this->db->from('brokerage');
        $this->db->join('broker','broker.brokrage_id=brokerage.id');
        $this->db->join('users', 'users.id = broker.userId');
        $this->db->where('brokerage.userId', $userId);
        return $query = $this->db->get()->result_array(); 
    }
    public function insertBroker($record){
        $this->db->insert("broker", $record);
        $query = $this->db->last_query();
        $queryName = "INSERTED BROKER";
        //$this->ixsolution_ion_auth->logFile($queryName,$query);
        return $this->db->insert_id();
    }
    public function get_brokerage_id($userId){
        return $this->db->select('id')->where('userId',$userId)->get('brokerage')->row_array();
    }
    public function get_broker_companylist_byBrokerId($id){
        $this->db->select('*');
        $this->db->from('broker');
        $this->db->join('company','company.brokerId=broker.brokerId','inner');
        $this->db->join('users', 'users.id = company.userId');
        $this->db->where('broker.brokerId', $id);
        return $query = $this->db->get()->result_array(); 
    }
}