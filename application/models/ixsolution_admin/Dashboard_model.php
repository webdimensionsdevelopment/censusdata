<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }
    public function get_no_of_companies(){
        return $this->db->get('company')->num_rows();
    }
    public function get_no_of_quote_assigned(){
        $where = '(company.brokerId>=0 OR company.brokrage_id>=0)';
        return $this->db->where($where)->get('company')->num_rows();

    }
    public function get_no_of_quote_unassigned(){
        $where = '(company.brokerId IS NULL AND company.brokrage_id IS NULL)';
        return $this->db->where($where)->get('company')->num_rows();
    }
    public function get_no_of_QuoteStatus(){
        return $this->db->where('quote_status','Census Downloaded')->get('company')->num_rows();
    }
    public function get_no_of_activeEmployees(){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('users_groups','users_groups.user_id=users.id');
        $this->db->where('users_groups.group_id',3);
        $this->db->where('users.active',1);
        return $this->db->get()->num_rows();
    }
    public function get_count_record($table)
    {
        $query = $this->db->count_all($table);

        return $query;
    }


    public function disk_totalspace($dir = DIRECTORY_SEPARATOR)
    {
        return disk_total_space($dir);
    }


    public function disk_freespace($dir = DIRECTORY_SEPARATOR)
    {
        return disk_free_space($dir);
    }


    public function disk_usespace($dir = DIRECTORY_SEPARATOR)
    {
        return $this->disk_totalspace($dir) - $this->disk_freespace($dir);
    }


    public function disk_freepercent($dir = DIRECTORY_SEPARATOR, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->disk_freespace($dir) * 100) / $this->disk_totalspace($dir), 0).$unit;
    }


    public function disk_usepercent($dir = DIRECTORY_SEPARATOR, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->disk_usespace($dir) * 100) / $this->disk_totalspace($dir), 0).$unit;
    }


    public function memory_usage()
    {
        return memory_get_usage();
    }


    public function memory_peak_usage($real = TRUE)
    {
        if ($real)
        {
            return memory_get_peak_usage(TRUE);
        }
        else
        {
            return memory_get_peak_usage(FALSE);
        }
    }


    public function memory_usepercent($real = TRUE, $display_unit = FALSE)
    {
        if ($display_unit === FALSE)
        {
            $unit = NULL;
        }
        else
        {
            $unit = ' %';
        }

        return round(($this->memory_usage() * 100) / $this->memory_peak_usage($real), 0).$unit;
    }
}
