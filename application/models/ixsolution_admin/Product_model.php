<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_products(){
        return $this->db->get("product")->result_array();
    }

    public function get_product_by_id($id){
        $this->db->where('productId', $id);
        return $this->db->get("product")->row_array();
    }

    public function product_activate($id){
        $status = array('status'=> '1');
        return $this->db->where('productId', $id)->update("product", $status);
    }

    public function product_deactivate($id){
        $status = array('status'=> '0');
        return $this->db->where('productId', $id)->update("product", $status);
    }

    public function product_update($id, $data){
        return $this->db->where('productId', $id)->update("product", $data);
    }

    public function product_insert($data){
        $this->db->insert("product", $data);
        return $this->db->insert_id();
    }

    public function product_delete($id){
        return $this->db->where('productId', $id)->delete("product"    );
    }
}
