<?php

class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    public function checkEmail($email){
        return $this->db->select('email')->where('email',$email)->get('users')->row_array();
    }
    public function addToBrokerageUserId($recordBrokerage){
        $this->db->insert("brokerage",$recordBrokerage);
        return $this->db->insert_id();
    }
     public function addToBrokerUserId($recordBroker){
        $this->db->insert("broker",$recordBroker);
        return $this->db->insert_id();
    }
}