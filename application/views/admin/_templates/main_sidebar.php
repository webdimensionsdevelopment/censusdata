<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<aside class="main-sidebar">
    <section class="sidebar">
        <?php if ($admin_prefs['user_panel'] == TRUE): ?>
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo base_url($avatar_dir . '/m_001.png'); ?>" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?php echo $user_login['firstname'] . $user_login['lastname']; ?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo lang('menu_online'); ?></a>
                </div>
            </div>

        <?php endif; ?>
        <?php if ($admin_prefs['sidebar_form'] == TRUE): ?>
            <!-- Search form -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control"
                           placeholder="<?php echo lang('menu_search'); ?>...">
                    <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                                class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>

        <?php endif; ?>
        <!-- Sidebar menu -->
        <ul class="sidebar-menu">
            <li>
                <a href="<?php echo site_url('/'); ?>">
                    <i class="fa fa-home text-primary"></i> <span><?php echo lang('menu_access_website'); ?></span>
                </a>
            </li>
 <?php if ($this->ion_auth->is_admin()) { ?>
            <li class="header text-uppercase"><?php echo lang('menu_main_navigation'); ?></li>
            <li class="<?php active_link_controller('dashboard') ?>">
                <a href="<?php echo site_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i> <span><?php echo lang('menu_dashboard'); ?></span>
                </a>
            </li>

           
                <li class="header text-uppercase"><?php echo lang('menu_administration'); ?></li>
                <li class="<?= active_link_controller('Company') ?>">
                    <a href="<?php echo site_url('admin/Company/company_list_view'); ?>">
                        <i class="fa fa-building" aria-hidden="true"></i><span><?php echo lang('menu_quote_requests'); ?></span>
                    </a>
                </li>

                <li class="<?= active_link_controller('users') ?>">
                    <a href="<?php echo site_url('admin/users'); ?>">
                        <i class="fa fa-user"></i> <span><?php echo lang('menu_users'); ?></span>
                    </a>
                </li>
                <li class="<?= active_link_controller('logDetails') ?>">
                    <a href="<?php echo site_url('admin/logDetails'); ?>">
                        <i class="fa fa-cogs" aria-hidden="true"></i> <span><?php echo lang('menu_logs'); ?></span>
                    </a>
                </li>
                <li class="<?= active_link_controller('logsBrokerDetails') ?>">
                    <a href="<?php echo site_url('admin/logsBrokerDetails'); ?>">
                        <i class="fa fa-cogs" aria-hidden="true"></i> <span><?php echo lang('menu_broker_logs'); ?></span>
                    </a>
                </li> 
                <li class="<?= active_link_controller('Quoterequestlog') ?>">
                    <a href="<?php echo site_url('admin/Quoterequestlog'); ?>">
                        <i class="fa fa-cogs" aria-hidden="true"></i> <span><?php echo lang('menu_quoteRequest_Log'); ?></span>
                    </a>
                </li> 
                <li class="<?= active_link_controller('product') ?>">
                    <a href="<?php echo site_url('admin/product'); ?>">
                        <i class="fa fa-product-hunt"></i> <span><?php echo lang('menu_products'); ?></span>
                    </a>
                </li>
                <li class="<?= active_link_controller('questionnaire') ?>">
                    <a href="<?php echo site_url('admin/questionnaire'); ?>">
                        <i class="fa fa-question"></i> <span><?php echo lang('menu_questionnaire'); ?></span>
                    </a>
                </li>
                <li class="<?= active_link_controller('document') ?>">
                    <a href="<?php echo site_url('admin/document'); ?>">
                       <i class="fa fa-book" aria-hidden="true"></i> <span><?php echo lang('menu_document'); ?></span>
                    </a>
                </li>
                <li class="<?= active_link_controller('admin_email_configuration') ?>">
                    <a href="<?php echo site_url('admin/admin_email_configuration'); ?>">
                        <i class="fa fa-envelope-o"></i> <span><?php echo lang('menu_admin_mail_config'); ?></span>
                    </a>
                </li>
                <li class="<?= active_link_controller('groups') ?>">
                    <a href="<?php echo site_url('admin/groups'); ?>">
                        <i class="fa fa-shield"></i> <span><?php echo lang('menu_security_groups'); ?></span>
                    </a>
                </li>

            <?php } ?>
            <?php if ($this->ion_auth->is_member()) { ?>
                <li class="<?= active_link_controller('admin/Company') ?>">
                    <a href="<?php echo site_url('admin/Company/company_list_view'); ?>">
                        <i class="fa fa-file"></i> <span><?php echo lang('menu_quote_requests'); ?></span>
                    </a>
                </li>
            <?php } ?>
            <?php if ($this->ion_auth->is_employee()) { ?>
                <li class="<?= active_link_controller('admin/Company') ?>">
                    <a href="<?php echo site_url('admin/Company/company_list_view'); ?>">
                        <i class="fa fa-file"></i> <span><?php echo lang('menu_quote_requests'); ?></span>
                    </a>
                </li>
            <?php } ?>


        </ul>
    </section>
</aside>
