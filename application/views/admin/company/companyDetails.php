<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.6/css/jquery.dataTables.css"><script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script><script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.6/js/jquery.dataTables.js"></script>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo '<h1>Company Details</h1>' ?>
        <?php echo $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Company Details View: <?php echo $company[0]['username'] . " - " . $company[0]['email']; ?></h3>
                        <div class="pull-right">
                            <a href="<?php echo base_url() . 'admin/Company/company_list_view' ?>" style="color: white;">
                                <span class="clickable filter" title="Back" data-container="body">
                                    <i class="fa fa-backward"> <?php echo 'Back'; ?></i>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="header">
                            <h3 style="text-align: center;">Company Details</h3>
                        </div><hr>
                        <div class="form-group col-md-12">
                            <label name="lblCompanyName" class="col-md-2" value="Company Name">Company Name :</label>
                            <div class="col-md-2">
                                <?php echo $company[0]['company']; ?>
                            </div>
                            <label name="lblQuoteRequestedDate" class="col-md-2" value="QuoteRequestedDate">Quote Requsted Date :</label>
                            <div class="col-md-2">
                                <?php $dt = new DateTime($company[0]['createdAt']); ?><?php echo $date = $dt->format('m/d/Y'); ?>
                            </div>
                            <label name="lblBrokerageName" class="col-md-2" value="BrokerName">Broker Name :</label>
                            <div class="col-md-2">
                                <?php echo $company[0]['brokerage_name']; ?>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label name="lblnpn" class="col-md-2" value="npn">NPN :</label>
                            <div class="col-md-2"><?php echo $company[0]['npn']; ?></div>
                            <label name="lblZipCode" class="col-md-2" value="BrokerName">Zip Code :</label>
                            <div class="col-md-2"><?php echo $company[0]['zipcode']; ?></div>
                            <label name="lblCity" class="col-md-2" value="City">City :</label>
                            <div class="col-md-2"><?php echo $company[0]['city']; ?></div>
                        </div>
                        <div class="form-group col-md-12">
                            <label name="lblSate" class="col-md-2" value="State">State :</label>
                            <div class="col-md-2"><?php echo $company[0]['state']; ?></div>
                            <label name="lblSicCode" class="col-md-2" value="sicCode">SIC Code :</label>
                            <div class="col-md-2"><?php echo $company[0]['code']; ?></div>
                            <label name="lblNumberOfEmployees" class="col-md-2" value="NumberOfEmployees"># of Employees :</label>
                            <div class="col-md-2"><?php echo $company[0]['no_of_emp']; ?></div>
                        </div>
                    </div>
                    <div class="modal-body" style="position: static !important;">
                        <div class="header">
                            <h3 style="text-align: center;">Document Details</h3>
                        </div><hr>
                        <div class="form-group col-md-12" style="margin-top: 0px;">
                            <label name="lblCompanyName" class="col-md-2" value="noOfFile"># </label>
                            <label name="lblCompanyName" class="col-md-6" value="">File Name</label>
                            <label name="lblCompanyName" class="col-md-2" value="">File Type</label>
                            
                        </div>
                        <?php if ($documents!=0) : ?>
                                <?php $count =0; foreach ($documents as $document): $count ++;?>
                                <div class="form-group col-md-12" style="margin-top: 0px;">
                                    <label name="lblCompanyName" class="col-md-2" value="<?php echo "File".$count?>"><?php echo $count?></label>
                                    <div class="col-md-6" style="margin-top: 0px;">
                                        <a href="<?php echo base_url(). 'admin/company/download_doc?docid='.$document['id']; ?> "> <?php echo $document['document_name'];?></a>
                                    </div>
                                    <div class="col-md-2" style="margin-top: 0px;">
                                        <?php echo $document['documentFile'];?>
                                    </div>
                                </div>
                            <?php endforeach;?>
                        <?php endif;?>
                    </div>
                    <div class="modal-body" style="position: static !important;">
                        <div class="header">
                            <h3 style="text-align: center;">Product Details</h3> 
                        </div><hr>
                        <?php $counter = count ($product);foreach ($product as $productArray) : ?>
                            <div class="header">
                                <h3 class="col-md-3" style="margin-top: 0px;">
                                    <?php echo $productArray['name']; ?> </h3>
                                <h4 class="col-md-2" style="margin-top: 0px;">Effective Date :</h4><span class="col-md-2"> <?php echo ($productArray['effective_date'])?$productArray['effective_date']:'---'; ?></span>
                                <h4 class="col-md-2" style="margin-top: 0px;">Renewal Date :</h4><span class="col-md-2"> <?php echo ($productArray['renewal_date'])?$productArray['renewal_date']:'---'; ?></span>
                                
                            </div>
                        <div class="col-md-12"><h4 class="col-md-2 col-md-offset-3" style="margin-top: 0px;font-style: bold;">Note :</h4><span class="col-md-6"><?php echo $productArray['note'][0]['ques_answer'];?></span></div>

                            <div class="body">
                                <table class="table table-striped table-hover" style="border-bottom:1px solid black;border-top:1px solid black;">
                                    <thead>
                                        <th><?php echo 'Questions'; ?></th>
                                        <th><?php echo 'Answer'; ?></th>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($productArray['questions'] as $key => $question) :?>
                                        <tr>
                                            <td><label><?php echo $question['question']; ?></label></td>
                                            <td><?php foreach ($productArray['questions'][$key]['answers'] as $key => $answer) :?>
                                                    <?php echo $answer['answer']?>
                                                <?php endforeach;?>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>    
                                </table>        
                            </div>
                        <?php endforeach;?>
                    </div>
                    <div class="modal-body">
                        <div class="header">
                            <h3 style="text-align: center;">Employees Details</h3>
                        </div><hr>
                        <table class="table table-striped table-hover" id="dev-table">
                            <thead>
                                <tr>
                                    <th><?php echo 'Employee Id' ?></th>
                                    <th><?php echo 'First Name' ?></th>
                                    <th><?php echo 'Last Name' ?></th>
                                    <th><?php echo 'Gender'; ?></th>
                                    <th><?php echo 'Date Of Birth'; ?></th>
                                    <th><?php echo 'Age'; ?></th>
                                    <th><?php echo 'Zip Code'; ?></th>
                                    <th><?php echo 'State'; ?></th>
                                    <th><?php echo 'Salary'; ?></th>
                                    <th><?php echo 'Relation'; ?></th>        
                                </tr>
                            </thead>
                            <tbody>
                                <?php $count=0; foreach ($employee as $user) : $count++;?>
                                    <tr>
                                        <td><?php echo $count; ?></td>
                                        <td><?php echo $user['firstName']; ?></td>
                                        <td><?php echo $user['lastName']; ?></td>
                                        <td><?php echo ($user['gender'] == 0) ? 'Female' : 'Male'; ?></td>
                                        <td><?php echo $user['dateOfBirth']; ?></td>
                                        <?php $currentYear = date('Y'); $dt = new DateTime($user['dateOfBirth']); $birthYear = $dt->format('Y') ?>
                                        <td><?php echo $currentYear - $birthYear; ?></td>
                                        <td><?php echo $user['zipcode']; ?></td>
                                        <td><?php echo $user['state']; ?></td>
                                        <td><?php echo ($user['salary']) ? $user['salary'] : '---'; ?></td>
                                        <td>Employee</td>
                                    </tr>
                                    <?php if ($user['employeeSpouse']!= 0) : ?>
                                        <?php foreach ($user['employeeSpouse'] as $employeespouse) : ?>
                                        <tr>
                                            <td><?php echo $count; ?></td>
                                            <td><?php echo $employeespouse['esFirstName']; ?></td>
                                            <td><?php echo $employeespouse['esLastName']; ?></td>
                                            <td><?php echo ($employeespouse['gender'] == 0) ? 'Female' : 'Male'; ?></td>
                                            <td><?php echo $employeespouse['dateOfBirth']; ?></td>
                                            <?php $currentYear = date('Y'); $dt = new DateTime($employeespouse['dateOfBirth']); $birthYear = $dt->format('Y') ?>
                                            <td><?php echo $currentYear - $birthYear; ?></td>
                                            <td>---</td>
                                            <td>---</td>
                                            <td>---</td>
                                            <td><?php echo ($employeespouse['emp_relation']) ? $employeespouse['emp_relation'] : '---' ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                   <?php endif; ?>
                                <?php endforeach; ?>    
                            </tbody>
                        </table>
                    </div><hr>					
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        $("#search").click(function () {
            var $this = $(this),
                    $panel = $this.parents('.panel');
            $panel.find('.panel-body').slideToggle();
            if ($this.css('display') != 'none') {
                $panel.find('.panel-body input').focus();
            }
        });
    });
    function searchTable(inputVal)
    {
        var table = $('#dev-table');
        table.find('tr').each(function (index, row)
        {
            var allCells = $(row).find('td');
            if (allCells.length > 0)
            {
                var found = false;
                allCells.each(function (index, td)
                {
                    var regExp = new RegExp(inputVal, 'i');
                    if (regExp.test($(td).text()))
                    {
                        found = true;
                        return false;
                    }
                });
                if (found == true)
                    $(row).show();
                else
                    $(row).hide();
            }
        });
    }
</script>
<script>
$(document).ready(function(){
    $.noConflict();
  $('#dev-table').DataTable({
      "searching":   true,
      "aaSorting": []
  });  
  $('.dev-table').DataTable({
      "searching":   true,
      "aaSorting": []
  });  
});
</script>