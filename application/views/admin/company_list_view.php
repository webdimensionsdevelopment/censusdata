<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.6/css/jquery.dataTables.css"><script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script><script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.6/js/jquery.dataTables.js"></script>
<style>
.modal-content  {
    -webkit-border-radius: 5px !important;
    -moz-border-radius: 5px !important;
    border-radius: 5px !important;
}
</style>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
    <div class="content" >
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="margin-bottom: 15px;">
                        <h3 class="panel-title">Companies</h3>
<!--                        <div class="pull-right">
                            <span class="clickable filter" data-toggle="tooltip" title="Toggle table filter" data-container="body">
                                <i class="glyphicon glyphicon-filter" id='search'>Search</i> 
                            </span>
                        </div>-->
                        <div class="pull-right">
                            <a href="<?php echo base_url() . 'admin/Company' ?>" style="color: white;">
                                <span class="clickable filter" title="Create Company" data-container="body">
                                    <i class="fa fa-plus"> <?php echo 'Create Company'; ?></i>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter Log Details" onkeyup="searchTable(this.value);"/>
                    </div>
                    <table class="table table-striped table-hover" id="dev-table">
                        <thead>
                        <th>Quote Request ID</th>
                        <th>Employer Company Name</th>
                        <th># of Employees</th>
                        <th>Quote Request Date</th>
                        <th>Broker Name</th>
                        <th>Broker NPN No.</th>
                        <th>Rep Assigned to Quote</th>
                        <th>Status</th>
                        <th>Action</th>
                        <th>Option</th>
                        </thead>
                        <tbody>
                            <?php if (!empty($company_detail)) { foreach ($company_detail as $comp_detail) { ?>
                                <tr>
                                        <td><a href="<?php echo base_url()."admin/Company/companyDetails/".$comp_detail->companyId; ?>"><?php echo $comp_detail->companyId ?></a></td>
                                        <td><a href="<?php echo base_url()."admin/Company/companyDetails/".$comp_detail->companyId; ?>"><?php echo $comp_detail->company ?></a><br>
                                        <?php
                                        foreach($assign_company as $comp_assign)
                                        {
                                            if($comp_assign->companyId==$comp_detail->companyId)
                                            {
                                        ?>
                                            <img src="<?php echo base_url()?>upload/GAFlex/index.png" width="70px" height="50px" alt="">
                                        <?php
                                            }
                                        }
                                        ?>
                                            </td>
                                        <td><?php echo $comp_detail->no_of_emp ?></td>
                                        <?php $dt = new DateTime($comp_detail->createdAt); ?>
                                        <td><?php echo $date = $dt->format('m/d/Y') ?></td>
                                        <td><?php echo $comp_detail->broker_fname . " " . $comp_detail->broker_lname; ?></td> 
                                        <td><?php echo $comp_detail->npn ?></td>
                                        <td><?php

                                            if ($comp_detail->assign_status == 'assign' && !empty($comp_detail->repfirst))
                                            {
                                                $comp_detail->assign_status = $comp_detail->repfirst . ' ' . $comp_detail->replast;
                                            }


                                            echo $comp_detail->assign_status;


                                            ?></td>
                                        <td><?php echo $comp_detail->quote_status ?></td>
                                        <td>
                                        <?php 
                                        if(empty($member))
                                        {  
                                        ?>
                                        <a href="<?php echo site_url() ?>admin/Company/update_company_view/<?php echo $comp_detail->companyId ?>">Edit</a><br><?php if($comp_detail->close_status=='1'){?><a href="<?php echo site_url() ?>admin/Company/insert_company/<?php echo $comp_detail->companyId ?>">Close</a><?php } else{ echo "Close"; }?><br><a href="" data-target="#viewnotemodel" data-id="<?php echo $comp_detail->companyId;?>" data-toggle="modal" class="open_view_note_modal">Notes</a>
                                        <?php
                                        }
                                       ?>
                                        <br><a href="" data-target="#assignmodel" data-id="<?php echo $comp_detail->companyId;?>" data-toggle="modal" class="open_assign_modal">Assign</a>
                                        
                                        </td>
                                        <td ><select id="sample<?php echo $comp_detail->companyId ?>" name="sample<?php echo $comp_detail->companyId ?>" onchange="examples(<?php echo $comp_detail->companyId ?>)">
                                                    <option value="">Samples</option>
                                                    <option value="1">Aetna</option>
                                                    <option value="2">BCBSIL</option>
                                                    <option value="3">Ease Central</option>
                                                    <option value="4">UHC</option>
                                                    <option value="5">LimeLight</option>
                                                    <!--<option value="6">Simple</option>-->
                                                </select>
                                            <a href=""  name="download<?php echo $comp_detail->companyId ?>" id="download<?php echo $comp_detail->companyId ?>" onclick="examples(<?php echo $comp_detail->companyId ?>)"><h3 class="box-title btn btn-block btn-primary btn-flat">Download</h3></a></td>
                                    </tr>
                            <?php } } ?>
                        </tbody>
                    </table>    
                </div>
            </div>
        </div>
    </div>
</div>

<!--start View Notes modal-->
    <div class="modal fade" id="viewnotemodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Company Notes</h4>
                </div>
                <div class="modal-body">
                    <table class="view_note_table table table-striped">
                        <tbody>
                           
                            
                        </tbody>
                    </table>
                    <form>
                        <div class="form-group">
                            <input type="hidden" name="cid" id="cid">
                            <input type="hidden" name="userid" id="userid">
                            <textarea id="note" name="note" required="" class="form-control" placeholder="Add Notes"></textarea>
                      </div>
                   </form>
                </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="close">Close</button>
                        <button type="button" id="add_note_btn" class="btn btn-primary">Add Note</button>
                    </div>
            </div>
         </div>
    </div>
    <!--End View Notes modal-->
    <!--start assign modal-->
    <div class="modal fade" id="assignmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" >Company Assign To Employee</h4>
                </div>
                <div class="modal-body">
                    <table class="view_assign_table table table-striped">
                        <tbody>
                        </tbody>
                    </table>
                    <form>
                        <div class="form-group">
                            <input type="hidden" name="cid" id="cid">
                            <select class="form-control itemName" id="employee" name="employee" style="width:100% !important">
                                <option value="" selected>Employee </option>
                                
                            </select>
                         
                      </div>
                   </form>
                </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="close1">Close</button>
                        <button type="button" id="add_assign_btn" class="btn btn-primary">Assign to employee</button>
                    </div>
            </div>
         </div>
    </div>
    <!--End View assign modal-->
<script>
    $(document).ready(function () {
        $("#search").click(function () {
            var $this = $(this),
                    $panel = $this.parents('.panel');
            $panel.find('.panel-body').slideToggle();
            if ($this.css('display') != 'none') {
                $panel.find('.panel-body input').focus();
            }
        });
    });
    function searchTable(inputVal)
    {
        var table = $('#dev-table');
        table.find('tr').each(function (index, row)
        {
            var allCells = $(row).find('td');
            if (allCells.length > 0)
            {
                var found = false;
                allCells.each(function (index, td)
                {
                    var regExp = new RegExp(inputVal, 'i');
                    if (regExp.test($(td).text()))
                    {
                        found = true;
                        return false;
                    }
                });
                if (found == true)
                    $(row).show();
                else
                    $(row).hide();
            }
        });
    }
    function examples(no)
    {
        var value = $('#sample' + no).val();
        if (value == '')
        {
            alert('please select any format for download');
        }
        else if (value == 1)
        {
            $("#download" + no).attr("href", "<?php echo site_url() ?>admin/Company/aetna_export/" + no);
        }
        else if (value == 2)
        {
            $("#download" + no).attr("href", "<?php echo site_url() ?>admin/Company/bcbsil_export/" + no);
        }
        else if (value == 3)
        {
            $("#download" + no).attr("href", "<?php echo site_url() ?>admin/Company/easecentral_export/" + no);
        }
        else if (value == 4)
        {
            $("#download" + no).attr("href", "<?php echo site_url() ?>admin/Company/uhc_export/" + no);
        }
        else if (value == 5)
        {
            $("#download" + no).attr("href", "<?php echo site_url() ?>admin/Company/limelight_export/" + no);
        }
        else if (value == 6)
        {
            $("#download" + no).attr("href", "<?php echo site_url() ?>admin/Company/export_company_detail/" + no);
        }
        else
        {
            $("#download" + no).attr("disbled", "disabled");
        }
    }
</script>
<script>
$(document).ready(function(){
    $.noConflict();
  $('#dev-table').DataTable({
      "searching":   true,
      "order": [[ 0, "desc" ]]
  });  
});
</script>
<script>
$(document).on('click','#add_note_btn', function(){
    note=$('#note').val();
    cid=$('#cid').val();
    uid=$('#userid').val();
    $.ajax({
       url: "<?php echo site_url()?>admin/Company/add_company_note/"+note+"/"+cid+"/"+uid,
      type: "GET",
      success: function(output) {
                    $('#note').val('');
                     $( "#close" ).click();
                     
                  }
    });
});
$(document).on("click", ".open_view_note_modal", function () {
cid = $(this).data('id');
$('#cid').val(cid);
$('#userid').val(<?php echo $userid;?>);
$.ajax({
   url:"<?php echo site_url()?>admin/Company/get_company_note/"+cid,  
   dataType: "json",
   success:function(res){
       var row = "<tbody>";
       $.each(res, function (index, comp_note) {
           var new_date=comp_note.created_at;
           var dates = new_date.split('-');
           var created_at = dates[1] + '-' + dates[2] + '-' + dates[0];
                   row += "<tr><td style='padding-right: 40px'>" + comp_note.note + "</td><td style='padding-right: 40px'>" + created_at + "</td><td style='padding-right: 40px'><a href='<?php echo site_url()?>admin/Company/delete_note/"+comp_note.id+"'>delete</a></td></tr>";
                });
       
                $(".view_note_table").html(row);
   }
});
});

</script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> 
   <script>
$('.itemName').select2();    
</script>
<script>
$(document).on('click','#add_assign_btn', function(){
    emp=$('#employee').val();
    cid=$('#cid').val();
    $.ajax({
       url: "<?php echo site_url()?>admin/Company/company_assign_employee/"+emp+"/"+cid,
      success: function(output) {
              window.location.reload();
      }
    });
});
$(document).on("click", ".open_assign_modal", function () {
cid = $(this).data('id');
$('#cid').val(cid);

$.ajax({
   url:"<?php echo site_url()?>admin/Company/get_company_assign_employee/"+cid,  
   dataType:"json",
   success:function(res){
         
     var row = "<tbody><tr><td colspan='4' style='padding-right: 40px'>";
        $.each(res.assigned, function (index, comp_emp) {
             row += comp_emp.first_name+" "+comp_emp.last_name;
        });
        row+="</td></tr>";
        $(".view_assign_table").html(row);
        var option= "<option value=''>Employee</option>";
        $.each(res.unassigned, function (index, comp_emp1){
           option+="<option value='"+comp_emp1.id+"'>"+comp_emp1.first_name+" "+comp_emp1.last_name+"</option>";
        });
        $('#employee').html(option);
        console.log(res.unassigned);
   }
});
});

</script>