<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.6/css/jquery.dataTables.css"><script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script><script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.6/js/jquery.dataTables.js"></script>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo '<h1>Quote Request Log Details</h1>' ?>
        <?php echo $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="margin-bottom: 15px;">
                        <h3 class="panel-title">Quote Request Details</h3>
                    </div>
                    <table class="table table-striped table-hover" id="dev-table">
                        <thead>
                        <th><?php echo lang('users_userName'); ?></th>
                        <th><?php echo lang('users_email'); ?></th>
                        <th><?php echo lang('users_action'); ?></th>
                        </thead>
                        <tbody>
                        <?php $count = 0;
                        foreach ($company as $key => $compArray): //echo "<pre />"; print_r($compArray);exit; ?>
                            <?php if ($count) { ?>
                                <tr>
                                    <td><?php echo anchor('admin/quoterequestlog/logQueries/' . $compArray['companyId'], $compArray['username']); ?></td>
                                    <td><?php echo $compArray['email']; ?></td>
                                    <td><?php echo anchor('admin/quoterequestlog/logQueries/' . $compArray['companyId'], lang('actions_see')); ?></td>
                                </tr>
                            <?php } ?>
                            <?php $count++;endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
$(document).ready(function(){
    $.noConflict();
    $('#dev-table').DataTable({
      "searching":   true,
      "aaSorting": []
    });  
});
</script>