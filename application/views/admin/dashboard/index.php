<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
    <?php if ($this->ion_auth->is_admin()) { ?>
        <section class="content">
            <?php echo $dashboard_alert_file_install; ?>
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-maroon"><i class="fa fa-building-o" aria-hidden="true"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text"># of Quotes<br>Requested</span>
                            <span class="info-box-number"><?php echo $noOfCompany; ?></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-lock" aria-hidden="true"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text"># of Assigned<br>Quotes</span>
                            <span class="info-box-number"><?php echo $assignedQuote; ?></span>
                        </div>
                    </div>
                </div>

                <div class="clearfix visible-sm-block"></div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="fa fa-unlock" aria-hidden="true"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text"># of Unassigned<br>Quotes</span>
                            <span class="info-box-number"><?php echo $unassignedQuote; ?></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="fa fa-product-hunt" aria-hidden="true"></i></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text"># of Quotes<br> Downloaded</span>
                            <span class="info-box-number"><?php echo $quoteStatus; ?></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?php
                    /*
                      if ($url_exist) {
                      echo 'OK';
                      } else {
                      echo 'KO';
                      }
                     */
                    ?>
                </div>
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Information</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="text-center text-uppercase"><strong># of Quotes Requested</strong></p>
                                    <div class="col-md-12">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <lable name="lblNpn" class="col-md-6 control-label">Broker NPN# :</lable>
                                                <input type="text" id='txtNpn' value="" placeholder="Broker NPN#" class="form-control col-md-2"><br><br><br>
                                            </div>
                                            <input type='button' value='Count' class="bg-green col-md-4" id='count'>
                                        </div>
                                        <div class="col-md-4">
                                            <lable name="lblNpn" class="col-md-12 control-label"># Requested Quotes:</lable>
                                            <label name='lblCount' id='lblCount' class="col-md-12 control-label" style="text-align: center;"><?php echo $noOfCompany; ?></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<?php } ?>
</div>
<script>
    $(document).ready(function () {
       $('#count').click(function(){
            var txt = $('#txtNpn').val();
            console.log(txt);
            $.ajax({
                    url: '<?= base_url(); ?>admin/dashboard/countNPN?'+'txt='+txt,
                    success: function (result) {
                    console.log(result);
                    $('#lblCount').text(result);
                }
            });
       });
    });
</script>