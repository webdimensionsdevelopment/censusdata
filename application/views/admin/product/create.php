<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$opts = ' required';
?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box custom-form">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('products_create_product'); ?></h3>
                    </div>
                    <div class="box-body">
                        <?php echo $message; ?>
                        <div class="col-sm-8">
                            <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal', 'id' => 'form-create_user')); ?>
                            <div class="form-group">
                                <?php echo lang('products_name', 'products_name', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($products_name,'',$opts); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('product_description', 'product_description', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_input($product_description); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label name="product_image" id="productImage" class="col-md-2 control-label">Product Image</label>
                                <div class="col-sm-10">
                                    <input type='file' onchange="readURL(this);" name="product_image" id="product_image" class="form-control" />
                                    <?php //echo form_input($product_image); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('occupation', 'occupation', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_dropdown($occupation, $options_selection, 'occupation'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('salary', 'salary', array('class' => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-10">
                                    <?php echo form_dropdown($salary, $options_selection, 'salary'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="btn-group">
                                        <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                                        <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                                        <?php echo anchor('admin/product', lang('actions_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <img src='<?php echo base_url().'/upload/default/default.jpg'?>' height="350px" width="350px" id='blah'>
                        </div>
                        
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(350)
                    .height(350);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>