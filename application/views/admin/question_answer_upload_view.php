<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
  <section class="content-header">
    <?php echo $pagetitle; ?>
    <?php echo $breadcrumb; ?>
  </section>
  <div class="content">
    <div class="row">
     <div class="col-md-12">
       <div class="box">
        <div class="box-body">  
          <form class="form-horizontal1" action="<?php echo site_url()?>admin/Company/insert_question_answer" method="post" enctype="multipart/form-data">
           <input type="hidden" id="cid" name="cid" value="<?php echo $cid?>">
           <input type="hidden" id="count" name="count" value="1">
           <div class="doc-table-cont">
             <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered table-condensed table-hover">
               <thead>
                <tr class="row-blue">
                 <th>Document Name</th>
                 <th>Document Type</th>
                 <th>Action</th>
               </tr>
             </thead>
             <tbody>
               <?php
               foreach($docValue as $docval)
               {
                 ?>
                 <tr>
                   <td><a href="<?php echo base_url(). 'admin/company/download_doc?docid='.$docval['id']; ?> "><?php echo urldecode($docval['document_name']);?></a></td>
                   <td><?php echo $docval['documentFile'];?></td>
                   <td><button class="btn remove_action" title="delete" type="button" id="delete<?php echo $docval['id']?>" onclick="delete_file(<?php echo $docval['id']?>)"><span class="glyphicon glyphicon-remove"></span></button></td>
                 </tr>
                 <?php
                   //echo $docval['document_name'] ."  ".$docval['documentFile']." ".$docval['id']."<br>";
               }
               ?>
             </tbody>
           </table>
         </div>
         <br>
         <div id="main">
           <div id="types1">
             <div class="clearfix">
              <div class="col-md-3 gutter-sm">
                <div id="files1">
                 <input type="hidden" name="file_count1" id="file_count1" value="1">
                 <div class="form-group">
                  <label for="file_name1" class="control-label">File</label>
                  <input type="file" name="file_name1_1" id="file_name1_1" class="form-control">
                </div>
              </div>
            </div>
            <div class="col-md-3 gutter-sm">
              <div class="form-group">
                <label for="file_type1" class="control-label">File Type</label>
                <select name="file_type1" id="file_type1" class="form-control">
                  <option value="">File Type</option>
                  <?php
                  foreach($document as $doc)
                  {
                   ?>
                   <option value="<?php echo $doc->documentId?>"><?php echo $doc->documentFile?></option>
                   <?php
                 }
                 ?>
               </select>
             </div>
           </div>
         </div>
       </div>


  </div>
  
  <div class="clearfix">
    <div class="col-md-3 gutter-sm">
      <div class="form-group">
        <label for="more_type" class=" control-label"></label>
        <input type="button" class="btn btn-primary" name="more_type" id="more_type" value="Add File">
      </div>
    </div>
  </div>
  <div class="clearfix">
    <div class="form-group">
      <div class='gutter-sm'>
        <button class="btn btn-primary btn-flat" type="submit">Next</button>
        <button class="btn btn-warning btn-flat" type="reset">Reset</button>
        <a class="btn btn-default btn-flat" href="<?php echo base_url()."admin/Company/company_list_view"; ?>">Cancel</a>
      </div>
    </div>
  </div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>

<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>

<script>
 $(document).on('click','#more_type',function(){
  cnt=$('#count').val();
  cnt++;
  $('#main').append("<div id='types"+cnt+"'> <div class='clearfix'> <div class='col-md-3 gutter-sm'> <div class='form-group'> <div id='files"+cnt+"'> <input type='hidden' name='file_count"+cnt+"' id='file_count"+cnt+"' value='1'> <label for='file_name"+cnt+"_1' class='control-label'>File</label> <input type='file' name='file_name"+cnt+"_1' id='file_name"+cnt+"_1' class='form-control'> </div> </div> </div> <div class='col-md-3 gutter-sm'> <div class='form-group'> <label for='file_type"+cnt+"' class='control-label'>File Type</label> <select name='file_type"+cnt+"' id='file_type"+cnt+"' class='form-control'><option value=''>File Type</option><?php foreach($document as $doc){ ?>  <option value='<?php echo $doc->documentId?>'><?php echo $doc->documentFile?></option><?php }?></select> </div> </div> </div> </div>");
$('#count').val(cnt);
});       
function add_files(no)
{
  cnt=$('#file_count'+no).val();
  cnt++;
  $('#files'+no).append("<div class='form-group'><label for='file_name"+no+"_"+cnt+"' class='control-label'>File</label><input type='file' class='form-control' name='file_name"+no+"_"+cnt+"' id='file_name"+no+"_"+cnt+"'></div></div>");
  $('#file_count'+no).val(cnt);
}    
</script>
<script>
  function delete_file(id)
  {
    $.ajax({
     url:"<?php echo site_url()?>admin/Company/delete_file/"+id,
     success:function(res){
       window.location.reload();  
     }
   });
  }
</script>