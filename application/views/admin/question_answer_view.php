<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
<div class="content">
    <div class="row">
         <div class="col-md-12">
             <div class="box">
                    <div class="box-body">
            
            <form class="form-horizontal" action="<?php echo site_url()?>admin/Company/insert_question_answer" method="post" enctype="multipart/form-data">
                <input type="hidden" id="cid" name="cid" value="<?php echo $cid?>">
                <input type="hidden" id="count" name="count" value="1">
               <div id="main">
               <div id="types1">
                <div class="form-group">
                    <label for="file_type1" class="col-sm-2 control-label">File Type</label>
                    <div class="col-sm-10">
                        <select name="file_type1" id="file_type1" class="form-control" >
                            <option value="">File Type</option>
                           <?php
                           foreach($document as $doc)
                           {
                           ?>
                            <option value="<?php echo $doc->documentId?>"><?php echo $doc->documentFile?></option>
                            <?php
                           }
                           ?>
                        </select>
                    </div>
                </div>
               <div id="files1">
                 <input type="hidden" name="file_count1" id="file_count1" value="1">
                <div class="form-group">
                    <label for="file_name1_1" class="col-sm-2 control-label">File</label>
                    <div class="col-sm-10">
                        <input type="file" name="file_name1_1" id="file_name1_1" onchange="validation(1,1)">
                    </div>
                </div>
               </div>
               <div class="form-group">
                    <label for="more_file1" class="col-sm-2 control-label"></label>
                    <div class="col-sm-10">
                        <input type="button" name="more_file1" id="more_file1" value="Add more file" onclick="add_files(1)">
                    </div>
                </div>
               </div>
               </div>
                 <div class="form-group">
                    <label for="more_type" class="col-sm-2 control-label"></label>
                    <div class="col-sm-10">
                        <input type="button" name="more_type" id="more_type" value="Add more file type">
                    </div>
                </div>
             <?php
               $no=1;
              $note_no=1;
              if(!empty($question) && $no_of_emp >= 10)
               foreach($question as $key=>$quest)
              {
                   echo "<h3>".$key."</h3><hr>";         
                  foreach($quest as $quest1)
                  {
   
               ?>
                <div class="form-group">
                    <input type="hidden" id="qid<?php echo $no?>" name="qid<?php echo $no?>" value="<?php echo $quest1->id?>">
                    <label for="" class="col-sm-2 control-label"><?php echo $no.". ".$quest1->question;?></label>
                     <div class="col-sm-10">
                    <?php
                    foreach($answer as $a)
                    {
                        if($a->question_id==$quest1->id)
                        {
                            if($quest1->question_type=='single')
                            {
                    ?>
                   
                        <input type="radio"  name="aid<?php echo $no?>[]" id="aid<?php echo $no?>" value="<?php echo $a->id?>"><?php echo $a->answer ?>                    
                <?php
                        }
                        else if($quest1->question_type =='textbox')
                        {
                            ?>
                            <?php echo $a->answer ?>: <input type="text"  name="aid<?php echo $no?>[]" id="aid<?php echo $no?>" value="">
                            <?php
                        }
                        else 
                        {
                      ?>
                   
                        <input type="checkbox" name="aid<?php echo $no?>[]" id="aid<?php echo $no?>" value="<?php echo $a->id?>"><?php echo $a->answer ?>                    
                <?php       
                        }
                        }
                     }
                     $note_no=$no;
                    $no++;
                ?>
                      </div> 
                </div>
                <?php
                }
                ?>
                <input type="hidden" id="cnt" name="cnt" value="<?php echo $no?>">
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Notes</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="ques_answer<?php echo ($note_no);?>" id="ques_answer<?php echo ($note_no);?>" placeholder="please put additional comment here"></textarea>
                    </div>
                </div>
           <?php
    }
    ?>
                <div class="form-group">
                  <div class='col-sm-offset-2 col-sm-10'>
                        <button class="btn btn-primary btn-flat" type="submit">Next</button>
                        <button class="btn btn-warning btn-flat" type="reset">Reset</button>
                        <a class="btn btn-default btn-flat" href="<?php echo base_url()."admin/Company/company_list_view"; ?>">Cancel</a>
                          <a class="btn btn-primary btn-flat" href="<?php echo base_url()."admin/Company/edit_employee_view/".$cid; ?>/1">Go Back</a>
                    </div>
                </div>
            </form>
        </div>
        </div>
    </div>
    </div>
</div>
</div>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>

<script>
 $(document).on('click','#more_type',function(){
    cnt=$('#count').val();
    cnt++;
    $('#main').append("<div id='types"+cnt+"'><div class='form-group'><label for='file_type"+cnt+"' class='col-sm-2 control-label'>File Type</label><div class='col-sm-10'><select name='file_type"+cnt+"' id='file_type"+cnt+"' class='form-control' ><option value=''>File Type</option><?php foreach($document as $doc){ ?>  <option value='<?php echo $doc->documentId?>'><?php echo $doc->documentFile?></option><?php }?></select></div></div><div id='files"+cnt+"'><input type='hidden' name='file_count"+cnt+"' id='file_count"+cnt+"' value='1'><div class='form-group'><label for='file_name"+cnt+"_1' class='col-sm-2 control-label'>File</label><div class='col-sm-10'><input type='file' name='file_name"+cnt+"_1' id='file_name"+cnt+"_1' onchange='validation("+cnt+",1)'></div></div></div><div class='form-group'><label for='more_file"+cnt+"' class='col-sm-2 control-label'></label><div class='col-sm-10'><input type='button' name='more_file"+cnt+"' id='more_file"+cnt+"' value='Add more file' onclick='add_files("+cnt+")'></div></div></div>");
    $('#count').val(cnt);
});       
function add_files(no)
{
    cnt=$('#file_count'+no).val();
   cnt++;
    $('#files'+no).append("<div class='form-group'><label for='file_name"+no+"_"+cnt+"' class='col-sm-2 control-label'>File</label><div class='col-sm-10'><input type='file' name='file_name"+no+"_"+cnt+"' id='file_name"+no+"_"+cnt+"' onchange='validation("+no+","+cnt+")'></div></div>");
    $('#file_count'+no).val(cnt);
}    
</script>
<script>
function validation(no,cnt)
{
  if($("#file_name"+no+"_"+cnt).val()!='')
  {
     document.getElementById("file_type"+no).required = true;     
  }
  else
  {
      document.getElementById("file_type"+no).required = false;
  }
  
}
</script>