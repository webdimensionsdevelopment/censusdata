<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$opts = ' required';
?>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box custom-form">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('products_create_product'); ?></h3>
                    </div>
                    <div class="box-body">
                        <?php echo $message; ?>

                        <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal', 'id' => 'form-create_user')); ?>
                        <div class="form-group">
                            <?php echo lang('product', 'products_name', array('class' => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-10">
                                <input type="hidden" name="id" value="<?php echo $question['question']['id']; ?>">
                                <?php echo form_dropdown($products_name, $options, $question['question']['prod_id']); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo lang('questionnaire_question', 'questionnaire_question', array('class' => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-10">
                                <?php echo form_input($questionnaire_question,'',$opts); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo lang('questionnaire_description', 'questionnaire_description', array('class' => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-10">
                                <?php echo form_input($questionnaire_description,'',$opts); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo lang('questionnaire_selection', 'questionnaire_selection', array('class' => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-10">
                                <?php echo form_dropdown($questionnaire_selection, $options_selection, $question['question']['question_type']); ?>
                            </div>
                        </div>
                        <?php echo lang('questionnaire_answer', 'questionnaire_answer', array('class' => 'col-sm-12 control-label', 'style' => 'text-align:center')); ?>
                        <?php foreach($question['answer'] as $quesAnswer) {?>
                        <div class="form-group multiple-form-group" data-max=100>
                            <?php echo lang('questionnaire_answer', 'questionnaire_answer', array('class' => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-9" style="padding-right: 0px;">
                                <input type="text" name="questionnaire_answer[<?php echo $quesAnswer['id']; ?>]" required class="form-control" value="<?php echo $quesAnswer['answer']; ?>">
                            </div>
                            <div class="col-sm-1" style="padding-left: 0px;"><span class="input-group-btn"><button type="button" class="btn btn-danger btn-remove">–</button></span></div>
                        </div>
                        <?php } ?>
                        <div class="form-group multiple-form-group" data-max=100>
                            <?php echo lang('questionnaire_answer', 'questionnaire_answer', array('class' => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-9" style="padding-right: 0px;">
                                <input type="text" name="questionnaire_answer[]" class="form-control" value="">
                            </div>
                            <div class="col-sm-1" style="padding-left: 0px;">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default btn-add">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="btn-group">
                                    <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => lang('actions_submit'))); ?>
                                    <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => lang('actions_reset'))); ?>
                                    <?php echo anchor('admin/questionnaire', lang('actions_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
    (function ($) {
        $(function () {

            var addFormGroup = function (event) {
                event.preventDefault();

                var $formGroup = $(this).closest('.form-group');
                var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
                var $formGroupClone = $formGroup.clone();

                $(this)
                    .toggleClass('btn-default btn-add btn-danger btn-remove')
                    .html('–');

                $formGroupClone.find('input').val('');
                $formGroupClone.insertAfter($formGroup);

                var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
                if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                    $lastFormGroupLast.find('.btn-add').attr('disabled', true);
                }
            };

            var removeFormGroup = function (event) {
                event.preventDefault();

                var $formGroup = $(this).closest('.form-group');
                var $multipleFormGroup = $formGroup.closest('.multiple-form-group');

                var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
                if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                    $lastFormGroupLast.find('.btn-add').attr('disabled', false);
                }

                var $formGroupClone = $formGroup.clone();
                var removeData = $formGroupClone.find('input');

                removeData.val("");


                $formGroup.remove();
            };

            var countFormGroup = function ($form) {
                var count = $form.find('.form-group').length;
                console.log(count);
                return $form.find('.form-group').length;

            };

            $(document).on('click', '.btn-add', addFormGroup);
            $(document).on('click', '.btn-remove', removeFormGroup);

        });
    })(jQuery);
</script>