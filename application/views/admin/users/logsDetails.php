<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.6/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.6/js/jquery.dataTables.js"></script>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo '<h1>Log Details</h1>' ?>
        <?php echo $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="margin-bottom: 15px;">
                        <h3 class="panel-title">Log Details</h3>
<!--                        <div class="pull-right">
                            <span class="clickable filter" data-toggle="tooltip" title="Toggle table filter" data-container="body">
                                <i class="glyphicon glyphicon-filter" id='search'>Search</i> 
                            </span>
                        </div>-->
                    </div>
                    <div class="panel-body">
                        <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter Log Details" onkeyup="searchTable(this.value);"/>
                    </div>
                    <table class="table table-striped table-hover" id="dev-table">
                        <thead>
                           
                                <th><?php echo lang('users_userName'); ?></th>
                                <th><?php echo lang('users_email'); ?></th>
                                <th><?php echo lang('users_logInTime'); ?></th>
                                <th><?php echo lang('users_logOutTime'); ?></th>
                                <th><?php echo lang('users_status'); ?></th>
                                <th><?php echo lang('users_action'); ?></th>
                           
                        </thead>
                        <tbody>
                            <?php $count = 0; foreach ($users as $key => $user): ?>
                            <?php if ($count) { ?>
                            <tr>
                                
                                          <td> 
                                            <?php echo anchor('admin/logDetails/logQueries/' . $user['id'], $user['username']); ?>
                                        </td>
                                        <td><?php echo $user['email']; ?></td>
                                        <td><?php echo $user[0]['logInTime']; ?></td>
                                        <td><?php echo $user[0]['logOutTime']; ?></td>
                                        <td><?php echo ($user['active']) ?('<span class="label label-success">' . lang('users_active') . '</span>') : ('<span class="label label-default">' . lang('users_inactive') . '</span>'); ?></td>
                                        <td>
                                        <?php echo anchor('admin/logDetails/logQueries/' .  $user['id'], lang('actions_see')); ?>
                                        </td>   
                            </tr>    
                             <?php } ?>
                            <?php $count++;endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
$(document).ready(function () {
    $("#search").click(function () {
        var $this = $(this),
                $panel = $this.parents('.panel');
        $panel.find('.panel-body').slideToggle();
        if ($this.css('display') != 'none') {
            $panel.find('.panel-body input').focus();
        }
    });
});
function searchTable(inputVal)
{
    var table = $('#dev-table');
    table.find('tr').each(function (index, row)
    {
        var allCells = $(row).find('td');
        if (allCells.length > 0)
        {
            var found = false;
            allCells.each(function (index, td)
            {
                var regExp = new RegExp(inputVal, 'i');
                if (regExp.test($(td).text()))
                {
                    found = true;
                    return false;
                }
            });
            if (found == true)
                $(row).show();
            else
                $(row).hide();
        }
    });
}
</script>
<script>
$(document).ready(function(){
    $.noConflict();
  $('#dev-table').DataTable({
      "searching":   true
  });  
});
</script>