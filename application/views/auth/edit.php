<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$opts = ' required';

?>
<div class="login-logo">
    <img src="<?php echo base_url().'/assets/images/flextrans.png'; ?>" alt="<?php echo $title_lg; ?>" />
</div>
<div class="">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box custom-form">
                    <div class="box-header with-border">
                        <h3 class="box-title"> <?php echo $pagetitle; ?></h3>
                    </div>
                    <div class="box-body">
                        <?php echo $message; ?>
                        <?php echo form_open(uri_string(), array('class' => 'form-horizontal', 'id' => 'form-edit_user')); ?>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php echo form_input($first_name,'',$opts); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php echo form_input($last_name,'',$opts); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php echo form_input($phone); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php echo form_input($password,'',$opts); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php echo form_input($password_confirm,'',$opts); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php echo form_hidden('id', $user->id); ?>
                                <div class="btn-group">
                                    <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => 'Submit')); ?>
                                    <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => 'Reset')); ?>
                                    <?php // echo anchor('admin/users', lang('actions_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>






