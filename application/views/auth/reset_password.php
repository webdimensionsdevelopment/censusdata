<div class="login-logo">
    <a href="#"><?php echo $title_lg; ?></a>
</div>
<div class="login-box-body">
    <p class="login-box-msg"><?php echo 'New Password (at least 8 characters long)'; ?></p>
    <?php echo $message; ?>
<?php echo form_open('auth/reset_password/' . $code);?>

    <div class="form-group has-feedback">
        <?php echo form_input($new_password); ?>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
        <div class="form-group has-feedback">
        <?php echo form_input($new_password_confirm); ?>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <?php echo form_input($user_id);?>
	<?php echo form_hidden($csrf); ?>
    <div class="row">
       <div class="col-xs-8">
            <?php echo form_submit('submit', 'Reset Password', array('class' => 'btn btn-primary btn-block btn-flat')); ?>
        </div>
    </div>
<?php echo form_close();?>
</div>