<div class="container" >
    <div class="row">
        <div class="col-md-12" style="margin-top: 10%">
            <div class="col-md-3">
                <ul class="nav navbar-nav ">
                    <li class="col-md-12 liTabList active" id="tabHome"><a href="#tab1" data-toggle="tab" id="campaign">Dashboard</a></li>
                    <li class="col-md-12 liTabList" id="tabSendInvitation"><a href="#tab2" data-toggle="tab" id="campaign">Send Invitation</a></li>
                    <li class="col-md-12 liTabList" id="tabListofBroker"><a href="#tab3" data-toggle="tab" id="campaign">List of Broker</a></li>
                </ul>
            </div>
            <div class="col-md-9">
                <div class="tab-pane active" id="tab1">
                    <h3>Dashboard</h3>
                </div>
                <div class="tab-pan" id="tab2" hidden>
                    <h3>Send Invitation to Broker</h3>
                    <form class="form-horizontal" name="sendInvitation" method="POST" enctype="multipart/form-data" >
                        <div class="form-group">
                            <lable name="lblUserName" class="col-md-3 label-heading asterisk">Name</lable>
                            <div class='col-md-4'>
                                <input type="text"  name="txtFirstName" id="firstName" class="form-control" value="" placeholder="First Name"/>
                            </div>
                            <div class='col-md-4'>
                                <input type="text" name="txtLastName" id="lastName" class="form-control" value="" placeholder="Last Name"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <lable name="lblEmail" class="col-md-3 label-heading asterisk">Email</lable>
                            <div class='col-md-8'>
                                <input type="txtEmail" name="txtEmail" id="lastName" class="form-control" value="" placeholder="Email" onchange="getValues(this.value);" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class='col-md-3'>
                                <input type="submit" name="sendInvitation" id="sendInvitation" class="form-control" value="Send" />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pan" id="tab3" hidden>
                    
                    <div class="col-md-12 table-responsive">
                        <table class="table table-bordered table-hover table-sortable" id="userDetails">
                            <thead>
                                <tr id='addr0' data-id="0">
                                   <th>Sr.No</th>
                                   <th>Email Address</th>
                                   <th>Active</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $j = $i=0; foreach($brokerList as $record) : $i++;?>
                                    <tr>
                                        <td><?php echo $i ?></td>
                                        <td><?php echo $record['email']; ?></td>
                                        <td>
                                            <?php if($record['active']==1) : ?>
                                            <label name = "active" value="Active">Active</label>
                                            <?php else : ?>
                                            <label name = "inactive" value="Inactive">Inactive</label>
                                            <?php endif;?>
                                        </td>
                                    </tr>
                                <?php $j++; endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="checkEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                User Already Registered
            </div>
            <div class="modal-body">
                <?php echo "Hello"; ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#tabHome').click(function () {
            $('#tab1').show();
            $('#tabHome').addClass("active");
            $('#tab2').hide();
            $('#tabSendInvitation').removeClass('active');
            $('#tab3').hide();
            $('#tabListofBroker').removeClass('active');
        });
        $('#tabSendInvitation').click(function () {
            $('#tab2').show();
            $('#tabSendInvitation').addClass("active");
            $('#tab1').hide();
            $('#tabHome').removeClass('active');
            $('#tab3').hide();
            $('#tabListofBroker').removeClass('active');
        });
        $('#tabListofBroker').click(function () {
            $('#tab2').hide();
            $('#tabSendInvitation').removeClass('active');
            $('#tab1').hide();
            $('#tabHome').removeClass('active');
            $('#tab3').show();
            $('#tabListofBroker').addClass("active");
        });
    });
    function getValues(email) {
        $.ajax({
            url: '<?= base_url(); ?>brokerage/checkEmail',
            data: {"email": email},
            success: function (result) {
                var value = result;
                if (value == 1) {
                    alert("Email Already Registered");
                }
            }
        });
    }
</script>    