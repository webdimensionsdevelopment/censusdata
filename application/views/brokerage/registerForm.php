<div class="container" >
    <div class="row">
        <div class="col-xs-offset-2 col-md-9" style="margin-top: 10%">
            <label class="col-lg-12" style="text-align: center;">Please complete your profile</label>
            <form class="form-horizontal" action="brokerage/registerForm" name="brokerage" method="POST" enctype="multipart/form-data" style="height: 750px;">
                <div class="form-group">
                    <lable name="lblUserName" class="col-md-3 label-heading asterisk">Name</lable>
                    <div class='col-md-4'>
                        <input type="text"  name="txtFirstName" id="firstName" class="form-control" value="" placeholder="FirstName" required/>
                    </div>
                    <div class='col-md-4'>
                        <input type="text" name="txtLastName" id="lastName" class="form-control" value="" placeholder="LastName" required/>
                    </div>
                </div>
                <div class="form-group">
                    <lable name="lblEmail" class="col-md-3 label-heading asterisk">Email</lable>
                    <div class='col-md-8'>
                        <input type="txtEmail" name="txtEmail" id="lastName" class="form-control" value="<?php echo $email['email']; ?>" placeholder="Email" readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <lable name="lblGender" class="col-md-3 label-heading asterisk">Gender</lable>
                    <div class='col-md-8'>
                        <input type="radio" name="rdGender" id="rdGender" value="Male" > Male / <input type="radio" name="rdGender" id="rdGender" value="Female"> Female
                    </div>
                </div>
                <div class="form-group">
                    <lable name="lblPassword" class="col-md-3 label-heading asterisk">Password</lable>
                    <div class='col-md-8'>
                        <input type="password" name="txtPassword" id="password" class="form-control" value="" placeholder="Password" required/>
                    </div>
                </div>
                <div class="form-group">
                    <lable name="lblConfirmPassword" class="col-md-3 label-heading asterisk">Confirm Password</lable>
                    <div class='col-md-8'>
                        <input type="password" name="txtConfirmPassword" id="confirmPassword" class="form-control" value="" placeholder="Confirm Password" required/>
                    </div>
                </div>
                <div class="form-group">
                    <lable name="lblImage" class="col-md-3">Profile Image</lable>
                    <div class='col-md-8'>
                        <input type="file" name="image" id="brokerageImage" class="form-control" placeholder="Choose Profile Pic...">
                    </div>
                </div>
                <div class="form-group">
                    <lable name="lblPhoneNumber" class="col-md-3">Phone Number</lable>
                    <div class='col-md-8'>
                        <input type="text" name="txtPhoneNumber" id="phoneNumber" class="form-control"  placeholder="Phone Number">
                    </div>
                </div>
                <div class="form-group">
                    <lable name="lblAddress" class="col-md-3">Address</lable>
                    <div class='col-md-8'>
                        <textarea name="txtAddress" id="txtAddress"  class="form-control" placeholder="Address"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <lable name="lblZipCode" class="col-md-3">Zip Code</lable>
                    <div class='col-md-8'>
                        <input  type="text" name="txtZipCode" id="txtZipCode"  class="form-control" placeholder="ZipCode" onchange="getValues(this.value);"/>
                    </div>
                </div>
                <div class="form-group">
                    <lable name="lblCountry" class="col-md-3">Country</lable>
                    <div class='col-md-8'>
                        <input  type="text" name="txtCountry" id="txtCountry"  class="form-control" placeholder="Country" />
                    </div>
                </div>
                <div class="form-group">
                    <lable name="lblState" class="col-md-3">State</lable>
                    <div class='col-md-8'>
                        <input  type="text" name="txtState" id="txtState"  class="form-control" placeholder="State" />
                    </div>
                </div>
                <div class="form-group">
                    <lable name="lblCity" class="col-md-3">City</lable>
                    <div class='col-md-8'>
                        <input  type="text" name="txtCity" id="txtCity"  class="form-control" placeholder="City" />
                    </div>
                </div>
                <div class="form-group">
                    <div class='col-md-9 col-md-offset-4' >
                        <input type="button" class="btn btn-danger btn-cancel-action cancelBrokerage" value="Cancel" id="cancelBrokerage" style="margin-right: 10px;">
                        <input type="submit"  name="brokerSubmit"  id="brokerSubmit"  class="btn btn-success btn-login-submit" value="Save" />
                    </div>
                </div>
            </form>
        </div>
    </div>    
</div>
<script>
    function getValues(zipcode) {
        $.ajax({
            url: '<?= base_url(); ?>user/getCountryStateCity',
            data: {"zipcode": zipcode},
            success: function (result) {
                var count_State_City = JSON.parse(result);
                $('#txtCountry').val(count_State_City.country);
                $('#txtState').val(count_State_City.state);
                $('#txtCity').val(count_State_City.city);
                console.log(count_State_City);
            }
        });
    }
    ;
</script>