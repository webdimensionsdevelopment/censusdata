<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="<?php echo base_url(); ?>bootstrap/js/bootstrap-datepicker.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> 
<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
    <div class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="box custom-form">
                    <div class="box-body">
                        <!-- <?php echo $message?> -->
                        <form class="form-horizontal" action="<?php echo site_url() ?>Company/insert_company" method="post" id="form1" name="form1">
                            <?php
                            if (!empty($company_detail)) {
                                ?>
                                <input type="hidden" class="form-control" id="companyid" name="companyid" value="<?php echo $company_detail[0]->companyId ?>">
                                <input type="hidden" class="form-control" id="companyid" name="userid" value="<?php echo $company_detail[0]->id ?>">
                                <div class="row row-gutter-sm">
                                    <div class="col-md-6 gutter-sm">
                                        <div class="form-group">
                                            <label for="cname" >Company Name:</label>
                                            <input type="text" class="form-control" id="cname" name="cname" value="<?php echo $company_detail[0]->company ?>" placeholder="Company Name" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 gutter-sm">
                                        <div class="form-group">
                                            <label for="address" >Address:</label>
                                            <textarea class="form-control" id="address" name="address" Placeholder="Address"><?php echo $company_detail[0]->address ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-gutter-sm">
                                    <div class="col-md-6 gutter-sm">
                                <div class="form-group">
                                    <label for="zipcode" >Zipcode:</label>
                                    <input type="text" class="form-control" id="zipcode" name="zipcode" Placeholder="Zipcode" value="<?php echo $company_detail[0]->zipcode ?>" required onchange="getValues(this.value);">
                                </div>
                                    </div>
                                <div class="col-md-6 gutter-sm">
                                <div class="form-group">
                                    <label for="city" >City:</label>
                                    <input type="text" class="form-control" id="city" name="city" value="<?php echo $company_detail[0]->city ?>" Placeholder="City" required>
                              </div>
                              </div>
                              </div>
                                <div class="row row-gutter-sm">
                                    <div class="col-md-6 gutter-sm">
                                    <div class="form-group">
                                    <label for="country" >County:</label>
                                     <input type="text" class="form-control" id="country" name="country" value="<?php echo $company_detail[0]->country ?>" Placeholder="County" required>
                                    </div>
                                </div>
                                <div class="col-md-6 gutter-sm">
                                <div class="form-group">
                                    <label for="state" >State:</label>
                                     <input type="text" class="form-control" id="state" name="state" value="<?php echo $company_detail[0]->state ?>" Placeholder="State" required>
                                    </div>
                                </div>
                                </div>
                              <div class="row row-gutter-sm">
                                    <div class="col-md-6 gutter-sm">
                                <div class="form-group">
                                    <label for="email" >Email Address:</label>
                                    <input type="email" class="form-control" id="email" name="email" value="<?php echo $company_detail[0]->email ?>" Placeholder="Email Address" required>
                                    </div>
                                </div>
                                   <div class="col-md-6 gutter-sm">
                                <div class="form-group">
                                    <label for="no_of_emp" ># of Employee</label>
                                    <input type="number" class="form-control" id="no_of_emp" name="no_of_emp" value="<?php echo $company_detail[0]->no_of_emp ?>" Placeholder="No of Employee" required min="0">
                                </div>
                                    </div>
                                </div>
                                <div class="row row-gutter-sm">
                                    <div class="col-md-6 gutter-sm">
                                <div class="form-group">
                                    <label for="sic" >SIC Code:</label>
                                        <select class="form-control itemName" id="sic" name="sic" required>
                                            <option value="">SIC Code</option>
                                            <?php
                                            foreach ($sic_code as $sc) {
                                                ?>
                                                <option value="<?php echo $sc->sicId ?>" <?php
                                        if ($company_detail[0]->sicId == $sc->sicId) {
                                            echo "selected";
                                        }
                                                ?>><?php echo $sc->code . " " . $sc->sicText ?></option>
                                                        <?php
                                                    }
                                                    ?>

                                        </select>
                                    </div>
                                </div>
                                 <div class="col-md-6 gutter-sm">
                                <div class="form-group">
                                    <label for="first_name" >First Name:</label>
                                   <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $company_detail[0]->first_name?>" Placeholder="First Name" required>
                                 </div>
                                    </div>
                                </div>
                                <div class="row row-gutter-sm">
                                    <div class="col-md-6 gutter-sm">
                                <div class="form-group">
                                    <label for="last_name" >Last Name:</label>
                                    
                                        <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $company_detail[0]->last_name?>" Placeholder="Last Name" required>
                                </div>
                                </div>
                                <div class="col-md-6 gutter-sm">
                                <div class="form-group">
                                    <label for="phone_number" >Business Phone:</label>
                                   
                                        <input type="text" class="form-control" id="phone_number" name="phone_number" value="<?php echo $company_detail[0]->phoneNumber?>" Placeholder="Phone Number" required>
                                
                                </div>
                                    </div>
                                </div>
                                  <div class="form-group">
                                    <div class='col-sm-offset-2 col-sm-10'>
                                        <button class="btn btn-primary" type="submit" id="add" name="add">Next</button>
                                        <button class="btn btn-warning" type="reset">Reset</button>
                                        <a class="btn btn-default" href="">Cancel</a>
                                    </div>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="row row-gutter-sm">
                                    <div class="col-md-6 gutter-sm">
                                        <div class="form-group">
                                            <label for="cname" >Company Name:</label>
                                            <input type="text" class="form-control" id="cname" name="cname" placeholder="Company Name" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 gutter-sm">
                                        <div class="form-group">
                                            <label for="address" >Street Address:</label>
                                            <textarea class="form-control" id="address" name="address" Placeholder="123 Easy Street"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-gutter-sm">
                                    <div class="col-md-6 gutter-sm">
                                        <div class="form-group">
                                            <label for="zipcode" >Zipcode:</label>
                                            <input type="text" class="form-control" id="zipcode" name="zipcode" Placeholder="Zipcode" required onchange="getValues(this.value);">
                                        </div>
                                    </div>
                                    <div class="col-md-6 gutter-sm">
                                        <div class="form-group">
                                            <label for="city" >City:</label>
                                            <input type="text" class="form-control" id="city" name="city" Placeholder="City" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-gutter-sm">
                                    <div class="col-md-6 gutter-sm">
                                        <div class="form-group">
                                            <label for="country" >County:</label>
                                            <input type="text" class="form-control" id="country" name="country" Placeholder="County" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 gutter-sm">
                                        <div class="form-group">
                                            <label for="state" >State:</label>
                                            <input type="text" class="form-control" id="state" name="state"  Placeholder="State" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-gutter-sm">
                                    <div class="col-md-6 gutter-sm">
                                        <div class="form-group">
                                            <label for="email" >Email Address:</label>
                                            <input type="email" class="form-control" id="email" name="email" Placeholder="Email Address" required>
                                        </div>
                                    </div>
                                    <div class="col-md-3 gutter-sm">
                                        <div class="form-group">
                                            <label for="no_of_emp" ># of Employee</label>
                                            <input type="number" class="form-control" id="no_of_emp" name="no_of_emp" Placeholder="No of Employee" required min="0">
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-gutter-sm">
                                    <div class="col-md-6 gutter-sm">
                                        <div class="form-group">
                                            <label for="sic" >SIC Code:</label>
                                            <select class="form-control itemName" id="sic" name="sic" required>
                                                <option value="">SIC Code</option>
                                                <?php
                                                foreach ($sic_code as $sc) {
                                                    ?>
                                                    <option value="<?php echo $sc->sicId ?>" ><?php echo $sc->code . " " . $sc->sicText ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 gutter-sm">
                                        <div class="form-group">
                                            <label for="first_name" >First Name:</label>
                                            <input type="text" class="form-control" id="first_name" name="first_name" Placeholder="First Name" required>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row row-gutter-sm">
                                    <div class="col-md-6 gutter-sm">
                                        <div class="form-group">
                                            <label for="last_name" >Last Name:</label>
                                            <input type="text" class="form-control" id="last_name" name="last_name" Placeholder="Last Name" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 gutter-sm">
                                        <div class="form-group">
                                            <label for="phone_number">Business Phone</label>
                                            <input type="text" class="form-control" id="phone_number" name="phone_number" Placeholder="Business Phone" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-gutter-sm">
                                    <div class="col-md-6 gutter-sm">
<!--                                        <div class="form-group check-list clear">-->
                                            <label for="product" >Product</label>
                                            <?php
                                            foreach ($product as $p) {
                                                ?> 
                                            <div class="check">
                                                <input type="checkbox" id="product<?php echo $p->productId ?>" name="product[]" value="<?php echo $p->productId ?>" class="" onchange="offering('<?php echo $p->productId ?>')"> 
                                                <label for="<?php echo $p->productId ?>"> <?php echo $p->name; ?></label>
                                           </div>
                                            <div class="row row-gutter-sm">
                                                <div class="col-md-4 gutter-sm">
                                                    <div class="form-group" id="eff_dates<?php echo $p->productId ?>" hidden>
                                                        <label for="effect_date" >Effective Date</label>
                                                        <input type="text" id="effect_date<?php echo $p->productId ?>" name="effect_date<?php echo $p->productId ?>" value="" class="form-control date" placeholder="MM/DD/YYYY" pattern='.{10,10}' title='10 characters required'  maxlength='10'>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-gutter-sm">
                                                <div class="col-md-6 gutter-sm">
                                                    <div class="form-group" id="offer<?php echo $p->productId ?>" hidden>
                                                        <div class="check">
                                                            <label for="offer_emp" >Currently offering to Employee</label>
                                                            <input type="checkbox" id="offer_emp<?php echo $p->productId ?>" name="offer_emp<?php echo $p->productId ?>" value="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row-gutter-sm">
                                                <div class="col-md-4 gutter-sm">
                                                    <div class="form-group" id="dates<?php echo $p->productId ?>" hidden>
                                                        <label for="renewal_date" >Renewal Date</label>
                                                        <input type="text" id="renewal_date<?php echo $p->productId ?>" name="renewal_date<?php echo $p->productId ?>" value="" class="form-control date" placeholder="MM/DD/YYYY" pattern='.{10,10}' title='10 characters required'  maxlength='10'>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="margin-top-sm"> 
                                        <button class="btn btn-primary" type="submit" id="add" name="add" value="add">Next</button>
                                        <button class="btn btn-warning" type="reset">Reset</button>
                                        <a class="btn btn-danger" href="">Cancel</a>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url() . "scripts/datePicker.js"; ?>"></script>
<link rel="stylesheet" href="<?php echo base_url() . "style/datepicker.css"; ?>"/>
<script>
    
</script>
<script>
    $('.itemName').select2();
</script>
<script>
    function getValues(zipcode) {
        $.ajax({
            url: '<?= base_url(); ?>Company/getCountryStateCity',
            data: {"zipcode": zipcode},
            success: function (result) {
                var count_State_City = JSON.parse(result);
                $('#country').val(count_State_City.country);
                $('#state').val(count_State_City.state);
                $('#city').val(count_State_City.city);
                console.log(count_State_City);
            }
        });
    }
</script>
<script>
    function offering(no)
    {
         if($("#product"+no).is(':checked')) {
            $('#offer'+no).show();
            $('#eff_dates'+no).show();
            document.getElementById("effect_date"+no).required = true;
        }
        else
        {
            $('#offer'+no).hide();
            $('#eff_dates'+no).hide();
            $('#dates'+no).hide();
            $('#effect_date'+no).val('');
            $('#renewal_date'+no).val('');
            $('#offer_emp'+no).attr('checked', false);
            document.getElementById("effect_date"+no).required = false;
        }
        $('#offer_emp'+no).change(function(){
            if($('#offer_emp'+no).is(":checked")){
                $('#dates'+no).show();
            
            }
            else
            {
                $('#dates'+no).hide();
                $('#renewal_date'+no).val('');
            }
        });
    }
</script>
<script>
 /*   var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
    $('#renewal_date').datepicker({
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
    });*/
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

<script>
    $(document).ready(function($){
        $('.date').mask("99/99/9999"); 
    });
    $(document).ready(function() {
       $(".date").keypress(function(e){
            var res = $(this).val().split("/");
               if(res[0] > 12)
               {
                  alert("Month should be less than 12");      
               }
               if(res[1] > 31)
               {
                   alert("Date should be less than 31");     
               }
        });
    });
</script>
