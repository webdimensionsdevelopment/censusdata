<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.6/css/jquery.dataTables.css"><script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script><script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.6/js/jquery.dataTables.js"></script>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
    <div class="content" >
        <div class="row">
            <div class="col-md-12">
               <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="margin-bottom: 15px;">
                        <h3 class="panel-title">Companies</h3>
                        <div class="pull-right">
                            <span class="clickable filter" data-toggle="tooltip" title="Toggle table filter" data-container="body">
                                <i class="glyphicon glyphicon-filter" id='search'>Search</i> 
                            </span>
                        </div>
                        <div class="pull-right">
                            <a href="<?php echo base_url() . 'Company' ?>" style="color: white;">
                                <span class="clickable filter" title="Create Company" data-container="body">
                                    <i class="fa fa-plus"> <?php echo 'Create Company'; ?></i>
                                </span>
                            </a>
                        </div>
                    </div>

                    <div class="panel-body">
                        <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter Log Details" onkeyup="searchTable(this.value);"/>
                    </div>
                    <table class="table table-striped table-hover" id="dev-table">
                        <thead>
                            <th>Quote Request ID</th>
                            <th>Company</th>
                            <th># of Employees</th>
                            <th>Date of Quote Requested</th>
                            <th>Rep Assigned</th>
                            <th>Status</th>
                            <th>Action</th>
                            <th>Option</th>
                            </thead>
                            <tbody>
                                <?php if (!empty($company_detail)) { foreach ($company_detail as $comp_detail) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url()."Company/companyDetails/".$comp_detail->companyId; ?>"><?php echo $comp_detail->companyId ?></a></td>
                                        <td><a href="<?php echo base_url()."Company/companyDetails/".$comp_detail->companyId; ?>"><?php echo $comp_detail->company ?></a></td>
                                        <td><?php echo $comp_detail->no_of_emp ?></td>
                                        <?php $dt = new DateTime($comp_detail->createdAt);?>
                                        <td><?php echo $date = $dt->format('m/d/Y'); ?></td>
                                        <td>
                                        <?php 
                                            if($comp_detail->assign_status=='assign')
                                            {
                                                foreach($all_broker as $brok)
                                                {
                                                    if($brok->brokerId==$comp_detail->brokerId)
                                                    echo $brok->first_name." ".$brok->last_name;
                                                }
                                                foreach($all_brokerage as $broker)
                                                {
                                                    if($broker->id==$comp_detail->brokrage_id)
                                                    echo $broker->first_name." ".$broker->last_name;
                                                }
                                                
                                            }
                                            else
                                            {
                                                echo $comp_detail->assign_status;
                                            }
                                         ?>
                                        </td>
                                        <td><?php echo $comp_detail->quote_status?></td>
                                        <td><a href="<?php echo site_url() ?>Company/update_company_view/<?php echo $comp_detail->companyId ?>">Edit</a><br>
                                             <?php  
                                        if(!empty($brokerage))
                                        {  
                                           if($comp_detail->assign_status=='unassign')
                                           { 
                                        ?>                                           
                                             <a href='' data-target='#assignmodel' data-id='<?php echo $comp_detail->companyId?>' data-toggle='modal' class='open_assign_modal'>Assign</a><br>
                                        <?php   
                                            }
                                        }
                                        ?>
                                            
                                        </td>
                                        <td>
                                            <select id="sample<?php echo $comp_detail->companyId ?>" name="sample<?php echo $comp_detail->companyId ?>" onchange="examples(<?php echo $comp_detail->companyId ?>)">
                                                <option value="">Samples</option>
                                                <option value="1">Aetna</option>
                                                <option value="2">BCBSIL</option>
                                                <option value="3">Ease Central</option>
                                                <option value="4">UHC</option>
                                                <option value="5">LimeLight</option>
                                                <option value="6">Simple</option>
                                            </select>
                                            <div class="box-header with-border">
                                                <a href=""  name="download<?php echo $comp_detail->companyId ?>" id="download<?php echo $comp_detail->companyId ?>" onclick="examples(<?php echo $comp_detail->companyId ?>)"><h3 class="box-title btn btn-block btn-primary btn-flat">Download</h3></a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } } ?>
                            </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<!--start assign modal-->
    <div class="modal fade" id="assignmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" >Company Assign To Broker</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <input type="hidden" name="cid" id="cid">
                            <select class="form-control itemName" id="broker" name="broker" style="width:100% !important">
                                <option value="" selected>Broker</option>
                                <?php
                                foreach($all_broker as $all_brok)
                                {
                               ?>
                                <option value='<?php echo $all_brok->brokerId?>'><?php echo $all_brok->first_name." ".$all_brok->last_name?></option>
                               <?php
                                }
                                ?>
                            </select>
                      </div>
                   </form>
                </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="close1">Close</button>
                        <button type="button" id="add_assign_btn" class="btn btn-primary">Assign to broker</button>
                    </div>
            </div>
         </div>
    </div>
    <!--End View assign modal-->
<script>
    $(document).ready(function () {
        $("#search").click(function () {
            var $this = $(this),
                    $panel = $this.parents('.panel');
            $panel.find('.panel-body').slideToggle();
            if ($this.css('display') != 'none') {
                $panel.find('.panel-body input').focus();
            }
        });
    });
    function searchTable(inputVal)
    {
        var table = $('#dev-table');
        table.find('tr').each(function (index, row)
        {
            var allCells = $(row).find('td');
            if (allCells.length > 0)
            {
                var found = false;
                allCells.each(function (index, td)
                {
                    var regExp = new RegExp(inputVal, 'i');
                    if (regExp.test($(td).text()))
                    {
                        found = true;
                        return false;
                    }
                });
                if (found == true)
                    $(row).show();
                else
                    $(row).hide();
            }
        });
    }
    function examples(no)
    {
        var value = $('#sample' + no).val();
        if (value == '')
        {
            alert('please select any format for download');
        }
        else if (value == 1)
        {
            $("#download" + no).attr("href", "<?php echo site_url() ?>company/aetna_export/" + no);
        }
        else if (value == 2)
        {
            $("#download" + no).attr("href", "<?php echo site_url() ?>company/bcbsil_export/" + no);
        }
        else if (value == 3)
        {
            $("#download" + no).attr("href", "<?php echo site_url() ?>company/easecentral_export/" + no);
        }
        else if (value == 4)
        {
            $("#download" + no).attr("href", "<?php echo site_url() ?>company/uhc_export/" + no);
        }
        else if (value == 5)
        {
            $("#download" + no).attr("href", "<?php echo site_url() ?>company/limelight_export/" + no);
        }
        else if (value == 6)
        {
            $("#download" + no).attr("href", "<?php echo site_url() ?>Company/export_company_detail/" + no);
        }
        else
        {
            $("#download" + no).attr("disbled", "disabled");
        }
    }

</script>
<script>
$(document).ready(function(){
    $.noConflict();
  $('#dev-table').DataTable({
      "searching":   false,
      "order": [[ 0, "desc" ]]
  });  
});
</script>
<script>
$(document).on('click','#add_assign_btn', function(){
    broker=$('#broker').val();
    cid=$('#cid').val();
    $.ajax({
       url: "<?php echo site_url()?>Company/company_assign_broker/"+broker+"/"+cid,
      success: function(output) {
           window.location.reload();
      }
    });
});
$(document).on("click", ".open_assign_modal", function () {
cid = $(this).data('id');
$('#cid').val(cid);
});

</script>