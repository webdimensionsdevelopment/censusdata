<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
<div class="content">
    <div class="row">
         <div class="col-md-12">
             <div class="box">
                    <div class="box-body">  
                     <?php 
                    if(!empty($this->session->flashdata('msg')))
                    {
                    ?>                  
                    <div class="alert alert-warning" role="alert">
                     <?php  echo $this->session->flashdata('msg');?>
                    </div>
                    <?php
                    }
                    ?>     
            <form class="form-horizontal" action="<?php echo site_url()?>Company/insert_employee" method="post" enctype="multipart/form-data">
                <input type="hidden" id="cid" name="cid" value="<?php echo $company;?>">
                <input type="hidden" id="comp_emp" name="comp_emp" value="<?php echo $company_emp;?>">
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                    <a href="<?php echo site_url()?>Company/employee_upload_format"><label for="download_file" class="btn btn-primary btn-flat">Download Format</label></a>
                    </div>
                </div>
                <div class="form-group">
                    <label for="file_name" class="col-sm-2 control-label">Upload CSV</label>
                    <div class="col-sm-10">
                        <input type="file" name="file_name" id="file_name" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button class="btn btn-primary btn-flat" type="submit">Next</button>
                        <button class="btn btn-warning btn-flat" type="reset">Reset</button>
                        <a class="btn btn-default btn-flat" href="">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
        </div>
</div>
    </div>
</div>
</div>