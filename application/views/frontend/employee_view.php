<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?> 
 <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

<style>
    .form-inline .form-control  {margin-bottom:2%; margin-right: 2%; margin-left: 2% }

</style>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
<div class="content">
    <div class="row">
         <div class="col-md-12">
             <div class="box">
                    <div class="box-body">
               <?php 
                    if(empty($employee_detail))
                    {
                    ?>
            <form class="form-horzontal" action="<?php echo site_url()?>Company/employee_upload_view" method="post">
                 <input type="hidden" id="cid" name="cid" value="<?php echo $company;?>">
                <!--<input type="hidden" id="comp_emp1" name="comp_emp1" value="<?php //echo $company_emp;?>">-->
                <div class="form-group">
                    <div class='col-sm-offset-2 col-sm-10'>
                        <input type="submit"  value="Upload Employee Data" class="btn btn-primary btn-flat">
                    </div>
                </div>
            </form>
             <?php
                    }
                    ?>
       <form class="form-inline" id="employee_frm" role="form" name="employee_frm" action="<?php echo site_url()?>Company/insert_employee" method="post">
       <input type="hidden" id="cid" name="cid" value="<?php echo $company;?>">
       <input type="hidden" id="comp_emp" name="comp_emp" value="<?php echo $company_emp;?>">
       <input type="hidden" name="count" id="count" value="">
        Estimate Contribution <br>
       Employee <input type="text" name="emp_estimate" id="emp_estimate" value="<?php echo $estimate[0]->employee_estimate?>"> %/$ <br>
       Dependent<input type="text" name="depen_estimate" id="depen_estimate" value="<?php echo $estimate[0]->dependant_estimate?>">%/$<br>

        <div class="form-group">
         <?php
         if(!empty($employee_detail))
         {
         ?>
           <div class='col-sm-10 col-sm-offset-2'>
                 <button class="btn btn-primary btn-flat" type="button" name="emails" id="emails" value="">Send Email to Employees</button>             
           </div>
         <?php
         }
         ?>
       </div>
       <div id="main">
           <h3>Employee Information</h3>
       </div>
       <div class="form-group">
           <div class='col-sm-10 col-sm-offset-2'>
                 <button class="btn btn-primary btn-flat" type="submit" name="add" id="add" value="submit">Next</button>
                        <button class="btn btn-warning btn-flat" type="reset">Reset</button>
                        <a class="btn btn-default btn-flat" href="">Cancel</a>
                        <a class="btn btn-primary btn-flat" id="back" href="<?php echo base_url().'Company/update_company_view/'.$company?>/1">Go Back </a>
           </div>
       </div>
       </form>
        <button id="add_btn" name="add_btn">Add more employees</button><br><br>
        </div>
    </div>
</div>
    </div>
</div>
</div>
<!--<script type="text/javascript" src="<?php //echo base_url()."scripts/datePicker.js"; ?>"></script>
<link rel="stylesheet" href="<?php //echo base_url()."style/datepicker.css"; ?>"/>-->
<script>
    $('document').ready(function(){
        
        count=$('#comp_emp').val();
        if(count<4)
        {
            count=4;
             <?php
            if(empty($employee_detail))
            {
            ?>
            $('#comp_emp').val(count);
            <?php
            }
            ?>
        }
        cnt=0;
        <?php
        if(empty($employee_detail))
        {
        ?>
        for(cnt=1;cnt<=count;cnt++)
        {
          if(cnt>2)
          {
               $("#main").append("<div id='detail"+cnt+"'><input type='text' name='fname"+cnt+"' id='fname"+cnt+"' placeholder='First Name' class='form-control' required><input type='text' name='lname"+cnt+"' id='lname"+cnt+"' placeholder='Last Name' class='form-control' required><input type='text' class='datepicker form-control' name='dob"+cnt+"' id='dob"+cnt+"' onblur='valid_date("+cnt+")' placeholder='Date Of Birth' required><input type='text' class='form-control' name='zipcode"+cnt+"' id='zipcode"+cnt+"' placeholder='Zipcode' onchange='getValues(this.value,"+cnt+")' required><input type='text' class='form-control' name='state"+cnt+"' id='state"+cnt+"' placeholder='State' required><input type='text' name='salary"+cnt+"' id='salary"+cnt+"' placeholder='Salary' class='form-control salary'><input type='text' name='occupation"+cnt+"' id='occupation"+cnt+"' placeholder='Occupation' class='form-control occupation'><textarea name='address"+cnt+"' id='address"+cnt+"' placeholder='Address' class='form-control'></textarea><input type='email' id='email"+cnt+"' name='email"+cnt+"' placeholder='Email Id' class='form-control' required><input type='text' class='datepicker form-control' name='join_date"+cnt+"' id='join_date"+cnt+"'  placeholder='Join Date'><input type='text' class='form-control' name='ssn"+cnt+"' id='ssn"+cnt+"' placeholder='SSN' required><select name='gender"+cnt+"' id='gender"+cnt+"' class='form-control' required><option value=''>Gender</option><option value='1'>Male</option><option value='2'>Female</option></select><input type='checkbox' name='spouse"+cnt+"' id='spouse"+cnt+"' onchange='spouse_details("+cnt+")'> spouse <select name='child"+cnt+"' id='child"+cnt+"' onchange='child_details("+cnt+")' class='form-control'><option value='0'>child</option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option></select><button id='remove_btn"+cnt+"' name='remove_btn"+cnt+"' onclick='removediv("+cnt+")' class='form-control'>-</button></div><hr id='"+cnt+"'>");
               
          }
          else
          {
               $("#main").append("<div id='detail"+cnt+"'><input type='text' name='fname"+cnt+"' id='fname"+cnt+"' placeholder='First Name' class='form-control' required><input type='text' name='lname"+cnt+"' id='lname"+cnt+"' placeholder='Last Name' class='form-control' required><input type='text' class='datepicker form-control' name='dob"+cnt+"' id='dob"+cnt+"' onblur='valid_date("+cnt+")' placeholder='Date Of Birth' required><input type='text' class='form-control' name='zipcode"+cnt+"' id='zipcode"+cnt+"' placeholder='Zipcode' onchange='getValues(this.value,"+cnt+")' required><input type='text' class='form-control' name='state"+cnt+"' id='state"+cnt+"' placeholder='State' required><input type='text' name='salary"+cnt+"' id='salary"+cnt+"' placeholder='Salary' class='form-control salary'><input type='text' name='occupation"+cnt+"' id='occupation"+cnt+"' placeholder='Occupation' class='form-control occupation'><textarea name='address"+cnt+"' id='address"+cnt+"' placeholder='Address' class='form-control'></textarea><input type='email' id='email"+cnt+"' name='email"+cnt+"' placeholder='Email Id' class='form-control' required><input type='text' class='datepicker form-control' name='join_date"+cnt+"' id='join_date"+cnt+"'  placeholder='Join Date'><input type='text' class='form-control' name='ssn"+cnt+"' id='ssn"+cnt+"' placeholder='SSN' required><select name='gender"+cnt+"' id='gender"+cnt+"' class='form-control' required><option value=''>Gender</option><option value='1'>Male</option><option value='2'>Female</option></select><input type='checkbox' name='spouse"+cnt+"' id='spouse"+cnt+"' onchange='spouse_details("+cnt+")'> spouse <select name='child"+cnt+"' id='child"+cnt+"' onchange='child_details("+cnt+")' class='form-control'><option value='0'>child</option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option></select></div><hr id='"+cnt+"'>");
              
          }
        }
           cnt--;
        <?php
        }
        else
        {
            foreach($employee_detail as $emp_detail)
            {
        ?>
                $('#add').val('update');
                <?php
                if($this->session->userdata('companyid')==$company)
                {
                }
                else
                {
                ?>
                $("#back").attr("href", "<?php echo base_url().'Company/update_company_view/'.$company?>");
                    <?php
                }
                ?>
                cnt++;
                child_cnt=0;
                if(cnt>2)
                {
                    $("#main").append("<div id='detail"+cnt+"'><input type='hidden' name='empid"+cnt+"' id='empid"+cnt+"' class='form-control' value='<?php echo $emp_detail->employeeId?>'><input type='text' name='fname"+cnt+"' id='fname"+cnt+"' placeholder='First Name' class='form-control' value='<?php echo $emp_detail->firstName?>' required><input type='text' name='lname"+cnt+"' id='lname"+cnt+"' placeholder='Last Name' class='form-control' value='<?php echo $emp_detail->lastName?>' required><input type='text' class='datepicker form-control' name='dob"+cnt+"' id='dob"+cnt+"' placeholder='Date Of Birth' value='<?php echo date('m/d/Y',strtotime($emp_detail->dateOfBirth))?>' onblur='valid_date("+cnt+")' required><input type='text' class='form-control' name='zipcode"+cnt+"' id='zipcode"+cnt+"' value='<?php echo $emp_detail->zipcode?>' placeholder='Zipcode' onchange='getValues(this.value,"+cnt+")' required><input type='text' class='form-control' name='state"+cnt+"' id='state"+cnt+"' value='<?php echo $emp_detail->state?>' placeholder='State' required><input type='text' name='salary"+cnt+"' id='salary"+cnt+"' placeholder='Salary' class='form-control salary' value='<?php echo $emp_detail->salary?>'><input type='text' name='occupation"+cnt+"' id='occupation"+cnt+"' placeholder='Occupation' class='form-control occupation' value='<?php echo $emp_detail->occupation?>'><textarea name='address"+cnt+"' id='address"+cnt+"' placeholder='Address' class='form-control'><?php echo $emp_detail->address?></textarea><input type='email' id='email"+cnt+"' name='email"+cnt+"' placeholder='Email Id' class='form-control' value='<?php echo $emp_detail->employeeEmailId?>' required><input type='text' class='datepicker form-control' name='join_date"+cnt+"' id='join_date"+cnt+"'  placeholder='Join Date' value='<?php echo date('m/d/Y',strtotime($emp_detail->join_date))?>'><input type='text' class='form-control' name='ssn"+cnt+"' id='ssn"+cnt+"' placeholder='SSN' value='<?php echo $emp_detail->ssn?>'required><select name='gender"+cnt+"' id='gender"+cnt+"' class='form-control' required><option value=''>Gender</option><option value='1' <?php if($emp_detail->gender=='male') { echo 'selected';}?>>Male</option><option value='2' <?php if($emp_detail->gender=='female') { echo 'selected';}?>>Female</option></select><input type='checkbox' name='spouse"+cnt+"' id='spouse"+cnt+"' onchange='spouse_details("+cnt+")'> spouse <select name='child"+cnt+"' id='child"+cnt+"' onchange='child_details("+cnt+")' class='form-control'><option value='0'>child</option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option></select> <button id='remove_btn"+cnt+"' name='remove_btn"+cnt+"' onclick='removediv("+cnt+")' class='form-control'>-</button></div><hr id='"+cnt+"'>");
                    
                }
                else
                {
                    $("#main").append("<div id='detail"+cnt+"'><input type='hidden' name='empid"+cnt+"' id='empid"+cnt+"' class='form-control' value='<?php echo $emp_detail->employeeId?>'><input type='text' name='fname"+cnt+"' id='fname"+cnt+"' placeholder='First Name' class='form-control' value='<?php echo $emp_detail->firstName?>' required><input type='text' name='lname"+cnt+"' id='lname"+cnt+"' placeholder='Last Name' class='form-control' value='<?php echo $emp_detail->lastName?>' required><input type='text' class='datepicker form-control' name='dob"+cnt+"' id='dob"+cnt+"' placeholder='Date Of Birth' value='<?php echo date('m/d/Y',strtotime($emp_detail->dateOfBirth))?>' onblur='valid_date("+cnt+")' required><input type='text' class='form-control' name='zipcode"+cnt+"' id='zipcode"+cnt+"' value='<?php echo $emp_detail->zipcode?>' placeholder='Zipcode' onchange='getValues(this.value,"+cnt+")' required><input type='text' class='form-control' name='state"+cnt+"' id='state"+cnt+"' value='<?php echo $emp_detail->state?>' placeholder='State' required><input type='text' name='salary"+cnt+"' id='salary"+cnt+"' placeholder='Salary' class='form-control salary' value='<?php echo $emp_detail->salary?>'><input type='text' name='occupation"+cnt+"' id='occupation"+cnt+"' placeholder='Occupation' class='form-control occupation' value='<?php echo $emp_detail->occupation?>'><textarea name='address"+cnt+"' id='address"+cnt+"' placeholder='Address' class='form-control'><?php echo $emp_detail->address?></textarea><input type='email' id='email"+cnt+"' name='email"+cnt+"' placeholder='Email Id' class='form-control' value='<?php echo $emp_detail->employeeEmailId?>' required><input type='text' class='datepicker form-control' name='join_date"+cnt+"' id='join_date"+cnt+"'  placeholder='Join Date' value='<?php echo date('m/d/Y',strtotime($emp_detail->join_date))?>'><input type='text' class='form-control' name='ssn"+cnt+"' id='ssn"+cnt+"' placeholder='SSN' value='<?php echo $emp_detail->ssn?>'required><select name='gender"+cnt+"' id='gender"+cnt+"' class='form-control' required><option value=''>Gender</option><option value='1' <?php if($emp_detail->gender=='male') { echo 'selected';}?>>Male</option><option value='2' <?php if($emp_detail->gender=='female') { echo 'selected';}?>>Female</option></select><input type='checkbox' name='spouse"+cnt+"' id='spouse"+cnt+"' onchange='spouse_details("+cnt+")'> spouse <select name='child"+cnt+"' id='child"+cnt+"' onchange='child_details("+cnt+")' class='form-control'><option value='0'>child</option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option></select></div><hr id='"+cnt+"'>");
                    
                }
                           <?php
             foreach($employee_relation as $emp_relation)
             {
                 if($emp_relation->employeeId==$emp_detail->employeeId && $emp_relation->emp_relation=='spouse')
                 {
                     
             ?>
                  $('#spouse'+cnt).prop('checked', true);
                  $("#detail"+cnt).append("<div id='spouse_detail"+cnt+"'><h5>Spouse Information</h5><input type='hidden' name='sempid"+cnt+"' id='sempid"+cnt+"' class='form-control' value='<?php echo $emp_relation->employeeSpouseId?>'><input type='text' name='sfname"+cnt+"' id='sfname"+cnt+"' value='<?php echo $emp_relation->esFirstName?>' placeholder='First Name' class='form-control' required><input type='text' name='slname"+cnt+"' id='slname"+cnt+"' value='<?php echo $emp_relation->esLastName?>' placeholder='Last Name' class='form-control' required><input type='text' name='sdob"+cnt+"' id='sdob"+cnt+"' value='<?php echo date('m/d/Y',strtotime($emp_relation->birthdate))?>' onblur='valid_date_spouse("+cnt+")' placeholder='Date Of Birth' class='datepicker form-control'><select name='sgender"+cnt+"' id='sgender"+cnt+"' class='form-control' required><option value=''>Gender</option><option value='1' <?php if($emp_relation->gen=='Male') { echo 'selected';}?>>Male</option><option value='2' <?php if($emp_relation->gen=='Female') { echo 'selected';}?>>Female</option></select><input type='text' name='sssn"+cnt+"' id='sssn"+cnt+"' class='form-control' value='<?php echo $emp_relation->ssn?>' placeholder='SSN'></div>");
                  $('.datepickers').mask('99/99/9999');
             <?php
                 }
                 else if($emp_relation->employeeId==$emp_detail->employeeId && $emp_relation->emp_relation=='child')
                 {
             ?>
                     child_cnt++;
                 $("#detail"+cnt).append("<div id='child_detail"+cnt+"_"+child_cnt+"' class='child'><h5>Child"+child_cnt+" Information</h5><input type='hidden' name='cempid"+cnt+"_"+child_cnt+"' id='cempid"+cnt+"_"+child_cnt+"' class='form-control' value='<?php echo $emp_relation->employeeSpouseId?>'><input type='text' name='cfname"+cnt+"_"+child_cnt+"' id='cfname"+cnt+"_"+child_cnt+"' value='<?php echo $emp_relation->esFirstName?>' placeholder='First Name' class='form-control' required><input type='text' name='clname"+cnt+"_"+child_cnt+"' id='clname"+cnt+"_"+child_cnt+"' value='<?php echo $emp_relation->esLastName?>' placeholder='Last Name' class='form-control' required><input type='text' name='cdob"+cnt+"_"+child_cnt+"' id='cdob"+cnt+"_"+child_cnt+"' value='<?php echo date('m/d/Y',strtotime($emp_relation->birthdate))?>' onblur='valid_date_child("+cnt+","+child_cnt+")' placeholder='Date Of Birth' class='datepickerc form-control'><select name='cgender"+cnt+"_"+child_cnt+"' id='cgender"+cnt+"_"+child_cnt+"' class='form-control' required><option value=''>Gender</option><option value='1' <?php if($emp_relation->gen=='Male') { echo 'selected';}?>>Male</option><option value='2' <?php if($emp_relation->gen=='Female') { echo 'selected';}?>>Female</option></select><input type='text' name='cssn"+cnt+"_"+child_cnt+"' id='cssn"+cnt+"_"+child_cnt+"' class='form-control' value='<?php echo $emp_relation->ssn?>' placeholder='SSN'></div>"); 
                    $('.datepickerc').mask('99/99/9999');
                    $('#child'+cnt).val(child_cnt);
             <?php
                 }
             }
            }
        }
        ?>

     
        $('#count').val(cnt);
        
        $('#add_btn').click(function(){
            last_id=$('#main').children().last().attr('id');
            cnt=$('#count').val();
            cnt++;
             $($('#'+last_id)).after("<div id='detail"+cnt+"'><input type='text' name='fname"+cnt+"' id='fname"+cnt+"' placeholder='First Name' class='form-control' required><input type='text' name='lname"+cnt+"' id='lname"+cnt+"' placeholder='Last Name' class='form-control' required><input type='text' class='datepickera form-control' name='dob"+cnt+"' id='dob"+cnt+"' placeholder='Date Of Birth' onblur='valid_date("+cnt+")' required><input type='text' class='form-control' name='zipcode"+cnt+"' id='zipcode"+cnt+"' placeholder='Zipcode' onchange='getValues(this.value,"+cnt+")' required><input type='text' class='form-control' name='state"+cnt+"' id='state"+cnt+"' placeholder='State' required><input type='text' name='salary"+cnt+"' id='salary"+cnt+"' placeholder='Salary' class='form-control salary'><input type='text' name='occupation"+cnt+"' id='occupation"+cnt+"' placeholder='Occupation' class='form-control occupation'><textarea name='address"+cnt+"' id='address"+cnt+"' placeholder='Address' class='form-control'></textarea><input type='text' id='phone"+cnt+"' name='phone"+cnt+"' placeholder='Phone Number' class='form-control'><input type='email' id='email"+cnt+"' name='email"+cnt+"' placeholder='Email Id' class='form-control' required><input type='text' class='datepickera form-control' name='join_date"+cnt+"' id='join_date"+cnt+"'  placeholder='Join Date'><input type='text' class='form-control' name='ssn"+cnt+"' id='ssn"+cnt+"' placeholder='SSN' required><select name='gender"+cnt+"' id='gender"+cnt+"' class='form-control' required><option value=''>Gender</option><option value='1'>Male</option><option value='2'>Female</option></select><input type='checkbox' name='spouse"+cnt+"' id='spouse"+cnt+"' onchange='spouse_details("+cnt+")'> spouse <select name='child"+cnt+"' id='child"+cnt+"' onchange='child_details("+cnt+")' class='form-control'><option value='0'>child</option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option></select> <button id='remove_btn"+cnt+"' name='remove_btn"+cnt+"' onclick='removediv("+cnt+")' class='form-control'>-</button></div><hr id='"+cnt+"'>");
            $('.datepickera').mask('99/99/9999');
            $('#count').val(cnt);
            tmp=parseInt($('#comp_emp').val())+1;
            $('#comp_emp').val(tmp);
            <?php
    if($occupation>0)
    {
    ?>
         $('.occupation').show();
    <?php        
    }
    else
    {
    ?>
       $('.occupation').hide();
    <?php
    }
    if($salary>0)
    {
    ?>
         $('.salary').show();
    <?php        
    }
    else
    {
    ?>
       $('.salary').hide();
    <?php
    }
   ?>
        });

    });
    function removediv(no)
    {
        empid=$("#empid"+no).val();
        $.ajax({
           url: "<?php echo site_url()?>Company/delete_employee/"+empid,
           success: function (res) {
          }
         });

        $("#detail"+no).remove();
        $("#"+no).remove();
        $("#spouse_detail"+no).remove();
        $("#detail"+no).children("div.child").remove();
        tmp=parseInt($('#comp_emp').val())-1;
        $('#comp_emp').val(tmp);
        
        
    }
    function spouse_details(no)
    {
    
         if($('#spouse'+no).is(":checked"))
         {
            $("#detail"+no).append("<div id='spouse_detail"+no+"'><h5>Spouse Information</h5><input type='text' name='sfname"+no+"' id='sfname"+no+"' placeholder='First Name' class='form-control' required><input type='text' name='slname"+no+"' id='slname"+no+"' placeholder='Last Name' class='form-control' required><input type='text' name='sdob"+no+"' id='sdob"+no+"' onblur='valid_date_spouse("+no+")' placeholder='Date Of Birth' class='datepickers form-control'><select name='sgender"+cnt+"' id='sgender"+cnt+"' class='form-control' required><option value=''>Gender</option><option value='1'>Male</option><option value='2'>Female</option></select><input type='text' name='sssn"+no+"' id='sssn"+no+"' class='form-control' placeholder='SSN'></div>");
            $('.datepickers').mask('99/99/9999');
         }
         else
         {
             sempid=$("#sempid"+no).val();
            empid=$("#empid"+no).val();
        $.ajax({
           url: "<?php echo site_url()?>Company/delete_employee_relation_data/"+sempid,
           success: function (res) {
          }
         });
            $("#spouse_detail"+no).remove();
         }
   }
    function child_details(no)
    {
       if($('#child'+no).val() > 0)
      {
           if($('#empid'+no).val())
          {
             child_last_id=$('#detail'+no).children("div.child").last().attr('id');
             if(child_last_id)
              {   
                 var id = child_last_id.split("_");
                  start=0;
                 if(child_last_id.indexOf('_')==-1)
                    start=0;     
                 else
                     start=parseInt(id[2]);
              }
              else
                  {
                      start=0;
                  }
              if($('#child'+no).val() > start)
             { 
                for(i=start+1;i<=$('#child'+no).val();i++)
                {    
                   $("#detail"+no).append("<div id='child_detail"+no+"_"+i+"' class='child'><h5>Child"+i+" Information</h5><input type='text' name='cfname"+no+"_"+i+"' id='cfname"+no+"_"+i+"' placeholder='First Name' class='form-control' required><input type='text' name='clname"+no+"_"+i+"' id='clname"+no+"_"+i+"' placeholder='Last Name' class='form-control' required><input type='text' name='cdob"+no+"_"+i+"' id='cdob"+no+"_"+i+"' onblur='valid_date_child("+no+","+i+")' placeholder='Date Of Birth' class='datepickerc form-control'><select name='cgender"+no+"_"+i+"' id='cgender"+no+"_"+i+"' class='form-control' required><option value=''>Gender</option><option value='1'>Male</option><option value='2'>Female</option></select><input type='text' name='cssn"+no+"_"+i+"' id='cssn"+no+"_"+i+"' class='form-control' placeholder='SSN'></div>");
                   $('.datepickerc').mask('99/99/9999');
                }
            }
            else
            {
                for(i=start;i>$('#child'+no).val();i--)
                {    
                     cempid=$("#cempid"+no+"_"+i).val();
                     
                     $.ajax({
                       url: "<?php echo site_url()?>Company/delete_employee_relation_data/"+cempid,
                       success: function (res) {
                      }
                     });
                    $("#child_detail"+no+"_"+i).remove();
                    
                }
            }
          }
          else
          {
             $("#detail"+no).children("div.child").remove();
            for(i=1;i<=$('#child'+no).val();i++)
            {    
                $("#detail"+no).append("<div id='child_detail"+no+"_"+i+"' class='child'><h5>Child"+i+" Information</h5><input type='text' name='cfname"+no+"_"+i+"' id='cfname"+no+"_"+i+"' placeholder='First Name' class='form-control' required><input type='text' name='clname"+no+"_"+i+"' id='clname"+no+"_"+i+"' placeholder='Last Name' class='form-control' required><input type='text' name='cdob"+no+"_"+i+"' id='cdob"+no+"_"+i+"' onblur='valid_date_child("+no+","+i+")' placeholder='Date Of Birth' class='datepickerc form-control'><select name='cgender"+no+"_"+i+"' id='cgender"+no+"_"+i+"' class='form-control' required><option value=''>Gender</option><option value='1'>Male</option><option value='2'>Female</option></select><input type='text' name='cssn"+no+"_"+i+"' id='cssn"+no+"_"+i+"' class='form-control' placeholder='SSN'></div>");
                $('.datepickerc').mask('99/99/9999');
            }
          }
     }
     else
     {
            empid=$('#empid'+no).val();
            $.ajax({
                 url: "<?php echo site_url()?>Company/delete_employee_child/"+empid+"/child",
                  success: function (res) {
                }
                 
           });
        $("#detail"+no).children("div.child").remove();
     }
         
    }
</script>
<script>
/*$(document).ready(function(){
    $(document).on("focus", ".datepicker", function(){
        $(this).datepicker();
    });
      $.noConflict();
});*/
</script>
<script>
$(document).ready(function(){
    <?php
    if($occupation>0)
    {
    ?>
         $('.occupation').show();
    <?php        
    }
    else
    {
    ?>
       $('.occupation').hide();
    <?php
    }
    if($salary>0)
    {
    ?>
         $('.salary').show();
    <?php        
    }
    else
    {
    ?>
       $('.salary').hide();
    <?php
    }
   ?>
});
</script>
<script>
$(document).on('click','#emails',function(){
   $.ajax({
      url:'<?php echo site_url()?>Company/sendemail',
      type : "POST", 
      data: $('#employee_frm').serialize(),
      success:function(res)
      {
          console.log(res);
      }
   });
});
</script>
<script>
    function getValues(zipcode,cnt) {
        $.ajax({
            url: '<?= base_url(); ?>Company/getCountryStateCity',
            data: {"zipcode": zipcode},
            success: function (result) {
                var count_State_City = JSON.parse(result);
                $('#state'+cnt).val(count_State_City.state);
                console.log(count_State_City);
            }
        });
    }
    $(document).ready(function () {
       $.noConflict();
        $('.datepicker').mask('99/99/9999');
    });
</script>
<script>
function valid_date(no)
{
   var emp_date = $('#dob'+no).val().split("/");
   var start = new Date($('#dob'+no).val());
   var end = new Date();
   var diff = new Date(end - start);
   var days = diff/1000/60/60/24;
   var valid_days=16*365;
   if(emp_date[0] > 12)
   {
      alert("Month should be less than 12");      
   }
   if(emp_date[1] > 31)
   {
      alert("Date should be less than 31");     
   }
   if(days>=valid_days)
   {
   }
   else
   {
       alert('Age must be greter than 16');
       $('#dob'+no).val('');
       document.getElementById('dob'+no).focus();
   }
}
function valid_date_spouse(no)
{
   var spouse_date = $('#sdob'+no).val().split("/");
   var start = new Date($('#sdob'+no).val());
   var end = new Date();
   var diff = new Date(end - start);
   var days = diff/1000/60/60/24;
   var valid_days=16*365;
   if(spouse_date[0] > 12)
   {
     alert("Month should be less than 12");      
   }
   if(spouse_date[1] > 31)
   {
     alert("Date should be less than 31");     
   }
   if(days>=valid_days)
   {
   }
   else
   {
       alert('Age must be greter than 16');
       $('#sdob'+no).val('');
       document.getElementById('sdob'+no).focus();
   }
}
function valid_date_child(no,cnt)
{
   var child_date = $('#cdob'+no+'_'+cnt).val().split("/");
   var start = new Date($('#cdob'+no+'_'+cnt).val());
   var end = new Date();
   var diff = new Date(end - start);
   var days = diff/1000/60/60/24;
   var valid_eight=8*365;
   var valid_twenty_seven=27*365;
   if(child_date[0] > 12)
   {
     alert("Month should be less than 12");      
   }
   if(child_date[1] > 31)
   {
     alert("Date should be less than 31");     
   }
   if(days>=valid_eight && days<=valid_twenty_seven)
   {
   }
   else
   {
      alert('Age must be greter than 8 year and less than 27');
      $('#cdob'+no+'_'+cnt).val('');
      document.getElementById('cdob'+no+'_'+cnt).focus();
   }
}
</script>