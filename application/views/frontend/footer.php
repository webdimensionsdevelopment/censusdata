<footer style="background: #E4E8ED; position:absolute; bottom:0; width:100%; height:60px;">
    <div class="container" style="margin-top: 18px;">
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p class="text-center">
            <?php echo CB_COPYRIGHT; ?>
            <?php for ($nf = 0; $nf < count($navfoot); $nf++) : ?>
                &middot;
                <a href="<?php echo site_url($navfoot[$nf]['href']); ?>">
                    <?php echo $navfoot[$nf]['text'] ?>
                </a>
            <?php endfor; ?>
        </p>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

</footer>
