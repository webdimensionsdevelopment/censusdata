<div class="container" >
    <ul id="progressbar">
    <li class="active">Step I</li>
        <li class="">Step II</li>
        <li>Step III</li>
    </ul>
    <h2 align="center">Group Quote Request Form</h2>
   
    <div class="inner-container white-frontend">
    <div class="row">
             
            <div class="col-md-12">
            <p class="text-info">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati fugiat in, ipsam hic perferendis itaque rerum, molestiae sit cum optio iste, omnis laborum repellat ex tempore iusto dolorem repellendus aut.</p>
            <form class="form-horizontal" action="<?php echo site_url() ?>Frontend_company/insert_company" method="post">
           
            <?php
                 if(!empty($company_detail))
                 {
                     
                 ?>
                  <input type="hidden" id="companyid" name="companyid" value="<?php echo $company_detail[0]->companyId?>">
                   <h4>Broker Info</h4><hr>
                   <div class="form-group">
                        <label for="brokerage_name" class="col-md-3 label-heading asterisk">Brokerage Name:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="brokerage_name" name="brokerage_name" value="<?php echo $company_detail[0]->brokerage_name?>" placeholder="Brokerage Name" required>
                        </div>  
                    </div>
                    <div class="form-group">
                        <label for="bfname" class="col-md-3 label-heading asterisk">Broker First Name:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="bfname" name="bfname" value="<?php echo $company_detail[0]->broker_fname?>" placeholder="Broker First Name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="blname" class="col-md-3 label-heading asterisk">Broker Last Name:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="blname" name="blname" value="<?php echo $company_detail[0]->broker_lname?>" placeholder="Broker Last Name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="npn" class="col-md-3 label-heading asterisk">NPN #:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="npn" name="npn" value="<?php echo $company_detail[0]->npn?>" placeholder="NPN #" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-md-3 label-heading asterisk">Phone #:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $company_detail[0]->phone?>" placeholder="Phone #" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="bemail" class="col-md-3 label-heading asterisk">Email Address:</label>
                        <div class="col-md-8">
                            <input type="email" class="form-control" id="bemail" name="bemail" value="<?php echo $company_detail[0]->bemail?>" placeholder="Email Address" required>
                        </div>
                    </div>
                    <hr> <h4>Company Info</h4><hr>
                    <div class="form-group">
                        <label for="cname" class="col-md-3 label-heading asterisk">Company Name:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="cname" name="cname" value="<?php echo $company_detail[0]->company?>" placeholder="Company Name" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address" class="col-md-3 label-heading asterisk">Address:</label>
                        <div class="col-md-8">
                            <textarea class="form-control" id="address" name="address" Placeholder="Address"><?php echo $company_detail[0]->address?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="zipcode" class="col-md-3 label-heading asterisk">Zipcode:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $company_detail[0]->zipcode?>" Placeholder="Zipcode" required onchange="getValues(this.value);">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="city" class="col-md-3 label-heading asterisk">City:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="city" name="city" value="<?php echo $company_detail[0]->city?>" Placeholder="City" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="country" class="col-md-3 label-heading asterisk">County:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="country" name="country" value="<?php echo $company_detail[0]->country?>" Placeholder="County" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="state" class="col-md-3 label-heading asterisk">State:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="state" name="state" value="<?php echo $company_detail[0]->state?>" Placeholder="State" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-md-3 label-heading asterisk">Email Address: (Optional)</label>
                        <div class="col-md-8">
                            <input type="email" class="form-control" id="email" name="email" value="<?php echo $company_detail[0]->email?>" Placeholder="Email Address">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="no_of_emp" class="col-md-3 label-heading asterisk"># of Full Time Employees:</label>
                        <div class="col-md-8">
                            <input type="number" class="form-control" id="no_of_emp" name="no_of_emp"  value="<?php echo $company_detail[0]->no_of_emp?>" required min="0">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sic" class="col-md-3 label-heading asterisk">SIC Code:</label>
                        <div class="col-md-8">
                            <select class="form-control itemName" id="sic" name="sic" required>
                                <option value="">SIC Code</option>
                                <?php
                                foreach ($sic_code as $sc) {
                                    ?>
                                    <option value="<?php echo $sc->sicId ?>" <?php if ($company_detail[0]->sicId == $sc->sicId) {
                                echo "selected";
                            } ?>><?php echo $sc->code . " " . $sc->sicText ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <?php
                    }
              else {
                    ?>
                <div class="grey-sec">
                     <h4>Broker Info</h4>
                     <div class="custom-form">
                       <div class="row row-gutter-sm">
                        <div class="col-md-6 gutter-sm">
                            <div class="form-group">
                                <label for="brokerage_name" class="asterisk">Brokerage Name:</label>
                                <input type="text" class="form-control" id="brokerage_name" name="brokerage_name" value="" placeholder="Brokerage Name" required>
                            </div>
                        </div>
                        <div class="col-md-3 gutter-sm">
                            <div class="form-group">
                                <label for="npn" class="asterisk">NPN #: 
                                <a href="#0" data-tooltip="National Producer Number" tabindex="500"><img src="<?php echo base_url() ?>/assets/images/question-mark.png" alt=""></a></label>
                                <input type="text" class="form-control" id="npn" name="npn" value="" placeholder="NPN #" required>
                            </div>
                        </div>
                    </div>
                    <div class="row row-gutter-sm">
                        <div class="col-md-6 gutter-sm">
                            <div class="form-group">
                                <label for="bfname" class="asterisk">Broker First Name:</label>
                                <input type="text" class="form-control" id="bfname" name="bfname" value="" placeholder="Broker First Name" required>
                            </div>
                        </div>
                        <div class="col-md-6 gutter-sm">              
                            <div class="form-group">
                                <label for="blname" class="asterisk">Broker Last Name:</label>
                                <input type="text" class="form-control" id="blname" name="blname" value="" placeholder="Broker Last Name" required>
                            </div>
                        </div>
                        </div>
                    <div class="row row-gutter-sm">
                        <div class="col-md-6 gutter-sm">
                            <div class="form-group">
                                <label for="phone" class="asterisk">Phone #:</label>
                                <input type="text" class="form-control" id="phone" name="phone" value="" placeholder="Phone #" required>
                            </div>
                        </div>
                        <div class="col-md-6 gutter-sm">
                            <div class="form-group">
                                <label for="bemail" class="asterisk">Email Address:</label>
                                <input type="email" class="form-control" id="bemail" name="bemail" value="" placeholder="Email Address" required>
                            </div>
                        </div>
                    </div>

                </div>          
            </div>
            <div class="grey-sec">
             <h4>Company Info</h4>
             <div class="custom-form">
               <div class="row row-gutter-sm">
                <div class="col-md-6 gutter-sm">
                    <div class="form-group">
                        <label for="cname" class="asterisk">Company Name:</label>
                            <input type="text" class="form-control" id="cname" name="cname" value="" placeholder="Company Name" required>
                        </div>
                    </div>
                <div class="col-md-6 gutter-sm">
                   <div class="form-group">
                    <label for="address" class="col-md-3 label-heading asterisk">Address:</label>                       
                    <textarea class="form-control" id="address" name="address" Placeholder="Address"></textarea>               
                </div>
            </div>
        </div>
        <div class="row row-gutter-sm">
            <div class="col-md-6 gutter-sm">
               <div class="form-group">
                <label for="zipcode" class="asterisk">Zipcode:</label>
                <input type="text" class="form-control" id="zipcode" name="zipcode" Placeholder="Zipcode" required onchange="getValues(this.value);">
            </div>
        </div>
        <div class="col-md-6 gutter-sm">
            <div class="form-group">
                <label for="city" class="asterisk">City:</label>
                <input type="text" class="form-control" id="city" name="city" Placeholder="City" required>
            </div>
        </div>
    </div>
    <div class="row row-gutter-sm">
        <div class="col-md-6 gutter-sm">
            <div class="form-group">
                <label for="country" class="col-md-3 label-heading asterisk">County:</label>
                <input type="text" class="form-control" id="country" name="country" Placeholder="County" required>
            </div>
        </div>
        <div class="col-md-6 gutter-sm">
            <div class="form-group">
                <label for="state" class="col-md-3 label-heading asterisk">State:</label>
                <input type="text" class="form-control" id="state" name="state" Placeholder="State" required>
            </div>
        </div>
    </div>
    <div class="row row-gutter-sm">
        <div class="col-md-6 gutter-sm">
            <div class="form-group">
                <label for="email" class="asterisk">Email Address: (Optional)</label>
                <input type="email" class="form-control" id="email" name="email" Placeholder="Email Address">
            </div>
        </div>
        <div class="col-md-3 gutter-sm">
            <div class="form-group">
                <label for="no_of_emp" class="asterisk"># of Full Time Employees:</label>
                <input type="number" class="form-control" id="no_of_emp" name="no_of_emp"  value="" required min="0" placeholder="# of Employee">
            </div>
        </div>
    </div>
    <div class="row row-gutter-sm">
        <div class="col-md-6 gutter-sm">
            <div class="form-group">
                <label for="sic" class="col-md-3 label-heading asterisk">SIC Code:
                    <a href="#0" data-tooltip="The Standard Industrial Classification Code (SIC Code) is a four digit code used to categorize business activities" tabindex="501"><img src="<?php echo base_url() ?>/assets/images/question-mark.png" alt=""></a>
                    </label>
                <select class="form-control itemName" id="sic" name="sic" required>
                    <option value="">SIC Code</option>
                    <?php
                    foreach ($sic_code as $sc) {
                        ?>
                        <option value="<?php echo $sc->sicId ?>"><?php echo $sc->code . " " . $sc->sicText ?></option>
                        <?php
                    }
                    ?>
                </select>

            </div>
        </div>
    </div>
</div>
</div>
<div class="grey-sec">
 <h4>Products to Quote</h4>
 <div class="custom-form">
   <div class="row row-gutter-sm">
     <div class="form-group check-list clear">
        <!-- <label for="product" class="col-md-3 label-heading asterisk">Product</label> -->

                            <?php
                            foreach ($product as $p) {
                                ?>
            <div class="col-md-4">
                <label class="check_box <?php echo $p->name; ?>">
                    <input type="checkbox" id="product<?php echo $p->productId ?>" name="product[]" value="<?php echo $p->productId ?>" class="product-control" onchange="offering(<?php echo $p->productId ?>)"><span><?php echo $p->name; ?> </span>
                    <span class="check">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        width="40px" height="40px" viewBox="0 0 42 42" enable-background="new 0 0 42 42" xml:space="preserve">
                        <path fill-rule="evenodd" clip-rule="evenodd" fill="#56BFDE" d="M32.591,14.06L19.8,28.46c-0.32,0.36-0.76,0.54-1.2,0.54
                        c-0.35,0-0.71-0.12-1-0.35l-8-6.4C8.91,21.7,8.8,20.69,9.35,20c0.551-0.69,1.561-0.8,2.25-0.25l6.811,5.45L30.2,11.93
                        c0.59-0.66,1.6-0.71,2.26-0.13C33.12,12.39,33.181,13.4,32.591,14.06z M21,0.2C9.53,0.2,0.2,9.53,0.2,21S9.53,41.8,21,41.8
                        S41.8,32.47,41.8,21S32.47,0.2,21,0.2z"/>
                    </svg></span>
                    <span class="prod_description"><?php echo $p->description; ?></span>
                    <span class="prod_image"><img src="<?php echo base_url().'upload/admin/products/'.$p->productImage;?>" alt="" class="grayscale grayscale-fade"></span>
                </label>                        

                <div id="offer<?php echo $p->productId ?>" class="offer_box" hidden>
                    <div class="form-group" id="eff_dates<?php echo $p->productId ?>" hidden>
                        <label for="effect_date" class="asterisk">Effective Date
                            <a href="#0" data-tooltip=" Please enter the requested start date for coverage. It generally must be the 1st or 15th of the month."><img src="<?php echo base_url() ?>/assets/images/question-mark.png" alt=""></a>
                            </label>
                                        <input type="text" id="effect_date<?php echo $p->productId ?>" name="effect_date<?php echo $p->productId ?>" value="" class="form-control date"  placeholder="MM/DD/YYYY" pattern='.{10,10}'  title='10 characters required'  maxlength='10' >
                                    </div>
                    <div class="check_cont">
                        <div class="icheckbox_square-blue ">
                         <input type="checkbox" id="offer_emp<?php echo $p->productId ?>" name="offer_emp<?php echo $p->productId ?>" value="" class="set_away">
                     </div>
                     <label for="offer_emp<?php echo $p->productId ?>">Currently offering to Employee</label>
                 </div>


                 <div class="form-group" id="dates<?php echo $p->productId ?>" hidden>
                    <label for="renewal_date" class="asterisk">Renewal Date
                        <a href="#0" data-tooltip="Please enter renewal date for previous coverage."><img src="<?php echo base_url() ?>/assets/images/question-mark.png" alt=""></a></label>
                    
                                        <input type="text" id="renewal_date<?php echo $p->productId ?>" name="renewal_date<?php echo $p->productId ?>" value="" class="form-control date"  placeholder="MM/DD/YYYY" pattern='.{10,10}' title='10 characters required'  maxlength='10'>

                </div>
            </div>


        </div>
        <?php
    }
    ?>
</div>
</div>
</div>

</div>
    <?php
    }
    ?>
</div>
</div>

<div class="form-group">
<div class="text-right">
        <button type="submit" class="btn next-btn" id="add" name="add" value="add">Save and continue</button>
    </div>
</div>

            </form>

        </div>
    </div>

<script>
    function getValues(zipcode) {
        $.ajax({
            url: '<?= base_url(); ?>Frontend_company/getCountryStateCity',
            data: {"zipcode": zipcode},
            success: function (result) {
                var count_State_City = JSON.parse(result);
                $('#country').val(count_State_City.country);
                $('#state').val(count_State_City.state);
                $('#city').val(count_State_City.city);
                console.log(count_State_City);
            }
        });
    }
</script>
<script>
        $('.itemName').select2();    
</script>
<script>
    function offering(no)
    {
        if($("#product"+no).is(':checked')) {
        $("#product"+no).siblings('.prod_image').find('.grayscale').addClass('grayscale-off');
        $("#product"+no).parent().addClass("active");
            $('#offer'+no).show();
            $('#eff_dates'+no).show();
            document.getElementById("effect_date"+no).required = true;
        }
        else
        {
        $("#product"+no).siblings('.prod_image').find('.grayscale').removeClass('grayscale-off');
        $("#product"+no).parent().removeClass("active");
            $('#offer'+no).hide();
            $('#eff_dates'+no).hide();
            $('#dates'+no).hide();
            $('#effect_date'+no).val('');
            $('#renewal_date'+no).val('');
            $('#offer_emp'+no).parent().removeClass("checked");
            document.getElementById("effect_date"+no).required = false;
        }
        $('#offer_emp'+no).change(function(){
            if($('#offer_emp'+no).is(":checked")){
            $('#offer_emp'+no).parent().addClass("checked");
                $('#dates'+no).show();
            
            }
            else
            {
            $('#offer_emp'+no).parent().removeClass("checked");
                $('#dates'+no).hide();
                $('#renewal_date'+no).val('');
            }
        });
    }
</script>
<script>
    $(document).ready(function($){
        $('.date').mask("99/99/9999"); 
    });
    $(document).ready(function() {
       $(".date").keypress(function(e){
            var res = $(this).val().split("/");
               if(res[0] > 12)
               {
                  alert("Month should be less than 12");      
               }
               if(res[1] > 31)
               {
                   alert("Date should be less than 31");     
               }
        });
    });
</script>
<script>
    $(document).ready(function(){
       <?php
       if(!empty($company_detail))
       {
       ?>
         $('#add').val('update');  
      <?php
       }
       ?>
    });
</script>