<div class="container" >
    <div class="row">
        <div class="col-md-offset-2 col-md-9" style="margin-top: 10%;">  
            <form class="form-horizontal" action="<?php echo site_url()?>Frontend_company/insert_question_answer"  enctype="multipart/form-data" method="post">
               <input type="text" id="cid" name="cid" value="<?php echo $cid?>">
               <input type="hidden" id="count" name="count" value="1">
               <div id="main">
               <div id="types1">
                    <div class="form-group">
                         <label for="file_type1" class="col-md-3 label-heading asterisk">File Type</label>
                        <div class="col-md-8">
                            <select name="file_type1" id="file_type1" class="form-control" required>
                                <option value="">File Type</option>
                               <?php
                               foreach($document as $doc)
                               {
                               ?>
                                <option value="<?php echo $doc->documentId?>"><?php echo $doc->documentFile?></option>
                                <?php
                               }
                               ?>
                            </select>
                        </div>
                    </div>
                   <div id="files1">
                       <input type="hidden" name="file_count1" id="file_count1" value="1">
                    <div class="form-group">
                        <label for="file_name1_1" class="col-md-3 label-heading asterisk">File</label>
                        <div class="col-md-8">
                            <input type="file" name="file_name1_1" id="file_name1_1"  required>
                        </div>
                    </div>
                   </div>
                    <div class="form-group">
                        <label for="more_file1" class="col-md-3 label-heading asterisk"></label>
                        <div class="col-md-8">
                            <input type="button" name="more_file1" id="more_file1" value="Add more file" onclick="add_files(1)">
                        </div>
                    </div>
                   </div>
               </div>
               <div class="form-group">
                    <label for="more_type" class="col-md-3 label-heading asterisk"></label>
                    <div class="col-md-8">
                        <input type="button" name="more_type" id="more_type" value="Add more file type">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-3">
                        <input class="form-control  btn btn-default" type="submit" name="add_file" id="add_file" value="Next">
                    </div>
                </div>
            </form>
        </div>
        </div>
</div>
<script>
 $(document).on('click','#more_type',function(){
    cnt=$('#count').val();
    cnt++;
    $('#main').append("<div id='types"+cnt+"'><div class='form-group'><label for='file_type"+cnt+"' class='col-md-3 label-heading asterisk'>File Type</label><div class='col-md-8'><select name='file_type"+cnt+"' id='file_type"+cnt+"' class='form-control' required><option value=''>File Type</option><?php foreach($document as $doc){ ?>  <option value='<?php echo $doc->documentId?>'><?php echo $doc->documentFile?></option><?php }?></select></div></div><div id='files"+cnt+"'><input type='hidden' name='file_count"+cnt+"' id='file_count"+cnt+"' value='1'><div class='form-group'><label for='file_name"+cnt+"_1' class='col-md-3 label-heading asterisk'>File</label><div class='col-md-8'><input type='file' name='file_name"+cnt+"_1' id='file_name"+cnt+"_1'  required></div></div></div><div class='form-group'><label for='more_file"+cnt+"' class='col-md-3 label-heading asterisk'></label><div class='col-md-8'><input type='button' name='more_file"+cnt+"' id='more_file"+cnt+"' value='Add more file' onclick='add_files("+cnt+")'></div></div></div>");
    $('#count').val(cnt);
});       
function add_files(no)
{
    cnt=$('#file_count'+no).val();
   cnt++;
    $('#files'+no).append("<div class='form-group'><label for='file_name"+no+"_"+cnt+"' class='col-md-3 label-heading asterisk'>File</label><div class='col-md-8'><input type='file' name='file_name"+no+"_"+cnt+"' id='file_name"+no+"_"+cnt+"'></div></div>");
    $('#file_count'+no).val(cnt);
}    
</script>