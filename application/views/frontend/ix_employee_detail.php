<style>
    .form-inline .form-control  {margin-bottom:2%; margin-right: 2%; margin-left: 2% }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

<div class="container" >
  <ul id="progressbar">
        <li class="done">Step I</li>
        <li class="active">Step II</li>
        <li>Step III</li>
    </ul>
   <div class="inner-container white-frontend">
    <div class="row">
        <div class="col-md-12">
          <div class="white-back-title">
            <h4>Employee Census</h4>
          </div>
             
       <form class="form-inline1" id="employee_frm" role="form" name="employee_frm" action="<?php echo site_url()?>Ix_frontend_company/insert_employee" method="post">
       <input type="hidden" id="cid" name="cid" value="<?php echo $company;?>">
       <input type="hidden" name="count" id="count" value="">
        <div class="grey-sec">
        <div id="main" class="custom-form">
        </div>
       </div>
       <div class="form-group">
          <div class='text-right'>
                <input type="submit" id="add" name="add" value="Next" class="btn next-btn">
          </div>
       </div>
       </form> 
      </div>
    </div>
</div>
</div>
<script>
    $(document).ready(function(){
       count=$('#comp_emp').val();
       cnt=0;
        <?php
        if(empty($employee_detail))
        {
        ?>
        for(cnt=1;cnt<=count;cnt++)
        {
            if(cnt>2)
            {
                $("#main").append("<div id='detail"+cnt+"' class='emp_form'><div class='emp_no'>"+cnt+"</div><div class='row row-gutter-sm'><div class='input-150 gutter-sm'><label for='fname'>First Name</label><input type='text' name='fname"+cnt+"' id='fname"+cnt+"' placeholder='First Name' class='form-control' required></div><div class='input-150 gutter-sm'><label for='lname'>Last Name</label><input type='text' name='lname"+cnt+"' id='lname"+cnt+"' placeholder='Last Name' class='form-control'></div><div class='input-150 gutter-sm'><label for='dob'>DOB</label><input type='text' class='datepicker form-control' name='dob"+cnt+"' id='dob"+cnt+"' placeholder='MM/DD/YYYY' pattern='.{10,10}' title='10 characters required' maxlength='10' required></div><div class='input-100 gutter-sm'><label for='zipcode'>Zipcode</label><input type='text' name='zipcode"+cnt+"' id='zipcode"+cnt+"' class='form-control' placeholder='Zipcode' onchange='getValues(this.value,"+cnt+")' required></div><div class='input-100 gutter-sm'><label for='state'>State</label><input type='text' name='state"+cnt+"' id='state"+cnt+"'  class='form-control' placeholder='State'></div><div class='input-100 gutter-sm'><label for='gender'>Gender</label><select name='gender"+cnt+"' id='gender"+cnt+"' class='form-control' required><option value=''>Gender</option><option value='1'>Male</option><option value='2'>Female</option></select></div><div class='input-150 gutter-sm salary'><label for='salary'>Salary <a href='#0' tabindex='500' data-tooltip='Please enter the base salary of each employee. This is required for disability and/or life insurance quotes.'><img src='<?php echo base_url() ?>/assets/images/question-mark.png' alt=''></a></label><input type='text' name='salary"+cnt+"' id='salary"+cnt+"' placeholder='Salary' class='form-control salary1'></div><div class='input-150 gutter-sm occupation'><label for='occupation'>Occupation <a href='#0' tabindex='500' data-tooltip='Please enter the job title of each employee. This is required for disability insurance quotes.'><img src='<?php echo base_url() ?>/assets/images/question-mark.png' alt=''></a></label><input type='text' name='occupation"+cnt+"' id='occupation"+cnt+"' placeholder='Occupation' class='form-control occupation1'></div><div class='input-150 gutter-sm onboard'><label>Address</label><textarea  name='address"+cnt+"' id='address"+cnt+"' placeholder='Address' class='form-control'></textarea></div><div class='input-150 gutter-sm'><label>Phone Number</label><input type='text' name='phone"+cnt+"' id='phone"+cnt+"' placeholder='Phone Number' class='form-control' required></div><div class='input-150 gutter-sm onboard'><label>Email Id</label><input type='text' name='email"+cnt+"' id='email"+cnt+"' placeholder='Email Id' class='form-control'></div><div class='input-150 gutter-sm onboard'><label>Join Date</label><input type='text' name='join_date"+cnt+"' id='join_date"+cnt+"' placeholder='Join Date' class='form-control'></div><div class='input-150 gutter-sm onboard'><label>SSN</label><input type='text' name='ssn"+cnt+"' id='ssn"+cnt+"' placeholder='SSN' class='form-control'></div><div class='input-80 gutter-sm'><label>Spouse</label><div class='check_cont'><div class='icheckbox_square-blue '><input type='checkbox' name='spouse"+cnt+"' id='spouse"+cnt+"' value='' onchange='spouse_details("+cnt+")' class='set_away'></div><label for='spouse"+cnt+"'>Yes</label></div></div><div class='input-80 gutter-sm'><label>Children</label><select name='child"+cnt+"' id='child"+cnt+"' onchange='child_details("+cnt+")' class='form-control'><option value='0'>0</option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option></select></div><div class='input-25 gutter-sm'><button id='remove_btn"+cnt+"' name='remove_btn"+cnt+"' onclick='removediv("+cnt+")' class='btn btn-danger btn-remove'>-</button></div></div></div><hr id='"+cnt+"'>");
            }
            else
            {
                $("#main").append("<div id='detail"+cnt+"' class='emp_form'><div class='emp_no'>"+cnt+"</div><div class='row row-gutter-sm'><div class='input-150 gutter-sm'><label for='fname'>First Name</label><input type='text' name='fname"+cnt+"' id='fname"+cnt+"' placeholder='First Name' class='form-control' required></div><div class='input-150 gutter-sm'><label for='lname'>Last Name</label><input type='text' name='lname"+cnt+"' id='lname"+cnt+"' placeholder='Last Name' class='form-control'></div><div class='input-150 gutter-sm'><label for='dob'>DOB</label><input type='text' class='datepicker form-control' name='dob"+cnt+"' id='dob"+cnt+"' placeholder='MM/DD/YYYY' pattern='.{10,10}' title='10 characters required' maxlength='10' required></div><div class='input-100 gutter-sm'><label for='zipcode'>Zipcode</label><input type='text' name='zipcode"+cnt+"' id='zipcode"+cnt+"' class='form-control' placeholder='Zipcode' onchange='getValues(this.value,"+cnt+")' required></div><div class='input-100 gutter-sm'><label for='state'>State</label><input type='text' name='state"+cnt+"' id='state"+cnt+"'  class='form-control' placeholder='State'></div><div class='input-100 gutter-sm'><label for='gender'>Gender</label><select name='gender"+cnt+"' id='gender"+cnt+"' class='form-control' required><option value=''>Gender</option><option value='1'>Male</option><option value='2'>Female</option></select></div><div class='input-150 gutter-sm salary'><label for='salary'>Salary <a href='#0' tabindex='500' data-tooltip='Please enter the base salary of each employee. This is required for disability and/or life insurance quotes.'><img src='<?php echo base_url() ?>/assets/images/question-mark.png' alt=''></a></label><input type='text' name='salary"+cnt+"' id='salary"+cnt+"' placeholder='Salary' class='form-control salary1'></div><div class='input-150 gutter-sm occupation'><label for='occupation'>Occupation <a href='#0' tabindex='500' data-tooltip='Please enter the job title of each employee. This is required for disability insurance quotes.'><img src='<?php echo base_url() ?>/assets/images/question-mark.png' alt=''></a></label><input type='text' name='occupation"+cnt+"' id='occupation"+cnt+"' placeholder='Occupation' class='form-control occupation1'></div><div class='input-150 gutter-sm onboard'><label>Address</label><textarea  name='address"+cnt+"' id='address"+cnt+"' placeholder='Address' class='form-control'></textarea></div><div class='input-150 gutter-sm'><label>Phone Number</label><input type='text' name='phone"+cnt+"' id='phone"+cnt+"' placeholder='Phone Number' class='form-control' required></div><div class='input-150 gutter-sm onboard'><label>Email Id</label><input type='text' name='email"+cnt+"' id='email"+cnt+"' placeholder='Email Id' class='form-control'></div><div class='input-150 gutter-sm onboard'><label>Join Date</label><input type='text' name='join_date"+cnt+"' id='join_date"+cnt+"' placeholder='Join Date' class='form-control'></div><div class='input-150 gutter-sm onboard'><label>SSN</label><input type='text' name='ssn"+cnt+"' id='ssn"+cnt+"' placeholder='SSN' class='form-control'></div><div class='input-80 gutter-sm'><label>Spouse</label><div class='check_cont'><div class='icheckbox_square-blue '><input type='checkbox' name='spouse"+cnt+"' id='spouse"+cnt+"' value='' onchange='spouse_details("+cnt+")' class='set_away'></div><label for='spouse"+cnt+"'>Yes</label></div></div><div class='input-80 gutter-sm'><label>Children</label><select name='child"+cnt+"' id='child"+cnt+"' onchange='child_details("+cnt+")' class='form-control'><option value='0'>0</option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option></select></div></div></div><hr id='"+cnt+"'>");
            }
        }
        cnt--;
        <?php
        }
        else
        {
            
        foreach($employee_detail as $emp_detail)
        {
       ?>
        $('#add').val('update');
        cnt++;
        child_cnt=0;
        if(cnt>2)
        {
            $("#main").append("<div id='detail"+cnt+"' class='emp_form'><div class='emp_no'>"+cnt+"</div><div class='row row-gutter-sm'><div class='input-150 gutter-sm'><input type='hidden' name='empid"+cnt+"' id='empid"+cnt+"' value='<?php echo $emp_detail->employeeId?>'><label for='fname'>First Name</label><input type='text' name='fname"+cnt+"' id='fname"+cnt+"' placeholder='First Name' class='form-control' value='<?php echo $emp_detail->firstName?>' required></div><div class='input-150 gutter-sm'><label for='lname'>Last Name</label><input type='text' name='lname"+cnt+"' id='lname"+cnt+"' placeholder='Last Name' class='form-control' value='<?php echo $emp_detail->lastName?>' required></div><div class='input-150 gutter-sm'><label for='dob'>DOB</label><input type='text' class='datepicker form-control' name='dob"+cnt+"' id='dob"+cnt+"' placeholder='MM/DD/YYYY' pattern='.{10,10}' title='10 characters required' maxlength='10' required value='<?php echo date('m-d-Y',strtotime($emp_detail->dateOfBirth))?>'></div><div class='input-100 gutter-sm'><label for='zipcode'>Zipcode</label><input type='text' name='zipcode"+cnt+"' id='zipcode"+cnt+"' class='form-control' placeholder='Zipcode' value='<?php echo $emp_detail->zipcode?>' onchange='getValues(this.value,"+cnt+")' required></div><div class='input-100 gutter-sm'><label for='state'>State</label><input type='text' name='state"+cnt+"' id='state"+cnt+"' value='<?php echo $emp_detail->state?>' class='form-control' placeholder='State'></div><div class='input-100 gutter-sm'><label for='gender'>Gender</label><select name='gender"+cnt+"' id='gender"+cnt+"' class='form-control' required><option value=''>Gender</option><option value='1' <?php if($emp_detail->gender=='male') { echo 'selected';}?>>Male</option><option value='2' <?php if($emp_detail->gender=='female') { echo 'selected';}?>>Female</option></select></div><div class='input-150 gutter-sm salary'><label for='salary'>Salary <a href='#0' tabindex='500' data-tooltip='Please enter the base salary of each employee. This is required for disability and/or life insurance quotes.'><img src='<?php echo base_url() ?>/assets/images/question-mark.png' alt=''></a></label><input type='text' name='salary"+cnt+"' id='salary"+cnt+"' placeholder='Salary' class='form-control salary1' value='<?php echo $emp_detail->salary?>'></div><div class='input-150 gutter-sm occupation'><label for='occupation'>Occupation <a href='#0' tabindex='500' data-tooltip='Please enter the job title of each employee. This is required for disability insurance quotes.'><img src='<?php echo base_url() ?>/assets/images/question-mark.png' alt=''></a></label><input type='text' name='occupation"+cnt+"' id='occupation"+cnt+"' placeholder='Occupation' class='form-control occupation1' value='<?php echo $emp_detail->occupation?>'></div><div class='input-150 gutter-sm onboard'><label>Address</label><textarea  name='address"+cnt+"' id='address"+cnt+"' placeholder='Address' class='form-control'><?php echo $emp_detail->address?></textarea></div><div class='input-150 gutter-sm'><label>Phone Number</label><input type='text' name='phone"+cnt+"' id='phone"+cnt+"' value='<?php echo $emp_detail->phoneNumber?>' placeholder='Phone Number' class='form-control' required></div><div class='input-150 gutter-sm onboard'><label>Email Id</label><input type='text' name='email"+cnt+"' id='email"+cnt+"' placeholder='Email Id' value='<?php echo $emp_detail->employeeEmailId?>' class='form-control'></div><div class='input-150 gutter-sm onboard'><label>Join Date</label><input type='text' name='join_date"+cnt+"' id='join_date"+cnt+"' value='<?php echo date('m/d/Y',strtotime($emp_detail->join_date))?>' placeholder='Join Date' class='form-control'></div><div class='input-150 gutter-sm onboard'><label>SSN</label><input type='text' name='ssn"+cnt+"' id='ssn"+cnt+"' value='<?php echo $emp_detail->ssn?>' placeholder='SSN' class='form-control'></div><div class='input-80 gutter-sm'><label>Spouse</label><div class='check_cont'><div class='icheckbox_square-blue '><input type='checkbox' name='spouse"+cnt+"' id='spouse"+cnt+"' value='' onchange='spouse_details("+cnt+")' class='set_away'></div><label for='spouse"+cnt+"'>Yes</label></div></div><div class='input-80 gutter-sm'><label>Children</label><select name='child"+cnt+"' id='child"+cnt+"' onchange='child_details("+cnt+")' class='form-control'><option value='0'>0</option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option></select></div><div class='input-25 gutter-sm'><button id='remove_btn"+cnt+"' name='remove_btn"+cnt+"' onclick='removediv("+cnt+")' class='btn btn-danger btn-remove'>-</button></div></div></div><hr id='"+cnt+"'>");
        }
        else
        {
           $("#main").append("<div id='detail"+cnt+"' class='emp_form'><div class='emp_no'>"+cnt+"</div><div class='row row-gutter-sm'><div class='input-150 gutter-sm'><input type='hidden' name='empid"+cnt+"' id='empid"+cnt+"' value='<?php echo $emp_detail->employeeId?>'><label for='fname'>First Name</label><input type='text' name='fname"+cnt+"' id='fname"+cnt+"' placeholder='First Name' class='form-control' value='<?php echo $emp_detail->firstName?>' required></div><div class='input-150 gutter-sm'><label for='lname'>Last Name</label><input type='text' name='lname"+cnt+"' id='lname"+cnt+"' placeholder='Last Name' class='form-control' value='<?php echo $emp_detail->lastName?>' required></div><div class='input-150 gutter-sm'><label for='dob'>DOB</label><input type='text' class='datepicker form-control' name='dob"+cnt+"' id='dob"+cnt+"' placeholder='MM/DD/YYYY' pattern='.{10,10}' title='10 characters required' maxlength='10' required value='<?php echo date('m-d-Y',strtotime($emp_detail->dateOfBirth))?>'></div><div class='input-100 gutter-sm'><label for='zipcode'>Zipcode</label><input type='text' name='zipcode"+cnt+"' id='zipcode"+cnt+"' class='form-control' placeholder='Zipcode' value='<?php echo $emp_detail->zipcode?>' onchange='getValues(this.value,"+cnt+")' required></div><div class='input-100 gutter-sm'><label for='state'>State</label><input type='text' name='state"+cnt+"' id='state"+cnt+"' value='<?php echo $emp_detail->state?>' class='form-control' placeholder='State'></div><div class='input-100 gutter-sm'><label for='gender'>Gender</label><select name='gender"+cnt+"' id='gender"+cnt+"' class='form-control' required><option value=''>Gender</option><option value='1' <?php if($emp_detail->gender=='male') { echo 'selected';}?>>Male</option><option value='2' <?php if($emp_detail->gender=='female') { echo 'selected';}?>>Female</option></select></div><div class='input-150 gutter-sm salary'><label for='salary'>Salary <a href='#0' tabindex='500' data-tooltip='Please enter the base salary of each employee. This is required for disability and/or life insurance quotes.'><img src='<?php echo base_url() ?>/assets/images/question-mark.png' alt=''></a></label><input type='text' name='salary"+cnt+"' id='salary"+cnt+"' placeholder='Salary' class='form-control salary1' value='<?php echo $emp_detail->salary?>'></div><div class='input-150 gutter-sm occupation'><label for='occupation'>Occupation <a href='#0' tabindex='500' data-tooltip='Please enter the job title of each employee. This is required for disability insurance quotes.'><img src='<?php echo base_url() ?>/assets/images/question-mark.png' alt=''></a></label><input type='text' name='occupation"+cnt+"' id='occupation"+cnt+"' placeholder='Occupation' class='form-control occupation1' value='<?php echo $emp_detail->occupation?>'></div><div class='input-150 gutter-sm onboard'><label>Address</label><textarea  name='address"+cnt+"' id='address"+cnt+"' placeholder='Address' class='form-control'><?php echo $emp_detail->address?></textarea></div><div class='input-150 gutter-sm'><label>Phone Number</label><input type='text' name='phone"+cnt+"' id='phone"+cnt+"' value='<?php echo $emp_detail->phoneNumber?>' placeholder='Phone Number' class='form-control' required></div><div class='input-150 gutter-sm onboard'><label>Email Id</label><input type='text' name='email"+cnt+"' id='email"+cnt+"' placeholder='Email Id' value='<?php echo $emp_detail->employeeEmailId?>' class='form-control'></div><div class='input-150 gutter-sm onboard'><label>Join Date</label><input type='text' name='join_date"+cnt+"' id='join_date"+cnt+"' value='<?php echo date('m/d/Y',strtotime($emp_detail->join_date))?>' placeholder='Join Date' class='form-control'></div><div class='input-150 gutter-sm onboard'><label>SSN</label><input type='text' name='ssn"+cnt+"' id='ssn"+cnt+"' value='<?php echo $emp_detail->ssn?>' placeholder='SSN' class='form-control'></div><div class='input-80 gutter-sm'><label>Spouse</label><div class='check_cont'><div class='icheckbox_square-blue '><input type='checkbox' name='spouse"+cnt+"' id='spouse"+cnt+"' value='' onchange='spouse_details("+cnt+")' class='set_away'></div><label for='spouse"+cnt+"'>Yes</label></div></div><div class='input-80 gutter-sm'><label>Children</label><select name='child"+cnt+"' id='child"+cnt+"' onchange='child_details("+cnt+")' class='form-control'><option value='0'>0</option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option></select></div></div></div><hr id='"+cnt+"'>");        }
        
        <?php
        foreach($employee_relation as $emp_relation)
        {
        if($emp_relation->employeeId==$emp_detail->employeeId && ($emp_relation->emp_relation=='Spouse' || $emp_relation->emp_relation=='spouse'))
        {
        ?>
            $('#spouse'+cnt).parent().addClass("checked");
            $("#detail"+cnt).append("<div id='spouse_detail"+cnt+"'><div class='row row-gutter-sm'><div class='input-150 gutter-sm'><h5>Spouse </h5></div><div class='input-150 gutter-sm'><label for='sfname"+cnt+"'>First Name</label><input type='hidden' name='sempid"+cnt+"' id='sempid"+cnt+"' class='form-control' value='<?php echo $emp_relation->employeeSpouseId?>'><input type='text' name='sfname"+cnt+"' id='sfname"+cnt+"' placeholder='First Name' value='<?php echo $emp_relation->esFirstName?>' class='form-control' required></div><div class='input-150 gutter-sm'><label for='slname"+cnt+"'>Last Name</label><input type='text' name='slname"+cnt+"' id='slname"+cnt+"' placeholder='Last Name' value='<?php echo $emp_relation->esLastName?>' class='form-control' required></div><div class='input-150 gutter-sm'><label for='sdob"+cnt+"'>DOB</label><input type='text' name='sdob"+cnt+"' id='sdob"+cnt+"'  value='<?php echo date('m-d-Y',strtotime($emp_relation->birthdate))?>' placeholder='MM/DD/YYYY' pattern='.{10,10}' title='10 characters required' maxlength='10' class='datepickers form-control'></div><div class='input-100 gutter-sm'><label for='sgender"+cnt+"'>Gender</label><select name='sgender"+cnt+"' id='sgender"+cnt+"' class='form-control' required><option value=''>Gender</option><option value='1' <?php if($emp_relation->gen=='Male') { echo 'selected';}?>>Male</option><option value='2' <?php if($emp_relation->gen=='Female') { echo 'selected';}?>>Female</option></select></div><div class='input-150 gutter-sm onboard'><label for='sssn"+cnt+"'>SSN</label><input type='text' name='sssn"+cnt+"' id='sssn"+cnt+"' value='<?php $emp_relation->ssn?>' placeholder='SSN' class='form-control'></div></div></div>");
        <?php
        }
        else if($emp_relation->employeeId==$emp_detail->employeeId && ($emp_relation->emp_relation=='Child' || $emp_relation->emp_relation=='child'))
        {
        ?>
        child_cnt++;
        $("#detail"+cnt).append("<div id='child_detail"+cnt+"_"+child_cnt+"' class='child'><div class='row row-gutter-sm'><div class='input-150 gutter-sm'><h5>Child"+child_cnt+" </h5></div><div class='input-150 gutter-sm'><label for='cfname"+cnt+"_"+child_cnt+"'>First Name</label><input type='hidden' name='cempid"+cnt+"_"+child_cnt+"' id='cempid"+cnt+"_"+child_cnt+"' class='form-control' value='<?php echo $emp_relation->employeeSpouseId?>'><input type='text' name='cfname"+cnt+"_"+child_cnt+"' id='cfname"+cnt+"_"+child_cnt+"' value='<?php echo $emp_relation->esFirstName?>' placeholder='First Name' class='form-control' required></div><div class='input-150 gutter-sm'><label for='clname"+cnt+"_"+child_cnt+"'>Last Name</label><input type='text' name='clname"+cnt+"_"+child_cnt+"' id='clname"+cnt+"_"+child_cnt+"' value='<?php echo $emp_relation->esLastName?>' placeholder='Last Name' class='form-control' required></div><div class='input-150 gutter-sm'><label for='cdob"+cnt+"_"+child_cnt+"'>DOB</label><input type='text' name='cdob"+cnt+"_"+child_cnt+"' id='cdob"+cnt+"_"+child_cnt+"' value='<?php echo date('m-d-Y',strtotime($emp_relation->birthdate))?>' placeholder='MM/DD/YYYY' pattern='.{10,10}' title='10 characters required' maxlength='10' class='datepickerc form-control'></div><div class='input-100 gutter-sm'><label for='cgender"+cnt+"_"+child_cnt+"'>Gender</label><select name='cgender"+cnt+"_"+child_cnt+"' id='cgender"+cnt+"_"+child_cnt+"' class='form-control' required><option value=''>Gender</option><option value='1' <?php if($emp_relation->gen=='Male') { echo 'selected';}?>>Male</option><option value='2' <?php if($emp_relation->gen=='Female') { echo 'selected';}?>>Female</option></select></div><div class='input-150 gutter-sm onboard'><label for='cssn"+cnt+"_"+child_cnt+"'>SSN</label><input type='text' name='cssn"+cnt+"_"+child_cnt+"' id='cssn"+cnt+"_"+child_cnt+"' value='<?php $emp_relation->ssn?>' placeholder='SSN' class='form-control'></div></div></div>");
        $('#child'+cnt).val(child_cnt);
        <?php
        }
        }
        }
      
        }
        ?>
        
        
        
        $('#count').val(cnt);
        $('#add_btn').click(function(){
            last_id=$('#main').children().last().attr('id');
            cnt=$('#count').val();
            cnt++;
            $($('#'+last_id)).after("<div id='detail"+cnt+"' class='emp_form'><div class='emp_no'>"+cnt+"</div><div class='row row-gutter-sm'><div class='input-150 gutter-sm'><label for='fname'>First Name</label><input type='text' name='fname"+cnt+"' id='fname"+cnt+"' placeholder='First Name' class='form-control' required></div><div class='input-150 gutter-sm'><label for='lname'>Last Name</label><input type='text' name='lname"+cnt+"' id='lname"+cnt+"' placeholder='Last Name' class='form-control'></div><div class='input-150 gutter-sm'><label for='dob'>DOB</label><input type='text' class='datepicker form-control' name='dob"+cnt+"' id='dob"+cnt+"' placeholder='MM/DD/YYYY' pattern='.{10,10}' title='10 characters required' maxlength='10' required></div><div class='input-100 gutter-sm'><label for='zipcode'>Zipcode</label><input type='text' name='zipcode"+cnt+"' id='zipcode"+cnt+"' class='form-control' placeholder='Zipcode' onchange='getValues(this.value,"+cnt+")' required></div><div class='input-100 gutter-sm'><label for='state'>State</label><input type='text' name='state"+cnt+"' id='state"+cnt+"'  class='form-control' placeholder='State'></div><div class='input-100 gutter-sm'><label for='gender'>Gender</label><select name='gender"+cnt+"' id='gender"+cnt+"' class='form-control' required><option value=''>Gender</option><option value='1'>Male</option><option value='2'>Female</option></select></div><div class='input-150 gutter-sm salary'><label for='salary'>Salary <a href='#0' tabindex='500' data-tooltip='Please enter the base salary of each employee. This is required for disability and/or life insurance quotes.'><img src='<?php echo base_url() ?>/assets/images/question-mark.png' alt=''></a></label><input type='text' name='salary"+cnt+"' id='salary"+cnt+"' placeholder='Salary' class='form-control salary1'></div><div class='input-150 gutter-sm occupation'><label for='occupation'>Occupation <a href='#0' tabindex='500' data-tooltip='Please enter the job title of each employee. This is required for disability insurance quotes.'><img src='<?php echo base_url() ?>/assets/images/question-mark.png' alt=''></a></label><input type='text' name='occupation"+cnt+"' id='occupation"+cnt+"' placeholder='Occupation' class='form-control occupation1'></div><div class='input-150 gutter-sm onboard'><label>Address</label><textarea  name='address"+cnt+"' id='address"+cnt+"' placeholder='Address' class='form-control'></textarea></div><div class='input-150 gutter-sm'><label>Phone Number</label><input type='text' name='phone"+cnt+"' id='phone"+cnt+"' placeholder='Phone Number' class='form-control' required></div><div class='input-150 gutter-sm onboard'><label>Email Id</label><input type='text' name='email"+cnt+"' id='email"+cnt+"' placeholder='Email Id' class='form-control'></div><div class='input-150 gutter-sm onboard'><label>Join Date</label><input type='text' name='join_date"+cnt+"' id='join_date"+cnt+"' placeholder='Join Date' class='form-control'></div><div class='input-150 gutter-sm onboard'><label>SSN</label><input type='text' name='ssn"+cnt+"' id='ssn"+cnt+"' placeholder='SSN' class='form-control'></div><div class='input-80 gutter-sm'><label>Spouse</label><div class='check_cont'><div class='icheckbox_square-blue '><input type='checkbox' name='spouse"+cnt+"' id='spouse"+cnt+"' value='' onchange='spouse_details("+cnt+")' class='set_away'></div><label for='spouse"+cnt+"'>Yes</label></div></div><div class='input-80 gutter-sm'><label>Children</label><select name='child"+cnt+"' id='child"+cnt+"' onchange='child_details("+cnt+")' class='form-control'><option value='0'>0</option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option></select></div><div class='input-25 gutter-sm'><button id='remove_btn"+cnt+"' name='remove_btn"+cnt+"' onclick='removediv("+cnt+")' class='btn btn-danger btn-remove'>-</button></div></div></div><hr id='"+cnt+"'>");
            $('.datepickera').mask('99/99/9999');
            $('#count').val(cnt);
            tmp=parseInt($('#comp_emp').val())+1;
            $('#comp_emp').val(tmp);
    <?php
    if($onboard=='1')
    {
    ?>
       $('.onboard').hide();
    <?php
    }
    else 
    {
    ?>
      $('.onboard').show();     
    <?php
    }
    if($occupation>0)
    {
    ?>
         $('.occupation').show();
         $('.occupation1').each(function(){
            var occup= $(this);
            occup.prop('required', true);
         });
    <?php        
    }
    else
    {
    ?>
       $('.occupation').hide();
       $('.occupation1').each(function(){
            var occup= $(this);
            occup.prop('required', false);
         });
    <?php
    }
    if($salary>0)
    {
    ?>
         $('.salary').show();
         $('.salary1').each(function(){
            var salary= $(this);
            salary.prop('required', true);
         });
    <?php        
    }
    else
    {
    ?>
       $('.salary').hide();
       $('.salary1').each(function(){
            var salary= $(this);
            salary.prop('required', false);
         });
    <?php
    }
   ?>
        });

    });
    function removediv(no)
    {
        empid=$("#empid"+no).val();
        $.ajax({
           url: "<?php echo site_url()?>Ix_frontend_company/delete_employee/"+empid,
           success: function (res) {
          }
         });
        $("#detail"+no).remove();
        $("#"+no).remove();
        $("#spouse_detail"+no).remove();
        $("#detail"+no).children("div.child").remove();
        tmp=parseInt($('#comp_emp').val())-1;
        $('#comp_emp').val(tmp);
   }
    function spouse_details(no)
    {
         if($('#spouse'+no).is(":checked"))
         {
            $('#spouse'+no).parent().addClass("checked");
            $("#detail"+no).append("<div id='spouse_detail"+no+"'><div class='row row-gutter-sm'><div class='input-150 gutter-sm'><h5>Spouse </h5></div><div class='input-150 gutter-sm'><label for='sfname"+no+"'>First Name</label><input type='text' name='sfname"+no+"' id='sfname"+no+"' placeholder='First Name' class='form-control' required></div><div class='input-150 gutter-sm'><label for='slname"+no+"'>Last Name</label><input type='text' name='slname"+no+"' id='slname"+no+"' placeholder='Last Name' class='form-control' required></div><div class='input-150 gutter-sm'><label for='sdob"+no+"'>DOB</label><input type='text' name='sdob"+no+"' id='sdob"+no+"' placeholder='MM/DD/YYYY' pattern='.{10,10}' title='10 characters required' maxlength='10' class='datepickers form-control'></div><div class='input-100 gutter-sm'><label for='sgender"+no+"'>Gender</label><select name='sgender"+no+"' id='sgender"+no+"' class='form-control' required><option value=''>Gender</option><option value='1'>Male</option><option value='2'>Female</option></select></div><div class='input-150 gutter-sm onboard'><label for='sssn"+no+"'>SSN</label><input type='text' name='sssn"+no+"' id='sssn"+no+"' placeholder='SSN' class='form-control'></div></div></div>");
            $('.datepickers').mask('99/99/9999');
                <?php
                if($onboard=='1')
                {
                ?>
                   $('.onboard').hide();
                <?php
                }
                else 
                {
                ?>
                  $('.onboard').show();     
                <?php
                }
                ?>
        }
         else
         {
             sempid=$("#sempid"+no).val();
            empid=$("#empid"+no).val();
            $.ajax({
                url: "<?php echo site_url()?>Ix_frontend_company/delete_employee_relation_data/"+sempid,
                success: function (res) {
                }
            });
            $('#spouse'+no).parent().removeClass("checked");
            $("#spouse_detail"+no).remove();
         }
   }
    function child_details(no)
    {

       if($('#child'+no).val() > 0)
      {
          if($('#empid'+no).val())
            {
                child_last_id=$('#detail'+no).children("div.child").last().attr('id');
                if(child_last_id)
                {
                    var id = child_last_id.split("_");
                    start=0;
                    if(child_last_id.indexOf('_')==-1)
                        start=0;
                    else
                        start=parseInt(id[2]);
                }
                else
                {
                    start=0;
                }
                
                if($('#child'+no).val() > start)
                {
                    
                    for(i=start+1;i<=$('#child'+no).val();i++)
                    {
                        $("#detail"+no).append("<div id='child_detail"+no+"_"+i+"' class='child'><div class='row row-gutter-sm'><div class='input-150 gutter-sm'><h5>Child"+i+" </h5></div><div class='input-150 gutter-sm'><label for='cfname"+no+"_"+i+"'>First Name</label><input type='text' name='cfname"+no+"_"+i+"' id='cfname"+no+"_"+i+"' placeholder='First Name' class='form-control' required></div><div class='input-150 gutter-sm'><label for='clname"+no+"_"+i+"'>Last Name</label><input type='text' name='clname"+no+"_"+i+"' id='clname"+no+"_"+i+"' placeholder='Last Name' class='form-control' required></div><div class='input-150 gutter-sm'><label for='cdob"+no+"_"+i+"'>DOB</label><input type='text' name='cdob"+no+"_"+i+"' id='cdob"+no+"_"+i+"' placeholder='MM/DD/YYYY' pattern='.{10,10}' title='10 characters required' maxlength='10' class='datepickerc form-control'></div><div class='input-100 gutter-sm'><label for='cgender"+no+"_"+i+"'>Gender</label><select name='cgender"+no+"_"+i+"' id='cgender"+no+"_"+i+"' class='form-control' required><option value=''>Gender</option><option value='1'>Male</option><option value='2'>Female</option></select></div><div class='input-150 gutter-sm onboard'><label for='cssn"+no+"_"+i+"'>SSN</label><input type='text' name='cssn"+no+"_"+i+"' id='cssn"+no+"_"+i+"' placeholder='SSN' class='form-control'></div></div></div>");
                        $('.datepickerc').mask('99/99/9999');
                          <?php
                          if($onboard=='1')
                          {
                          ?>
                            $('.onboard').hide();
                          <?php
                          }
                          else 
                          {
                          ?>
                           $('.onboard').show();     
                          <?php
                          }
                          ?>
                    }
                }
                else
                {
                    for(i=start;i>$('#child'+no).val();i--)
                    {
                        cempid=$("#cempid"+no+"_"+i).val();

                        $.ajax({
                            url: "<?php echo site_url()?>Ix_frontend_company/delete_employee_relation_data/"+cempid,
                            success: function (res) {
                            }
                        });
                        $("#child_detail"+no+"_"+i).remove();

                    }
                }
            }
            else
            {
            $("#detail"+no).children("div.child").remove();
            for(i=1;i<=$('#child'+no).val();i++)
            {    
                $("#detail"+no).append("<div id='child_detail"+no+"_"+i+"' class='child'><div class='row row-gutter-sm'><div class='input-150 gutter-sm'><h5>Child"+i+" </h5></div><div class='input-150 gutter-sm'><label for='cfname"+no+"_"+i+"'>First Name</label><input type='text' name='cfname"+no+"_"+i+"' id='cfname"+no+"_"+i+"' placeholder='First Name' class='form-control' required></div><div class='input-150 gutter-sm'><label for='clname"+no+"_"+i+"'>Last Name</label><input type='text' name='clname"+no+"_"+i+"' id='clname"+no+"_"+i+"' placeholder='Last Name' class='form-control' required></div><div class='input-150 gutter-sm'><label for='cdob"+no+"_"+i+"'>DOB</label><input type='text' name='cdob"+no+"_"+i+"' id='cdob"+no+"_"+i+"' placeholder='MM/DD/YYYY' pattern='.{10,10}' title='10 characters required' maxlength='10' class='datepickerc form-control'></div><div class='input-100 gutter-sm'><label for='cgender"+no+"_"+i+"'>Gender</label><select name='cgender"+no+"_"+i+"' id='cgender"+no+"_"+i+"' class='form-control' required><option value=''>Gender</option><option value='1'>Male</option><option value='2'>Female</option></select></div><div class='input-150 gutter-sm onboard'><label for='cssn"+no+"_"+i+"'>SSN</label><input type='text' name='cssn"+no+"_"+i+"' id='cssn"+no+"_"+i+"' placeholder='SSN' class='form-control'></div></div></div>");
            }
            $('.datepickerc').mask('99/99/9999');
            <?php
            if($onboard=='1')
            {
            ?>
               $('.onboard').hide();
            <?php
            }
            else 
            {
            ?>
              $('.onboard').show();     
            <?php
            }
            ?>
            }
       }
       else
       {
          $("#detail"+no).children("div.child").remove();
       }
    }
</script>

<script>
$(document).ready(function(){
    <?php
    if($onboard=='1')
    {
    ?>
       $('.onboard').hide();
    <?php
    }
    else 
    {
    ?>
      $('.onboard').show();     
    <?php
    }
    if($occupation>0)
    {
    ?>
         $('.occupation').show();
         $('.occupation1').each(function(){
            var occup= $(this);
            occup.prop('required', true);
         });
    <?php        
    }
    else
    {
    ?>
       $('.occupation').hide();
       $('.occupation1').each(function(){
            var occup= $(this);
            occup.prop('required', false);
         });
    <?php
    }
    if($salary>0)
    {
    ?>
         $('.salary').show();
         $('.salary1').each(function(){
            var salary= $(this);
            salary.prop('required', true);
         });
         
    <?php        
    }
    else
    {
    ?>
       $('.salary').hide();
       $('.salary1').each(function(){
            var salary= $(this);
            salary.prop('required', false);
         }); 
    <?php
    }
   ?>
});
</script>

<script>
    function getValues(zipcode,cnt) {
        $.ajax({
            url: '<?= base_url(); ?>Ix_frontend_company/getCountryStateCity',
            data: {"zipcode": zipcode},
            success: function (result) {
                var count_State_City = JSON.parse(result);
                $('#state'+cnt).val(count_State_City.state);
                console.log(count_State_City);
            }
        });
    }
    $(document).ready(function () {
       $.noConflict();
        $('.datepicker').mask('99/99/9999');
    });
</script>