<div class="container" >
    <ul id="progressbar">
        <li class="done">Step I</li>
        <li class="active">Step II</li>
        <li>Step III</li>
    </ul>
    <div class="inner-container white-frontend">
        <div class="row">
            <div class="col-md-12">  
                <div class="white-back-title">
                    <h4>Upload Census</h4>
                    <h4>Testing</h4>
                </div>
                <div class="form-group">
                    <?php 
                    if(!empty($this->session->flashdata('msg')))
                    {
                    ?>                  
                    <div class="alert alert-warning" role="alert">
                     <?php  echo $this->session->flashdata('msg');?>
                    </div>
                    <?php
                    }
                    ?>
                    <br>
                    <a href="<?php echo site_url()?>Ix_frontend_company/employee_upload_format" class="btn btn-lg btn-primary">Download Format</a>
                </div>
                <form class="form-horizontal" action="<?php echo site_url()?>Ix_frontend_company/insert_employee" method="post" enctype="multipart/form-data">
                    <div class="grey-sec">
                        <div class="custom-form">
                            <input type="hidden" id="cid" name="cid" value="<?php echo $company;?>">
                            <input type="hidden" id="comp_emp" name="comp_emp" value="<?php echo $company_emp;?>">
                            <div class="row row-gutter-sm">
                                <div class="col-md-6 gutter-sm">
                                    <div class="form-group">
                                        <label for="file_name" class="asterisk">Upload CSV</label>
                                            <input type="file" name="file_name" id="file_name" class="form-control" required>
                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="text-right">
                            <input class="btn next-btn" type="submit" name="add_file" id="add_file" value="Next">
                             <a class="btn btn-primary btn-flat" id="back" href="<?php echo base_url()?>Ix_frontend_company/employee_view">Go Back </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>