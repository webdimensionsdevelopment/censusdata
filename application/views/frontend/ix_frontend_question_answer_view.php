<div class="container">
<ul id="progressbar">
  <li class="done">Step I</li>
  <li class="done">Step II</li>
  <li class="active">Step III</li>
</ul>
<div class="inner-container white-frontend question-container">
  <div class="row">
    <div class="col-md-12" >
      <div class="white-back-title"><h4>Upload Document</h4><h4>Testing</h4></div>
      
    <form class="form-horizontal" action="<?php echo site_url()?>Ix_frontend_company/insert_question_answer" enctype="multipart/form-data" method="post">
     <div class="custom-form mb-30">
      <input type="hidden" id="cid" name="cid" value="<?php echo $cid?>">
      <input type="hidden" id="count" name="count" value="1">
     
               <div id="main">
               <div id="types1">
                   <div class="row row-gutter-sm">
                    <div id="files1" class="col-md-6 gutter-sm">
                       <input type="hidden" name="file_count1" id="file_count1" value="1">
                      <div class="form-group">
                        <label for="file_name1_1" class="asterisk">File</label>
                        <input type="file" name="file_name1_1" id="file_name1_1"  class="form-control" onchange="validation(1,1)">
                      </div>
                   </div>
                    <div class="col-md-6 gutter-sm">
                      <div class="form-group">
                         <label for="file_type1" class=" asterisk">File Type</label>
                        
                            <select name="file_type1" id="file_type1" class="form-control">
                                <option value="">File Type</option>
                               <?php
                               foreach($document as $doc)
                               {
                               ?>
                                <option value="<?php echo $doc->documentId?>"><?php echo $doc->documentFile?></option>
                                <?php
                               }
                               ?>
                            </select>
                        </div>
                      </div>
                   
                   </div>
                   </div>
               </div>
               <div class="form-group">
                    <label for="more_type" class="asterisk"></label>
                    
                        <input type="button" name="more_type" id="more_type" value="Add File" class="btn btn-primary">
                    
                </div>
                <hr style="border-top:solid 1px #C9CCD0; margin-bottom:60px;">
      <?php
      $no=1;
      $note_no=1;
      $displayNum = 1;
      if(!empty($question) && $no_of_emp >= 10)
      {
         
       foreach($question as $key=>$quest)
      {

            $displayNum=1;

           echo "<h3 class='ques-group'>".$key."</h3>";         
          foreach($quest as $quest1)
          {


       ?>
       <div class="form-group">
        <input type="hidden" id="qid<?php echo $no?>" name="qid<?php echo $no?>" value="<?php echo $quest1->id?>">
        <h5 class="question-title"><?php echo  $displayNum.". ".$quest1->question;?> <a href='#0' data-tooltip='<?php echo $quest1->question_desc; ?>'><img src='<?php echo base_url() ?>/assets/images/question-mark.png' alt=''></a></h5>

         <div class="form-group check-list clear options">
          <?php
          foreach($answer as $a)
          {
            if($a->question_id==$quest1->id)
            {
              if($quest1->question_type=='single')
              {
                ?>
                 <div class="check">
                <input type="radio"  name="aid<?php echo $no?>[]" id="aid<?php echo $a->id?>" value="<?php echo $a->id?>">
                <label for="aid<?php echo $a->id?>"><?php echo $a->answer ?>                    </label>
                </div>
                <?php
              }
              else if($quest1->question_type=='textbox')
              {
                 ?>
                 <div class="check">
                     <label for="aid<?php echo $a->id?>"><?php echo $a->answer ?></label>
                <input type="text"  class="form-control"  name="aid<?php echo $no?>[]" id="aid<?php echo $a->id?>" value="" required>

                </div>
                <?php
              }
              else 
              {
                ?>
                <div class="check">
                <input type="checkbox" name="aid<?php echo $no?>[]" id="<?php echo $a->id?>" value="<?php echo $a->id?>">                   
                <label for="<?php echo $a->id?>"><?php echo $a->answer ?> </label>
                </div>
                <?php       
              }
            }
          }
          $note_no=$no;
          $no++;
               $displayNum++;

            echo '</div>';
          }
          
          ?>

      
           
          <div class="row row-gutter-sm">
            <div class="col-md-8 gutter-sm">
            <div class="form-group">
              <label for="" class="asterisk">Notes</label>
                <textarea class="form-control h-200" name="ques_answer<?php echo ($note_no);?>" id="ques_answer<?php echo ($note_no);?>" placeholder="please put additional comment here"></textarea>
            </div>
            </div>
        </div>
          <?php
      
    }
         
    }
    ?>


    <input type="hidden" id="cnt" name="cnt" value="<?php echo $no?>">
    
    </div>
    <div class="form-group">
      <div class="text-right">
         <a class="btn next-btn prev" href="<?php echo base_url()?>Ix_frontend_company/edit_employee_view">Go Back</a>
        <button type="submit" class="btn next-btn next submit">Submit</button>
      </div>
    </div>
    
  </form>
</div>
</div>
</div>


 <script>
       $(document).ready(function(){
            var callbacks_list = $('.demo-callbacks ul');
            $('.check-list input').on('ifCreated ifClicked ifChanged ifChecked ifUnchecked ifDisabled ifEnabled ifDestroyed', function(event){
              callbacks_list.prepend('<li><span>#' + this.id + '</span> is ' + event.type.replace('if', '').toLowerCase() + '</li>');
            }).iCheck({
              checkboxClass: 'icheckbox_square-blue',
              radioClass: 'iradio_square-blue',
              increaseArea: '20%'
            });
          });
</script>   
<script>
 $(document).on('click','#more_type',function(){
    cnt=$('#count').val();
    cnt++;
    $('#main').append("<div id='types"+cnt+"'><div class='row row-gutter-sm'><div id='files"+cnt+"' class='col-md-6 gutter-sm'><input type='hidden' name='file_count"+cnt+"' id='file_count"+cnt+"' value='1'><div class='form-group'><label for='file_name"+cnt+"_1' class='asterisk'>File</label><input type='file' name='file_name"+cnt+"_1' id='file_name"+cnt+"_1' class='form-control' onchange='validation("+cnt+",1)'></div></div><div class='col-md-6 gutter-sm'><div class='form-group'><label for='file_type"+cnt+"' class='col-md-3 label-heading asterisk'>File Type</label><select name='file_type"+cnt+"' id='file_type"+cnt+"' class='form-control'><option value=''>File Type</option><?php foreach($document as $doc){ ?>  <option value='<?php echo $doc->documentId?>'><?php echo $doc->documentFile?></option><?php }?></select></div></div></div></div>");
    $('#count').val(cnt);
});       
function add_files(no)
{
    cnt=$('#file_count'+no).val();
   cnt++;
    $('#files'+no).append("<div class='form-group'><label for='file_name"+no+"_"+cnt+"' class='col-meaasterisk'>File</label><input type='file' name='file_name"+no+"_"+cnt+"' id='file_name"+no+"_"+cnt+"' class='form-control' onchange='validation("+no+","+cnt+")'></div>");
    $('#file_count'+no).val(cnt);
}    
</script>
<script>
function validation(no,cnt)
{
  if($("#file_name"+no+"_"+cnt).val()!='')
  {
     document.getElementById("file_type"+no).required = true;     
  }
  else
  {
      document.getElementById("file_type"+no).required = false;
  }
  
}
</script>