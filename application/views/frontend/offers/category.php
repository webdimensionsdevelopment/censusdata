
<div class="container categoryfilter" >
    <!-- Three columns of text below the carousel -->
    <div class="row" >
        <?php if($this->session->flashdata('message')){?>
            <div class="<?php echo $this->session->flashdata('class')?>">
                <?php echo $this->session->flashdata('message')?>
            </div>
        <?php } ?>
        <?php echo form_open_multipart(site_url("offers/filter"), array("method"=>"GET","class" => "form-horizontal","id"=>"category_form", "name"=>"category_form")) ?>
        <div class="col-md-2">
            <label class="mar-20-right" for="min-filter">Min Price</label>
            <div class="input-group" >
                <span class="input-group-addon">$</span>
                <input placeholder="0.00" name="min-price" class="form-control number" id="min-filter" min="0" step="0.01" value="<?php echo (!empty($_GET['min-price'])  ? $_GET['min-price'] : "")?>"
                    data-regexp="[-+.0-9e]" pattern="^(?:(?:\+|-)?(?:\d+)(?:.\d+)?(?:e-?\d+)?)$" type="number">
            </div>
        </div>
        <div class="col-md-2">
            <label class="mar-20-right" for="max-filter">Max Price</label>
            <div class="input-group" >
                <span class="input-group-addon">$</span>
                <input placeholder="0.00" name="max-price" class="form-control number" id="max-filter" min="0" step="0.01" value="<?php echo (!empty($_GET['max-price'])  ? $_GET['max-price'] : "")?>"
                       data-regexp="[-+.0-9e]" pattern="^(?:(?:\+|-)?(?:\d+)(?:.\d+)?(?:e-?\d+)?)$" type="number">
            </div>
        </div>
<!--        <div class="col-md-3" >
            <label class="mar-20-right" for="max-filter">Category</label>
            <select name="categories" id="categories"
                    class=" form-control col-md-12" />
            <option value="">Please Select</option>
            <?php for ($m = 0; $m < count($categories); $m++) : ?>
                <option value="<?php echo $categories[$m]['href'] ?>" <?php if(!empty($_GET['categories']))echo ($_GET['categories']==$categories[$m]['href'] ? "selected": "") ?>><?php echo $categories[$m]['text'] ?></option>
            <?php endfor; ?>
            </select>
        </div>-->
        <div class="col-md-3">
            <label class="mar-20-right" for="max-filter">Search Deals</label>
            <div class="input-group" >
                <span class="input-group-addon"></span>
                <input name="deal-search" placeholder="Deal Title" class="form-control number" id="deal-search" value="<?php echo (!empty($_GET['deal-search'])  ? $_GET['deal-search'] : "")?>" type="text">
            </div>
        </div>
        <div class="col-md-2">
            <input type="submit" style="width: 100%; margin-top: 22px" class="btn btn-primary" id="filter-category"
                   value="Filter"/>
        </div>
        <?php echo form_close() ?>
    </div><!-- /.row -->
</div>
