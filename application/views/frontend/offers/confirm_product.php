<div class="container marketing">
    <!-- Three columns of text below the carousel -->
    <div class="row">
        <?php if($this->session->flashdata('message')){?>
            <div class="<?php echo $this->session->flashdata('class')?>">
                <?php echo $this->session->flashdata('message')?>
            </div>
        <?php } ?>
        <div class="col-md-8">
            <h3>When Your Item Arrives</h3>
            <p>Spend a few days with your new item and get to know it. When you feel you're ready, leave only honest feedback whether positive or negative. You won't be rewarded in anyway for leaving a positive or negative review. Your honest feedback is critical to companies and is what allows them to give great deals on Snagshout.</p>
            <hr>
            <h3>Shipping Note</h3>
            <p>Free shipping applies to Amazon Prime members or orders of $49 or more. If you are not an Amazon Prime member or if your order is less than $49, please check Amazon.com for shipping prices before snagging this deal.</p>
            <p>Need help? Feel free to contact us.</p>
        </div>
        <div class="col-md-4">
            <h3>Summary</h3>
            <p style="font-size: 18px;">Price : <span style="float: right"><?php echo $marketing['price']; ?></span></p>
            <p style="font-size:18px;"> Shipping: <span style="float: right"><?php echo $marketing['shipping_price']; ?></span></p>
            <hr/>
            <p style="font-size:18px;font-weight: bold"> Total: <span style="float: right"><?php echo $marketing['total']; ?></span></p>
            <a style="margin-bottom: 10px;margin-top: 20px;" href="<?php echo base_url()."offers/dealaccepted/". $marketing['product_id']?>"
               class="btn-block btn btn-default">Confirm</a>
            <p>By clicking the "Confirm" button you are agreeing to leave an honest review and to the Snagshout terms.</p>
        </div>
    </div><!-- /.row -->