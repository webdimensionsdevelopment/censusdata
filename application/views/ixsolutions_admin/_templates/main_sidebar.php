<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<aside class="main-sidebar">
    <section class="sidebar">
        <?php if ($admin_prefs['user_panel'] == TRUE): ?>
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo base_url($avatar_dir . '/m_001.png'); ?>" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?php echo $user_login['firstname'] . $user_login['lastname']; ?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> <?php echo lang('menu_online'); ?></a>
                </div>
            </div>

        <?php endif; ?>
        <?php if ($admin_prefs['sidebar_form'] == TRUE): ?>
            <!-- Search form -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control"
                           placeholder="<?php echo lang('menu_search'); ?>...">
                    <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                                class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>

        <?php endif; ?>
        <!-- Sidebar menu -->
        <ul class="sidebar-menu">
            <li>
                <a href="<?php echo site_url('/'); ?>">
                    <i class="fa fa-home text-primary"></i> <span><?php echo lang('menu_access_website'); ?></span>
                </a>
            </li>
            <li class="header text-uppercase"><?php echo lang('menu_main_navigation'); ?></li>
            <?php if ($this->ixsolution_ion_auth->is_admin() || $this->ixsolution_ion_auth->is_brokerage()) : ?>
                <li class="<?= active_link_controller('dashboard') ?>">
                    <a href="<?php echo site_url('ixsolutions_admin/dashboard'); ?>">
                        <i class="fa fa-dashboard"></i> <span><?php echo lang('menu_dashboard'); ?></span>
                    </a>
                </li>
               <!-- <li class="header text-uppercase"><?php echo lang('menu_administration'); ?></li>
                <li class="<?= active_link_controller('users') ?>">
                    <a href="<?php //echo site_url('ixsolutions_admin/users'); ?>">
                        <i class="fa fa-user"></i> <span><?php //echo lang('menu_users'); ?></span>
                    </a>
                </li>-->
                <li class="<?= active_link_controller('logsDetails') ?>">
                    <a href="<?php echo site_url('ixsolutions_admin/logsDetails'); ?>">
                        <i class="fa fa-cogs" aria-hidden="true"></i> <span><?php echo lang('menu_logs'); ?></span>
                    </a>
                </li>
                <li class="<?= active_link_controller('groups') ?>">
                    <a href="<?php echo site_url('ixsolutions_admin/groups'); ?>">
                        <i class="fa fa-shield"></i> <span><?php echo lang('menu_security_groups'); ?></span>
                    </a>
                </li>
                <li class="<?= active_link_controller('product') ?>">
                    <a href="<?php echo site_url('ixsolutions_admin/product'); ?>">
                        <i class="fa fa-product-hunt"></i> <span><?php echo lang('menu_products'); ?></span>
                    </a>
                </li>
                <li class="<?= active_link_controller('questionnaire') ?>">
                    <a href="<?php echo site_url('ixsolutions_admin/questionnaire'); ?>">
                        <i class="fa fa-question"></i> <span><?php echo lang('menu_questionnaire'); ?></span>
                    </a>
                </li>
                <li class="<?= active_link_controller('document') ?>">
                    <a href="<?php echo site_url('ixsolutions_admin/document'); ?>">
                        <i class="fa fa-file"></i> <span><?php echo lang('menu_document'); ?></span>
                    </a>
                </li>
                <li class="<?= active_link_controller('Broker') ?>">
                    <a href="<?php echo site_url('ixsolutions_admin/brokerage'); ?>"><i class="fa fa-users" aria-hidden="true"></i>Broker</a>
                </li>
                <li class="<?= active_link_controller('company') ?>">
                    <a href="<?php echo site_url('company/company_list_view'); ?>"><i class="fa fa-building" aria-hidden="true"></i> Company</a>
               </li>
            <?php endif;?>
            <?php if ($this->ixsolution_ion_auth->is_broker()) { ?>
                <li class="<?= active_link_controller('company') ?>">
                    <a href="<?php echo site_url('company/company_list_view'); ?>">
                        <i class="fa fa-building-o" aria-hidden="true"></i>Company</a>
                </li>
                <li class="<?= active_link_controller('LogsDetails') ?>">
                    <a href="<?php echo site_url('ixsolutions_admin/LogsDetails/companiesLog/'); ?>">
                        <i class="fa fa-cogs" aria-hidden="true"></i>Company Log Details</a>
                </li>
            <?php } ?>
            <?php if ($this->ixsolution_ion_auth->is_company()) { ?>
                <li class="<?= active_link_controller('company') ?>">
                    <a href="<?php echo site_url('company/company_list_view'); ?>"><i class="fa fa-building" aria-hidden="true"></i>Company List</a>
                    <a href="<?php echo site_url('company/company_list_view'); ?>">Company</a>
                </li>
            <?php } ?>
        </ul>
    </section>
</aside>
