<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="box custom-form">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('brokerage_invite_broker'); ?></h3>
                    </div>
                    <div class="box-body">
                        <?php echo $message; ?>
                        <?php echo form_open(current_url(), array('class' => 'form-horizontal', 'id' => 'form-inviteBroker')); ?>
                        <div class="row row-gutter-sm">
                            <div class="col-md-6 gutter-sm">
                                <div class="form-group">
                                    <?php echo lang('brokerage_FirstName', 'first_name'); ?>
                                        <?php echo form_input($first_name); ?>
                                    </div>
                                </div>
                            <div class="col-md-6 gutter-sm">
                                <div class="form-group">
                                    <?php echo lang('brokerage_LastName', 'last_name'); ?>
                                        <?php echo form_input($last_name); ?>
                                    </div>
                                </div>
                        </div>
                         <div class="row row-gutter-sm">
                            <div class="col-md-6 gutter-sm">
                                <div class="form-group">
                                    <?php echo lang('brokerage_email', 'email'); ?>
                                        <?php echo form_input($email); ?>
                                    </div>
                                </div>
                            </div>
                        <div class="form-group">
                          
                                <div class="margin-top-sm">
                                    <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary', 'content' => lang('actions_submit'))); ?>
                                    <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning', 'content' => lang('actions_reset'))); ?>
                                    <?php echo anchor('ixsolutions_admin/brokerage', lang('actions_cancel'), array('class' => 'btn btn-danger')); ?>
                                </div>
                        
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
