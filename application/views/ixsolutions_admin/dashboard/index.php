<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
    <section class="content">
        <?php echo $dashboard_alert_file_install; ?>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-maroon"><i class="fa fa-building-o" aria-hidden="true"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text"># of Quotes<br>Requested</span>
                        <span class="info-box-number"><?php echo $noOfCompany; ?></span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-lock" aria-hidden="true"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text"># of Assigned<br>Quotes</span>
                        <span class="info-box-number"><?php echo $assignedQuote; ?></span>
                    </div>
                </div>
            </div>

            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-unlock" aria-hidden="true"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text"># of Unassigned<br>Quotes</span>
                        <span class="info-box-number"><?php echo $unassignedQuote; ?></span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-product-hunt" aria-hidden="true"></i></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text"># of Quotes<br> Downloaded</span>
                        <span class="info-box-number"><?php echo $quoteStatus; ?></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                /*
                  if ($url_exist) {
                  echo 'OK';
                  } else {
                  echo 'KO';
                  }
                 */
                ?>
            </div>

            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Title</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <p class="text-center"><strong>xxx</strong></p>
                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
