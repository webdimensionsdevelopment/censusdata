<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-8">
                             <div class="box custom-form">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo lang('groups_create'); ?></h3>
                                </div>
                                <div class="box-body">
                                    <?php echo $message;?>

                                    <?php echo form_open(current_url(), array('class' => 'form-horizontal', 'id' => 'form-create_group')); ?>
                                        <div class="row row-gutter-sm">
                                            <div class="col-md-6 gutter-sm">
                                                <div class="form-group">
                                                    <?php echo lang('groups_name', 'group_name'); ?>
                                                        <?php echo form_input($group_name);?>
                                                    </div>
                                                </div>
                                            <div class="col-md-6 gutter-sm">
                                                <div class="form-group">
                                                    <?php echo lang('groups_description', 'description'); ?>
                                                        <?php echo form_input($description);?>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                          
                                                <div class="margin-top-sm">
                                                    <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary', 'content' => lang('actions_submit'))); ?>
                                                    <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning', 'content' => lang('actions_reset'))); ?>
                                                    <?php echo anchor('ixsolutions_admin/groups', lang('actions_cancel'), array('class' => 'btn btn-danger')); ?>
                                                </div>
                                            
                                        </div>
                                    <?php echo form_close();?>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
