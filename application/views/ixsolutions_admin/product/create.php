<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="box custom-form">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('products_create_product'); ?></h3>
                    </div>
                    <div class="box-body">
                        <?php echo $message; ?>

                        <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal', 'id' => 'form-create_user')); ?>
                        <div class="row row-gutter-sm">
                            <div class="col-md-6 gutter-sm">
                            <div class="form-group">
                                <?php echo lang('products_name', 'products_name'); ?>
                                    <?php echo form_input($products_name); ?>
                                </div>
                            </div>
                             <div class="col-md-6 gutter-sm">
                                <div class="form-group">
                                    <?php echo lang('product_description', 'product_description'); ?>
                                        <?php echo form_input($product_description); ?>
                                    </div>
                                </div>
                        </div>
                        <div class="row row-gutter-sm">
                            <div class="col-md-6 gutter-sm">
                                <div class="form-group">
                                    <?php echo lang('product_image', 'product_image'); ?>
                                        <?php echo form_input($product_image); ?>
                                    </div>
                                </div>
                             <div class="col-md-4 gutter-sm">
                                <div class="form-group">
                                    <?php echo lang('occupation', 'occupation'); ?>
                                        <?php echo form_dropdown($occupation, $options_selection, 'occupation'); ?>
                                    </div>
                                </div>
                        </div>
                        <div class="row row-gutter-sm">
                             <div class="col-md-4 gutter-sm">
                                <div class="form-group">
                                    <?php echo lang('salary', 'salary'); ?>
                                        <?php echo form_dropdown($salary, $options_selection, 'salary'); ?>
                                    </div>
                                </div>
                        </div>
                        <div class="form-group">
                           
                                <div class="margin-top-sm">
                                    <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary', 'content' => lang('actions_submit'))); ?>
                                    <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning', 'content' => lang('actions_reset'))); ?>
                                    <?php echo anchor('ixsolutions_admin/product', lang('actions_cancel'), array('class' => 'btn btn-danger')); ?>
                                </div>
                           
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
