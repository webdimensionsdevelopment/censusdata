<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="box custom-form">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('products_create_product'); ?></h3>
                    </div>
                    <div class="box-body">
                        <?php echo $message; ?>

                        <?php echo form_open_multipart(current_url(), array('class' => 'form-horizontal', 'id' => 'form-create_user')); ?>
                        <div class="row row-gutter-sm">
                            <div class="col-md-6 gutter-sm">                        
                                <div class="form-group">
                                    <?php echo lang('product', 'products_name'); ?>
                                        <?php echo form_dropdown($products_name, $options, 'product'); ?>
                                    </div>
                                </div>
                            <div class="col-md-6 gutter-sm"> 
                                <div class="form-group">
                                    <?php echo lang('questionnaire_question', 'questionnaire_question'); ?>
                                        <?php echo form_input($questionnaire_question); ?>
                                    </div>
                                </div>
                        </div>
                        <div class="row row-gutter-sm">
                            <div class="col-md-6 gutter-sm">         
                                <div class="form-group">
                                    <?php echo lang('questionnaire_description', 'questionnaire_description'); ?>
                                        <?php echo form_input($questionnaire_description); ?>
                                    </div>
                            </div>
                            <div class="col-md-4 gutter-sm">
                                <div class="form-group">
                                    <?php echo lang('questionnaire_selection', 'questionnaire_selection'); ?>
                                        <?php echo form_dropdown($questionnaire_selection, $options_selection, 'question_type'); ?>
                                    </div>
                            </div>
                        </div>
                        <div class="row row-gutter-sm">
                            <div class="col-md-12 gutter-sm">
                                <div class="form-group" data-max=100>
                                    <?php echo lang('questionnaire_answer', 'questionnaire_answer'); ?>
                                    <div class="inline-form form-inline" >
                        
                        
                            
                          
                                <input type="text" name="questionnaire_answer[]" class="form-control">
                                 <button type="button" class="btn btn-default btn-add">+</button>
                            </div>
                            
                        </div>
                        </div>
                        </div>
                        <div class="form-group">
                            
                                <div class="margin-top-sm">
                                    <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary', 'content' => lang('actions_submit'))); ?>
                                    <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning', 'content' => lang('actions_reset'))); ?>
                                    <?php echo anchor('ixsolutions_admin/questionnaire', lang('actions_cancel'), array('class' => 'btn btn-danger')); ?>
                                </div>
                           
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
    (function ($) {
        $(function () {

            var addFormGroup = function (event) {
                event.preventDefault();

                var $formGroup = $(this).closest('.form-group');
                var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
                var $formGroupClone = $formGroup.clone();

                $(this)
                    .toggleClass('btn-default btn-add btn-danger btn-remove')
                    .html('–');

                $formGroupClone.find('input').val('');
                $formGroupClone.insertAfter($formGroup);

                var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
                if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                    $lastFormGroupLast.find('.btn-add').attr('disabled', true);
                }
            };

            var removeFormGroup = function (event) {
                event.preventDefault();

                var $formGroup = $(this).closest('.form-group');
                var $multipleFormGroup = $formGroup.closest('.multiple-form-group');

                var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
                if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                    $lastFormGroupLast.find('.btn-add').attr('disabled', false);
                }

                $formGroup.remove();
            };

            var countFormGroup = function ($form) {
                return $form.find('.form-group').length;
            };

            $(document).on('click', '.btn-add', addFormGroup);
            $(document).on('click', '.btn-remove', removeFormGroup);

        });
    })(jQuery);
</script>