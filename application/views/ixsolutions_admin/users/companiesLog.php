<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.6/css/jquery.dataTables.css"><script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script><script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.6/js/jquery.dataTables.js"></script>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo '<h1>Company Log Details</h1>' ?>
        <?php echo $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary" >
                    <div class="panel-heading" style="margin-bottom: 15px;">
                        <h3 class="panel-title">Company Log Details</h3>
                    </div>
                    <div class="panel-body">
                        <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter Log Details" onkeyup="searchTable(this.value);"/>
                    </div>
                    <table class="table table-striped table-hover" id="dev-table">
                        <thead>
                            <tr>
                                <th><?php echo lang('users_userName'); ?></th>
                                <th><?php echo lang('users_email'); ?></th>
                                <th><?php echo lang('users_created_on')?></th>
                                <th><?php echo lang('users_action'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($companies as $company) : ?>
                                <tr>
                                    <td><?php echo anchor('ixsolutions_admin/logsDetails/logQueries/' . $company['companyId'], $company['username']); ?></td>
                                    <td><?php echo $company['email']; ?></td>
                                    <td><?php echo $company['createdAt']; ?>
                                    <td><?php echo anchor('ixsolutions_admin/logsDetails/logQueries/' .  $company['companyId'], lang('actions_see')); ?></td>  
                                </tr>    
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
$(document).ready(function(){
    $.noConflict();
  $('#dev-table').DataTable({
      "searching":   true,
      "aaSorting": []
  });  
});
</script>