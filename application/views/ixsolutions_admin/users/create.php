<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="box custom-form">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('users_create_user'); ?></h3>
                    </div>
                    <div class="box-body">
                        <?php echo $message; ?>

                        <?php echo form_open(current_url(), array('class' => 'form-horizontal', 'id' => 'form-create_user')); ?>
                        <div class="row row-gutter-sm">
                            <div class="col-md-6 gutter-sm">
                                <div class="form-group">
                                    <?php echo lang('users_firstname', 'first_name'); ?>
                                        <?php echo form_input($first_name); ?>
                                    </div>
                                </div>
                            
                            <div class="col-md-6 gutter-sm">
                                <div class="form-group">
                                    <?php echo lang('users_lastname', 'last_name'); ?>
                                        <?php echo form_input($last_name); ?>
                                    </div>
                                </div>
                            </div>
                        <!--                                        <div class="form-group">
                        <?php // echo lang('users_company', 'company');  ?>
                                                                    <div class="col-sm-10">
                        <?php // echo form_input($company); ?>
                                                                    </div>
                                                                </div>-->
                        <div class="row row-gutter-sm">                 
                        <div class="col-md-6 gutter-sm">                                                                
                            <div class="form-group">
                                <?php echo lang('users_email', 'email'); ?>
                                    <?php echo form_input($email); ?>
                                </div>
                            </div>
                            
                             <div class="col-md-6 gutter-sm">    
                                <div class="form-group">
                                    <?php echo lang('user_type', 'user_type'); ?>
                                        <?php echo form_dropdown($membertype, $options, 'employee'); ?>
                                    </div>
                                </div>
                            </div>
                        <!--                                        <div class="form-group">
                        <?php // echo lang('users_phone', 'phone');  ?>
                                                                    <div class="col-sm-10">
                        <?php // echo form_input($phone); ?>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                        <?php // echo lang('users_password', 'password');  ?>
                                                                    <div class="col-sm-10">
                        <?php // echo form_input($password); ?>
                                                                        <div class="progress" style="margin:0">
                                                                            <div class="pwstrength_viewport_progress"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                        <?php // echo lang('users_password_confirm', 'password_confirm');  ?>
                                                                    <div class="col-sm-10">
                        <?php // echo form_input($password_confirm); ?>
                                                                    </div>
                                                                </div>-->
                        <div class="form-group">
                                <div class="margin-top-sm">
                                    <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary', 'content' => lang('actions_submit'))); ?>
                                    <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning', 'content' => lang('actions_reset'))); ?>
                                    <?php echo anchor('ixsolutions_admin/users', lang('actions_cancel'), array('class' => 'btn btn-danger')); ?>
                                </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
