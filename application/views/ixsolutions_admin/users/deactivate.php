<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-8">
                             <div class="box custom-form">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo sprintf(lang('users_deactivate_question'), '<span class="label-primary">'.$firstname.$lastname).'</span>';?></h3>
                                </div>
                                <div class="box-body">
                                    <?php echo form_open('ixsolutions_admin/users/deactivate/'. $id, array('class' => 'form-horizontal', 'id' => 'form-status_user')); ?>
                                        <div class="form-group check-list clear">
                                                <div class="check">
                                               
                                                    <input type="radio" name="confirm" id="confirm1" value="yes" checked="checked">  <label for="confirm1"><?php echo strtoupper(lang('actions_yes', 'confirm')); ?>
                                                </label>
                                                </div>
                                                <div class="check">
                                                
                                                    <input type="radio" name="confirm" id="confirm0" value="no"> <label for="confirm0"><?php echo strtoupper(lang('actions_no', 'confirm')); ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                           
                                                <?php echo form_hidden($csrf); ?>
                                                <?php echo form_hidden(array('id'=>$id)); ?>
                                                <div class="margin-top-sm">
                                                    <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary ', 'content' => lang('actions_submit'))); ?>
                                                    <?php echo anchor('ixsolutions_admin/users', lang('actions_cancel'), array('class' => 'btn btn-danger ')); ?>
                                                </div>
                                         
                                        </div>
                                    <?php echo form_close();?>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
