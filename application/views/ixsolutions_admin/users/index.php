<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.6/css/jquery.dataTables.css"><script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script><script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.6/js/jquery.dataTables.js"></script>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="margin-bottom: 15px;">
                        <h3 class="panel-title">Users</h3>
                        <div class="pull-right">
                            <span class="clickable filter" data-toggle="tooltip" title="Toggle table filter" data-container="body">
                                <i class="glyphicon glyphicon-filter" id='search'>Search</i> 
                            </span>
                        </div>
                        <div class="pull-right">
                            <a href="<?php echo base_url() . 'ixsolutions_admin/users/create' ?>" style="color: white;">
                                <span class="clickable filter" title="Create User" data-container="body">
                                    <i class="fa fa-plus"> <?php echo lang('users_create_user'); ?></i>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter Log Details" onkeyup="searchTable(this.value);"/>
                    </div>
                    <table class="table table-striped table-hover" id="dev-table">
                        <thead>
                            <tr>
                                <th><?php echo lang('users_firstname'); ?></th>
                                <th><?php echo lang('users_lastname'); ?></th>
                                <th><?php echo lang('users_email'); ?></th>
                                <th><?php echo lang('users_groups'); ?></th>
                                <th><?php echo lang('users_status'); ?></th>
                                <th><?php echo lang('users_action'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($users as $user): ?>
                                <tr>
                                    <td><?php echo htmlspecialchars($user->first_name, ENT_QUOTES, 'UTF-8'); ?></td>
                                    <td><?php echo htmlspecialchars($user->last_name, ENT_QUOTES, 'UTF-8'); ?></td>
                                    <td><?php echo htmlspecialchars($user->email, ENT_QUOTES, 'UTF-8'); ?></td>
                                    <td>
                                        <?php foreach ($user->groups as $group): ?>
                                            <?php echo anchor('ixsolutions_admin/groups/edit/' . $group->id, '<span class="label" style="background:' . $group->bgcolor . ';">' . htmlspecialchars($group->name, ENT_QUOTES, 'UTF-8') . '</span>'); ?>
                                        <?php endforeach ?>
                                    </td>
                                    <td><?php echo ($user->active) ? anchor('ixsolutions_admin/users/deactivate/' . $user->id, '<span class="label label-success">' . lang('users_active') . '</span>') : anchor('ixsolutions_admin/users/activate/' . $user->id, '<span class="label label-default">' . lang('users_inactive') . '</span>'); ?></td>
                                    <td>
                                        <?php echo anchor('ixsolutions_admin/users/edit/' . $user->id, lang('actions_edit')) . " | "; ?>
                                        <?php echo anchor('ixsolutions_admin/users/profile/' . $user->id, lang('actions_see')); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        $("#search").click(function () {
            var $this = $(this),
                    $panel = $this.parents('.panel');
            $panel.find('.panel-body').slideToggle();
            if ($this.css('display') != 'none') {
                $panel.find('.panel-body input').focus();
            }
        });
    });
    function searchTable(inputVal)
    {
        var table = $('#dev-table');
        table.find('tr').each(function (index, row)
        {
            var allCells = $(row).find('td');
            if (allCells.length > 0)
            {
                var found = false;
                allCells.each(function (index, td)
                {
                    var regExp = new RegExp(inputVal, 'i');
                    if (regExp.test($(td).text()))
                    {
                        found = true;
                        return false;
                    }
                });
                if (found == true)
                    $(row).show();
                else
                    $(row).hide();
            }
        });
    }
</script>
<script>
$(document).ready(function(){
    $.noConflict();
  $('#dev-table').DataTable({
      "searching":   false
  });  
});
</script>