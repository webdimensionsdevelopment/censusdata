<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.6/css/jquery.dataTables.css"><script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script><script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.6/js/jquery.dataTables.js"></script>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo '<h1>Log Details</h1>' ?>
        <?php echo $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary" >
                    <div class="panel-heading" style="margin-bottom: 15px;">
                        <h3 class="panel-title">Log Details</h3>
                    </div>
                    <div class="panel-body">
                        <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter Log Details" onkeyup="searchTable(this.value);"/>
                    </div>
                    <table class="table table-striped table-hover" id="dev-table">
                        <thead>
                            <tr>
                                <th><?php echo lang('users_userName'); ?></th>
                                <th><?php echo lang('users_email'); ?></th>
                                <th><?php echo lang('users_groups'); ?></th>
                                <th><?php echo lang('users_logInTime'); ?></th>
                                <th><?php echo lang('users_logOutTime'); ?></th>
                                <th><?php echo lang('users_status'); ?></th>
                                <th><?php echo lang('users_action'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($brokers != null) { ?>
                            <?php foreach ($brokers as $broker) : ?>
                                <tr>
                                    <td><?php echo anchor('ixsolutions_admin/logsDetails/companiesLog/' . $broker['brokerId'], $broker['username']); ?></td>
                                    <td><?php echo $broker['email']; ?></td>
                                    <td><strong><?php echo $broker['name']; ?></strong></td>
                                    <td><?php echo ($broker['logInTime'])?$broker['logInTime'] : '---' ; ?></td>
                                    <td><?php echo ($broker['logOutTime']) ? $broker['logOutTime'] : '---'  ; ?></td>
                                    <td><?php echo ($broker['active']) ?('<span class="label label-success">' . lang('users_active') . '</span>') : ('<span class="label label-default">' . lang('users_inactive') . '</span>'); ?></td>
                                    <td><?php echo anchor('ixsolutions_admin/logDetails/companiesLog/' .  $broker['brokerId'], lang('actions_see')); ?></td>  
                                </tr>    
                            <?php endforeach;?>
                            <?php } ?>

                            <?php if($companies != null) { ?>
                            <?php foreach ($companies as $company) : ?>
                                <tr>
                                    <td><?php echo anchor('ixsolutions_admin/logsDetails/companiesLog/' . $company['userId'], $company['username']); ?></td>
                                    <td><?php echo $company['email']; ?></td>
                                    <td><strong><?php echo 'company'; ?></strong></td>
                                    <td><?php echo ($company['logDetails'])?$company['logDetails']['logInTime'] : '---' ; ?></td>
                                    <td><?php echo ($company['logDetails']) ? $company['logDetails']['logOutTime'] : '---'  ; ?></td>
                                    <td><?php echo ($company['active']) ?('<span class="label label-success">' . lang('users_active') . '</span>') : ('<span class="label label-default">' . lang('users_inactive') . '</span>'); ?></td>
                                    <td><?php echo anchor('ixsolutions_admin/logDetails/companiesLog/' .  $company['userId'], lang('actions_see')); ?></td>  
                                </tr>    
                            <?php endforeach;?>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
$(document).ready(function(){
    $.noConflict();
  $('#dev-table').DataTable({
      "searching":   true,
      "aaSorting": []
  });  
});
</script>