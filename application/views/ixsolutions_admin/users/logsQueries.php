<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.6/css/jquery.dataTables.css"><script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script><script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.6/js/jquery.dataTables.js"></script>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo '<h1>Log Queries</h1>' ?>
        <?php echo $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="margin-bottom: 15px;">
                        <h3 class="panel-title">User Log Queries Of: <?php echo $users['username'] . " - " . $users['email']; ?></h3>
                    </div>
                    <div class="panel-body">
                        <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter Log Details" onkeyup="searchTable(this.value);"/>
                    </div>
                    <div id="body">
                        <table class="table table-striped table-hover" id="dev-table">
                            <thead>
                                <tr>
                                    <th>Sr. No.</th>
                                    <th><?php echo lang('users_activity'); ?></th>
                                    <th><?php echo lang('users_updated_By');  ?></th>
                                    <th><?php echo lang('users_queryString'); ?></th>
                                    <th><?php echo lang('users_downloadTime'); ?></th>
                                    <th><?php echo lang('users_downloadFile'); ?></th>
                                </tr>
                            </thead>
                            <tbody id="tablebody">
                                <?php $count = 0; foreach ($logRecords as $key => $user): $count++?>
                                    <tr>
                                        <td><?php echo $count; ?></td>
                                        <td><?php echo $user['activity']; ?></td>
                                        <td><?php echo $user['name']; ?></td>
                                        <td><?php echo (preg_match('#\b(INSERT|DELETE|UPDATE)\b#', $user['query_string']) ? '---' : $user['query_string']); ?></td>
                                        <td><?php echo (($user['downloadTime']) ? $user['downloadTime'] : '---'); ?></td>
                                        <td><?php echo (($user['downloadFile']) ? $user['downloadFile'] : '---'); ?></td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
$(document).ready(function(){
    $.noConflict();
  $('#dev-table').DataTable({
        "searching":   true,
        "aaSorting": []
  });  
});
</script>