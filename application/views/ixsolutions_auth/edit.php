<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="login-logo">
    <a href="#"><?php echo $title_lg; ?></a>
</div>
<div class="">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"> <?php echo $pagetitle; ?></h3>
                    </div>
                    <div class="box-body">
                        <?php echo $message; ?>
                        <?php echo form_open(uri_string(), array('class' => 'form-horizontal', 'id' => 'form-edit_user')); ?>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php echo form_input($company_name); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php echo form_input($email); ?>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php echo form_input($password); ?>
                                <div class="progress" style="margin:0">
                                    <div class="pwstrength_viewport_progress"></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php echo form_input($password_confirm); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php echo form_hidden('id', $user->id); ?>
                                <div class="btn-group">
                                    <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => 'Submit')); ?>
                                    <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => 'Rest')); ?>
                                    <?php // echo anchor('admin/users', lang('actions_cancel'), array('class' => 'btn btn-default btn-flat')); ?>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>






