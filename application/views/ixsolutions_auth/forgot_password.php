<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="login-logo">
    <a href="#"><?php echo $title_lg; ?></a>
</div>
<div class="login-box-body">
    <p class="login-box-msg"><?php echo 'Please enter your email so we can send you an email to reset your password.'; ?></p>
    <?php echo $message; ?>
<?php echo form_open("ixsolutions_auth/forgot_password");?>

      <div class="form-group has-feedback">
        <?php echo form_input($email); ?>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <div class="row">
       <div class="col-xs-8">
            <?php echo form_submit('submit', 'Submit', array('class' => 'btn btn-primary btn-block btn-flat')); ?>
        </div>
    </div>

<?php echo form_close();?>
</div>