<html lang="en">
    <head>
        <title>Census</title>         
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url(); ?>style/jquery-ui.css" rel="stylesheet" media="screen">
        <link href="<?php echo base_url(); ?>style/datepicker.css" rel="stylesheet" media="screen">
        <!-- Styles -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,500,600,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
        <!-- SCRIPTS -->
        <script type="text/javascript">
//            var global_base_url = "<?php //echo site_url('/') ?>";
        </script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>/scripts/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url(); ?>/scripts/jquery-ui.js"></script>
        <script src="<?php echo base_url(); ?>/scripts/jquery-ui.min.js"></script>
        <script src="<?php echo base_url(); ?>/scripts/custom/jquery-1.11.1.js"></script>
        <script src="<?php echo base_url(); ?>/scripts/custom/jquery.validate.min.js"></script>
        <!--formden.js communicates with FormDen server to validate fields and submit via AJAX -->
        <script type="text/javascript" src="https://formden.com/static/cdn/formden.js"></script>

        <!-- Special version of Bootstrap that is isolated to content wrapped in .bootstrap-iso -->
        <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

        <!--Font Awesome (added because you use icons in your prepend/append)-->
        <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
    </head>