<div class="container" >
    <div class="row">
        
        <div class="col-xs-offset-2 col-md-9" style="margin-top:10%;">
            <label class="col-lg-12" style="text-align: center;">View Product List</label>
            <a class="btn btn-success btn-login-submit" href="<?php echo  'product/addProduct'; ?>"  >Add Product</a>
            <div class="col-md-12 table-responsive">
                <hr>
                <table class="table table-bordered table-hover table-sortable" id="userDetails">
                    <thead>
                        <tr id='addr0' data-id="0">
                            <th>Sr.No</th>
                            <th>Product Name</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $j = $i = 0; foreach ($products as $record) : $i++;?>
                            <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $record['name']; ?></td>
                                <td>
                                    <?php if ($record['status'] == 1) : ?>
                                        <label name = "active" value="Active">Active</label>
                                    <?php else : ?>
                                        <label name = "inactive" value="Inactive">Inactive</label>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <input type="button"  name="Edit"  id="brokerSubmit"  class="btn btn-success btn-login-submit" value="Edit" onclick="location.href = '<?php echo site_url() . "product/addProduct/".$record['productId']; ?>';"/>
                                    &nbsp;<a class="btn btn-danger btn-cancel-action" href="<?php echo  'deleteProduct/' . $record['productId'] ?>" onclick="return confirm('Are you sure you want to delete this item?');" >Delete</a>
                                </td>
                            </tr>
                        <?php $j++;endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>