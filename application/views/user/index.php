<div class="container" >
    <div class="row">
        <div class="col-xs-offset-2 col-md-9" style="margin-top:10%;">
            <label class="col-lg-12" style="text-align: center;">Register at IXsolutions</label>
            <form class="form-horizontal" name="IxSolutions_USERS" method="POST" enctype="multipart/form-data" action="<?php echo base_url(). "ixsolutions_admin/users/register_user"?>" >
                <div class="form-group" >
                    <lable name="lblUserName" class="col-md-3 label-heading asterisk">Name</lable>
                    <div class='col-md-4'>
                        <input type="text"  name="txtFirstName" id="firstName" class="form-control" value="" placeholder="First Name" required/>
                    </div>
                    <div class='col-md-4'>
                        <input type="text" name="txtLastName" id="lastName" class="form-control" value="" placeholder="Last Name" required/>
                    </div>
                </div>
                <div class="form-group" >
                    <lable name="lblEmail" class="col-md-3 label-heading asterisk">Email</lable>
                    <div class='col-md-8'>
                        <input type="txtEmail" name="txtEmail" id="lastName" class="form-control" value="" placeholder="Email" required/>
                    </div>
                </div>
                <div class="form-group" >
                    <lable name="lblGender" class="col-md-3 label-heading asterisk">Gender</lable>
                    <div class='col-md-8'>
                        <input type="radio" name="rdGender" id="rdGender" value="Male" > Male / <input type="radio" name="rdGender" id="rdGender" value="Female"> Female
                    </div>
                </div>
                <div class="form-group" >
                    <lable name="lblPassword" class="col-md-3 label-heading asterisk">Password</lable>
                    <div class='col-md-8'>
                        <input type="password" name="txtPassword" id="password" class="form-control" value="" placeholder="Password" required/>
                    </div>
                </div>
                <div class="form-group" >
                    <lable name="lblConfirmPassword" class="col-md-3 label-heading asterisk">Confirm Password</lable>
                    <div class='col-md-8'>
                        <input type="password" name="txtConfirmPassword" id="confirmPassword" class="form-control" value="" placeholder="Confirm Password" required/>
                    </div>
                </div>
                <div class="form-group" >
                    <lable name="lblImage" class="col-md-3">Profile Image</lable>
                    <div class='col-md-8'>
                        <input type="file" name="image" id="brokerageImage" class="form-control" placeholder="Choose Profile Pic...">
                    </div>
                </div>
                <div class="form-group" >
                    <lable name="lblPhoneNumber" class="col-md-3">Phone Number</lable>
                    <div class='col-md-8'>
                        <input type="text" name="txtPhoneNumber" id="phoneNumber" class="form-control"  placeholder="Phone Number">
                    </div>
                </div>
                <div class="form-group" >
                    <lable name="lblAddress" class="col-md-3">Address</lable>
                    <div class='col-md-8'>
                        <textarea name="txtAddress" id="txtAddress"  class="form-control" placeholder="Address"></textarea>
                    </div>
                </div>
                <div class="form-group" >
                    <lable name="lblZipCode" class="col-md-3">Zip Code</lable>
                    <div class='col-md-8'>
                        <input  type="text" name="txtZipCode" id="txtZipCodeBrokerage"  class="form-control" placeholder="ZipCode" onchange="getValues(this.value);"/>
                    </div>
                </div>
                <div class="form-group" >
                    <lable name="lblCountry" class="col-md-3">Country</lable>
                    <div class='col-md-8'>
                        <input  type="text" name="txtCountry" id="txtCountryBrokerage"  class="form-control" placeholder="Country" />
                    </div>
                </div>
                <div class="form-group" >
                    <lable name="lblState" class="col-md-3">State</lable>
                    <div class='col-md-8'>
                        <input  type="text" name="txtState" id="txtStateBrokerage"  class="form-control" placeholder="State" />
                    </div>
                </div>
                <div class="form-group" >
                    <lable name="lblCity" class="col-md-3">City</lable>
                    <div class='col-md-8'>
                        <input  type="text" name="txtCity" id="txtCityBrokerage"  class="form-control" placeholder="City" />
                    </div>
                </div>
                <div class="form-group" >
                    <lable name="lblUserName" class="col-md-3 label-heading asterisk">Select Type</lable>
                    <div class='col-md-8'>
                        <select class="form-control" id="options"  name="options" >
                            <option value="0" selected>--Select Option--</option>
                            <?php
                            foreach ($userType as $value) {
                                if ($value['name'] != 'Admin' && $value['name'] != 'Company') {
                                    ?>
                                    <option value="<?php echo $value['name']; ?>"><?php echo $value['name']; ?></option>
                                <?php }
                            } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group" >
                    <div class='col-md-9 col-md-offset-4' >
                        <input type="button" class="btn btn-danger btn-cancel-action cancelBrokerage" value="Cancel" id="cancelBrokerage" style="margin-right: 10px;">
                        <input type="submit"  name="submit"  class="btn btn-success btn-login-submit" value="Save" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function getValues(zipcode) {
        $.ajax({
            url: '<?= base_url(); ?>register/getCountryStateCity',
            data: {"zipcode": zipcode},
            success: function (result) {
                var count_State_City = JSON.parse(result);
                $('#txtCountryBrokerage').val(count_State_City.country);
                $('#txtStateBrokerage').val(count_State_City.state);
                $('#txtCityBrokerage').val(count_State_City.city);
                console.log(count_State_City);
            }
        });
    }
    $('#dateOfBirth').datepicker({
        format: 'mm/dd/yyyy'
    });
</script>    