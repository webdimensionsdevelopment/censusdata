<div class="container" >
    <div class="row">
        <div class="col-md-12" style="margin-top:10%;">
            <div class="col-md-12">
                <div class="col-md-3">
                    <h4 ><?php echo $name; ?></h4>
                    <ul class="nav navbar-nav " style="text-align: center;">
                        <li class="liTabList active" id="tabProfileView"><a href="#tab1" data-toggle="tab" id="campaign"> -> Profile View</a></li><br>
                        <li class="liTabList" id="tabEditProfile"><a href="#tab2" data-toggle="tab" id="campaign"> -> Edit Profile</a></li>
                    </ul>
                </div>
                <div class="col-md-9">
                    <div class="tab-pane active" id="tab1">
                        <h3>Profile View</h3>
                        <div class = "col-md-6">
                            <div class="col-md-12 table-responsive">
                                <table class="table table-bordered table-hover table-sortable" id="userDetails">
                                    <caption>Details</caption>
                                    <tr id='addr0' data-id="0">
                                        <td data-name="name">
                                            User Name
                                        </td>
                                        <td data-name="name">
                                            <?php echo $first_name; ?>- <?php echo $last_name; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td><?php echo $email; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Phone Number</td>
                                        <td><?php echo $phoneNumber; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Gender</td>
                                        <td><?php echo $gender; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Address</td>
                                        <td><?php echo $address; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Zip-code</td>
                                        <td><?php echo $zipcode; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Country</td>
                                        <td><?php echo $country; ?></td>
                                    </tr><tr>
                                        <td>State</td>
                                        <td><?php echo $state; ?></td>
                                    </tr><tr>
                                        <td>City</td>
                                        <td><?php echo $city; ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class ="cold-md-4">
                            <div class="img-responsive" style="padding-bottom: 30px; padding-top: 30px;">
                                <?php // echo  base_url() ."upload/brokerage/Nishant1/". $image; exit;?>
                                <img src="<?php echo $image; ?>" height="200px" width="200px" class="image">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pan" id="tab2" hidden>
                        <h1>Edit profile</h1>
                        <form class="form-horizontal" name="brokerage" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <lable name="lblUserName" class="col-md-3 label-heading asterisk">Name</lable>
                                <div class='col-md-4'>
                                    <input type="text"  name="txtFirstName" id="firstName" class="form-control" value="<?php echo $first_name; ?>" placeholder="First Name" required/>
                                </div>
                                <div class='col-md-4'>
                                    <input type="text" name="txtLastName" id="lastName" class="form-control" value="<?php echo $last_name; ?>" placeholder="Last Name" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <lable name="lblEmail" class="col-md-3 label-heading asterisk">Email</lable>
                                <div class='col-md-8'>
                                    <input type="txtEmail" name="txtEmail" id="lastName" class="form-control" value="<?php echo $email; ?>" placeholder="Email" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <lable name="lblPassword" class="col-md-3 label-heading asterisk">Password</lable>
                                <div class='col-md-8'>
                                    <input type="password" name="txtPassword" id="password" class="form-control" value="" placeholder="Password" />
                                </div>
                            </div>
                            <div class="form-group">
                                <lable name="lblConfirmPassword" class="col-md-3 label-heading asterisk">Confirm Password</lable>
                                <div class='col-md-8'>
                                    <input type="password" name="txtConfirmPassword" id="confirmPassword" class="form-control" value="" placeholder="Confirm Password" />
                                </div>
                            </div>
                            <div class="form-group">
                                <lable name="lblGender" class="col-md-3 label-heading asterisk">Gender</lable>
                                <div class='col-md-8'>
                                    <input type="radio" name="rdGender" id="rdGender" value="Male" <?php echo ($gender == 'Male') ? 'checked' : ''; ?>> Male <input type="radio" name="rdGender" id="rdGender" value="Female" <?php echo ($gender == 'Female') ? 'checked' : ''; ?>> Female
                                </div>
                            </div>
                            <div class="form-group">
                                <lable name="lblImage" class="col-md-3">Profile Image</lable>
                                <div class='col-md-8'>
                                    <input type="file" name="image" id="brokerageImage" class="form-control" placeholder="Choose Profile Pic...">
                                </div>
                            </div>
                            <div class="form-group" >
                                <lable name="lblPhoneNumber" class="col-md-3">Phone Number</lable>
                                <div class='col-md-8'>
                                    <input type="text" name="txtPhoneNumber" id="phoneNumber" class="form-control" value="<?php echo $phoneNumber; ?>" placeholder="Phone Number">
                                </div>
                            </div>
                            <div class="form-group" >
                                <lable name="lblAddress" class="col-md-3">Address</lable>
                                <div class='col-md-8'>
                                    <textarea name="txtAddress" id="txtAddress"  class="form-control" placeholder="Address"><?php echo $address; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group" >
                                <lable name="lblZipCode" class="col-md-3">Zip Code</lable>
                                <div class='col-md-8'>
                                    <input  type="text" name="txtZipCode" id="txtZipCode" value="<?php echo $zipcode; ?>" class="form-control" placeholder="ZipCode" onchange="getValues(this.value);"/>
                                </div>
                            </div>
                            <div class="form-group" >
                                <lable name="lblCountry" class="col-md-3">Country</lable>
                                <div class='col-md-8'>
                                    <input  type="text" name="txtCountry" id="txtCountry" value="<?php echo $country; ?> " class="form-control" placeholder="Country" />
                                </div>
                            </div>
                            <div class="form-group" >
                                <lable name="lblState" class="col-md-3">State</lable>
                                <div class='col-md-8'>
                                    <input  type="text" name="txtState" id="txtState" value="<?php echo $state; ?>" class="form-control" placeholder="State" />
                                </div>
                            </div>
                            <div class="form-group" >
                                <lable name="lblCity" class="col-md-3">City</lable>
                                <div class='col-md-8'>
                                    <input  type="text" name="txtCity" id="txtCity" value="<?php echo $city; ?>" class="form-control" placeholder="City" />
                                </div>
                            </div>
                            <div class="form-group" >
                                <div class='col-md-9' >
                                    <input type="button" class="btn btn-danger btn-cancel-action cancelBrokerage" value="Cancel" id="cancelBrokerage" style="margin-right: 10px;" onclick="location.href = '<?php echo site_url() . "user/profileViewEdit"; ?>';">
                                    <input type="submit"  name="submit"  id="brokerageSubmit"  class="btn btn-success btn-login-submit" value="Save" />
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#tabProfileView').click(function () {
                $('#tab1').show();
                $('#tabProfileView').addClass("active");
                $('#tab2').hide();
                $('#tabEditProfile').removeClass('active');
            });
            $('#tabEditProfile').click(function () {
                $('#tab2').show();
                $('#tabEditProfile').addClass("active");
                $('#tab1').hide();
                $('#tabProfileView').removeClass('active');
            });
        });
        function getValues(zipcode) {
            $.ajax({
                url: '<?= base_url(); ?>user/getCountryStateCity',
                data: {"zipcode": zipcode},
                success: function (result) {
                    var count_State_City = JSON.parse(result);
                    if (count_State_City.country) {
                        $('#txtCountry').val(count_State_City.country);
                        $('#txtState').val(count_State_City.state);
                        $('#txtCity').val(count_State_City.city);
                    }
                    else {
                        alert("Please re-enter Zipcode...");
                        $("#txtZipCode").focus();
                    }
                    console.log(count_State_City);
                }
            });
        }
    </script>    